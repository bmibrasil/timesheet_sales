//Variáveis da sessão

var listaColaboradores = [];
var listaNomes = [];
var listaValores = [];
var listaValoresOrganizacao = [];
var listaValoresProjetosAbertos=[]
var listaPeriodos = [];
var listaPeriodoId = [];
var listaMostraPeriodos = [];
var listaValoresPeriodoSelecionado = [];

var tabelaConsolidado;
var tabelaConsolidadoProjeto;

var colaborador = 0;
var periodo=0;

function admin_init(){
	
	colaborador=sessionStorage.getItem("colaborador");
	permissoes=sessionStorage.getItem("permissoes");
	periodo=sessionStorage.getItem("periodo");
	
	if ((colaborador==0) || (colaborador==null) || (colaborador==undefined)){
		
		window.document.location.href = '../../login/login.html';
	}
	colaboradorLogado=sessionStorage.getItem("colaboradorLogado");
	tabelaConsolidado = document.getElementById("timesheet_consolidado");
	tabelaConsolidadoProjeto = document.getElementById("timesheet_consolidado_projeto");
	//document.getElementById("btn_relatorio").addEventListener("click",relatorioConsolidado );
	//document.getElementById("btn_relatorio_aberto").addEventListener("click",relatorioDetalhado );
	document.getElementById("menuop1").addEventListener("click", carregaLancamentos);
	document.getElementById("menuop2").addEventListener("click", sair);
	buscarPeriodosConsolidado();
}


function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;
		}
	}
	return i;
}

//PREENCHIMENTO DAS COMBOS

function preencheListaPeriodos(){
	
	var html = '<option value="0" Disabled Selected>Período...</option>';
	var data = new Date();
	var dataFormatada=dataAtualFormatada(data)+'';
	var select = "";
	
	for (i=0;i<listaMostraPeriodos.length;i++){
		
		html += '<option value="'+listaMostraPeriodos[i].periodo+'" '+select+'>' +listaMostraPeriodos[i].nomePeriodoLancado+'</option>';
	}
	document.getElementById("timesheet_consolidado_periodo").addEventListener("change",chamaMontadorListaIdPeriodos);
	document.getElementById("timesheet_consolidado_periodo").innerHTML = html;
}


function preencheConsolidadoColaborador(){
	
	var html = '<tr><th>Colaborador</th><td></td><th>Total de Horas</th></tr><tr><td> </td><td> </td><td> </td></tr>';
	var lista = formataHorasColabrador(listaPeriodos, listaValores, document.getElementById("timesheet_consolidado_periodo").value);
	
	for (i=0;i<listaValoresPeriodoSelecionado.length;i++){
		html += '<tr><td>'+listaValoresPeriodoSelecionado[i].nomeColaborador+'</td>'+'<td></td><td align="right">'+transformaMinutoHora(lista[i])+'</td></tr><tr><td> </td><td> </td><td> </td></tr>';
	}
	tabelaConsolidado.innerHTML = html;
}


function preencheConsolidadoOrganizacao(){
	
	var html ='	<tr><th>Projeto</th><td></td><th>Total de Horas</th></tr><tr><td> </td><td> </td><td> </td></tr>';
	var lista = formataHorasOrganizacao(listaValores);

	for (i=0;i<lista.length;i++){
		html += '<tr><td>'+lista[i].nomeOrganizacao+'</td>'+'<td></td><td align="right">'+transformaMinutoHora(lista[i].total)+'</td></tr><tr><td> </td><td> </td><td> </td></tr>';
	}
	tabelaConsolidadoProjeto.innerHTML = html;
}


function buscarPeriodos(){

	var url = retornaServer();
	var data = {
			page: "timesheet",
			action: "buscarPeriodos",
			colaborador:colaboradorLogado
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function consolidaTimeSheet(){
	
	if(listaPeriodoId !=""){
		var url = retornaServer();
		var data = {
				page: "itemTimesheet",
				action: "consolidaTimeSheet", 
				periodosId : listaPeriodoId
		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");
	}else
		alert("Selecione um periodo.")
}


function relatorioConsolidado(){

	if(periodo != 0){
		var url = retornaServer();
		var data = {
				page: "itemTimesheet",
				action: "relatorioConsolidado", 
				periodo : periodo
		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");
	}else
		alert("Por favor selecione um periodo.");
}


function relatorioDetalhado(){

	if(periodo != 0){
		var url = retornaServer();
		var data = {
				page: "itemTimesheet",
				action: "relatorioDetalhado", 
				periodo : periodo
		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");
	}else
		alert("Por favor selecione um periodo.");
}


function buscarPeriodosConsolidado(){
	
	var url = retornaServer();
	var data = {
		page: "timesheet",
		action: "buscarPeriodosConsolidado",
		colaborador: colaboradorLogado
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}

//PROCESSAMENTO DO RETORNO DOS WEBSERVICES

function callback(data){

	if(data.action=="buscarPeriodosConsolidado"){
		listaPeriodos = jQuery.parseJSON(data.retorno);
		removeDuplicatasPeriodos(listaPeriodos);
		preencheListaPeriodos();
	}
	
	if(data.action=="consolidaTimeSheet"){
		listaValores = jQuery.parseJSON(data.retorno);
		preencheConsolidadoColaborador();
		preencheConsolidadoOrganizacao();
	}

	if(data.action=="relatorioConsolidado"){
		listaValoresProjetos = jQuery.parseJSON(data.retorno); 
		var x = document.getElementById("timesheet_consolidado_periodo");
		var periodoNome = x.options[x.selectedIndex].innerText;
		exportarExcel(data.retorno,"Consolidado" + periodoNome, "gestor");	    
	}

	if(data.action=="relatorioDetalhado"){
		listaValoresProjetosAbertos = jQuery.parseJSON(data.retorno);
		var x = document.getElementById("timesheet_consolidado_periodo");
		var periodoNome = x.options[x.selectedIndex].innerText;
		exportarExcel(data.retorno,"Detalhado" + periodoNome, "gestor");
	}
}


function exportarExcel(jsonData, tituloRelatorio, ShowLabel){
	
	var arrData = typeof jsonData != 'object' ? JSON.parse(jsonData) : jsonData;
    removeCamposVazios(arrData);
	
	var tabela = '<table>' + 
				 	'<tr>' + '<th>' + tituloRelatorio + '</th>' + '</tr>' +
				 	'<tr>' + '</tr>' + 
				 	'<tr>';
	
	if(ShowLabel){
			
		for( var index in arrData[0] ) {
			
			tabela += '<th style="text-align:left;">' + index + '</th>';
	 				
		}
		
	}
	
	tabela += '</tr>';
	
	for( contador = 0; contador < arrData.length; contador++ ){
		
		tabela += '<tr>';
		
    	for (var index in arrData[contador]) {
    		
    		tabela += '<td style="text-align:left;">' + arrData[contador][index] + '</td>';
    		
        }
    	
    	tabela += '</tr>';
    	
    }
				 	
	tabela += '</table>';
	
	var fileName = "Relatorio_Timesheet_";
    fileName += tituloRelatorio.replace(/ /g,"_");
    var uri = 'data:application/vnd.ms-excel;charset=utf-8,%EF%BB%BF';
    
    var link = document.createElement("a");
    link.href = uri + tabela.replace(/ /g,'%20');
    link.style = "visibility:hidden";
    link.download = fileName + ".xls";
    
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
	
}


function removeDuplicatasPeriodos(listaPeriodos){
	
	listaMostraPeriodos = listaPeriodos.filter( (valor, index, array ) => array.findIndex( find => find["nomePeriodoLancado"] === valor["nomePeriodoLancado"] ) === index );
}


function chamaMontadorListaIdPeriodos(){
	periodo = document.getElementById("timesheet_consolidado_periodo").value;
	montaListaIdPeriodos(listaPeriodos,periodo);
}


function montaListaIdPeriodos(listaPeriodos, periodo){
	
	listaPeriodoId = listaPeriodos.filter( (valor, index, array ) => array[index].periodo === periodo);
	listaPeriodoId = formataListaIdPeriodos(listaPeriodoId);
	consolidaTimeSheet();
}


function formataListaIdPeriodos(listaPeriodoId){
	
	var listaPeriodosIds = "";
	
	listaPeriodoId.forEach(valor => listaPeriodosIds = listaPeriodosIds.concat('\'', valor.id, '\',') );
	listaPeriodosIds = listaPeriodosIds.substring(0 , listaPeriodosIds.length-1);
	
	return listaPeriodosIds;
}


function formataHorasOrganizacao(listaValoresOrganizacao){
	
	var valorHorasOrganizacao = 0;
	var listaValoresNduplicados = [];
	var lista = [];
	
	listaValoresNduplicados = listaValoresOrganizacao.filter( (valor, index, array ) => array.findIndex( find => find["nomeOrganizacao"] === valor["nomeOrganizacao"] ) === index );
	
	listaValoresNduplicados.forEach(function(organizacao){
		
		var object = new Organizacao(organizacao.nomeOrganizacao,0);
		
		listaValoresOrganizacao.forEach(function(valor){
			
			if(organizacao.nomeOrganizacao === valor.nomeOrganizacao){
				object.setTotal(parseInt(object.getTotalHoras())+parseInt(valor.totalDuracaoMinutos));
			}
		});
		lista.push(object);
	});
	
	return lista;
}


function formataHorasColabrador(listaPeriodos,listaValores,periodo){
	
	var valorMinutos = 0;
	var lista = [];
	listaValoresPeriodoSelecionado = [];
	
	listaPeriodos.forEach(function(valor,index,array){
		if(valor.periodo === periodo)
			listaValoresPeriodoSelecionado.push(array[index]);
	});
	
	listaValoresPeriodoSelecionado.forEach(function(timesheet,index){
		valorMinutos = 0;
		listaValores.forEach(function(valor){
			
			if(timesheet.id === valor.periodo){
				
				valorMinutos += parseInt(valor.totalDuracaoMinutos); 
			}
		});
		lista.push(valorMinutos);
	});
	
	return lista;
}


function sair()
{
	localStorage.clear();
	sessionStorage.setItem("colaboradorLogado",0);
	sessionStorage.setItem("colaborador",0);
	sessionStorage.setItem("lancamento",0);
	// mais tarde você pode parar de observar
	window.document.location.href = '../../login/login.html';

}


function carregaLancamentos()
{
	sessionStorage.setItem("colaboradorLogado",colaboradorLogado);
	sessionStorage.setItem("colaborador",colaboradorLogado);
	sessionStorage.setItem("lancamento",0);
	window.document.location.href = '../lancamentosTimeSheet/lancamentosTimeSheet.html';

}


function somaHora(hrA, hrB, zerarHora) {
	//  if(hrA.length != 5 || hrB.length != 5) return "00:00";

	var temp = 0;
	var nova_h = 0;
	var novo_m = 0;

	var hora1 = hrA.substr(0,hrA.indexOf(":")) * 1;
	var hora2 = hrB.substr(0, hrB.indexOf(":")) * 1;
	var minu1 = hrA.substr(hrA.indexOf(":")+1, 2) * 1;
	var minu2 = hrB.substr(hrB.indexOf(":")+1, 2) * 1;

	temp = minu1 + minu2;
	while(temp > 59) {
		nova_h=nova_h+1;
		temp = temp - 60;
	}
	novo_m = temp.toString().length >= 2 ? temp : ("0" + temp);

	temp = hora1 + hora2 + nova_h;
	while((temp > 23) && (zerarHora)) {
		temp = temp - 24;
	}
	nova_h = temp.toString().length >= 2 ? temp : ("0" + temp);

	return nova_h + ':' + novo_m;
}


function transformaMinutoHora(minutos){
	
	var hora = 0;
	var minuto = 0;
	
	hora = Math.trunc(minutos / 60);
	minuto = minutos % 60;
	
	if (hora   < 10) {hora   = "0"+hora;}
    if (minuto < 10) {minuto = "0"+minuto;}
	
	return hora+":"+minuto;
}


function dataAtualFormatada(data){
    
    var dia = data.getDate();
    if (dia.toString().length == 1)
      dia = "0"+dia;
    var mes = data.getMonth()+1;
    if (mes.toString().length == 1)
      mes = "0"+mes;
    var ano = data.getFullYear();  
    
    return ano+"-"+mes+"-"+dia+' 00:00:00.0';
}


function removeCamposVazios(listaObj){
	
	listaObj.forEach(function(valor){
		for (var i in valor){
			if(valor.hasOwnProperty(i)){
				if(valor[i] === ""){
					delete valor[i];
				}
			}
	    }
	});
}


class Organizacao{
	
	constructor(nomeOrganizacao,total){
		this.nomeOrganizacao = nomeOrganizacao;
		this.total = total;
	}
	
	setTotal(total){
		this.total = total;
	}
	
	getNomeOrganizacao(){
		return this.nomeOrganizacao;
	}
	
	getTotalHoras(){
		return this.total;
	}
}
