//Variáveis da sessão
var listaColaboradores = [];
var listaPeriodos = [];
var listaAprovacao = [];
var observer;
var config;
var target;
var periodo;
var indiceColaborador;
var contadorBloqueio=0;

var colaborador = 0;
var cliente = 0;
var projeto = 0;
var etapa = 0;
var tipoHora = 0;
var nome="";
var nomeColaboradorLogado="";

var tabelaLancamentos;
var listaLancamentos = [];


function admin_init(){
	
	colaborador=sessionStorage.getItem("colaborador");
	
	nomeColaboradorLogado = sessionStorage.getItem("nomeColaborador");
	
	if ((colaborador==0) || (colaborador==null) || (colaborador==undefined)) {
		
		window.document.location.href = '../../login/login.html';
	}
	colaboradorLogado = sessionStorage.getItem("colaboradorLogado");
	tabelaLancamentos = document.getElementById("lancamentostimesheet_lancamentos");
	
	buscarColaboradores();
	
	document.getElementById("menuop1").addEventListener("click", carregaTimeSheet);
	document.getElementById("menuop2").addEventListener("click", timeSheet_Consolidado);
	document.getElementById("menuop3").addEventListener("click", sair);
	document.getElementById("lancamentos_checkbloqueado").addEventListener("click", bloqueiaAcesso);
	document.getElementById("lancamentos_checkbloqueado").checked =false;
	document.getElementById("lancamentos_checkaprovacao").addEventListener("click", AprovaTimesheet);
	document.getElementById("lancamentos_checkaprovacao").checked =false;
}


function bloqueiaAcesso(){  
	
	var bloqueado=false;
	var bloqueio =document.getElementById("lancamentos_checkbloqueado").checked;
	indiceColaborador=document.getElementById("lancamentotimesheet_colaborador").selectedIndex;
	if (bloqueio==true)
		bloqueado=true;
	else
		bloqueado=false;
	var url = retornaServer();
	var data = {
			page: "colaborador",
			action: "bloqueiaColaborador",
			acesso: colaborador,
			bloqueado: bloqueado
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function AprovaTimesheet(){
	
	var aprovacao = false;
	var aprovado = document.getElementById("lancamentos_checkaprovacao").checked;
	var periodo = document.getElementById("lancamentotimesheet_periodo").value
	var dataDia = new Date();
	var dataAprovacao = dataAtualFormatada(dataDia);
	dataAprovacao = dataAprovacao.substring(0,10);
	
	if(listaLancamentos.length > 0){
		if (aprovado == true)
			aprovacao = true;
		else
			aprovacao = false;
		var url = retornaServer();
		var data = {
				page: "aprovacao",
				action: "aprovaTimeSheet",
				colaborador: colaborador,
				aprovador: nomeColaboradorLogado,
				periodo: periodo,
				valorAprovacao: aprovacao,
				dataAprovacao: dataAprovacao
		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");
	}else{
			alert("É necessario ter ao menos um lançamento para aprovar no periodo.");
			document.getElementById("lancamentos_checkaprovacao").checked =false;
		}
}


function isMobile()
{
	var userAgent = navigator.userAgent.toLowerCase();
	if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
		return true;
	else
		return false;
}

//PREENCHIMENTO DAS COMBOS

function desabilitarComboColaborador(){
	
	document.getElementById('timesheet_colaborador').value=nome;
	document.getElementById('timesheet_colaborador').innerHTML="";
	document.getElementById('timesheet_colaborador').disabled = true;
}


function preencheListaColaboradores(){
	
	//var html ='';
	var html = '<option value="0" disabled>Colab...</option>';
	var select = "";
	var pos=0;
	var valorBloqueio=0;
	
	for (i=0;i<listaColaboradores.length;i++){
		
		html += '<option value="'+listaColaboradores[i].id+'" '+select+'>' +listaColaboradores[i].nome+'</option>';
	}
	document.getElementById("lancamentotimesheet_colaborador").addEventListener("change", mudaColaborador);
	document.getElementById("lancamentotimesheet_colaborador").innerHTML = html;
	pos = setSelectedValue(document.getElementById("lancamentotimesheet_colaborador"),colaboradorLogado);
	
	if (listaColaboradores[pos-1].bloqueado == "true"){
		
		valorBloqueio=true;
	}
	else{
		valorBloqueio=false;
	}
	
	document.getElementById("lancamentos_checkbloqueado").checked = valorBloqueio;
	listaColaboradores[pos-1].cargaHorariaPrevista = editaHora(listaColaboradores[pos-1].cargaHorariaPrevista);	
	document.getElementById("lancamentotimesheet_cargaprevista").innerHTML = "<center> <H1>" +listaColaboradores[pos-1].cargaHorariaPrevista+"</H1> Carga Horária Prevista </center>";
	
	if (listaColaboradores.length >1){
		
		document.getElementById("lancamentos_checkaprovacao").disabled=false;
		document.getElementById("lancamentos_checkbloqueado").disabled=false;
	}
	colaborador = document.getElementById("lancamentotimesheet_colaborador").value;
	buscarPeriodos();
}


function dataAtualFormatada(data){
    
    var dia = data.getDate();
    if (dia.toString().length == 1)
      dia = "0"+dia;
    var mes = data.getMonth()+1;
    if (mes.toString().length == 1)
      mes = "0"+mes;
    var ano = data.getFullYear();  
    
    return ano+"-"+mes+"-"+dia+' 00:00:00.0';
}


function preencheListaPeriodos(){
	
	var html = '<option value="0" Disabled Selected>Período...</option>';
	var posicaoPeriodo=0;
	var data = new Date();
	var dataFormatada = dataAtualFormatada(data)+'';
	var select = "";

	for (i=0;i<listaPeriodos.length;i++){
		
		html += '<option value="'+listaPeriodos[i].id+'" '+select+'>' +listaPeriodos[i].nomePeriodoLancado+'</option>';
	}
	document.getElementById("lancamentotimesheet_periodo").innerHTML = html;
	periodo=document.getElementById("lancamentotimesheet_periodo").value;
	setSelectedValue(document.getElementById("lancamentotimesheet_periodo"),listaPeriodos[0].id);
	document.getElementById("lancamentotimesheet_periodo").addEventListener("change", mudaPeriodo);
	mudaPeriodo();
}


function timeSheet_Consolidado()
{
	sessionStorage.setItem("colaboradorLogado",colaboradorLogado);
	sessionStorage.setItem("lancamento",0);
	sessionStorage.setItem("periodo",periodo);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../timesheetConsolidado/timesheetConsolidado.html';	
}


function buscarPeriodos(){

	var url = retornaServer();
	var data = {
			page: "timesheet",
			action: "buscarPeriodos",
			colaborador: colaborador
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function buscarAprovacao(){

	var periodo = document.getElementById("lancamentotimesheet_periodo").value
	var url = retornaServer();
	var data = {
			page: "aprovacao",
			action: "buscarAprovacao",
			colaborador: colaborador,
			periodo: periodo
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function buscarColaboradores(){
	
	var url = retornaServer();
	var data = {
			page: "colaborador",
			action: "buscarColaboradores",
			colaborador:colaboradorLogado
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function buscarLancamentos(idperiodo){
	
	var url = retornaServer();
	var data = {
			page: "itemTimesheet",
			action: "buscarLancamentos", 
			periodo: idperiodo,
			colaborador:  colaborador
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function excluirLancamento(lancamentoId){
	
	var r=confirm("Você deseja realmente excluir o lançamento?");
	if (r==true)
	  {
		var url = retornaServer();
		var data = {
				page: "itemTimesheet",
				action: "excluirLancamento",
				lancamento: lancamentoId

		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");  
	  }		
}


function editarLancamento(lancamentoId)
{
	
	var achei=0;
	var i=0;
	while ( achei==0)
	{
		if (listaLancamentos[i].id==lancamentoId)
		{
			achei=1;	  
		}
		i=i+1;
	}
	sessionStorage.setItem("lancamento",listaLancamentos[i-1].id);
	sessionStorage.setItem("periodo",listaLancamentos[i-1].periodo);
	sessionStorage.setItem("colaborador",listaPeriodos[0].colaborador);
	sessionStorage.setItem("colaboradorSelecionado",document.getElementById("lancamentotimesheet_colaborador").selectedOptions[0].innerText);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../timesheet/timesheet.html';
}

//PROCESSAMENTO DO RETORNO DOS WEBSERVICES

function callback(data){
	
	if(data.action=="buscarColaboradores"){
		listaColaboradores = jQuery.parseJSON(data.retorno);
		preencheListaColaboradores();
	}
	
	if(data.action=="buscarPeriodos"){
		listaPeriodos = jQuery.parseJSON(data.retorno);
		preencheListaPeriodos();
	}

	if(data.action=="buscarLancamentos"){
		listaLancamentos = jQuery.parseJSON(data.retorno);
		preencheTabelaLancamentos();
	}

	if(data.action=="buscarAprovacao"){
		listaAprovacao = jQuery.parseJSON(data.retorno);

		if(listaLancamentos.length > 0){

			if (listaAprovacao.length >  0)
			{
				if (listaAprovacao[0].valorAprovacao == "true"){
					document.getElementById("lancamentos_checkaprovacao").checked = true;
				}
			}
			else
			{
				document.getElementById("lancamentos_checkaprovacao").checked = false;
			}
		}else{
			document.getElementById("lancamentos_checkaprovacao").checked = false;
		}
	}
	
	if(data.action =="salvarAprovacao"){
		location.reload();
	}

	if(data.action=="excluirLancamento"){
		location.reload();
	}

	if(data.action=="bloqueiaColaborador"){
		
		if (document.getElementById("lancamentos_checkbloqueado").checked == "true"){
		  listaColaboradores[indiceColaborador].bloqueado=true;
		}
		else{
		  listaColaboradores[indiceColaborador].bloqueado=false;
		}
		location.reload();
	}
}


function setSelectedValue(selectObj, valueToSet) {
	pos=0;
	for (var i = 0; i < selectObj.options.length; i++) {
		
		if (selectObj.options[i].value==valueToSet) {
			
			selectObj.options[i].selected = true;
			pos=i;
		}
	}
	return pos;
}


function mudaColaborador() {
	
	// ADENDO - QUEBRA DE SEGURANÇA NA LISTA DE COLABORADORES - ESTA VOLTANDO SENHA NAO CRIPTOGRAFADAs
	
	tabelaLancamentos.innerHTML = '';
	var idCol = document.getElementById("lancamentotimesheet_colaborador").value;
	var colaboradorPos = document.getElementById("lancamentotimesheet_colaborador").selectedIndex-1;
	
	listaColaboradores[colaboradorPos].cargaHorariaPrevista = editaHora(listaColaboradores[colaboradorPos].cargaHorariaPrevista);
	document.getElementById("lancamentotimesheet_cargaprevista").innerHTML="<center><H1>"+ listaColaboradores[colaboradorPos].cargaHorariaPrevista+"</H1> Carga Horária Prevista  </center>";
	
	if (listaColaboradores[colaboradorPos].bloqueado == "false"){
		
		document.getElementById("lancamentos_checkbloqueado").checked = false;
	}
	else{
		document.getElementById("lancamentos_checkbloqueado").checked = true;
	}
	colaborador = idCol;
	buscarPeriodos();
}


function mudaPeriodo(){
	
	var idPer = document.getElementById("lancamentotimesheet_periodo").value;	
	buscarLancamentos(idPer);
}


function carregaTimeSheet()
{
	contadorBloqueio=document.getElementById("lancamentotimesheet_periodo").selectedIndex;
	
	if(contadorBloqueio == 0){
		
		sessionStorage.setItem("colaborador",colaborador);
		sessionStorage.setItem("lancamento",0);
		sessionStorage.setItem("periodo",0);
		// mais tarde você pode parar de observar
		observer.disconnect();
		window.document.location.href = '../timesheet/timesheet.html';
		
	}else if(listaPeriodos[contadorBloqueio-1].bloqueado == "true"){
		
			document.getElementById("menuop1").disabled = true;
			alert("Período bloqueado para Inclusão");
			
		}else{
			
			sessionStorage.setItem("colaborador",colaborador);
			sessionStorage.setItem("lancamento",0);
			sessionStorage.setItem("periodo",0);
			// mais tarde você pode parar de observar
			observer.disconnect();
			window.document.location.href = '../timesheet/timesheet.html';
		}
	
}


//TRATAMENTO DOS EVENTOS


function preencheTabelaLancamentos(){
	
	var html = '';
	var j=0;
	var totalLancamentos='00:00';
	var achei=0;
	
	if(listaLancamentos.length > 0) {
		
		for(i = 0; i < listaLancamentos.length; i++){
			
			totalLancamentos = somaHora(totalLancamentos,listaLancamentos[i].horasTrabalhadas,false);
			
			//Encontra a indicação do dia da semana a partir da data do lançamento
			var semana = "";
			var dias_semana = ["DOM", "SEG", "TER", "QUA", "QUI", "SEX", "SAB"];
		    var string_data = listaLancamentos[i].data;
		    var arr = string_data.split("/").reverse();
		    var data_obtida = new Date(arr[0], arr[1] - 1, arr[2]);
		    var dia = data_obtida.getDay();
		    semana = dias_semana[dia];
		    contadorBloqueio = document.getElementById("lancamentotimesheet_periodo").selectedIndex;
		    
		    if(listaPeriodos[contadorBloqueio-1].periodoBloqueado != listaPeriodos[contadorBloqueio-1].periodoLancado){
		    	
		    	if (isMobile() ==false){

		    		html+='<div class="item" id="div'+listaLancamentos[i].id+'">'
		    		+'<div><b>'
		    		+semana+" "+listaLancamentos[i].data+ " de "+listaLancamentos[i].inicio.substr(0,5)+ " até "+listaLancamentos[i].termino.substr(0,5)+"<font color=#515caf> <br>Duração: "+listaLancamentos[i].horasTrabalhadas.substr(0,5)+'</font></b>'
		    		+"<br>"+listaLancamentos[i].nomeOrganizacao+" - "+listaLancamentos[i].nomeProjeto +" - "+listaLancamentos[i].nomeEtapa+" - "+listaLancamentos[i].nomeConsultor+" - "+listaLancamentos[i].nomeTipo+"<br>" +listaLancamentos[i].descricao +'</div>'
		    		+'<button class="btnedit" onclick="editarLancamento('+"'"+listaLancamentos[i].id+"'"+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
		    		+'<button class="btndelete" onclick="excluirLancamento('+"'"+listaLancamentos[i].id+"'"+')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>'
		    		+'</div>'
		    	}
		    	else{
		    		
		    		html+='<div class="item" id="div'+listaLancamentos[i].id+'">'
		    		+'<button class="action" onclick="editarLancamento('+"'"+listaLancamentos[i].id+"'"+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
		    		+'<div><b>'
		    		+semana+" "+listaLancamentos[i].data+ " de "+listaLancamentos[i].inicio.substr(0,5)+ " até "+listaLancamentos[i].termino.substr(0,5)+"<font color=#515caf> <br>Duração: "+listaLancamentos[i].horasTrabalhadas.substr(0,5)+'</font></b>'
		    		+"<br>"+listaLancamentos[i].nomeOrganizacao+" - "+listaLancamentos[i].nomeProjeto +" - "+listaLancamentos[i].nomeEtapa+" - "+listaLancamentos[i].nomeConsultor+" - "+listaLancamentos[i].nomeTipo+"<br>" +listaLancamentos[i].descricao +'</div>'
		    		+'<button class="action" onclick="excluirLancamento('+"'"+listaLancamentos[i].id+"'"+')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>'
		    		+'</div>'
		    	}    	   
		    }else{
		    	
		    	if (isMobile() ==false){
		    		
		    		html+='<div class="item" id="div'+listaLancamentos[i].id+'">'
		    		+'<div><b>'
		    		+semana+" "+listaLancamentos[i].data+ " de "+listaLancamentos[i].inicio.substr(0,5)+ " até "+listaLancamentos[i].termino.substr(0,5)+"<font color=#515caf> <br>Duração: "+listaLancamentos[i].horasTrabalhadas.substr(0,5)+'</font></b>'
		    		+"<br>"+listaLancamentos[i].nomeOrganizacao+" - "+listaLancamentos[i].nomeProjeto +" - "+listaLancamentos[i].etapa+" - "+listaLancamentos[i].consultor+"<br>" +listaLancamentos[i].descricao +'</div>'
		    		+'<button disabled class="btnedit" onclick="editarLancamento('+"'"+listaLancamentos[i].id+"'"+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px;opacity:0.5;"></i></button>'
		    		+'<button disabled class="btndelete" onclick="excluirLancamento('+"'"+listaLancamentos[i].id+"'"+')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px;opacity:0.5;"></i></button>'
		    		+'</div>'
		    	}
		    	else{
		    		
		    		html+='<div class="item" id="div'+listaLancamentos[i].id+'">'
		    		+'<button disabled class="action" onclick="editarLancamento('+"'"+listaLancamentos[i].id+"'"+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px;opacity:0.5;"></i></button>'
		    		+'<div><b>'
		    		+semana+" "+listaLancamentos[i].data+ " de "+listaLancamentos[i].inicio.substr(0,5)+ " até "+listaLancamentos[i].termino.substr(0,5)+"<font color=#515caf> <br>Duração: "+listaLancamentos[i].horasTrabalhadas.substr(0,5)+'</font></b>'
		    		+"<br>"+listaLancamentos[i].nomeOrganizacao+" - "+listaLancamentos[i].nomeProjeto +" - "+listaLancamentos[i].etapa+" - "+listaLancamentos[i].consultor+"<br>" +listaLancamentos[i].descricao +'</div>'
		    		+'<button disabled class="action" onclick="excluirLancamento('+"'"+listaLancamentos[i].id+"'"+')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px;opacity:0.5;"></i></button>'
		    		+'</div>'
		    	}    	   
		    }
		}
		tabelaLancamentos.innerHTML = html;
	}else{
		tabelaLancamentos.innerHTML = html;
	}
	document.getElementById("lancamentotimesheet_cargarealizada").innerHTML="<center><H1>"+ totalLancamentos.toString("hh:mm")+"</H1> Carga Horária Realizada </center>";
	buscarAprovacao();	
}


function sair()
{
	localStorage.clear();
	sessionStorage.setItem("colaborador",0);
	sessionStorage.setItem("lancamento",0);
	sessionStorage.setItem("periodo",0);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../../login/login.html';
}


target = document.querySelectorAll('.item-list')[0];

//cria uma nova instância de observador
observer = new MutationObserver(function(mutations) {
	mutations.forEach(function(mutation) {
		console.log("tipo de mutação " +mutation.type);

		$(function() {$('.example-1').listSwipe();});

	});    
});

//configuração do observador:
config = { attributes: true, childList: true, characterData: true };

//passar o nó alvo pai, bem como as opções de observação
observer.observe(target, config);

function editaHora(hora){
	
	horas = hora.substr(0,hora.indexOf(":")) * 1;
	minutos = hora.substr(hora.indexOf(":")+1,2) * 1;
	
	horas = horas.toString().length >= 2 ? horas : ("0" + horas);
	minutos = minutos.toString().length >= 2 ? minutos : ("0" + minutos);
	
	hora = horas + ':' + minutos;
	
	return hora;
}


function somaHora(hrA, hrB, zerarHora) {
	//  if(hrA.length != 5 || hrB.length != 5) return "00:00";

	var temp = 0;
	var nova_h = 0;
	var novo_m = 0;

	var hora1 = hrA.substr(0,hrA.indexOf(":")) * 1;
	var hora2 = hrB.substr(0, hrB.indexOf(":")) * 1;
	var minu1 = hrA.substr(hrA.indexOf(":")+1, 2) * 1;
	var minu2 = hrB.substr(hrB.indexOf(":")+1, 2) * 1;

	temp = minu1 + minu2;
	while(temp > 59) {
		nova_h=nova_h+1;
		temp = temp - 60;
	}
	novo_m = temp.toString().length >= 2 ? temp : ("0" + temp);

	temp = hora1 + hora2 + nova_h;
	while((temp > 23) && (zerarHora)) {
		temp = temp - 24;
	}
	nova_h = temp.toString().length >= 2 ? temp : ("0" + temp);

	return nova_h + ':' + novo_m;
}