const INTERNO = "Interno";
const EXTERNO = "Externo";

const mapProjetoInternoTipoHora = {

	"Administrativo": INTERNO,
	"Marketing": INTERNO,
	"Vendas": INTERNO,
	"P&D": INTERNO
}

const mapProjetoExternoTipoHora = {

	"Consultoria": EXTERNO,
	"Execução": EXTERNO,
	"Horas Técnicas": EXTERNO,
	"Desenvolvimento": EXTERNO,
	"Preparação de Sala de Aula": EXTERNO,
}

const projetoTipoHoraValido = (tipoProjeto, tipoHora) => {
	return projetoInternoTipoHoraValido(tipoProjeto, tipoHora) || projetoExternoTipoHoraValido(tipoProjeto, tipoHora);
}

const projetoInternoTipoHoraValido = (tipoProjeto, tipoHora) => {
	return (mapProjetoInternoTipoHora[tipoHora] == tipoProjeto);
}

const projetoExternoTipoHoraValido = (tipoProjeto, tipoHora) => {
	return (mapProjetoExternoTipoHora[tipoHora] == tipoProjeto);
}