//Variáveis da sessão
var listaColaboradores = [];
var listaLancamento = [];
var listaTipoHoras = [];
var listaConsultor = [];
var listaConsultorFiltrada = [];
var HorasTotal=0;
var txtHoraInicial=0;
var listaPeriodos = [];
var listaOrganizacao = [];
var listaProjeto = [];
var listaEtapa = [];

var colaborador = 0;
var colaboradorLogado = 0;
var cliente = 0;
var projeto = 0;
var etapa = 0;
var consultor = 0;

var inicio=0;
var termino=0;
var tipoHora = 0;
var nome="";
var lancamento=0;
var edicao="0";
var horasTrabalhadas=0;
var data="";
var diaSemana;
var senha;
var id=0;
var periodo=0;
var bloqueado=0;

/**/

function admin_init(){

	colaborador=sessionStorage.getItem("colaborador");
	colaboradorLogado=sessionStorage.getItem("colaboradorLogado");
	periodo = sessionStorage.getItem("periodo");
	
	if ((colaborador==0) || (colaborador==null) || (colaborador==undefined)) {
		
		window.document.location.href = '../../login/login.html';
	}
	lancamento=sessionStorage.getItem("lancamento");
	senha=sessionStorage.getItem("senha");
	edicao=sessionStorage.getItem("edicao");
	bloqueado=sessionStorage.getItem("bloqueado");

	buscarTipoHoras();

	document.getElementById("timesheet_cliente").addEventListener("change", mudaCliente);
	document.getElementById("timesheet_projeto").addEventListener("change", mudaProjeto);
	document.getElementById("timesheet_etapa").addEventListener("change", mudaEtapa);
	document.getElementById("timesheet_data").addEventListener("change", buscaData);
	document.getElementById("menuop1").addEventListener("click", carregaLancamentos);
	document.getElementById("menuop2").addEventListener("click", menu);
	document.getElementById("menuop3").addEventListener("click", sair);
	document.getElementById("btn_salvar").addEventListener("click",btn_salvar_click);
	document.getElementById("timesheet_descricao").addEventListener("keypress",validaTextArea); 
}


function buscaData()
{
	var dataSel=document.getElementById("timesheet_data").value;
	diaSemanaData(dataSel);
	encontraPeriodo(dataSel);
}


function validaTextArea()
{
	if ((event.keyCode == 13) || (event.keyCode == 47) |(event.keyCode == 92))  {
		event.preventDefault();
	}
}

//TRATAMENTO DOS EVENTOS

//verifica se foi escolhido valor na combo diferente do default
function verificaValorSelecionadoCombo(str) {
	var selectObj  = document.getElementById(str);
	var valueToSet  = document.getElementById(str).value;
	if (selectObj.options[0].value==valueToSet) 
		return false;
	else
		return true;

}

//verifica se o campo esta vazio
function verificaPreeenchimento(str) {
	var v = document.getElementById(str).value;
	if ((v == null) || (v == ""))
		return false
		else	
			return true;
}


function validaTimeSheet() {
	if ((verificaValorSelecionadoCombo("timesheet_cliente")
			&& verificaValorSelecionadoCombo("timesheet_periodo")
			&& verificaValorSelecionadoCombo("timesheet_projeto")
			&& verificaValorSelecionadoCombo("timesheet_etapa")
			&& verificaValorSelecionadoCombo("timesheet_consultor")
			&& verificaValorSelecionadoCombo("timesheet_tipoHora")
			&& verificaPreeenchimento("timesheet_data")
			&& verificaPreeenchimento("timesheet_diaSemana")
			&& verificaPreeenchimento("timesheet_inicio")
			&& verificaPreeenchimento("timesheet_termino")
			&& verificaPreeenchimento("timesheet_horasTrabalhadas")
			&& verificaPreeenchimento("timesheet_descricao")
	)==true)
	{
		return  true;
	}
	else
	{
		alert('Existem campos que não foram preenchidos, Por favor preencha');
		return false;
	}

}


function carregaLancamentos()
{
	sessionStorage.setItem("colaborador",colaboradorLogado);
	sessionStorage.setItem("lancamento",0);
	sessionStorage.setItem("periodo",0);
	window.document.location.href = '../lancamentosTimeSheet/lancamentosTimeSheet.html';

}


function sair()
{
	sessionStorage.setItem("colaborador",0);
	sessionStorage.setItem("colaboradorLogado",0);
	sessionStorage.setItem("lancamento",0);
	sessionStorage.setItem("periodo",0);
	window.document.location.href = '../../login/login.html';

}


function menu()
{
	sessionStorage.setItem("colaborador",colaborador);
	sessionStorage.setItem("colaboradorLogado",colaboradorLogado);
	sessionStorage.setItem("lancamento",0);
	sessionStorage.setItem("periodo",0);
	window.document.location.href = '../../menu/menu.html';

}


function editaLancamento(){

	document.getElementById('timesheet_periodo').value = listaLancamento[0].periodo;
	
	var i=0;
	var achei=0;
	var idLancamentoOrganizacao = listaLancamento[0].cliente.substring(0, listaLancamento[0].cliente.length - 3);
	var idLancamentoProjeto = listaLancamento[0].projeto;
	var idLancamentoEtapa = listaLancamento[0].etapa;
	var idConsultor = listaLancamento[0].consultor;
	
	while (achei==0){
		
		if (listaOrganizacao[i].idOrg == idLancamentoOrganizacao){
			achei=1;
			setSelectedValue(document.getElementById("timesheet_cliente"),i+1);
		}
		i=i+1;
	}
	cliente=listaOrganizacao[i-1].idOrg;
	
	comboListPrj(listaConsultor, cliente);
	preencheListaProjeto();
	
	i=0;
	achei=0;
	while (achei==0){
		
		if (listaProjeto[i].idProj == idLancamentoProjeto){	
			achei=1;
			setSelectedValue(document.getElementById("timesheet_projeto"),i+1);
		}
		i=i+1;
	}
	projeto=listaProjeto[i-1].idProj;
	
	comboListEtp(listaConsultor, projeto);
	preencheListaEtapas();
	
	i=0;
	achei=0;
	while (achei==0){
		
		if (listaEtapa[i].idEtapa == idLancamentoEtapa){	
			achei=1;
			setSelectedValue(document.getElementById("timesheet_etapa"),i+1);
		}
		i=i+1;
	}
	
	preencheListaConsultores(comboListConsultor(listaConsultor, idLancamentoEtapa));
	
	i=0;
	achei=0;
	while (achei==0){
		
		if (listaTipoHoras[i].id == listaLancamento[0].tipo){	
			achei=1;
			setSelectedValue(document.getElementById("timesheet_tipoHora"),i+1);
		}
		i=i+1;
	}
	
	i=0;
	achei=0;
	while (achei==0){
		
		if (listaConsultor[i].id == idConsultor){	
			achei=1;
			setSelectedValue(document.getElementById("timesheet_consultor"),i+1);
		}
		i=i+1;
	}
	
	document.getElementById('timesheet_inicio').value=listaLancamento[0].inicio.substring(0,5);
	document.getElementById('timesheet_inicio').innerHTML="";
	document.getElementById('timesheet_termino').value=listaLancamento[0].termino.substring(0,5);
	document.getElementById('timesheet_termino').innerHTML="";
	document.getElementById('timesheet_horasTrabalhadas').value=listaLancamento[0].horasTrabalhadas.substring(0,5);
	document.getElementById('timesheet_horasTrabalhadas').innerHTML="";
	document.getElementById('timesheet_descricao').value=listaLancamento[0].descricao;
	document.getElementById('timesheet_descricao').innerHTML="";
	document.getElementById('timesheet_data').value=listaLancamento[0].data;
	document.getElementById('timesheet_data').innerHTML="";
}

function btn_salvar_click(){
	if (validaTimeSheet() ==true)
	{
		salvarTimeSheet();
	}
}

//PREENCHIMENTO DAS COMBOS

function preencheListaOrganizacao(){
	var html = '<option value="0" Disabled Selected>Cliente...</option>';
	var select = "";
	for (i=0;i<listaOrganizacao.length;i++){
		if(cliente==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value=\"'+listaOrganizacao[i].idOrg+'\" '+select+'>' +listaOrganizacao[i].nomeOrganizacao+'</option>';
	}

	document.getElementById("timesheet_cliente").innerHTML = html;
	setSelectedValue(document.getElementById("timesheet_cliente"),0);
}


function preencheListaProjeto(){

	var html = '<option value="0" Disabled Selected>Projeto...</option>';
	var select = "";
	for (i=0;i<listaProjeto.length;i++){
		if(projeto==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaProjeto[i].idProj+'" '+select+'>'+listaProjeto[i].nomeProjeto+'</option>';
	}
	
	document.getElementById("timesheet_projeto").innerHTML = html;
	if (listaProjeto.length == 1)
	{
	setSelectedValue(document.getElementById("timesheet_projeto"),1);
	mudaProjeto();
	}
	else
	{setSelectedValue(document.getElementById("timesheet_projeto"),0);}
}


function preencheListaPeriodos(){
	
	var html = '<option value="0">Períodos...</option>';
	var select = "";
	for (i=0;i<listaPeriodos.length;i++){
		if(periodo == i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaPeriodos[i].id+'" '+select+'>' +listaPeriodos[i].nomePeriodoLancado+'</option>';
	}
	document.getElementById("timesheet_periodo").innerHTML = html;
	setSelectedValue(document.getElementById("timesheet_periodo"),listaPeriodos.length);
}


function encontraPeriodo(dataLancamento){

	var i=0;
	var posicao=0;
	var dataInicial=0;
	var datafinal=0;
	
	for (i=0;i<listaPeriodos.length;i++){
		
		dataInicial = montaDataPeriodo("21",listaPeriodos[i].periodo);
		dataFinal = montaDataPeriodo("20",listaPeriodos[i].periodo);
		
		if ((new Date(dataLancamento) >= new Date(dataInicial)) && (new Date(dataLancamento) <= new Date(dataFinal))){
			posicao=i;
		}
	}
	setSelectedValue(document.getElementById("timesheet_periodo"), posicao+1);
}


function montaDataPeriodo(dia, anoMes){
	
	var ano = anoMes.substring(0,4);
	var mes = anoMes.substring(4,6);
	var mesS=0;
	
	if(dia === "21"){
		
		mes = parseInt(mes) - 1;
		mes.toString();
	}
	
	if((mes < 10) && (mes.length  <2)){
		
		mes='0'+mes;
		if (mes==0) mes=12;
		
	}
	
	
	return ano+'-'+mes+'-'+dia;
}


function preencheListaEtapas(){
	var html = '<option value="0" Disabled Selected>Etapa...</option>';
	var select = "";
	for (i=0;i<listaEtapa.length;i++){
		if(etapa==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaEtapa[i].idEtapa+'" '+select+'>' 
		         +listaEtapa[i].nomeEtapa+"-"
		         +listaEtapa[i].descricaoEtapa+'</option>';
	}

	
	document.getElementById("timesheet_etapa").innerHTML = html;
	if (listaEtapa.length == 1)
	{setSelectedValue(document.getElementById("timesheet_etapa"),1);
	mudaEtapa();}
	else
	{ setSelectedValue(document.getElementById("timesheet_etapa"),0);}
}


function preencheListaTipoHoras(){
	
	var html = '<option value="0">Tipo de Hora...</option>';
	var select = "";
	for (i=0;i<listaTipoHoras.length;i++){
		if(tipoHora==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaTipoHoras[i].id+'" '+select+'>' +listaTipoHoras[i].descricao+'</option>';
	}

	document.getElementById("timesheet_tipoHora").innerHTML = html;
	setSelectedValue(document.getElementById("timesheet_tipoHora"),0);
}


function preencheListaConsultores( listaConsultorFiltrada ){
	var html = '<option value="0">Consultor...</option>';
	var select = "";

	for (i=0;i<listaConsultorFiltrada.length;i++){
		if(consultor==i){
			select = "selected";
		}else{
			select = "";
		}
		
		html += '<option value="'+ listaConsultorFiltrada[i].id + '" '+select+'>' 
					+ listaConsultorFiltrada[i].name +" - "
					+ listaConsultorFiltrada[i].descricao
				+'</option>';
	}
	

	

	
	document.getElementById("timesheet_consultor").innerHTML = html;
	if (listaConsultorFiltrada.length == 1)
		{
	       setSelectedValue(document.getElementById("timesheet_consultor"),1);
		}
	else
		{ setSelectedValue(document.getElementById("timesheet_consultor"),0);}
//CALCULO INICIO E FIM DAS HORAS
}
function buscaIntervalo(){
	
	var horas;
	var minutos;
	txtHoraInicial = document.getElementById("timesheet_inicio");
	txtHoraFinal = document.getElementById("timesheet_termino");

	SegundosInicial = (txtHoraInicial.value.substr(0,2) * 3600);
	SegundosInicial += (txtHoraInicial.value.substr(3,2) * 60);

	SegundosFinal = (txtHoraFinal.value.substr(0,2) * 3600);
	SegundosFinal += (txtHoraFinal.value.substr(3,2) * 60);

	HorasTotal = parseInt((SegundosFinal - SegundosInicial)/3600);
	MinutosTotal = parseInt(((SegundosFinal - SegundosInicial)%3600)/60);

	if (HorasTotal <0){
		
		horas= "00";
		minutos="00";
	}
	else{
		
		var cont = 2-(HorasTotal.toString().length);
		horas = "0".repeat(cont) +HorasTotal.toString() ;
		
		var cont = 2-(MinutosTotal.toString().length);
		minutos = "0".repeat(cont) +MinutosTotal.toString();
	}
	document.getElementById("timesheet_horasTrabalhadas").value = horas + ":" + minutos;
	document.getElementById('timesheet_horasTrabalhadas').innerHTML="";
}


$(document).on('keyup , change' , function(){
	buscaIntervalo();
});

//PROCESSAMENTO DO RETORNO DOS WEBSERVICES

function callback(data){
		
	if(data.action=="buscarTipoHoras"){
		listaTipoHoras = jQuery.parseJSON(data.retorno);
		preencheListaTipoHoras();
		
		if(lancamento != 0)
			buscarLancamento();
		else
			buscarPeriodos();
	}
	
	if(data.action=="buscarPeriodos"){
		listaPeriodos = jQuery.parseJSON(data.retorno);
		preencheListaPeriodos();
		
		if(lancamento != 0) {
			i=0;
			achei=0;
			while (achei==0) {
				
				if (listaPeriodos[i].id == listaLancamento[0].periodo){	
					achei=1;
					setSelectedValue(document.getElementById("timesheet_periodo"),i);
				}
				i=i+1;
			}
		}
		if(listaPeriodos != ""){
			document.getElementById('timesheet_colaborador').value=listaPeriodos[0].nomeColaborador;
			document.getElementById('timesheet_colaborador').innerHTML="";
			document.getElementById('timesheet_colaborador').disabled = true;
			var data =new Date();
			document.getElementById("timesheet_data").valueAsDate=data;
			document.getElementById('timesheet_data').innerHTML="";
			buscaData(); 
			var currentTime = data.getHours() + ':' + data.getMinutes();
			document.getElementById("timesheet_termino").value= formataHorario(currentTime);
			document.getElementById('timesheet_termino').innerHTML="";
			document.getElementById('timesheet_horasTrabalhadas').innerHTML="";
			var diaSelecionado=document.getElementById("timesheet_data").value;
			bloqueado = listaPeriodos[0].bloqueado;
			diaSemanaData(diaSelecionado);
			encontraPeriodo(diaSelecionado);
			buscarConsultor();
		}else{
			alert("Não existe capa de Timesheet para este usuário!");
		}
	}
	
	if(data.action=="buscarConsultor"){
		listaConsultor = jQuery.parseJSON(data.retorno);
		comboListOrg(listaConsultor);
		preencheListaOrganizacao();
		
		if (lancamento != 0){
			id=lancamento;
			editaLancamento();
		}
	}

	if(data.action=="buscarLancamento"){
		listaLancamento = jQuery.parseJSON(data.retorno);
		buscarPeriodos();
	}

	if(data.action=="salvarTimeSheet"){
		retornoSalvar = jQuery.parseJSON(data.retorno);
		document.getElementById('btn_salvar').disabled = false;
		document.getElementById("btn_salvar").addEventListener("click",btn_salvar_click);
		if (data.edicao != 0)	{ //eh uma edicao
			
			if (data.retorno == 1)
				alert("Já existe um lançamento neste intervalo ou que compreende este intervalo");
			  
			else{
				alert ("Edição do TimeSheet salvo!");
				sessionStorage.setItem("lancamento",0);
				sessionStorage.setItem("periodo",0);
				location.reload();
			}
		}
		else{
			if (data.retorno == 1)
				alert("Já existe um lançamento neste intervalo ou que compreende este intervalo");
			  
			else{
				alert ("Lançamento do TimeSheet salvo!");
				sessionStorage.setItem("lancamento",0);
				sessionStorage.setItem("periodo",0);
				location.reload();
			}
		}
	}
}

function construirPeriodoNovoLancamento(data){

	var dia = parseInt(data.replace(/.*-.*-/gi, ''), 10);
	var mes = parseInt(data.replace(/[0-9]{4}-/gi, '').replace(/-.*/gi, ''), 10);
	var ano = parseInt(data.replace(/-.*-.*/gi, ''));
	
	if( dia >= 21 ){
		if( mes === 12){
			mes = '0'.concat(1);
			ano += 1;
		}else{
			mes += 1;
			if(mes < 10){
				mes = '0'.concat(mes);
			}				
		}
	}else{
		if(mes < 10){
			mes = '0'.concat(mes);
		}
	}
	var periodo = ano.toString().concat(mes.toString()).concat('.0');
	
	return periodo;
	
}

function salvarTimeSheet(){
	
	var data1 = new Date(timesheet_data.value+"T00:00:00-03:00");

	//data1.format("DD/MM/YYYY hh:mm");
	var data2 = new Date();

	//data2.format("dd/MM/yyyy HH:mm:ss");
	var diferenca = dateDiff(data1, data2).days;

	//verifica se data lançada é maior que a atual
	var diferencaMaior=0;
		
	if (data1 > data2)
	{
		diferencaMaior=1;
	}	

	var diaSemanalancamento =diaSemanaData(formatarData(data2));
	var diaSemanaTrabalho=diaSemanaData(formatarData(data1));
	var i = document.getElementById("timesheet_periodo").selectedIndex;
	
	var dataNovoLancamento = document.getElementById("timesheet_data").value;
	var periodoNovoLancamento = construirPeriodoNovoLancamento(dataNovoLancamento);
	
	if(parseInt(periodoNovoLancamento,10)  > parseInt(listaPeriodos[i-1].periodoBloqueado,10)){
	
		buscaIntervalo();
	
		if ((HorasTotal<=0) && (MinutosTotal <=0) && (txtHoraFinal.value!="") && (txtHoraInicial.value!="") ) {
	
			alert ( "O ínicio do intervalo não pode ser maior ou igual que o fim do intervalo!\n\t\t\t\tPor favor verifique o valor lançado.")
			txtHoraFinal.value=":";
			txtHoraFinal.innerHTML="";
			document.getElementById('timesheet_horasTrabalhadas').value=":";
			document.getElementById('timesheet_horasTrabalhadas').innerHTML="";
			txtHoraInicial.focus();
		}else {

			if(bloqueado == "true"){
				
				if(diferencaMaior > 0){
					alert("Não é permitido fazer lançamento para datas futuras!")
					
				}else{
					
					if(diferenca <= 1){
						
						NovoTimesheet();
						
					}else{
						
						if((diaSemanalancamento == 1 || diaSemanalancamento == 0) && diaSemanaTrabalho == 5 && diferenca <= 3){
							
							NovoTimesheet();
							
						}else if(diaSemanalancamento == 1 && diaSemanaTrabalho == 6 && diferenca == 2){
							
							NovoTimesheet();
							
						}else{
							alert("\t\t\t\t\tUsuário bloqueado!\nUsuários bloqueados so podem lançar com até 1 dia de diferença.");
						}
							
					}
				}
			}else{
				
				if(diferencaMaior > 0)
					alert("Não é permitido fazer lançamento para datas futuras!")
				else
					NovoTimesheet();
			}
		}
	}else
		alert("Período bloqueado para lançamentos!")
}


function NovoTimesheet(){

	var tipoProjeto=0;
	var e =document.getElementById("timesheet_cliente");
	var clienteid = e.options[e.selectedIndex].value;
	
	e=document.getElementById("timesheet_projeto");
	var projetoid = e.options[e.selectedIndex].value;
	
	i=0;
	achei=0;
	
	while (achei==0){
		if (listaProjeto[i].idProj == projetoid){
			tipoProjeto=listaProjeto[i].tipoDeProjeto;	
			achei=1;
		}
		i=i+1;
	}
	e=document.getElementById("timesheet_tipoHora");
	var tipohoraid = e.options[e.selectedIndex].text;
	e=document.getElementById("timesheet_etapa");
	var etapaid = e.options[e.selectedIndex].value;
	e=document.getElementById("timesheet_consultor");
	var consultorId = e.options[e.selectedIndex].value;
	e=document.getElementById("timesheet_periodo");
	var periodoid = e.options[e.selectedIndex].value;
	var data = document.getElementById("timesheet_data").value;
	var horaInicio = document.getElementById("timesheet_inicio").value;
	var horaTermino = document.getElementById("timesheet_termino").value;
	var horaTotal = document.getElementById("timesheet_horasTrabalhadas").value;
	var descricao = document.getElementById("timesheet_descricao").value;
	var diaSemana = document.getElementById("timesheet_diaSemana").value;
		
	if (projetoTipoHoraValido(tipoProjeto, tipohoraid)){
		
		var  a = document.getElementById("timesheet_tipoHora");
		tipohoraid = a.options[a.selectedIndex].value;
		edicao = lancamento;
		var url = retornaServer();
		var data = {
				page: "itemTimesheet",
				action: "salvarTimeSheet",
				id: id,
				data:data,
				colaborador: colaborador,
				cliente:clienteid,
				projeto: projetoid,
				etapa: etapaid,
				consultor: consultorId,
				tipoHora: tipohoraid,
				diaSemana:diaSemana, 				
				inicio: horaInicio,
				termino: horaTermino,
				horasTrabalhadas: horaTotal,
				descricao: descricao,
				edicao:edicao,
				periodo: periodoid
		}
		document.getElementById("btn_salvar").addEventListener("click",()=>{});
		document.getElementById('btn_salvar').disabled = true;
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");
	}
	else
	{
		if (tipoProjeto == "Interno" && !projetoInternoTipoHoraValido(tipoProjeto, tipohoraid))  
		{
			alert ( "Somente podem ser lançados para projetos internos horas do tipo:administrativas, vendas , marketing e P&D.");
		}
		if (tipoProjeto == "Externo" && !projetoExternoTipoHoraValido(tipoProjeto, tipohoraid))
		{
			alert ( "Somente podem ser lançados para projetos de externos horas do tipo: consultoria, execução, horas técnicas, desenvolvimento e\n\t\t\t\t\tpreparação de sala de aula.");
		}
	}
}


function buscarConsultor(){
	
	var url = retornaServer();
	var data = {
			page: "consultor",
			action: "buscarConsultor",
			colaborador: colaborador	
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function buscarTipoHoras(){
	var url = retornaServer();
	var data = {
			page: "tipoHora",
			action: "buscarTipoHoras"
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function buscarPeriodos(){
	var url = retornaServer();
	var data = {
			page: "timesheet",
			action: "buscarPeriodos",
			colaborador: colaborador
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function buscarLancamento(){
	var url = retornaServer();
	var data = {
			page: "itemTimesheet",
			action: "buscarLancamento",
			lancamento: lancamento,	
			periodo: periodo
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function setSelectedValue(selectObj, valueToSet) {
	
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].index == valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}


function mudaCliente() {

	var e = document.getElementById("timesheet_cliente");
	clienteid = e.options[e.selectedIndex].value;
	
	var achei = -1;
	
	if(clienteid != 0){
		listaOrganizacao.filter(function(valor, index){
			if(valor.idOrg === clienteid){
				achei = index;
			}
		});
	}
	
	clientePos=achei;
	cliente=clienteid;
	comboListPrj(listaConsultor,document.getElementById("timesheet_cliente").value);
	setSelectedValue(document.getElementById("timesheet_etapa"),0);
    setSelectedValue(document.getElementById("timesheet_projeto"),0);
    setSelectedValue(document.getElementById("timesheet_consultor"),0);
	preencheListaProjeto();
}


function mudaProjeto() {

	var e = document.getElementById("timesheet_projeto");
	projetoid = e.options[e.selectedIndex].value;
	
	var achei = -1;
	
	if(projetoid != 0){
		listaProjeto.filter(function(valor, index){
			if(valor.idProj === projetoid){
				achei = index;
			}	
		});
	}
	
	projetoPos=achei;
	projeto=projetoid;
	comboListEtp(listaConsultor,document.getElementById("timesheet_projeto").value);
    preencheListaEtapas();

}

function mudaEtapa(){
	
	var e = document.getElementById("timesheet_etapa");
	etapaId = e.options[e.selectedIndex].value;
	
	var achei = -1;
	
	if(etapaId != 0){
		listaEtapa.filter(function(valor, index){
			if(valor.idEtapa === etapaId){
				achei = index;
			}	
		});
	}
	
	etapaPos=achei;
	etapa=etapaId;
	const listaConsultorFiltrada = comboListConsultor(listaConsultor, document.getElementById("timesheet_etapa").value);
	preencheListaConsultores( listaConsultorFiltrada );
	
}


function dateDiff(dt1, dt2)
{
	/*
	 * setup 'empty' return object
	 */
	var ret = {days:0, months:0, years:0};

	/*
	 * If the dates are equal, return the 'empty' object
	 */
	if (dt1 == dt2) return ret;

	/*
	 * ensure dt2 > dt1
	 */
	if (dt1 > dt2)
	{
		var dtmp = dt2;
		dt2 = dt1;
		dt1 = dtmp;
	}

	/*
	 * First get the number of full years
	 */

	var year1 = dt1.getFullYear();
	var year2 = dt2.getFullYear();

	var month1 = dt1.getMonth();
	var month2 = dt2.getMonth();

	var day1 = dt1.getDate();
	var day2 = dt2.getDate();

	/*
	 * Set initial values bearing in mind the months or days may be negative
	 */
	

	ret['years'] = year2 - year1;
	ret['months'] = month2 - month1;
	ret['days'] = day2 - day1;

	/*
	 * Now we deal with the negatives
	 */

	/*
	 * First if the day difference is negative
	 * eg dt2 = 13 oct, dt1 = 25 sept
	 */
	if (ret['days'] < 0)
	{
		/*
		 * Use temporary dates to get the number of days remaining in the month
		 */
		var dtmp1 = new Date(dt1.getFullYear(), dt1.getMonth() + 1, 1, 0, 0, -1);

		var numDays = dtmp1.getDate();

		ret['months'] -= 1;
		ret['days'] += numDays;

	}

	/*
	 * Now if the month difference is negative
	 */
	if (ret['months'] < 0)
	{
		ret['months'] += 12;
		ret['years'] -= 1;
	}

	return ret;
}


function diaSemanaData(dataEscolhida)
{

	var dataEsc=dataEscolhida;
	var dt1   = parseInt(dataEsc.substring(8,10));
	var mon1  = parseInt(dataEsc.substring(5,7));
	var yr1   = parseInt(dataEsc.substring(0,4));
	var dataNova= new Date(yr1, mon1-1, dt1);

	var dia= dataNova.getDay();
	var semana=new Array(6);
	semana[0]='Domingo';
	semana[1]='Segunda-Feira';
	semana[2]='Terça-Feira';
	semana[3]='Quarta-Feira';
	semana[4]='Quinta-Feira';
	semana[5]='Sexta-Feira';
	semana[6]='Sábado';
	document.getElementById("timesheet_diaSemana").value=semana[dia];
	document.getElementById('timesheet_diaSemana').innerHTML="";
	return dia;
}


function formataHorario(horario){
	
	var hora = horario.substring(0,2);
	var minutos = horario.substring(3,5);
	
	if(hora < 10)
		hora = "0" + hora;
	if(minutos < 10)
		minutos = "0" + minutos;
	
	return hora + ":" + minutos;
}


function comboListOrg(listaConsultor){
	
	listaOrganizacao = listaConsultor.filter( (valor, index, array ) => array.findIndex( find => find["idOrg"] === valor["idOrg"] ) === index );
}


function comboListPrj(listaConsultor, idOrganizacao){

	listaProjeto = listaConsultor.filter( (valor, index, array ) => array.findIndex( find => find["idProj"] === valor["idProj"] ) === index )
	  .filter( (valor, index, array ) => array[index].idOrg === idOrganizacao);
}


function comboListEtp(listaConsultor, idProjeto){
	
	listaEtapa = listaConsultor.filter( (valor, index, array ) => array.findIndex( find => find["idEtapa"] === valor["idEtapa"] ) === index )
	  .filter( (valor, index, array ) => array[index].idProj === idProjeto);
}


function comboListConsultor(listaConsultor, idEtapa){
	
	return listaConsultorFiltrada = listaConsultor.filter( valor => valor.idEtapa === idEtapa )
	 
	
}