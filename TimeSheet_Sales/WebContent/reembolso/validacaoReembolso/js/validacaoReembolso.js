//Variáveis da sessão
var listaColaboradores = [];
var listaClientes = [];
var listaProjetos = [];
var listaPeriodos = [];
var listaAprovacao = [];
var listaEtapas = [];
var listaArquivos=[];
var listaTipoReembolso = [];
var observer;
var config;
var target;
var periodo;
var indiceColaborador;

var colaborador = 0;
var cliente = 0;
var projeto = 0;
var etapa = 0;
var tipoHora = 0;
var nome="";
var status="";
var permissoes="";
var volta;

var listaLancamentosReembolso = [];


function admin_init(){
	
	colaboradorLogado=sessionStorage.getItem("colaboradorLogado");
	colaborador=sessionStorage.getItem("colaborador");
	permissoes=sessionStorage.getItem("permissoes");

	if ((colaborador == 0) || (colaborador==null) || (colaborador==undefined) ||(permissoes.substring(2)==0)) {
		
		window.document.location.href = '../../login/login.html';
		
	}else{	
		
		setSelectedValue(document.getElementById("validacaoreembolso_status"), "Pendente");
		
		buscarReembolsosStatus();
		
		document.getElementById("validacaoreembolso_status").addEventListener("change", buscarReembolsosStatus);
		
		document.getElementById("menuop1").addEventListener("click", carregaReembolso);
		document.getElementById("menuop3").addEventListener("click", sair);
		
	}

}



function isMobile(){
	
	var userAgent = navigator.userAgent.toLowerCase();
	
	if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
		return true;
	else
		return false;

}

function btn_salvar_click(){
	salvarReembolso();
}

function buscarReembolsosStatus(){
	
	status = document.getElementById("validacaoreembolso_status").value;

	var url = retornaServer();
	
	var data = {
			page: "reembolso",
			status: status,
			action: "buscarReembolsosStatus",
	}
	
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}

function aprovarReembolso(lancamentoId){
	
	var valorAvaliacao="";
	var mensagemPrompt = prompt("Aprovar o reembolso? Entre com a justificativa" , "ok");
	
	if ( mensagemPrompt != null ){
	
		valorAvaliacao="Aprovado";
		avaliarLancamentoReembolso(lancamentoId, valorAvaliacao, mensagemPrompt);	
	
	}

}

function reprovarReembolso(lancamentoId){
	
	var valorAvaliacao="";
	var mensagemPrompt = prompt("Reprovar o reembolso? Entre com a justificativa","");
	
	if ( mensagemPrompt != null ){
		
		valorAvaliacao="Reprovado";
		avaliarLancamentoReembolso(lancamentoId, valorAvaliacao,mensagemPrompt);	
	
	}
}

function avaliarLancamentoReembolso(lancamentoId, valorAvaliacao,justificativa){
	
	var url = retornaServer();
	
	var data = {
			page: "reembolso",
			action: "avaliarReembolso",
			reembolso:lancamentoId,
			status: valorAvaliacao,
			justificativa:justificativa

	}
	
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");  
}

function buscarArquivosReembolso(codigoReembolso){

	if (listaLancamentosReembolso.length > 0){
		
		var url = retornaServer();
		
		var data = {
				page: "arquivoReembolso",
				action: "buscarArquivosReembolso",
				reembolso:codigoReembolso	
		}
		
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");

	}

}

function editarLancamento(lancamentoId){
	
	var achei=false;
	var i=0;
	
	while ( achei === false  ){
		
		if (listaLancamentosReembolso[i].id==lancamentoId)
			achei = true;	  
		
		i = i + 1;
		
	}
	
	sessionStorage.setItem("lancamentoReembolso",listaLancamentosReembolso[i - 1].id);
	sessionStorage.setItem("colaborador",listaLancamentosReembolso[i - 1].colaborador);
	sessionStorage.setItem("seraReembolsavel", "true");
	
	observer.disconnect();
	window.document.location.href = '../reembolso/reembolso.html';

}

function callback(data){
	
	if(data.action=="buscarArquivosReembolso"){
		numeroArquivos = jQuery.parseJSON(data.numeroArquivos);
		listaArquivos=jQuery.parseJSON(data.listaArquivos);
	}
	
	if(data.action=="buscarReembolsosStatus"){
		listaLancamentosReembolso = jQuery.parseJSON(data.retorno);
		preencheTabelaLancamentosReembolso();
	}

	if(data.action=="avaliarReembolso"){
		location.reload();
	}

}

function setSelectedValue(selectObj, valueToSet) {

	for (var i = 0; i < selectObj.options.length; i++) {
	
		if (selectObj.options[i].value==valueToSet)
			selectObj.options[i].selected = true;
		
	}
	
	return i;
	
}

function mudaColaborador() {
	
	tabelaLancamentos.innerHTML = '';
	
	var idCol=document.getElementById("lancamentoreembolso_colaborador").value;
	var idPer=document.getElementById("lancamentoreembolso_periodo").value;
	
	colaborador=idCol;
	periodo=idPer; 
	
	var e = document.getElementById("reembolso_cliente");
	
	var cont=0;
	var achei=-1;
	
	while ( (cont < listaColaboradores.length ) || (achei == -1) ){

		if( listaColaboradores[cont].id == idCol )
			achei = cont;
		
		cont++;
	}
	
	colaboradorPos = achei;

	buscarReembolsosColaborador(listaColaboradores[colaboradorPos].id);

}

function carregaReembolso(){
	
	sessionStorage.setItem("colaborador",colaborador);
	sessionStorage.setItem("lancamentoReembolso",0);
	
	observer.disconnect();
	window.document.location.href = '../reembolso/reembolso.html';

}

function preencheTabelaLancamentosReembolso(){
	
	var html = '';
	var j = 0;
	var tabelaLancamentos = document.getElementById("validacaoreembolso_lancamentos");
	
	if(listaLancamentosReembolso.length > 0){
		
		for(i = 0; i < listaLancamentosReembolso.length; i++){
		
			if (isMobile() ==false){
			
				if (listaLancamentosReembolso[i].statusreembolso=="Pendente")
				{

					html+='<div class="item" id="div'+listaLancamentosReembolso[i].id+'">'
					+'<div><b>'
					+listaLancamentosReembolso[i].nomeColaborador+"<br>"
					+listaLancamentosReembolso[i].data+ "-" +listaLancamentosReembolso[i].tipo+ "<font color=#515caf> <br>Valor Solicitado: "+currency(listaLancamentosReembolso[i].valorsolicitado)+" - Valor Previsto: "+currency(listaLancamentosReembolso[i].valorreembolso)+'</font></b>'
					+"<br>"+listaLancamentosReembolso[i].codigoProjeto+"-"+listaLancamentosReembolso[i].projeto +"<br>"+'</div>';				

					html+='<button class="btnedit" onclick="editarLancamento(\''+listaLancamentosReembolso[i].id+'\')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>';

					html+='<button class="btndelete" onclick="reprovarReembolso(\''+listaLancamentosReembolso[i].id+'\')"><i class="fa fa-times-circle" aria-hidden="true" style="font-size:24px"></i></button>'
					+'<button class="btnaprova" onclick="aprovarReembolso(\''+listaLancamentosReembolso[i].id+'\')"><i class="fa fa-check" aria-hidden="true" style="font-size:24px"></i></button>'
					+'</div></br>';
				}
				else
				{
					
					html+='<div class="item" id="div'+listaLancamentosReembolso[i].id+'">'
					
					+'<div><b>'
						+listaLancamentosReembolso[i].nomeColaborador+"<br>"
						+listaLancamentosReembolso[i].data+ "-" +listaLancamentosReembolso[i].tipo+ "<font color=#515caf> <br>Valor Solicitado: "+currency(listaLancamentosReembolso[i].valorsolicitado)+" - Valor Previsto: "+currency(listaLancamentosReembolso[i].valorreembolso)+'</font></b>'
						+"<br>"+listaLancamentosReembolso[i].codigoProjeto+"-"+listaLancamentosReembolso[i].projeto +"<br>"+
					'</div>';
					
						html+='<button class="btnedit" onclick="editarLancamento(\''+listaLancamentosReembolso[i].id+'\')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<button class="btndelete" onclick="alertaAvaliado()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button></div></br>';

				}
			}
			else
			{
				
				if (listaLancamentosReembolso[i].statusreembolso=="Pendente")
				{	
					html+='<div class="item" id="div'+listaLancamentosReembolso[i].id+'">'
					+'<button class="btnedit" onclick="editarLancamento(\''+listaLancamentosReembolso[i].id+'\')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>'
					+'<div><b>'
					+listaLancamentosReembolso[i].nomeColaborador+"<br>"
					+listaLancamentosReembolso[i].data+ "-" +listaLancamentosReembolso[i].tipo+ "<font color=#515caf> <br>Valor Solicitado: "+currency(listaLancamentosReembolso[i].valorsolicitado)+" - Valor Previsto: "+currency(listaLancamentosReembolso[i].valorreembolso)+'</font></b>'
					+"<br>"+listaLancamentosReembolso[i].codigoProjeto+"-"+listaLancamentosReembolso[i].projeto +"<br>"+'</div>';
					
					html+='<button class="btdelete" onclick="reprovarReembolso(\''+listaLancamentosReembolso[i].id+'\')"><i class="fa fa-times-circle" aria-hidden="true" style="font-size:24px"></i></button>'
					+'<button class="btnaprova" onclick="aprovarReembolso(\''+listaLancamentosReembolso[i].id+'\')"><i class="fa fa-check" aria-hidden="true" style="font-size:24px"></i></button>'
					+'</div><br>';
				}
				else
				{
					
					html+='<div class="item" id="div'+listaLancamentosReembolso[i].id+'">'
					+'<button class="btnedit" onclick="editarLancamento(\''+listaLancamentosReembolso[i].id+'\')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>'
					+'<div><b>'
					+listaLancamentosReembolso[i].nomeColaborador+"<br>"
					+listaLancamentosReembolso[i].data+ "-" +listaLancamentosReembolso[i].tipo+ "<font color=#515caf> <br>Valor Solicitado: "+currency(listaLancamentosReembolso[i].valorsolicitado)+" - Valor Previsto: "+currency(listaLancamentosReembolso[i].valorreembolso)+'</font></b>'
					+"<br>"+listaLancamentosReembolso[i].codigoProjeto+"-"+listaLancamentosReembolso[i].projeto +"<br>"+'</div>';
					
					html+='<button class="btdelete" onclick="alertaAvaliado()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>';
					+'</div><br>';

				}
			}     	   
		}

		tabelaLancamentos.innerHTML = html;
	}else{
		tabelaLancamentos.innerHTML = html;
	}
}

function comboListaReembolsoArquivos(listaArquivos){
	
}

function currency(numero){
	var formatado  = formatMoney(numero, 2, "R$ ", ".", ",");
    return formatado;
}

function formatMoney(number, places, symbol, thousand, decimal) {
	places = !isNaN(places = Math.abs(places)) ? places : 2;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}

function alertaAvaliado(){
	alert ("Este reembolso já foi avaliado!");	
}


function sair()
{
	localStorage.clear();
	sessionStorage.setItem("colaborador",0);
	sessionStorage.setItem("lancamento",0);
	sessionStorage.setItem("periodo",0);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../../login/login.html';

}


target = document.querySelectorAll('.item-list')[0];



//cria uma nova instância de observador
observer = new MutationObserver(function(mutations) {
	mutations.forEach(function(mutation) {
		console.log("tipo de mutação" +mutation.type);

		$(function() {$('.example-1').listSwipe();});

	});    
});

//configuração do observador:
config = { attributes: true, childList: true, characterData: true };

//passar o nó alvo pai, bem como as opções de observação
observer.observe(target, config);