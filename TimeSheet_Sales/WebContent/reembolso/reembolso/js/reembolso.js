//Variáveis da sessão
var listaColaboradores = [];
var listaClientes = [];
var listaLancamento = [];
var listaParametros = [];
var listaProjetos = [];
var listaEtapas = [];
var listaArquivos = [];
var listaTipoReembolso = [];
var listaConsultores = [];
var HorasTotal=0;
var txtHoraInicial=0;
var valorQuilometragemPadrao=0;

var colaborador = 0;
var colaboradorLogado = 0;
var cliente = 0;
var projeto = 0;
var etapa = 0;
var inicio=0;
var termino=0;
var tipoHora = 0;
var nome="";
var lancamentoReembolso=0;
var edicao=0;
var horasTrabalhadas=0;
var data="";
var diaSemana;
var senha;
var edicao=0;
var id=0;
var periodo=0;
var bloqueio=0;
var tipoReembolsoid=0;
var valorPedido=0;
var configuracao='0000000';
var distanciaCasa;
var numArquivo=0;
var codigo=0;
var codigoReembolso;
var quantidadedias=1;
var distanciaCasaTrabalho = 0;
var dadosReembolso;


function admin_init(){

	nome = sessionStorage.getItem("nomeColaborador");

	distanciaCasaTrabalho = sessionStorage.getItem("distanciaCasaTrabalho");
	
	var seraReembolsavel = sessionStorage.getItem("seraReembolsavel");
	
	if( seraReembolsavel === "true"){
		document.getElementById("container_seraReembolsado").style.display = "show";
		document.getElementById("container_numeroReembolso").classList.remove("col-lg-12");
		document.getElementById("container_numeroReembolso").classList.add("col-lg-6");
	}else{
		document.getElementById("container_seraReembolsado").style.display = "none";
	}

	desabilitarComboColaborador();

	colaborador=sessionStorage.getItem("colaborador");
	colaboradorLogado=sessionStorage.getItem("colaboradorLogado");
	codigoReembolso=document.getElementById("reembolso_cliente");
	
	if ((colaborador==0) || (colaborador==null) || (colaborador==undefined)) 
	{
		window.document.location.href = '../../login/login.html';
	}

	lancamentoReembolso=sessionStorage.getItem("lancamentoReembolso");
	senha=sessionStorage.getItem("senha");
	edicao=sessionStorage.getItem("edicao");
	
	buscarTipoReembolso();
	
	document.getElementById("reembolso_cliente").addEventListener("change", mudaCliente);
	document.getElementById("reembolso_projeto").addEventListener("change", mudaProjeto);
//	document.getElementById("frame").addEventListener("change", mostraResultadoUpload);
	document.getElementById("reembolso_quilometragem").addEventListener("change", calculaValorSolicitadoKm);
//	document.getElementById("reembolso_quantidadeDias").addEventListener("change", calculaValorSolicitadoDias);
	document.getElementById("reembolso_valorsolicitado").addEventListener("change", calculaValorReembolso);
	document.getElementById("reembolso_tipoReembolso").addEventListener("change", mudaTipoReembolso);
	document.getElementById("reembolso_data").addEventListener("change", buscaData);
	document.getElementById("menuop1").addEventListener("click", carregaLancamentos);
	document.getElementById("menuop2").addEventListener("click", menu);
	document.getElementById("menuop3").addEventListener("click", sair);
	
	if( distanciaCasaTrabalho == null )
		document.getElementById('reembolso_distanciacasa').value=0;
	else
		document.getElementById('reembolso_distanciacasa').value= distanciaCasaTrabalho;

	//document.getElementById("reembolso_arquivo").addEventListener("change", carregaArquivos);

//	colaboradorId=session.getItem("colaboradorId");

	document.getElementById("btn_salvar").addEventListener("click",btn_salvar_click);

	if (lancamentoReembolso != 0)
	{
		
		//buscarPeriodos();

	}


	document.getElementById("reembolso_observacao").addEventListener("keypress",validaTextArea); 

}


function mostrarArquivosReembolso(numeroArquivoReembolso){
	
	var i=0
	var tabela=document.getElementById("reembolso_tabelaArquivo"); 
	tabela.innerHTML="";
	
	if (tabela != null)
	{
		for (i=0; i < numeroArquivoReembolso;i++)
		{
	
			var row = tabela.insertRow(numArquivo);
			var cell = row.insertCell(0);
			var p = document.createElement('p');
			var a = document.createElement('a');
			
			var linkText = document.createTextNode("reembolso_" + listaArquivos[i].numeroReembolso+"_"+listaArquivos[i].numeroArquivos);
			
			a.appendChild(linkText);
			//	var caminho = "http://www.bmiavase.com.br:8080/BMI_AVASE_SERVER_V2/resources/reports/"+data.listaArquivos[k-1];
			a.href = retornaCaminhoArquivo()+"reembolso_"+listaArquivos[i].numeroReembolso+"_"+listaArquivos[i].numeroArquivos+"."+listaArquivos[i].extensao;
			a.target="parent";
			//alert(caminhoNovo);    
			p.appendChild(a);
			cell.appendChild(p);
			var button = document.createElement("button");
			
			if( listaLancamento[0].statusreembolso === "Pendente" ){
				button.innerHTML = '<button class="btntrash" onclick="removerArquivoReembolso('+listaArquivos[i].id+')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>';
			}else{
				button.innerHTML = '<button class="btntrash" style="opacity: 0.5;" disabled><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>'
			}
			var cell = row.insertCell(1);
			cell.appendChild(button);
		}
	}
}

function buscarResultadoUpload()
{	
	
	codigo = id;
	
	if ((codigo!=0) || (lancamentoReembolso!=0))
	{
		var url = retornaServer();
		var data = {
				page: "arquivoReembolso",
				action: "buscarArquivosReembolso",
				reembolso:codigo 	
		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");


	}

}


function removerArquivoReembolso(codigoArquivo)
{


	if ((codigo!=0) || (lancamentoReembolso!=0))
	{
		var url = retornaServer();
		var data = {
				page: "arquivoReembolso",
				action: "removerArquivoReembolso",
				arquivo:codigoArquivo	
		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");


	}

}


function carregaArquivos(){
	var x = document.getElementById("reembolso_arquivo");
	var txt = "";
	if ('files' in x) {
		if (x.files.length == 0) {
			txt = "Select one or more files.";
		} else {
			for (var i = 0; i < x.files.length; i++) {
				txt += "<br><strong>" + (i+1) + ". file</strong><br>";
				var file = x.files[i];
				if ('name' in file) {
					txt += "name: " + file.name + "<br>";
				}
				if ('size' in file) {
					txt += "size: " + file.size + " bytes <br>";
				}
			}
		}
	} 
	else {
		if (x.value == "") {
			txt += "Select one or more files.";
		} else {
			txt += "The files property is not supported by your browser!";
			txt  += "<br>The path of the selected file: " + x.value; // If the browser does not support the files property, it will return the path of the selected file instead. 
		}
	}
	document.getElementById("reembolso_listaaquivos").innerHTML = txt;
}



function calculaValorSolicitadoDias(valorPedido)
{

	var valorCalc;

	if (( configuracao.substring(3,4)==0) && (configuracao.substring(1,2)==1))
	{ 	
		valorCalc=parseInt(listaParametros[1].valor) * parseInt(document.getElementById("reembolso_quantidadeDias").value);
	}
	if (( configuracao.substring(3,4)==1) && (configuracao.substring(1,2)==1))
	{ 	
		valorCalc=parseInt(listaParametros[2].valor) * parseInt(document.getElementById("reembolso_quantidadeDias").value);
	} 

	if ((( configuracao.substring(3,4)==0) && (configuracao.substring(1,2)==0))||
			(( configuracao.substring(3,4)==1) && (configuracao.substring(1,2)==0)))
	{ 	
		valorCalc=valorPedido;
	}


	if (valorPedido <valorCalc)
	{
		valorCalc=valorPedido;
	}
	return valorCalc;
}

function calculaValorSolicitadoKm()
{
	var quilometragemTela = document.getElementById("reembolso_quilometragem").value;
		
	var quilometragemTelaInt = parseInt(quilometragemTela, 10);
	var distanciaCasaTrabalhoInt = parseInt(distanciaCasaTrabalho, 10);
	
	if(quilometragemTelaInt < distanciaCasaTrabalhoInt){
		document.getElementById("reembolso_valorsolicitado").disabled = true;
		alert("A quilometragem está fora do limite reembolsavel!");
	}else{
	
		var valorSolicitado = valorQuilometragemPadrao * (quilometragemTela - distanciaCasaTrabalho);
		
		if(valorSolicitado >= 0){
			document.getElementById("reembolso_valorsolicitado").value = valorSolicitado;
			document.getElementById("reembolso_valorsolicitado").disabled = true;
			document.getElementById("reembolso_valorreembolso").value = valorSolicitado;
		}else{
			document.getElementById("reembolso_valorsolicitado").value = 0;
			document.getElementById("reembolso_valorreembolso").value = 0;			
		}
		
	}
	
}


function calculaValorReembolso()
{   var valorCalculado=0;

	quantidadedias=parseInt(document.getElementById("reembolso_quantidadeDias").value);
	valorPedido=parseInt(document.getElementById("reembolso_valorsolicitado").value);
	
	valorCalculado=valorPedido;
	if (configuracao.substring(1,2)==1)
	{
		var desconto=(parseFloat(valorQuilometragemPadrao)* parseInt(distanciaCasaTrabalho));
		valorCalculado=valorPedido - desconto ;
	
	}
	
	if (configuracao.substring(2,3)==1)
	{
		valorCalculado=calculaValorSolicitadoDias(valorPedido);
	}
	
	document.getElementById("reembolso_valorreembolso").value=valorCalculado;
	
	if (valorCalculado <= 0)
	{
	
		alert ("Não é possível fazer reembolso pois os descontos padrões são maiores que o valor descontado");
		document.getElementById("reembolso_quilometragem").value=0;
		document.getElementById("reembolso_quantidadeDias").value=0;
		document.getElementById("reembolso_valorsolicitado").value=0;
		document.getElementById("reembolso_valorreembolso").value=0;
	}
}


function buscaData()
{

	var dataSel=document.getElementById("reembolso_data").value;
	diaSemanaData(dataSel);
//	encontraPeriodo(dataSel);
}



function validaTextArea()
{

	if ((event.keyCode == 13) || (event.keyCode == 47) |(event.keyCode == 92))  {
		event.preventDefault();
	}
}


//TRATAMENTO DOS EVENTOS

//verifica se foi escolhido valor na combo diferente do default
function verificaValorSelecionadoCombo(str) {
	var selectObj  = document.getElementById(str);
	var valueToSet  = document.getElementById(str).value;
	if (selectObj.options[0].value==valueToSet) 
		return false;
	else
		return true;

}

//verifica se o campo esta vazio
function verificaPreeenchimento(str) {
	var v = document.getElementById(str).value;
	if ((v == null) || (v == "") || (v == "0"))
		return false
		else	
			return true;
}

function validaReembolso() {
	if ((verificaValorSelecionadoCombo("reembolso_cliente")
			&& verificaValorSelecionadoCombo("reembolso_projeto")
			&& verificaValorSelecionadoCombo("reembolso_etapa")
			&& verificaValorSelecionadoCombo("reembolso_tipoReembolso")
			&& verificaPreeenchimento("reembolso_data")
			&& verificaPreeenchimento("reembolso_diaSemana")
			&& verificaPreeenchimento("reembolso_local")
			&& verificaPreeenchimento("reembolso_observacao")
	)==true)
	{
		return  true;
	}
	else
	{
		alert('Existem campos que não foram preenchidos, Por favor preencha');
		return false;
	}

}


function carregaLancamentos()
{
	sessionStorage.setItem("colaborador",colaboradorLogado);
	sessionStorage.setItem("lancamentoReembolso",0);
	sessionStorage.setItem("periodo",0);
	window.document.location.href = '../../reembolso/lancamentosReembolso/lancamentosReembolso.html';

}

function sair()
{
	sessionStorage.setItem("colaborador",0);
	sessionStorage.setItem("colaboradorLogado",0);
	sessionStorage.setItem("lancamentoReembolso",0);
	sessionStorage.setItem("periodo",0);
	window.document.location.href = '../../login/login.html';

}

function menu()
{
	sessionStorage.setItem("colaborador",colaborador);
	sessionStorage.setItem("colaboradorLogado", colaboradorLogado);
	sessionStorage.setItem("lancamentoReembolso",0);
	sessionStorage.setItem("periodo",0);
	window.document.location.href = '../../menu/menu.html';

}

function btn_salvar_click(){
	if (validaReembolso() == true){
//		var op =confirm("Foram anexados todos os comprovantes para permitir a conferência do reembolso?");
//		if (op=1)
		//{
		salvarReembolso();
		//}
		
	}

}


//PREENCHIMENTO DAS COMBOS

function desabilitarComboColaborador(){
	document.getElementById('reembolso_colaborador').value=nome;
	document.getElementById('reembolso_colaborador').innerHTML="";
	document.getElementById('reembolso_colaborador').disabled = true;
}


function preencheListaCliente(){
	var html = '<option value="0" Disabled Selected>Cliente...</option>';
	var select = "";
	for (i=0;i<listaOrganizacao.length;i++){
		if(cliente==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaOrganizacao[i].idOrg+'" '+select+'>' +listaOrganizacao[i].nomeOrganizacao+'</option>';
	}

	document.getElementById("reembolso_cliente").innerHTML = html;
	setSelectedValue(document.getElementById("reembolso_cliente"),0);
}

function preencheListaProjeto(){

	var html = '<option value="0" Disabled Selected>Projeto...</option>';
	var select = "";
	for (i=0;i<listaProjetos.length;i++){
		if(projeto==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaProjetos[i].idProj+'" '+select+'>'+listaProjetos[i].nomeProjeto+'</option>';
	}

	document.getElementById("reembolso_projeto").innerHTML = html;
	setSelectedValue(document.getElementById("reembolso_projeto"),0);
}


function preencheListaEtapas(){
	var html = '<option value="0" Disabled Selected>Etapa...</option>';
	var select = "";
	for (i=0;i<listaEtapa.length;i++){
		if(etapa==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaEtapa[i].idEtapa+'" '+select+'>' +listaEtapa[i].nomeEtapa+'</option>';
	}

	document.getElementById("reembolso_etapa").innerHTML = html;
	setSelectedValue(document.getElementById("reembolso_etapa"),0);
}

function listaOrganizacoesFilter(listaConsultor){

	listaOrganizacao = listaConsultor.filter( (valor, index, array ) => array.findIndex( find => find["idOrg"] === valor["idOrg"] ) === index );
}


function listaProjetosFilter(listaConsultor, idOrganizacao){

	listaProjetos = listaConsultor.filter( (valor, index, array ) => array.findIndex( find => find["idProj"] === valor["idProj"] ) === index )
	.filter( (valor, index, array ) => array[index].idOrg === idOrganizacao);
}


function listaEtapaFilter(listaConsultor, idProjeto){

	listaEtapa = listaConsultor.filter( (valor, index, array ) => array.findIndex( find => find["idEtapa"] === valor["idEtapa"] ) === index )
	.filter( (valor, index, array ) => array[index].idProj === idProjeto);
}

function preencheListaTipoReembolso(){
	var html = '<option value="0">Tipo de Reembolso...</option>';
	var select = "";
	for (i=0;i<listaTipoReembolso.length;i++){
		if(tipoHora==i){
			select = "selected";
		}else{
			sellected = "";
		}
		html += '<option value="'+listaTipoReembolso[i].id+'" '+select+'>' +listaTipoReembolso[i].nome+'</option>';
	}

	document.getElementById("reembolso_tipoReembolso").innerHTML = html;
	setSelectedValue(document.getElementById("reembolso_tipoReembolso"),0);
}

function preencherListaClientesEdicao(){
	
	id = lancamentoReembolso;
	
	var i=0;
	var achei=0;
	
	document.getElementById("divNumeroReembolso").style.visibility = "visible";
	document.getElementById("divNumeroReembolso").style.display = "block";
	document.getElementById("divUpload").style.visibility = "visible";
	document.getElementById("divUpload").style.display = "block";

	document.getElementById('reembolso_numero').value=listaLancamento[0].name;
	document.getElementById('reembolso_numero').innerHTML="";
	
	var idOrganizacao = listaLancamento[0].cliente.substring(0, listaLancamento[0].cliente.length - 3);
	
	while (achei==0){
		
		if (listaOrganizacao[i].idOrg == idOrganizacao){	
			achei=1;
			setSelectedValue(document.getElementById("reembolso_cliente"),listaOrganizacao[i].idOrg);
		}
		i=i+1;
	}
	
	listaProjetosFilter(listaConsultores, idOrganizacao)
	preencheListaProjeto();
	preencherListaProjetosEdicao();
	
}

function preencherListaProjetosEdicao(){
	
	var i=0;
	var achei=0;
	
	var idProjeto = listaLancamento[0].projeto;
	
	while (achei==0){
		
		if (listaProjetos[i].idProj == listaLancamento[0].projeto){	
			
			achei=1;
			setSelectedValue(document.getElementById("reembolso_projeto"),listaProjetos[i].idProj);
		}
		
		i=i+1;
	}
	
	listaEtapaFilter(listaConsultores, idProjeto);
	preencheListaEtapas();
	preencherListaEtapasEdicao();
	
	
}

function preencherListaEtapasEdicao(){
	
	var i=0;
	var achei=0;
	
	while (achei==0){
		
		if (listaEtapa[i].idEtapa == listaLancamento[0].etapa){	
			
			achei=1;
			setSelectedValue(document.getElementById("reembolso_etapa"),listaEtapa[i].idEtapa);
			
		}
		i=i+1;
	}
	
	preencherTipoReembolso();
	preencherResto();
}

function preencherResto(){
	
	document.getElementById('reembolso_colaborador').value=listaLancamento[0].nomeColaborador;
	document.getElementById('reembolso_colaborador').innerHTML="";
	document.getElementById('reembolso_numReembolsoForm').value=listaLancamento[0].name;
	document.getElementById('reembolso_numReembolsoForm').innerHTML="";
	document.getElementById('reembolso_idReembolsoForm').value=listaLancamento[0].id;
	document.getElementById('reembolso_idReembolsoForm').innerHTML="";
	document.getElementById('reembolso_local').value=listaLancamento[0].local;
	document.getElementById('reembolso_local').innerHTML="";
	document.getElementById('reembolso_valorsolicitado').value=listaLancamento[0].valorsolicitado;
	document.getElementById('reembolso_valorsolicitado').innerHTML="";
	document.getElementById('reembolso_valorreembolso').value=listaLancamento[0].valorreembolso;
	document.getElementById('reembolso_valorreembolso').innerHTML="";
	document.getElementById('reembolso_data').value=listaLancamento[0].data;
	document.getElementById('reembolso_data').innerHTML="";
	document.getElementById('reembolso_diaSemana').value=listaLancamento[0].diasemana;
	document.getElementById('reembolso_diaSemana').innerHTML="";
	document.getElementById('reembolso_observacao').value=listaLancamento[0].observacao;
	document.getElementById('reembolso_observacao').innerHTML="";

	if( distanciaCasaTrabalho == null )
		document.getElementById('reembolso_distanciacasa').value=0;
	else
		document.getElementById('reembolso_distanciacasa').value= distanciaCasaTrabalho;
	
	document.getElementById('reembolso_distanciacasa').innerHTML="";
	
	document.getElementById('reembolso_quilometragem').value=listaLancamento[0].quilometragem;
	document.getElementById('reembolso_quilometragem').innerHTML="";
	
	if( listaLancamento[0].origem == undefined )
		document.getElementById('reembolso_origem').value="";
	else
		document.getElementById('reembolso_origem').value=listaLancamento[0].origem;
	
	document.getElementById('reembolso_origem').innerHTML="";
	
	if( listaLancamento[0].destino == undefined )
		document.getElementById('reembolso_destino').value="";
	else
		document.getElementById('reembolso_destino').value=listaLancamento[0].destino;
	document.getElementById('reembolso_destino').innerHTML="";
	document.getElementById('reembolso_quantidadeDias').value=listaLancamento[0].quantidadeDias;
	document.getElementById('reembolso_quantidadeDias').innerHTML="";
	mostraCamposReembolso();
	
}

function preencherTipoReembolso(){
	
	var i=0;
	var achei=0;
	
	while (achei==0){
		
		if (listaTipoReembolso[i].id == listaLancamento[0].tipo){	
			achei=1;
			setSelectedValue(document.getElementById("reembolso_tipoReembolso"),listaLancamento[0].tipo);
		}
		i=i+1;
	} 
	configuracao=listaTipoReembolso[i-1].configuracao;
	
}

function callback(data){
	
	if(data.action=="buscarConsultor"){
		listaConsultores = jQuery.parseJSON(data.retorno);
		listaOrganizacoesFilter(listaConsultores);
		preencheListaCliente();
		
		if( lancamentoReembolso != 0){
			buscarLancamentoReembolso();
		}
		
	}

	if(data.action=="buscarArquivosReembolso"){
		numeroArquivos = jQuery.parseJSON(data.numeroArquivos);
		listaArquivos=jQuery.parseJSON(data.listaArquivos);
		mostrarArquivosReembolso(numeroArquivos);
	}

	if(data.action=="removerArquivoReembolso"){
		location.reload();
	}

	if(data.action=="buscarParametros"){
		listaParametros = jQuery.parseJSON(data.retorno);
		valorQuilometragemPadrao=listaParametros[0].valor;
		buscarConsultor();
	}

	if(data.action=="buscarTipoReembolso"){
		listaTipoReembolso = jQuery.parseJSON(data.retorno);
		preencheListaTipoReembolso();
		buscarParametros();
	}

	if(data.action=="buscarLancamentoReembolso"){
		listaLancamento = jQuery.parseJSON(data.retorno);
		preencherListaClientesEdicao();
		buscarResultadoUpload();
		
	}

	if(data.action=="buscarColaborador"){
		listaColaborador = jQuery.parseJSON(data.retorno);
		document.getElementById('reembolso_colaborador').value=listaColaborador[0].nome;
		document.getElementById('reembolso_colaborador').innerHTML="";

		document.getElementById('reembolso_distanciacasa').value=listaColaborador[0].distanciaCasaTrabalho;
		document.getElementById('reembolso_distanciacasa').innerHTML="";

		var data =new Date();
		document.getElementById("reembolso_data").valueAsDate=data; 

		document.getElementById('reembolso_data').innerHTML="";
		distanciaCasa = listaColaborador[0].distanciaCasaTrabalho;

		var diaSelecionado=document.getElementById("reembolso_data").value;
		diaSemanaData(diaSelecionado);
	}

	if(data.action=="salvarReembolso"){
		retornoSalvar = jQuery.parseJSON(data.retorno);
		codigo = data.codigo;
		if (retornoSalvar == 0)
		{
			
			if( id == 0 ){
				alert('Reembolso salvo com sucesso');
				document.getElementById("reembolso_numero").value=codigo;
				document.getElementById("divNumeroReembolso").style.visibility = "visible";
				document.getElementById("divNumeroReembolso").style.display = "block";
				document.getElementById("divUpload").style.visibility = "visible";
				document.getElementById("divUpload").style.display = "block";
				
				alert ("Pedido de reembolso criado! Por favor anexe os comprovantes");
				
			}else{
				alert('Reembolso atualizado com sucesso');
				document.getElementById("reembolso_numero").value=codigo;
				document.getElementById("divNumeroReembolso").style.visibility = "visible";
				document.getElementById("divNumeroReembolso").style.display = "block";
				document.getElementById("divUpload").style.visibility = "visible";
				document.getElementById("divUpload").style.display = "block";
				
				alert ("Pedido de reembolso atualizado! Por favor anexe os comprovantes");
			}
			
		}
		else
		{

			alert(data.mensagem);
		}
		
		buscarIdNumeroReembolso();

	}
	
	if(data.action=="buscarResultadoUpload"){
		mostrarArquivosReembolso(numeroArquivos);
	}
	
	if(data.action=="buscarIdNumeroReembolso"){
		var codigo = data.codigo;
		document.getElementById('reembolso_numero').value=codigo;
		document.getElementById('reembolso_numero').innerHTML="";
		id = data.idReembolso;
		
		document.getElementById('reembolso_numReembolsoForm').value=data.codigo;
		document.getElementById('reembolso_numReembolsoForm').innerHTML="";
		document.getElementById('reembolso_idReembolsoForm').value=data.idReembolso;
		document.getElementById('reembolso_idReembolsoForm').innerHTML="";
		
		if(lancamentoReembolso == 0)
			lancamentoReembolso = data.idReembolso
		
		buscarLancamentoReembolso();
	}
}


function buscarIdNumeroReembolso(){
	
	var url = retornaServer();
	
	dadosReembolso.action = "buscarIdNumeroReembolso";
	
	url=url+encodeURI(JSON.stringify(dadosReembolso));
	callWebservice(url,"callback");
}

function buscarConsultor(){
	
	var url = retornaServer();
	var data = {
			
			page: "consultor",
			action: "buscarConsultor",
			colaborador: colaborador
			
	}
	
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
	
}

//function buscarClientes(){
//
//	var url = retornaServer();
//	var data = {
//			page: "timesheet",
//			action: "buscarClientes"
//	}
//	url=url+encodeURI(JSON.stringify(data));
//	callWebservice(url,"callback");
//
//
//}


function salvarReembolso(){

	//document.getElementById("reembolso_horasTrabalhadas").value=semana[dia];

	var data1 = new Date(reembolso_data.value+"T10:30:00-03:00");

//	data1.format("DD/MM/YYYY hh:mm");
	var data2 = new Date();

//	data2.format("dd/MM/yyyy HH:mm:ss");
	var diferenca = diferencaDatas(data1, data2);

	var diaSemanaTrabalho=diaSemanaData(formatarData(data1));
	var diaSemanalancamento =diaSemanaData(formatarData(data2));
	
	if (diferenca  <= 0){
		alert("Não pode fazer lançamento de reembolso de eventos que ainda nao ocorreram!");
	}else{
		if( diferenca > 30 ){
			alert("O período para lançamento do reembolso não pode ser superior a 30 dias do evento a ser reembolsado!")
		}else		
		{   

			var status=0;
			var tipoProjeto=0;
			var e =document.getElementById("reembolso_cliente");
			var clienteid = e.options[e.selectedIndex].value;
			e=document.getElementById("reembolso_projeto");
			var projetoid = e.options[e.selectedIndex].value;
			i=0;
			achei=0;
			while (achei==0)
			{
				if (listaProjetos[i].idProj==projetoid)
				{	
					tipoProjeto=listaProjetos[i].tipoProjeto;		
					achei=1;
				}
				i=i+1;
			}
			e=document.getElementById("reembolso_tipoReembolso");
			var tiporeembolsoid = e.options[e.selectedIndex].value;
			e=document.getElementById("reembolso_etapa");
			var etapaid = e.options[e.selectedIndex].value;
			var data = document.getElementById("reembolso_data").value;
			var local = document.getElementById("reembolso_local").value;
			var quilometragem = document.getElementById("reembolso_quilometragem").value;
			var origem = document.getElementById("reembolso_origem").value;
			var destino = document.getElementById("reembolso_destino").value;
			var quantidadeDias = document.getElementById("reembolso_quantidadeDias").value;
			var valorsolicitado = document.getElementById("reembolso_valorsolicitado").value;
			var valorreembolso = document.getElementById("reembolso_valorreembolso").value;
			var diaSemana = document.getElementById("reembolso_diaSemana").value;
			var observacao = document.getElementById("reembolso_observacao").value;
			var serareembolsavel = document.getElementById("reembolso_seraReembolsavel").value;
		
			if (quilometragem=="")
			{
				quilometragem=0;
				origem="";
				destino="";

			}
			var url = retornaServer();

			var data = {
					page: "reembolso",
					action: "salvarReembolso",
					id: id,
					data:data,
					colaborador:colaborador,
					diasemana:diaSemana,
					cliente:clienteid,
					projeto: projetoid,
					tiporeembolso: tiporeembolsoid,
					etapa: etapaid,
					local: local,
					quilometragem: quilometragem,
					origem: origem,
					destino: destino,
					quantidadeDias: quantidadeDias,
					valorsolicitado: valorsolicitado,
					valorreembolso: valorreembolso,
					observacao: observacao,
					status: status,
					serareembolsavel: serareembolsavel,

			}
			dadosReembolso = data;
			
			url=url+encodeURI(JSON.stringify(data));
			callWebservice(url,"callback");

		}
	}

	
}

function diferencaDatas(data1, data2){
	
	var timeDiff = data2.getTime() - data1.getTime();
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

	return diffDays;
	
}

//function buscarProjetosCliente(){
//
//	var url = retornaServer();
//	var data = {
//			page: "timesheet",
//			action: "buscarTodosProjetosCliente", 
//			cliente:cliente	
//	}
//	url=url+encodeURI(JSON.stringify(data));
//	callWebservice(url,"callback");
//
//}

//function buscarEtapasProjeto(){
//	var url = retornaServer();
//	var data = {
//			page: "timesheet",
//			action: "buscarEtapasProjeto",
//			projeto: projeto	
//	}
//	url=url+encodeURI(JSON.stringify(data));
//	callWebservice(url,"callback");
//}


function buscarColaborador(){
	var url = retornaServer();
	var data = {
			page: "colaborador",
			action: "buscarColaborador",
			colaborador: colaborador,
			senha: senha
	}

	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");

}


function buscarTipoReembolso(){
	var url = retornaServer();
	var data = {
			page: "tipoReembolso",
			action: "buscarTipoReembolso"
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}

function buscarParametros(){
	var url = retornaServer();
	var data = {
			page: "parametros",
			action: "buscarParametros"
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function buscarLancamentoReembolso(){
	var url = retornaServer();
	var data = {
			page: "reembolso",
			action: "buscarLancamentoReembolso",
			lancamentoReembolso: lancamentoReembolso,	
			periodo: periodo
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}


function mudaCliente() {

	var e = document.getElementById("reembolso_cliente");
	clienteid = e.options[e.selectedIndex].value;

	var achei = -1;

	if(clienteid != 0){
		listaOrganizacao.filter(function(valor, index){
			if(valor.idOrg === clienteid){
				achei = index;
			}
		});
	}

	clientePos=achei;
	cliente=clienteid;
	listaProjetosFilter(listaConsultores,document.getElementById("reembolso_cliente").value);
	preencheListaProjeto();
}


function mudaProjeto() {

	var e = document.getElementById("reembolso_projeto");
	projetoid = e.options[e.selectedIndex].value;

	var achei = -1;

	if(projetoid != 0){
		listaProjetos.filter(function(valor, index){
			if(valor.idProj === projetoid){
				achei = index;
			} 
		});
	}

	projetoPos=achei;
	projeto=projetoid;
	listaEtapaFilter(listaConsultores,document.getElementById("reembolso_projeto").value);
	preencheListaEtapas();
}


function mudaTipoReembolso() {

	var e = document.getElementById("reembolso_tipoReembolso");
	tipoReembolsoid = e.options[e.selectedIndex].value;
	var cont=0;
	var achei=-1;
	while ((cont <listaTipoReembolso.lenght ) || (achei ==-1) && (tipoReembolsoid !=0))
	{

		if (listaTipoReembolso[cont].id==tipoReembolsoid)
		{
			achei=cont;
		}
		cont++;
	}
	tipoReembolsoPos=achei;
	configuracao=listaTipoReembolso[tipoReembolsoPos].configuracao;
	mostraCamposReembolso();


//	window.location.reload(false);
}


function mostraCamposReembolso()
{
	if(lancamentoReembolso==0)
	{
		document.getElementById("reembolso_quantidadeDias").value="1";
		document.getElementById("reembolso_quilometragem").value="0";
		document.getElementById("reembolso_origem").value="";
		document.getElementById("reembolso_destino").value="";
		document.getElementById("reembolso_valorsolicitado").value="0";
		document.getElementById("reembolso_valorreembolso").value="0";

	}
	if (configuracao.substring(0,1) ==0)
	{
		document.getElementById("reembolso_divKM").style.visibility = "hidden";
		document.getElementById("reembolso_divKM").style.display = "none";
		document.getElementById("reembolso_valorsolicitado").style.disabled=false;
	}

	if (configuracao.substring(0,1) ==1)

	{
		document.getElementById("reembolso_divKM").style.visibility = "visible";
		document.getElementById("reembolso_divKM").style.display = "block";
		document.getElementById("reembolso_valorsolicitado").style.disabled=true;


	}

	if (configuracao.substring(2,3) ==0)
	{
		document.getElementById("divQteDias").style.visibility = "hidden";
		document.getElementById("divQteDias").style.display = " none";
		document.getElementById("divValorSolicitado").className='col-lg-6';
		document.getElementById("divValorReembolso").className='col-lg-6';

	}
	if (configuracao.substring(2,3) ==1)
	{
		document.getElementById("divQteDias").style.visibility = "visible";
		document.getElementById("divQteDias").style.display = "block";
		document.getElementById("divValorSolicitado").className='col-lg-4';
		document.getElementById("divValorReembolso").className='col-lg-4';
	}

	if (configuracao.substring(3,4) ==0)
	{

		document.getElementById("labelValorSolicitado").innerHTML=' Valor Solicitado(Real)';
		document.getElementById("labelValorReembolso").innerHTML=' Valor Previsto do Reembolso(Real)';
	}
	if (configuracao.substring(3,4) ==1)

	{
		document.getElementById("labelValorSolicitado").innerHTML=' Valor Solicitado(Dólar)';
		document.getElementById("labelValorReembolso").innerHTML=' Valor Previsto do Reembolso(Dólar)';
	}



}

function dateDiff(dt1, dt2)
{
	/*
	 * setup 'empty' return object
	 */
	var ret = {days:0, months:0, years:0};

	/*
	 * If the dates are equal, return the 'empty' object
	 */
	if (dt1 == dt2) return ret;

	/*
	 * ensure dt2 > dt1
	 */
	if (dt1 > dt2)
	{
		var dtmp = dt2;
		dt2 = dt1;
		dt1 = dtmp;
	}

	/*
	 * First get the number of full years
	 */

	var year1 = dt1.getFullYear();
	var year2 = dt2.getFullYear();

	var month1 = dt1.getMonth();
	var month2 = dt2.getMonth();

	var day1 = dt1.getDate();
	var day2 = dt2.getDate();

	/*
	 * Set initial values bearing in mind the months or days may be negative
	 */

	ret['years'] = year2 - year1;
	ret['months'] = month2 - month1;
	ret['days'] = day2 - day1;

	/*
	 * Now we deal with the negatives
	 */

	/*
	 * First if the day difference is negative
	 * eg dt2 = 13 oct, dt1 = 25 sept
	 */
	if (ret['days'] < 0)
	{
		/*
		 * Use temporary dates to get the number of days remaining in the month
		 */
		var dtmp1 = new Date(dt1.getFullYear(), dt1.getMonth() + 1, 1, 0, 0, -1);

		var numDays = dtmp1.getDate();

		ret['months'] -= 1;
		ret['days'] += numDays;

	}

	/*
	 * Now if the month difference is negative
	 */
	if (ret['months'] < 0)
	{
		ret['months'] += 12;
		ret['years'] -= 1;
	}

	return ret;
}


function diaSemanaData( dataEscolhida ){

	var dataEsc = dataEscolhida;
	var dt1   = parseInt(dataEsc.substring(8,10));
	var mon1  = parseInt(dataEsc.substring(5,7));
	var yr1   = parseInt(dataEsc.substring(0,4));
	var dataNova= new Date(yr1, mon1-1, dt1);
	var dia = dataNova.getDay();
	var semana = new Array(6);
	
	semana[0]='Domingo';
	semana[1]='Segunda-Feira';
	semana[2]='Terça-Feira';
	semana[3]='Quarta-Feira';
	semana[4]='Quinta-Feira';
	semana[5]='Sexta-Feira';
	semana[6]='Sábado';
	
	document.getElementById("reembolso_diaSemana").value = semana[dia];
	document.getElementById('reembolso_diaSemana').innerHTML = "";
	
	return dia;
	
}


function actionImageUpload(){
	const numeroReembolso = document.getElementById('reembolso_numReembolsoForm').value;
	const idReembolso = document.getElementById('reembolso_idReembolsoForm').value;
	
	if(document.getElementById("input-file-reembolso").files.length == 0){
		alert("Por favor insira a imagem do comprovante!");
	}else {
		alert("Imagem salva com sucesso!");
	}
}