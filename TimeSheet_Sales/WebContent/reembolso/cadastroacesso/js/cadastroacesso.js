
//Variáveis da sessão
var acesso = 0;
var nomeColaborador = "";
var email = "";
var dataNome = [];
var dataID = [];
var unidade = 0;

$(document).ready(function(){
	cadastroacesso_init();
});


function verificaValorSelecionadoCombo(str) {
	var selectObj  = document.getElementById(str);
	var valueToSet  = document.getElementById(str).value;
	if (selectObj.options[0].value==valueToSet) 
		return false;
	else
		return true;

}

//verifica se o campo esta vazio
function verificaPreeenchimento(str) {
	var v = document.getElementById(str).value;
	if ((v == null) || (v == ""))
		return false
		else	
			return true;
}

function validaCadastro() {
	if (verificaPreeenchimento("cadastroacesso_senha")==true)
	{
		return  true;
	}
	else
	{
		alert('Existem campos que não foram preenchidos, Por favor preencha');
		return false;
	}

}

function cadastroacesso_init() {
	//Atualiza variaveis globais com informações da sessão
    colaborador = sessionStorage.getItem("colaborador");
    nomeColaborador = sessionStorage.getItem("nomeColaborador");
    email = sessionStorage.getItem("email");
	document.getElementById("cadastroacesso_salvar").addEventListener("click", cadastroacesso_salvar_click);
	document.getElementById("cadastroacesso_nome").value = nomeColaborador;
	document.getElementById("cadastroacesso_email").value = email;
	
}

function cadastroacesso_salvar_click(){
	cadastroacesso_salvar();
}

function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}



function callback(data){
	if (data.action=="cadastroAcesso"){
		realizaAcesso(colaborador);
	}
}

function realizaAcesso(colaborador){
	localStorage.clear();
	sessionStorage.setItem("email",email);
	sessionStorage.setItem("nomeusuario",nomeColaborador);
	sessionStorage.setItem("colaborador",colaborador);
	sessionStorage.setItem("colaboradorLogado",colaborador);
	sessionStorage.setItem("lancamento",0);
	sessionStorage.setItem("edicao",0);	
	window.location.href="../reembolso/reembolso.html";
}

function cadastroacesso_salvar(){

	if (validaCadastro()==true){
		var senha = document.getElementById("cadastroacesso_senha").value;
		
		//salva unidade escolhida
		var url = retornaServer();
		var data = {
				page:   "cadastroAcesso",
				action: "cadastroAcesso",
				colaborador:      acesso,
				email:   email,
				senha:   senha,
				nome: nomeColaborador
		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");
	}
	else
	{
		alert ("Existem campos que não foram preenchidos!. Por favor complete o preenchimento.")
		}
}

