//Variáveis da sessão
var listaColaboradores = [];
var listaClientes = [];
var listaProjetos = [];
var listaPeriodos = [];
var listaAprovacao = [];
var listaEtapas = [];
var listaTipoReembolso = [];
var observer;
var config;
var target;
var periodo;
var indiceColaborador;

var colaborador = 0;
var prorietario = "";
var cliente = 0;
var projeto = 0;
var etapa = 0;
var tipoHora = 0;
var nome="";

var tabelaLancamentos;
var listaLancamentosReembolso = [];

function admin_init(){

	colaborador = sessionStorage.getItem("colaborador");	
	nome = sessionStorage.getItem("nomeColaborador");
	colaboradorLogado = sessionStorage.getItem("colaboradorLogado");
	
	sessionStorage.setItem("seraReembolsavel", "false");
	
	if( (colaborador==0) || (colaborador==null) || (colaborador==undefined) ){
		window.document.location.href = '../../login/login.html';
	}
	
	tabelaLancamentos = document.getElementById("lancamentosreembolso_lancamentos");
	
	buscarColaboradores();

	document.getElementById("menuop1").addEventListener("click", carregaReembolso);
	document.getElementById("menuop3").addEventListener("click", sair);
	document.getElementById("lancamentoreembolso_status").addEventListener("change", buscarLancamentosReembolsoColaborador);

}

function isMobile(){
	var userAgent = navigator.userAgent.toLowerCase();
	if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
		return true;
	else
		return false;
}


function btn_salvar_click(){
	salvarReembolso();
}

//PREENCHIMENTO DAS COMBOS

function desabilitarComboColaborador(){
	document.getElementById('reembolso_colaborador').value = nome;
	document.getElementById('reembolso_colaborador').innerHTML = "";
	document.getElementById('reembolso_colaborador').disabled = true;
}


function preencheListaColaboradores(){
	var html = '';
	var select = "";
	
	listaColaboradores.forEach( () => {
		html += '<option value="'+listaColaboradores.id+'" '+select+'>' +listaColaboradores.nome+'</option>';
	});
	
	document.getElementById("lancamentoreembolso_colaborador").addEventListener("change", mudaColaborador);
	document.getElementById("lancamentoreembolso_colaborador").innerHTML = html;
	
	var pos = setSelectedValue(document.getElementById("lancamentoreembolso_colaborador"),colaboradorLogado);
	

}

function buscarColaboradores(){
	
	tabelaLancamentos.innerHTML = '';
	
	var url = retornaServer();
	
	var data = {
			page: "colaborador",
			action: "buscarColaboradorReembolso",
			colaborador:colaborador,
			proprietario: colaboradorLogado
	}
	
	url = url + encodeURI(JSON.stringify(data));
	
	callWebservice(url,"callback");
}


function buscarLancamentosReembolsoColaborador(){
	
	tabelaLancamentos.innerHTML = '';
    
	status = document.getElementById("lancamentoreembolso_status").value;
    
	var url = retornaServer();
	
	var data = {
			page: "reembolso",
			action: "buscarReembolsosColaborador", 
			colaborador:colaborador,
			status: status
			
	}
	
	url = url + encodeURI(JSON.stringify(data));
	
	callWebservice(url,"callback");

	
}

function excluirLancamento(lancamentoId){
	
	var r=confirm("Você deseja realmente excluir o reembolso?");
	if (r==true)
	  {
		var url = retornaServer();
		var data = {
				page: "reembolso",
				action: "excluirReembolso",
				lancamento: lancamentoId

		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");  
	  }
		
}


function editarLancamento(lancamentoId){
	
	var achei=0;
	var i=0;
	while( achei == 0 ){
		
		if (listaLancamentosReembolso[i].id == lancamentoId ){
			
			achei=1;	  
			
		}
		
		i=i+1;
	}
	
	sessionStorage.setItem("lancamentoReembolso",listaLancamentosReembolso[i-1].id);
	//sessionStorage.setItem("colaborador",listaLancamentosReembolso[i-1].colaborador);
	sessionStorage.setItem("colaborador",colaborador);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../reembolso/reembolso.html';
	
}

//PROCESSAMENTO DO RETORNO DOS WEBSERVICES

function callback(data){
	
	if(data.action=="buscarReembolsosColaborador"){
		listaLancamentosReembolso = jQuery.parseJSON(data.retorno);
		preencheTabelaLancamentosReembolso();
	}
	
	if(data.action=="excluirReembolso"){
		window.location.reload();
	}

	if(data.action=="buscarColaboradorReembolso"){
		listaColaboradores = jQuery.parseJSON(data.retorno);
		preencheListaColaboradores();
		buscarLancamentosReembolsoColaborador();
	}

}

function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;
			
		}
	}
	return i;
}


function mudaColaborador() {
	tabelaLancamentos.innerHTML = '';
	var idCol=document.getElementById("lancamentoreembolso_colaborador").value;
	var idPer=document.getElementById("lancamentoreembolso_periodo").value;
    colaborador=idCol;
    periodo=idPer; 
	var e = document.getElementById("reembolso_cliente");
//	clienteid = e.options[e.selectedIndex].value;
	var cont=0;
	var achei=-1;
	while ((cont <listaColaboradores.length ) || (achei ==-1))
	{

		if (listaColaboradores[cont].id==idCol)
		{
			achei=cont;
		}
		cont++;
	}
	colaboradorPos=achei;
		
	
	buscarReembolsosColaborador(listaColaboradores[colaboradorPos].id);

}






function carregaReembolso()
{
	sessionStorage.setItem("colaborador",colaborador);
	sessionStorage.setItem("lancamentoReembolso",0);
	sessionStorage.setItem("edicao", 1);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../reembolso/reembolso.html';

}






//TRATAMENTO DOS EVENTOS

function preencheTabelaLancamentosReembolso(){
	var html = '';
	var status='';
	var indiceTipoReembolso;
	var indiceProjeto;
	var indiceCliente;
	var indiceFase;
	var j=0;
	var totalLancamentos='00:00';

	var achei=0;
	
	if(listaLancamentosReembolso.length > 0)
	{
		for(i = 0; i < listaLancamentosReembolso.length; i++)
		{
			
			if(listaLancamentosReembolso[i].statusreembolso == "Pendente"){
				if (isMobile() == false)
				{
	
					html+='<div class="item" id="div'+listaLancamentosReembolso[i].id+'">'
					+'<div><b>'
					+listaLancamentosReembolso[i].data+ " - " +listaLancamentosReembolso[i].tipo+ "<font color=#515caf> <br>Valor Solicitado: "+currency(listaLancamentosReembolso[i].valorsolicitado)+" - Valor Previsto: "+currency(listaLancamentosReembolso[i].valorreembolso)+'</font></b>'
					+"<br>"+listaLancamentosReembolso[i].codigoProjeto+" - "+listaLancamentosReembolso[i].projeto +"<br><strong> Status: </strong>"+listaLancamentosReembolso[i].statusreembolso
					+"<br><strong> Observação: </strong><br>"+listaLancamentosReembolso[i].observacao+ "<br>"+'</div>'
					+'<button class="btnedit" onclick="editarLancamento('+ "'" + listaLancamentosReembolso[i].id+ "'" +')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
					+'<button class="btndelete" onclick="excluirLancamento('+ "'" +listaLancamentosReembolso[i].id+ "'" +')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>'
					+'</div>'
				}
				else
				{
					html+='<div class="item" id="div'+listaLancamentosReembolso[i].id+'">'
					+'<button class="action" onclick="editarLancamento(' + "'" +listaLancamentosReembolso[i].id+ "'" + ')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
					+'<div><b>'
					+listaLancamentosReembolso[i].data+ " - " +listaLancamentosReembolso[i].tipo+ "<font color=#515caf> <br>Valor Solicitado: "+currency(listaLancamentosReembolso[i].valorsolicitado)+" - Valor Previsto: "+currency(listaLancamentosReembolso[i].valorreembolso)+'</font></b>'
					+"<br>"+listaLancamentosReembolso[i].codigoProjeto+" - "+listaLancamentosReembolso[i].projeto +"<br><strong> Status: </strong>"+listaLancamentosReembolso[i].statusreembolso
					+"<br><strong> Observação: </strong><br>"+listaLancamentosReembolso[i].observacao+ "<br>"+'</div>'
					+'<button class="action" onclick="excluirLancamento('+ "'" + listaLancamentosReembolso[i].id+ "'" + ')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>'
					+'</div>'
				}
			}else{
				if (isMobile() == false)
				{
	
					html+='<div class="item" id="div'+listaLancamentosReembolso[i].id+'">'
					+'<div><b>'
					+listaLancamentosReembolso[i].data+ " - " +listaLancamentosReembolso[i].tipo+ "<font color=#515caf> <br>Valor Solicitado: "+currency(listaLancamentosReembolso[i].valorsolicitado)+" - Valor Previsto: "+currency(listaLancamentosReembolso[i].valorreembolso)+'</font></b>'
					+"<br>"+listaLancamentosReembolso[i].codigoProjeto+" - "+listaLancamentosReembolso[i].projeto +"<br><strong> Status: </strong>"+listaLancamentosReembolso[i].statusreembolso
					+"<br><strong> Observação: </strong><br>"+listaLancamentosReembolso[i].observacao+ "<br>"+'</div>'
					+'</div>'
				}
				else
				{
					html+='<div class="item" id="div'+listaLancamentosReembolso[i].id+'">'
					+'<div><b>'
					+listaLancamentosReembolso[i].data+ " - " +listaLancamentosReembolso[i].tipo+ "<font color=#515caf> <br>Valor Solicitado: "+currency(listaLancamentosReembolso[i].valorsolicitado)+" - Valor Previsto: "+currency(listaLancamentosReembolso[i].valorreembolso)+'</font></b>'
					+"<br>"+listaLancamentosReembolso[i].codigoProjeto+" - "+listaLancamentosReembolso[i].projeto +"<br><strong> Status: </strong>"+listaLancamentosReembolso[i].statusreembolso
					+"<br><strong> Observação: </strong><br>"+listaLancamentosReembolso[i].observacao+ "<br>"+'</div>'
					+'</div>'
				}
			}
		}

		tabelaLancamentos.innerHTML = html;
		//checkButton();
	}else{
		tabelaLancamentos.innerHTML = html;
	}
	
}

function currency(numero){
	var formatado  = formatMoney(numero, 2, "R$ ", ".", ",");
    return formatado;
}

function formatMoney(number, places, symbol, thousand, decimal) {
	places = !isNaN(places = Math.abs(places)) ? places : 2;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}

function sair()
{
	localStorage.clear();
	sessionStorage.setItem("colaborador",0);
	sessionStorage.setItem("lancamento",0);
	sessionStorage.setItem("periodo",0);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../../login/login.html';

}


target = document.querySelectorAll('.item-list')[0];

//cria uma nova instância de observador
observer = new MutationObserver(function(mutations) {
	mutations.forEach(function(mutation) {
		console.log("tipo de mutação" +mutation.type);

		$(function() {$('.example-1').listSwipe();});

	});    
});

//configuração do observador:
config = { attributes: true, childList: true, characterData: true };

//passar o nó alvo pai, bem como as opções de observação
observer.observe(target, config);



