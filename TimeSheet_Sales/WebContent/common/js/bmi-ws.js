function retornaServer(){
	//return 'http://54.233.171.120/rest_capvase/srv_capvase/ws_capvase/';
	//return 'http://10.10.4.193:80/TimeSheet/rest_timesheet_sales/srv_timesheet_sales/ws_timesheet_sales/';
	return 'http://localhost:80/TimeSheet_Sales/rest_timesheet_sales/srv_timesheet_sales/ws_timesheet_sales/';
	//return 'http://18.228.20.169:80/TimeSheet_Sales/rest_timesheet_sales/srv_timesheet_sales/ws_timesheet_sales/';
}

function retornaCaminhoArquivo(){ 
	//return 'http://54.233.171.120/rest_capvase/srv_capvase/ws_capvase/';
	//return 'http://10.10.4.193:80/TimeSheet/arquivosReembolso/';
	return 'http://118.228.20.169:80/TimeSheet_Sales/reembolso/arquivosReembolso/';
}

function callWebserviceMethod(url,callbackMethod){
    //url=url+"?"+callbackMethod+"=?";
    $.ajax({
        url: url,
        crossDomain: true,
        type: "GET",
        contentType: "application/json; charset=utf-8;",
        async: false,
        dataType: 'jsonp',
        jsonp: callbackMethod,
        jsonpCallback:callbackMethod        
    })
}

function callWebservice(url){
	callWebserviceMethod(url,"callback");
}

function blockBackButton(){
	history.pushState(null, document.title, location.href);
	window.addEventListener('popstate', function (event){
	  history.pushState(null, document.title, location.href);
	});
}
