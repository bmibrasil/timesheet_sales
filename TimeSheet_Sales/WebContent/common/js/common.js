function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}

function buscarDados(data){

	let url = retornaServer();
	
	url = url + encodeURI(JSON.stringify(data));
	
	callWebservice(url,"callback");
	
}

function formatarData( data ){
	
	var dia = data.getDate();
	var mes = data.getMonth() + 1; 
	var ano = data.getFullYear();
	
	if( dia < 10 ){	dia = '0' + dia; }
	if( mes < 10 ){ mes = '0' + mes; } 	

	return	ano + '-' + mes + '-' + dia;
}