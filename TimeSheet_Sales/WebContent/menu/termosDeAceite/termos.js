const politicasReembolsoInterno = "<h6 align='center'>POLÍTICA DE REEMBOLSO DE DESPESAS</h6>" + 
"<ol>" +
	"<li><h6>Objetivos</h6>" + 
		"O objetivo deste documento é organizar e orientar o processo de reembolso referente às despesas incorridas pelos " +
		"colaboradores da WHITE FOX e suas coligadas (“<b>WHITE FOX</b>”)." + 
		"<br><br>Esta política define:" + 
		"<ul>" +
			"<li type='disc'>a caracterização das despesas;</li>" +
			"<li type='disc'>os requisitos para apresentar despesas;</li>" +
			"<li type='disc'>os itens de despesas reembolsáveis e não reembolsáveis.</li>" +
			"</ul>" +
	"</li><br>" +
	"<li><h6>Escopo</h6>" + 
		"A Política de despesas aplica-se e deve ser respeitada por todos os colaboradores e associados <b>WHITE FOX</b>, " +
		"em qualquer situação de relacionamento com a empresa." +
		"<br><br>Como colaborador da <b>WHITE FOX</b>, é sua responsabilidade entender e apresentar suas despesas em " +
		"conformidade com a política de despesas. Os relatórios de despesas estão sujeitos aos requisitos e restrições " +
		"impostos pela legislação." +
	"</li><br>" +
	"<li><h6>Declaração da Política</h6>" +
	"<ol>" +
		"<li><b>Caracterização das despesas</b></li>" +
			"São as despesas efetuadas pelos colaboradores e associados, em deslocamentos, para atividades de viagens." +
		"<br><li><b>Reembolso de despesas</b></li>" +
		"<ul>" +
			"<li type='disc'>Os colaboradores serão reembolsados somente pelas despesas relativas aos negócios da empresa.</li>" +
			"<li type='disc'>Os colaboradores deverão enviar os relatórios de despesas corporativas através do formulário - Anexo 1.</li>" +
			"<li type='disc'>A área administrativa será responsável por analisar e aprovar os relatórios de despesas de seus" +
				"colaboradores. Em casos de divergências serão encaminhadas para o diretor da área responsável.</li>" +
			"<li type='disc'>Os colaboradores devem enviar os relatórios de despesas, no prazo máximo de 30 dias, após a data " +
				"de realização dos eventos. Relatórios de despesas enviados após este prazo, não terão os " +
				"reembolsos realizados.</li>" +
			"<li type='disc'>O reembolso das despesas somente será realizado após a aprovação do relatório " +
				"pela área administrativa.</li>" +
			"<li type='disc'>Todos os relatórios de despesas estão sujeitos a auditoria.</li>" +
			"<li type='disc'>A <b>WHITE FOX</b> não emite adiantamentos em dinheiro para viagens corporativas. " +
				"Situações específicas serão tratadas diretamente com o Gestor do colaborador e associado.</li>" +
		"</ul>" +
		"<li><b>Despesas autorizadas para reembolso</b></li><br>" +
			"<ul>" +
				"<li type='disc'><b>Deslocamentos na cidade de origem (exceto para equipe comercial):</b> esses deslocamentos " +
					"deverão ser feitos, preferencialmente, através de carro próprio. Uber/ 99, poderão ser utilizados na " +
					"impossibilidade de utilização de carro próprio, com autorização prévia do gestor. Os reembolsos serão feitos " +
					"nas seguintes condições:</li>" +
			"<ul>" +
				"<li type='none'><b>Carro próprio:</b> R$ 0,85/km rodado e só será pago a diferença de quilometragem, a maior, " +
					"do deslocamento habitual e diário do colaborador para a empresa.</li>" +
				"<li type='none'><b>Estacionamento:</b> valor integral da despesa, mediante comprovante, em eventos aprovados " +
					"previamente pelo gestor.</li>" +
				"<li type='none'><b>Uber ou 99:</b> pagamento da diferença do valor a maior, em relação ao deslocamento em " +
					"quilometragem habitual e diário do colaborador para a empresa, mediante recibo.</li>" +
				"<li type='none'><b>Exemplo:</b> da residência ao escritório, temos uma distância de 15km e no deslocamento ao " +
					"cliente, essa distância for de 17km, teremos o reembolso de 2 km multiplicado por R$ 0,85/km.</li>" +
				"<li type='none'><b>Obs:</b> De modo geral, o colaborador deverá utilizar preferencialmente o transporte público.</li>" +
			"</ul>" +
			"<li type='disc'><b>Deslocamentos para outros municípios fora da cidade de origem:</b> esses deslocamentos, " +
				"deverão ser feitos, preferencialmente, através de carro próprio. Uber/ 99, poderão ser utilizados na " +
				"impossibilidade de utilização de carro próprio. Os reembolsos serão feitos nas seguintes condições:</li>" +
			"<ul>" +
				"<li type='none'><b>Carro próprio:</b> R$ 0,85/km rodado de todo o percurso, aprovado pelo gestor.</li>" +
				"<li type='none'><b>Avião:</b> serão permitidos, apenas para deslocamentos acima de 300 km, " +
					"com compra de passagens realizada pela área Administrativa com antecedência mínima de 3 semanas. " +
					"Qualquer deslocamento aéreo deve ser aprovado pelo gestor.</li>" +
				"<li type='none'><b>Estacionamento e Pedágio:</b> valor integral da despesa, mediante comprovante.</li>" +
				"<li type='none'><b>Uber ou 99:</b> pagamento integral da despesa, mediante comprovante.</li>" +
				"<li type='none'><b>Obs:</b> compras de passagens aéreas e reservas em hotel, " +
					"deverão ser solicitados para a equipe de Logística.</li></ul>" +
				"<li type='disc'><b>Em viagens de ônibus ou avião</b>, serão reembolsados os taxis/Uber nos seguintes trajetos:</li>" +
				"<ul>" +
					"<li type='disc'>Residência – Aeroporto/Rodoviária;</li>" +
					"<li type='disc'>Rodoviária/Aeroporto – Hotel;</li>" +
					"<li type='disc'>Hotel – Aeroporto/Rodoviária;</li>" +
					"<li type='disc'>Aeroporto/Rodoviária – Residência.</li>" +
				"</ul>" +
				"<li type='disc'><b>Refeições durante viagem de negócios</b> – Os colaboradores em situação de viagem serão " +
					"reembolsados pelos custos das próprias refeições dentro dos limites definidos. Em viagens nacionais," +
					" o valor máximo a ser reembolsado por dia é de R$ 58,00 (jantar). Em viagens internacionais, o valor " +
					"máximo a ser reembolsado por dia é de USD 80,00 (incluindo café da manhã, almoço e jantar). " +
					"Caso a nota fiscal englobar o valor de outras pessoas, o reembolso será limitado ao valor por dia. " +
					"Não serão reembolsadas despesas com bebidas alcoólicas.</li>" +
				"<li type='disc'><b>Consumo do Frigobar</b> – São reembolsáveis as despesas com água.</li>" +
				"<li type='disc'><b>Lavanderia</b> – São reembolsáveis as despesas para estadias superiores a uma semana, " +
					"como limite máximo de 10% do valor da diária.</li>" +
				"<li type='disc'><b>Refeições com clientes:</b> Apenas despesas previamente aprovadas pelo Gestor. " +
					"Não serão reembolsadas despesas em reuniões com fornecedores ou parceiros.</li>" +
				"<li type='disc'><b>Compras</b> - Todas as compras de itens como equipamentos, assinaturas de produtos ou serviços e" +
					" materiais de escritório não são reembolsáveis e devem ser solicitados à área responsável.</li>" +
				"<li type='disc'><b>Reivindicações de perdas/danos</b> - Perdas/danos de itens pessoais durante uma viagem de negócios " +
					"não serão reembolsados pela <b>WHITE FOX</b>. Bagagens perdidas ou desviadas durante o deslocamento " +
					"deverão ser imediatamente reclamadas ao departamento de bagagens da companhia de transportes.</li>" +
				"<li type='disc'><b>Taxas de conversão de moeda</b> serão reembolsadas pela <b>WHITE FOX</b>. " +
					"O colaborador deverá enviar comprovante da taxa de câmbio com extrato de seu cartão de crédito e/ou " +
					"comprovante da casa de câmbio.</li>" +
				"<li type='none'>A taxa cambial considerada para pagamento é a de venda.</li>" +
				"<li type='disc'><b>Taxas bancárias para saques</b> - Taxas bancárias e/ou taxas associadas a saques em dinheiro " +
					"durante uma viagem de negócios internacional não serão reembolsáveis.</li>" +
				"<li type='disc'><b>Vistos, passaportes, renovações de passaporte e taxas do consulado para viagens de negócios</b>" +
					"– Não serão reembolsáveis vistos de trabalho tipo B1/B2, H1. Eventuais despesas com residência temporária " +
					"para projetos de longo prazo deverão ser aprovadas pelo gestor.</li>" +
				"<li type='disc'><b>Vacinações</b> - Os colaboradores podem ser reembolsados por despesas com vacinações relativas " +
					"a viagens de negócios, mediante justificativa. A aprovação do gestor será necessária.</li>" +
				"<li type='disc'><b>Vacinações de rotina</b> (como vacinas contra a gripe ou pneumonia) <b>NÃO</b> são reembolsáveis.</li>" +
			"</ul>" +
		"</ul><br>" +
		"<li><b>Conformidade da política</b></li>" +
	"</ol>" +
	"<ul>" +
		"<li type='none'><b>Medição de conformidade</b> - A conformidade com a política é verificada por vários métodos," +
			" incluindo (mas não limitado a) relatórios, auditorias internas e externas, auditorias aleatórias por " +
			"amostragem e etc.</li>" +
		"<li type='none'><b>Exceções</b> - Exceções a essa política necessitam de aprovação do gestor da política e devem " +
			"ser documentadas conforme processo de exceção no relatório de despesas. Exceções podem causar atrasos no " +
			"processo de reembolso.</li>" +
		"<li type='none'><b>Não conformidade</b> - É necessário estar em conformidade com as políticas de despesas da " +
			"<b>WHITE FOX</b> e ao Código de Conduta Corporativa. A falsificação ou declaração falsa dos relatórios de " +
			"despesas ou da documentação comprobatória resultará em penalidades, que pode incluir demissão imediata. " +
			"Quaisquer dúvidas, podem ser levadas ao Gestor ou a Área Administrativa.</li>" +
	"</ul><br>" +
	"<li><h6>Termos Gerais</h6>" +
		"A empresa reserva-se o direito de mudar essa política a qualquer momento. Todas as atualizações deste " +
		"documento serão publicadas." +
		"<br><b>Obs:</b> Quando nos contratos firmados com os clientes, houver uma política de reembolso específica desse cliente," +
		" os colaboradores serão orientados dos valores negociados no contrato.</li>" +
"</ol>";

const politicasReembolsoExterno = "Termo de Reembolso de Usuario Externo!";

const exclusao = "O usuário não tem procedência!(Não é Externo nem Interno).";

const termos = new Map();

termos.set("Interno", politicasReembolsoInterno);
termos.set("Externo", politicasReembolsoExterno);
termos.set("", exclusao);
