let listaPermissoes = [];
let nome = '';
let colaborador = "0";
let bloqueio = 0;
let permissoes = '';
let procedencia = '';

const openModal = () => {
	document.getElementById('texto_termo').innerHTML = termos.get(procedencia);
	$("#modal").modal();
}

const actionButtonReembolso = () => {
	buscarPermissao();
}

$(document).ready(function() {
	$("#menu_reembolso").click(actionButtonReembolso);
});

const scrollDown = () => {
	let $texto_termo = $('#texto_termo');
	let $botao_aceite = $('#botao_aceite');
	
	$texto_termo.scrollTop($texto_termo[0].scrollTop + 280);
	
	if($texto_termo[0].scrollHeight - $($texto_termo[0]).scrollTop() == 300){
		$texto_termo[0].style.overflow = "auto";
		$texto_termo.scrollTop($texto_termo[0].scrollTop + 200);
		$botao_aceite.text("Aceitar");
		$botao_aceite.attr('onclick', 'inserirPermissao();');
	}
}

$("#texto_termo").ready(function() {
	$("#botao_aceite").click(scrollDown);
});


function admin_init() {
	nome = sessionStorage.getItem("nomeColaborador");
	colaborador = sessionStorage.getItem("colaborador");
	bloqueio = sessionStorage.getItem("bloqueio");
	permissoes = sessionStorage.getItem("permissoes");
	procedencia = sessionStorage.getItem("procedencia");

	if( (colaborador == 0) || (colaborador == null) || (colaborador == undefined) ){
		window.document.location.href = '../login/login.html';
	}
	
	document.getElementById("menuop1").addEventListener("click", sair);
	document.getElementById("menu_timesheet").addEventListener("click",inserirTimesheet);
}


function inserirPermissao() {
	
	document.getElementById("botao_aceite").disabled = true;
	
	const dataAtual = formatarData(new Date());
	const aceite = true;
	let id = 0;
	
	var data = {
		page : "menu",
		action : "inserirPermissao",
		id : id,
		data : dataAtual,
		aceite : aceite,
		colaborador : colaborador
	}
	
	if(listaPermissoes.length != 0){ id = listaPermissoes[0].id; }
	
	buscarDados(data);
}

function buscarPermissao() {
	
	var data = {
		page : "menu",
		action : "buscarPermissao",
		colaborador : colaborador
	}
	
	buscarDados(data);
}

function callback(data) {

	if (data.action == "buscarPermissao"){
		listaPermissoes = jQuery.parseJSON(data.retorno);
		( listaPermissoes[0] != null && listaPermissoes[0].aceite == "true" ) ? inserirReembolso() : openModal();
	}

	if (data.action == "inserirPermissao") {
		inserirReembolso();
	}
}

function atribuirValoresAmbiente() {
	sessionStorage.setItem("colaborador", colaborador);
	sessionStorage.setItem("permissoes", permissoes);
	sessionStorage.setItem("bloqueio", bloqueio);
}

function inserirTimesheet() {

	atribuirValoresAmbiente();
	sessionStorage.setItem("colaborador", colaborador);
	sessionStorage.setItem("lancamento", sessionStorage.getItem("lancamento"));
	sessionStorage.setItem("permissoes", sessionStorage.getItem("permissoes"));
	sessionStorage.setItem("edicao", sessionStorage.getItem("edicao"));
	sessionStorage.setItem("nomeColaborador", nome);
	sessionStorage.setItem("bloqueio", sessionStorage.getItem("bloqueio"));
	sessionStorage.setItem("colaboradorLogado", sessionStorage.getItem("colaboradorLogado"));

	window.document.location.href = '../timesheet/timesheet/timesheet.html';
}

function inserirReembolso() {

	atribuirValoresAmbiente();
	sessionStorage.setItem("bloqueio", sessionStorage.getItem("bloqueio"));
	sessionStorage.setItem("lancamentoReembolso", sessionStorage.getItem("lancamentoReembolso"));
	sessionStorage.setItem("colaborador", colaborador);
	sessionStorage.setItem("nomeColaborador", nome);
	sessionStorage.setItem("edicao", sessionStorage.getItem("edicao"));
	sessionStorage.setItem("colaboradorLogado", sessionStorage.getItem("colaboradorLogado"));

	window.document.location.href = '../reembolso/reembolso/reembolso.html';
}

function sair() {
	localStorage.clear();
	sessionStorage.setItem("colaborador", "0");
	sessionStorage.setItem("lancamentoTimesheet", "0");
	sessionStorage.setItem("lancamentoReembolso", "0");
	
	window.document.location.href = '../login/login.html';
}
