function admin_init(){
	localStorage.clear();
	document.getElementById("login_btLogin").addEventListener("click",login_byLogin);
	/*document.getElementById("login_btLogin").ontouch = login_byLogin;*/
}

function login_byLogin() {
	
	localStorage.clear();
	
	var email = document.getElementById("email").value;
	var senha = document.getElementById("senha").value;
	
	buscarColaborador(email, senha);
}

function buscarColaborador(email, senha){

	var data = {
			page: "colaborador",
			action: "buscarColaborador",
			colaborador: 0,
			email: email,
			senha:senha
	}
	
	buscarDados( data );
}


function callback(data){

	if(data.action == "buscarColaborador"){
		
		if (data.statusCode == "404" ) {
			
			alert("Email ou Senha Invalidos");
			
		} else{
			
			sessionStorage.setItem("colaboradorLogado",data.idsalesForce);
			sessionStorage.setItem("colaborador",data.idsalesForce);
			sessionStorage.setItem("lancamento",0);
			sessionStorage.setItem("lancamentoReembolso",0);
			sessionStorage.setItem("edicao",0);
			sessionStorage.setItem("nomeColaborador",data.primeiroNome + " " + data.ultimoNome);
			sessionStorage.setItem("distanciaCasaTrabalho", data.distanciaCasaTrabalho );
			sessionStorage.setItem("procedencia",data.procedencia);
			
			location.href = "../menu/menu.html"; 
		}
	}
}