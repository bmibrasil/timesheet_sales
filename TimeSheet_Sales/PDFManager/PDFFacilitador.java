package PDFManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import Beans.Config;
import Beans.Elemento;
import Beans.Grupo;
import Exceptions.ElementoNaoEncontradoException;
import Exceptions.ProxyGrupoIncorretoException;
import Exceptions.ProxyNaoIniciadoException;
import Service.CicloService;
import Service.ConfigService;
import Service.ElementoService;
import Service.GrupoService;
import Service.ModeloService;
import Service.VariaveisFluxoService;
import Tools.CAPService;
import Util.FatoresReceita;

public class PDFFacilitador {
	private int grupo;
	//private String nomeGrupo;
	private Document document;
	private ElementoService es;
	//private LEService les;
	private GrupoService gr;
	private int periodo;
	//private String organizacao;  
	private List<Elemento> lista; 
	//private List<LE> listaLE;
	private List<Grupo> listaGr; 
	private String empresas[] = { "BioMaster", "CleanSystem", "DentalByte", "DigiMetrics", "HardTech" };
    private CAPService capService;
    private CicloService cicloService;
	
	
	public PDFFacilitador(String organizacao, int grupo, String nomeGrupo){
		this.grupo=grupo;
		//this.nomeGrupo=nomeGrupo;
		document = new Document();
		es= new ElementoService();
		//les= new LEService(grupo);
		gr= new GrupoService();
		ConfigService configService = new ConfigService();
		Config config = configService.getConfig(grupo);
		periodo = config.getPeriodo();
		//this.organizacao=organizacao;  
		lista=new ArrayList<Elemento>(); 
		//ListaLE=new ArrayList<LE>();
		listaGr=new ArrayList<Grupo>(); 
		capService = new CAPService();
	}

	public PDFFacilitador(String organizacao, Config config, int periodo){
		this.grupo = config.getGrupo();
		config.setPeriodo(periodo);
		//this.nomeGrupo=config.getNome();
		this.periodo = periodo;
		document = new Document();
		es= new ElementoService();
		//les= new LEService(grupo);
		gr= new GrupoService();
		//this.organizacao=organizacao;  
		lista=new ArrayList<Elemento>(); 
		//listaLE=new ArrayList<LE>();
		listaGr=new ArrayList<Grupo>(); 
		capService = new CAPService();
	}

	
	public static String format(double x){
		return (""+Math.round(x*1000)/10.0+"%");//.replaceAll(".0", "");
	}
	
	private  String valorEconomicop1[][]={
			//{" "," "," "," ", " "," "," "," "},
			{"Gera��o de Lucro Econ�mico - Holding"," "," "," "," "," ",},
			{"","","","","",""},
			{"Lucro Econ�mico",   "","","", "",""},
			{"Spread",   "","","", "",""},
			{"RONA",   "","","", "",""},
			{"NOPAT",   "","","", "",""},
			{"WACC",   "","","", "",""},
			{"Custo Capital Pr�prio",   "","","", "",""},
			{"Custo Capital Terceiros",   "","","", "",""},
			{"Capital Investido",   "","","", "",""},
			{"Ativo Fixo",   "","","", "",""},
			{"Capital de Giro L�quido",   "","","", "",""},
			{"Disponibilidade (Caixa + Aplic. Fin)",   "","","", "",""},

			{" "," "," "," ", " "," "},
			{"Gera��o de Lucro Econ�mico - DentalByte"," "," "," "," "," ",},
			{"","","","","",""},
			{"Lucro Econ�mico",   "","","", "",""},
			{"Spread",   "","","", "",""},
			{"RONA",   "","","", "",""},
			{"NOPAT",   "","","", "",""},
			{"WACC",   "","","", "",""},
			{"Custo Capital Pr�prio",   "","","", "",""},
			{"Custo Capital Terceiros",   "","","", "",""},
			{"Capital Investido",   "","","", "",""},
			{"Ativo Fixo",   "","","", "",""},
			{"Capital de Giro L�quido",   "","","", "",""},
			{"Disponibilidade (Caixa + Aplic. Fin)",   "","","", "",""},

			{" "," "," "," ", " "," "},
			{"Gera��o de Lucro Econ�mico - Digimetrics"," "," "," "," "," ",},
			{"","","","","",""},
			{"Lucro Econ�mico",   "","","", "",""},
			{"Spread",   "","","", "",""},
			{"RONA",   "","","", "",""},
			{"NOPAT",   "","","", "",""},
			{"WACC",   "","","", "",""},
			{"Custo Capital Pr�prio",   "","","", "",""},
			{"Custo Capital Terceiros",   "","","", "",""},
			{"Capital Investido",   "","","", "",""},
			{"Ativo Fixo",   "","","", "",""},
			{"Capital de Giro L�quido",   "","","", "",""},
			{"Disponibilidade (Caixa + Aplic. Fin)",   "","","", "",""},
	};
	
	
	
	private  String valorEconomicop2[][]={
		
			{"Gera��o de Lucro Econ�mico - CleanSystem"," "," "," "," "," ",},
			{"","","","","",""},
			{"Lucro Econ�mico",   "","","", "",""},
			{"Spread",   "","","", "",""},
			{"RONA",   "","","", "",""},
			{"NOPAT",   "","","", "",""},
			{"WACC",   "","","", "",""},
			{"Custo Capital Pr�prio",   "","","", "",""},
			{"Custo Capital Terceiros",   "","","", "",""},
			{"Capital Investido",   "","","", "",""},
			{"Ativo Fixo",   "","","", "",""},
			{"Capital de Giro L�quido",   "","","", "",""},
			{"Disponibilidade (Caixa + Aplic. Fin)",   "","","", "",""},


			{" "," "," "," ", " "," "},
			{"Gera��o de Lucro Econ�mico - HardTech"," "," "," "," "," ",},
			{"","","","","",""},
			{"Lucro Econ�mico",   "","","", "",""},
			{"Spread",   "","","", "",""},
			{"RONA",   "","","", "",""},
			{"NOPAT",   "","","", "",""},
			{"WACC",   "","","", "",""},
			{"Custo Capital Pr�prio",   "","","", "",""},
			{"Custo Capital Terceiros",   "","","", "",""},
			{"Capital Investido",   "","","", "",""},
			{"Ativo Fixo",   "","","", "",""},
			{"Capital de Giro L�quido",   "","","", "",""},
			{"Disponibilidade (Caixa + Aplic. Fin)",   "","","", "",""},

			{" "," "," "," ", " "," "},
			{"Gera��o de Lucro Econ�mico - BioMaster"," "," "," "," "," ",},
			{"","","","","",""},
			{"Lucro Econ�mico",   "","","", "",""},
			{"Spread",   "","","", "",""},
			{"RONA",   "","","", "",""},
			{"NOPAT",   "","","", "",""},
			{"WACC",   "","","", "",""},
			{"Custo Capital Pr�prio",   "","","", "",""},
			{"Custo Capital Terceiros",   "","","", "",""},
			{"Capital Investido",   "","","", "",""},
			{"Ativo Fixo",   "","","", "",""},
			{"Capital de Giro L�quido",   "","","", "",""},
			{"Disponibilidade (Caixa + Aplic. Fin)",   "","","", "",""},

	};

	private  String execucaoEstrategica[][]={
			{"Execu��o Estrat�gica - Holding","","","","",""},
			{"Total",   "","","", "",""},
			{"Gest�o de Opera��es",   "","","", "","",},
			{"Gest�o de Pessoas",   "","","", "","",},
			{"Gest�o de Clientes",   "","","", "","",},
			{"Gest�o de Finan�as",   "","","", "","",},


			
			{"Execu��o Estrat�gica - DentalByte","","","","",""},
			{"Total",   "","","", "",""},
			{"Gest�o de Opera��es",   "","","", "","",},
			{"Gest�o de Pessoas",   "","","", "","",},
			{"Gest�o de Clientes",   "","","", "","",},
			{"Gest�o de Finan�as",   "","","", "","",},

			{"Execu��o Estrat�gica - Digimetrics","","","","",""},
			{"Total",   "","","", "",""},
			{"Gest�o de Opera��es",   "","","", "","",},
			{"Gest�o de Pessoas",   "","","", "","",},
			{"Gest�o de Clientes",   "","","", "","",},
			{"Gest�o de Finan�as",   "","","", "","",},

			
			{"Execu��o Estrat�gica - CleanSystem","","","","",""},
			{"Total",   "","","", "",""},
			{"Gest�o de Opera��es",   "","","", "","",},
			{"Gest�o de Pessoas",   "","","", "","",},
			{"Gest�o de Clientes",   "","","", "","",},
			{"Gest�o de Finan�as",   "","","", "","",},

			{"Execu��o Estrat�gica - Hardtech","","","","",""},
			{"Total",   "","","", "",""},
			{"Gest�o de Opera��es",   "","","", "","",},
			{"Gest�o de Pessoas",   "","","", "","",},
			{"Gest�o de Clientes",   "","","", "","",},
			{"Gest�o de Finan�as",   "","","", "","",},

			{"Execu��o Estrat�gica - BioMaster","","","","",""},
			{"Total",   "","","", "",""},
			{"Gest�o de Opera��es",   "","","", "","",},
			{"Gest�o de Pessoas",   "","","", "","",},
			{"Gest�o de Clientes",   "","","", "","",},
			{"Gest�o de Finan�as",   "","","", "","",},

 }; 

	

	private  String valorAcao[][]={
			{"Composi��o A��o", "%","","","","","","","","","",""},
			{"", "A","B","A*B","C","A*C","D","A*D","E","A*E","F","A*F"},
			{"DO","5% "," "," "," "," "," ","","","","",""},
			{"EE","10% "," "," "," "," "," ","","","","",""},
			{"EVA","50% "," "," "," "," "," ","","","","",""},
			{"LE","15% "," "," "," "," "," ","","","","",""},
			{"PSO","10% "," "," "," "," "," ","","","","",""},
			{"VAD","10% "," "," "," "," "," ","","","","",""},
			{"Total",""," "," "," "," "," ","","","","",""},
			{"Valor Anterior A��o"," "," "," "," "," "," ","","","","",""},
			{"Pre�o Calculado A��o",""," "," "," "," "," ","","","","",""},
			{"Patrimonio L�quido"," "," "," "," "," "," ","","","","",""},
			{"N�mero A��es"," "," "," "," "," "," ","","","","",""},
			{"Pre�o M�nimo A��o"," "," "," "," "," "," ","","","","",""},
			{"Pre�o A��o"," "," "," "," "," "," ","","","","",""},
		
	};
	
	private  String variavelFluxo[][]={
			{"Composi��o", "%","Valor","Ajuste Cap","Peso X Ajuste","Valor","Ajuste Cap","Peso X Ajuste","Valor","Ajuste Cap","Peso X Ajuste","Valor","Ajuste Cap","Peso X Ajuste","Valor","Ajuste Cap","Peso X Ajuste"},
			{"CD - Confian�a"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"CD - Conhecimento"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"CD - Significado"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"CD - Comprometimento"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"JOTA - �ndice de Funcionalidade","  "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"JOTA - Pactua��o","  "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"JOTA - Mecanismo de Controle"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"JOTA - Perfil Gerencial"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"PSO - Excel�ncia"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"PSO - Foco"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"PSO - Coes�o"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"PSO - Resili�ncia"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"PSO - Relev�ncia"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"CVS - Sociedade"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"CVS - Clientes"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"CVS - Colaboradores"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"VAD - Ader�ncia a Cren�as e Valores"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"VAD - Produtividade Consistente",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"VAD - V�nculo Emocional",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"Soma Pesos",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"FAE -Fator de Alinhamento Estrat�gico",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"FPE - Fator de Penalidade Estrat�gico",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"FAO - Fator de Aloca��o de Or�amento"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"FCO - Fator de Comp. do Opex",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"FCC - Fator de Comp. do Capex",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"FAP - Fator de Ajuste de Proj. Esp.",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"FVTec Fator de Var. Capex em Tec."," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"M�ximo",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"Base",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"M�nimo",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"Resultado Per�odo Anterior",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"Varia��o Total Per�odo",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"Resultado Calculado",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			{"Resultado Final",""," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
		
	};
	
	
	
	private  String[][] valorEconomicoFormatop1 = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//{"N 8 L 5  0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0"},
			{"B 8 L 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},

			{"N 8 L 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0"},
			{"B 8 L 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},


			{"N 8 L 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0"},
			{"B 8 L 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},

	};
	private  String[][] valorEconomicoFormatop2 = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			{"B 8 L 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},


			{"N 8 L 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0"},
			{"B 8 L 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},

			{"N 8 L 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0"},
			{"B 8 L 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},


			{"N 8 L 1  0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0"},
			{"B 8 L 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},

	};

	private  String[][] execucaoEstrategicaFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},

			
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			
	};

	
	
	
	private  String[][] valorAcaoFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Demonstrativo de Resultado
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5","N 8 C 2 1.5"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
		
		
	};
	
	
	private  String[][] variavelFluxoFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Demonstrativo de Resultado

			
			{"B 8 L 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"B 8 L 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5"},
			{"B 8 L 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5"},
	};

	
	public  void preencheValorEconomicop1(String[][] matriz)
	{ 
		int coluna=1;//,linha=2;
		//int col=1,contEmp=0;
		//cria hassh de elementos do balan�o
		    listaGr =gr.getGruposParceiros(grupo);
			Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			
			for (coluna=0;coluna<listaGr.size();coluna++)
			{
		    
				lista =es.getHerdeiros("%%","%%",periodo-1,listaGr.get(coluna).getId());
				for (Elemento e:lista){
					hash.put(e.getCampo()+e.getOrganizacao()+e.getGrupo(), e);
			}
			}			
		   for (coluna=0;coluna<listaGr.size();coluna++)
		   {
			matriz[1][coluna+1] =   listaGr.get(coluna).getNome(); 
			matriz[2][coluna+1] =   Long.toString(Math.round(hash.get("EVA"+"Holding"+listaGr.get(coluna).getId()).getValor())); 
			matriz[3][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread"+"Holding"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[4][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Roic"+"Holding"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[5][coluna+1] =   Long.toString(Math.round(hash.get("TSR_FCFYld_FCF_Nopat"+"Holding"+listaGr.get(coluna).getId()).getValor()));
			matriz[6][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc"+"Holding"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[7][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc_CstCap_PL_tx"+"Holding"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[8][coluna+1] =   Long.toString(Math.round((
					(hash.get("DRE_Lair_Juros_DivCP"+"Holding"+listaGr.get(coluna).getId()).getValor()+hash.get("DRE_Lair_Juros_DivLP"+"Holding"+listaGr.get(coluna).getId()).getValor())/
					(hash.get("DRE_Lair_Juros_DivCP_vl"+"Holding"+listaGr.get(coluna).getId()).getValor() +hash.get("DRE_Lair_Juros_DivLP_vl"+"Holding"+listaGr.get(coluna).getId()).getValor()))*100))+"%";
			matriz[9][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv"+"Holding"+listaGr.get(coluna).getId()).getValor()));
			matriz[10][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_AtFx"+"Holding"+listaGr.get(coluna).getId()).getValor()));
			matriz[11][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"Holding"+listaGr.get(coluna).getId()).getValor()));
			matriz[12][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_Disp"+"Holding"+listaGr.get(coluna).getId()).getValor()));


			matriz[16][coluna+1] =   Long.toString(Math.round(hash.get("EVA"+"DentalByte"+listaGr.get(coluna).getId()).getValor())); 
			matriz[17][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread"+"DentalByte"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[18][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Roic"+"DentalByte"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[19][coluna+1] =   Long.toString(Math.round(hash.get("TSR_FCFYld_FCF_Nopat"+"DentalByte"+listaGr.get(coluna).getId()).getValor()));
			matriz[20][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc"+"DentalByte"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[21][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc_CstCap_PL_tx"+"DentalByte"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[22][coluna+1] =   Long.toString(Math.round((
					(hash.get("DRE_Lair_Juros_DivCP"+"DentalByte"+listaGr.get(coluna).getId()).getValor()+hash.get("DRE_Lair_Juros_DivLP"+"DentalByte"+listaGr.get(coluna).getId()).getValor())/
					(hash.get("DRE_Lair_Juros_DivCP_vl"+"DentalByte"+listaGr.get(coluna).getId()).getValor() +hash.get("DRE_Lair_Juros_DivLP_vl"+"DentalByte"+listaGr.get(coluna).getId()).getValor()))*100))+"%";
			matriz[23][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv"+"DentalByte"+listaGr.get(coluna).getId()).getValor()));
			matriz[24][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_AtFx"+"DentalByte"+listaGr.get(coluna).getId()).getValor()));
			matriz[25][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"DentalByte"+listaGr.get(coluna).getId()).getValor()));
			matriz[26][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_Disp"+"DentalByte"+listaGr.get(coluna).getId()).getValor()));



			matriz[30][coluna+1] =   Long.toString(Math.round(hash.get("EVA"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor())); 
			matriz[31][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[32][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Roic"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[33][coluna+1] =   Long.toString(Math.round(hash.get("TSR_FCFYld_FCF_Nopat"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()));
			matriz[34][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[35][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc_CstCap_PL_tx"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[36][coluna+1] =   Long.toString(Math.round((
					(hash.get("DRE_Lair_Juros_DivCP"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()+hash.get("DRE_Lair_Juros_DivLP"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor())/
					(hash.get("DRE_Lair_Juros_DivCP_vl"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor() +hash.get("DRE_Lair_Juros_DivLP_vl"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()))*100))+"%";
			matriz[37][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()));
			matriz[38][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_AtFx"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()));
			matriz[39][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()));
			matriz[40][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_Disp"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()));

  
		}

	}
	
	
	public  void preencheValorEconomicop2(String[][] matriz)
	{ 
		int coluna=1;//,linha=2;
		//int col=1,contEmp=0;
		//cria hassh de elementos do balan�o
		    listaGr =gr.getGruposParceiros(grupo);
			Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			
			for (coluna=0;coluna<listaGr.size();coluna++)
			{
		    
				lista =es.getHerdeiros("%%","%%",periodo-1,listaGr.get(coluna).getId());
				for (Elemento e:lista){
					hash.put(e.getCampo()+e.getOrganizacao()+e.getGrupo(), e);
			}
			}			
		   for (coluna=0;coluna<listaGr.size();coluna++)
		   {
			int lin = 0;
			matriz[++lin][coluna+1] =   listaGr.get(coluna).getNome();
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA"+"CleanSystem"+listaGr.get(coluna).getId()).getValor())); 
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Roic"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("TSR_FCFYld_FCF_Nopat"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc_CstCap_PL_tx"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round((
					(hash.get("DRE_Lair_Juros_DivCP"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()+hash.get("DRE_Lair_Juros_DivLP"+"CleanSystem"+listaGr.get(coluna).getId()).getValor())/
					(hash.get("DRE_Lair_Juros_DivCP_vl"+"CleanSystem"+listaGr.get(coluna).getId()).getValor() +hash.get("DRE_Lair_Juros_DivLP_vl"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()))*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_AtFx"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_Disp"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()));


			matriz[lin+=4][coluna+1] =   Long.toString(Math.round(hash.get("EVA"+"HardTech"+listaGr.get(coluna).getId()).getValor())); 
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread"+"HardTech"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Roic"+"HardTech"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("TSR_FCFYld_FCF_Nopat"+"HardTech"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc"+"HardTech"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc_CstCap_PL_tx"+"HardTech"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round((
					(hash.get("DRE_Lair_Juros_DivCP"+"HardTech"+listaGr.get(coluna).getId()).getValor()+hash.get("DRE_Lair_Juros_DivLP"+"HardTech"+listaGr.get(coluna).getId()).getValor())/
					(hash.get("DRE_Lair_Juros_DivCP_vl"+"HardTech"+listaGr.get(coluna).getId()).getValor() +hash.get("DRE_Lair_Juros_DivLP_vl"+"HardTech"+listaGr.get(coluna).getId()).getValor()))*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv"+"HardTech"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_AtFx"+"HardTech"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"HardTech"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_Disp"+"HardTech"+listaGr.get(coluna).getId()).getValor()));

			matriz[lin+=4][coluna+1] =   Long.toString(Math.round(hash.get("EVA"+"BioMaster"+listaGr.get(coluna).getId()).getValor())); 
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread"+"BioMaster"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Roic"+"BioMaster"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("TSR_FCFYld_FCF_Nopat"+"BioMaster"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc"+"BioMaster"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc_CstCap_PL_tx"+"BioMaster"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round((
					(hash.get("DRE_Lair_Juros_DivCP"+"BioMaster"+listaGr.get(coluna).getId()).getValor()+hash.get("DRE_Lair_Juros_DivLP"+"BioMaster"+listaGr.get(coluna).getId()).getValor())/
					(hash.get("DRE_Lair_Juros_DivCP_vl"+"BioMaster"+listaGr.get(coluna).getId()).getValor() +hash.get("DRE_Lair_Juros_DivLP_vl"+"BioMaster"+listaGr.get(coluna).getId()).getValor()))*100))+"%";
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv"+"BioMaster"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_AtFx"+"BioMaster"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"BioMaster"+listaGr.get(coluna).getId()).getValor()));
			matriz[++lin][coluna+1] =   Long.toString(Math.round(hash.get("EVA_CapInv_Disp"+"BioMaster"+listaGr.get(coluna).getId()).getValor()));

			 
		}

	}

	
	public  void preencheExecucaoEstrategica(String[][] matriz)
	{ 
		//int col=1,contEmp=0;
		 int coluna;
			//cria hassh de elementos do balan�o
		    listaGr =gr.getGruposParceiros(grupo);
			Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			
			for (coluna=0;coluna<listaGr.size();coluna++)
			{
		    
				lista =es.getHerdeiros("%%","%%",periodo-1,listaGr.get(coluna).getId());
				for (Elemento e:lista){
					hash.put(e.getCampo()+e.getOrganizacao()+e.getGrupo(), e);
			}
			}			
		   for (coluna=0;coluna<listaGr.size();coluna++)
		   {
			     	
		    matriz[0][coluna+1]=listaGr.get(coluna).getNome();
		    matriz[1][coluna+1] =   Long.toString(Math.round(hash.get("EE"+"Holding"+listaGr.get(coluna).getId()).getValor()*100))+"%"; 
			matriz[2][coluna+1] =   Long.toString(Math.round(hash.get("EE_GO"+"Holding"+listaGr.get(coluna).getId()).getValor()*100))+"%"; 
			matriz[3][coluna+1] =   Long.toString(Math.round(hash.get("EE_GP"+"Holding"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[4][coluna+1] =   Long.toString(Math.round(hash.get("EE_GC"+"Holding"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[5][coluna+1] =   Long.toString(Math.round(hash.get("EE_GF"+"Holding"+listaGr.get(coluna).getId()).getValor()*100))+"%";

			matriz[7][coluna+1] =   Long.toString(Math.round(hash.get("EE"+"DentalByte"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[8][coluna+1] =   Long.toString(Math.round(hash.get("EE_GO"+"DentalByte"+listaGr.get(coluna).getId()).getValor()*100))+"%"; 
			matriz[9][coluna+1] =   Long.toString(Math.round(hash.get("EE_GP"+"DentalByte"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[10][coluna+1] =   Long.toString(Math.round(hash.get("EE_GC"+"DentalByte"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[11][coluna+1] =   Long.toString(Math.round(hash.get("EE_GF"+"DentalByte"+listaGr.get(coluna).getId()).getValor()*100))+"%";    

			matriz[13][coluna+1] =   Long.toString(Math.round(hash.get("EE"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[14][coluna+1] =   Long.toString(Math.round(hash.get("EE_GO"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()*100))+"%"; 
			matriz[15][coluna+1] =   Long.toString(Math.round(hash.get("EE_GP"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[16][coluna+1] =   Long.toString(Math.round(hash.get("EE_GC"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[17][coluna+1] =   Long.toString(Math.round(hash.get("EE_GF"+"DigiMetrics"+listaGr.get(coluna).getId()).getValor()*100))+"%";    


			matriz[19][coluna+1] =   Long.toString(Math.round(hash.get("EE"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[20][coluna+1] =   Long.toString(Math.round(hash.get("EE_GO"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()*100))+"%"; 
			matriz[21][coluna+1] =   Long.toString(Math.round(hash.get("EE_GP"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[22][coluna+1] =   Long.toString(Math.round(hash.get("EE_GC"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[23][coluna+1] =   Long.toString(Math.round(hash.get("EE_GF"+"CleanSystem"+listaGr.get(coluna).getId()).getValor()*100))+"%";    


			matriz[25][coluna+1] =   Long.toString(Math.round(hash.get("EE"+"HardTech"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[26][coluna+1] =   Long.toString(Math.round(hash.get("EE_GO"+"HardTech"+listaGr.get(coluna).getId()).getValor()*100))+"%"; 
			matriz[27][coluna+1] =   Long.toString(Math.round(hash.get("EE_GP"+"HardTech"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[28][coluna+1] =   Long.toString(Math.round(hash.get("EE_GC"+"HardTech"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[29][coluna+1] =   Long.toString(Math.round(hash.get("EE_GF"+"HardTech"+listaGr.get(coluna).getId()).getValor()*100))+"%"; 


			matriz[31][coluna+1] =   Long.toString(Math.round(hash.get("EE"+"BioMaster"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[32][coluna+1] =   Long.toString(Math.round(hash.get("EE_GO"+"BioMaster"+listaGr.get(coluna).getId()).getValor()*100))+"%"; 
			matriz[33][coluna+1] =   Long.toString(Math.round(hash.get("EE_GP"+"BioMaster"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[34][coluna+1] =   Long.toString(Math.round(hash.get("EE_GC"+"BioMaster"+listaGr.get(coluna).getId()).getValor()*100))+"%";
			matriz[35][coluna+1] =   Long.toString(Math.round(hash.get("EE_GF"+"BioMaster"+listaGr.get(coluna).getId()).getValor()*100))+"%"; 

		   }
		}
	

		
	public double lim(double x){
		if (x<-0.5) x=-0.5;
		if (x>1) x=1;
		return x;
	}
	
	public  void preencheValorAcao(String[][] matriz) throws ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException{  
		listaGr =gr.getGruposParceiros(grupo);
	    int numGrupo=0;
		for (int coluna=2;coluna<=(listaGr.size()*2);coluna+=2){
			cicloService= new CicloService(listaGr.get((numGrupo)).getId(),periodo-1);
			cicloService.initializeProxy();
			matriz[ 0][coluna]=listaGr.get((numGrupo)).getNome();
			
			matriz[ 2][coluna] = ""+Math.round(    cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varDO",  "Holding", periodo-1).getValor()*100)+"%";
			matriz[ 2][coluna+1] = ""+Math.round(0.05*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varDO",  "Holding", periodo-1).getValor()*100)+"%";
			matriz[ 3][coluna] = ""+Math.round(    cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varEE",  "Holding", periodo-1).getValor()*100)+"%";
			matriz[ 3][coluna+1] = ""+Math.round(0.10*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varEE",  "Holding", periodo-1).getValor()*100)+"%";
			matriz[ 4][coluna] = ""+Math.round(lim(cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varEVA", "Holding", periodo-1).getValor()*100))+"%";
			System.out.println("EVA Bruto"+cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varEVA",  "Holding", periodo-1).getValor());
			double calcEva=0.50*lim(cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varEVA",  "Holding", periodo-1).getValor());
			System.out.println("eva cALC"+calcEva);
			matriz[ 4][coluna+1] = ""+Math.round(calcEva*100)+"%";
			matriz[ 5][coluna] = ""+Math.round(    cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varLE",  "Holding", periodo-1).getValor()*100)+"%";
			matriz[ 5][coluna+1] = ""+Math.round(0.15*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varLE",  "Holding", periodo-1).getValor()*100)+"%";
			matriz[ 6][coluna] = ""+Math.round(    cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varPSO", "Holding", periodo-1).getValor()*100)+"%";
			matriz[ 6][coluna+1] = ""+Math.round(0.10*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varPSO",  "Holding", periodo-1).getValor()*100)+"%";
			matriz[ 7][coluna] = ""+Math.round(    cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varVAD", "Holding", periodo-1).getValor()*100)+"%";
			matriz[ 7][coluna+1] = ""+Math.round(0.10*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varVAD",  "Holding", periodo-1).getValor()*100)+"%";
			
			matriz[ 8][coluna+1] =""+Math.round(  
					
					(
					(0.05*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varDO",  "Holding", periodo-1).getValor())
					+(0.10*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varEE",  "Holding", periodo-1).getValor())
					+(calcEva)
					+(0.15*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varLE",  "Holding", periodo-1).getValor())
					+(0.10*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varPSO",  "Holding", periodo-1).getValor())
					+(0.10*cicloService.getElemento("IF_PE_ValorAcoes_Calc_var_vpa_varVAD",  "Holding", periodo-1).getValor())
					)
					*100)+"%";
					
			matriz[ 9][coluna] = ""+Math.round(    cicloService.getElemento("IF_PE_ValorAcoes",                     "Holding", periodo-2,listaGr.get((numGrupo)).getId()).getValor());
			matriz[ 10][coluna] = ""+Math.round(    cicloService.getElemento("IF_PE_ValorAcoes_Calc",                "Holding", periodo-1).getValor());
			matriz[11][coluna] = ""+Math.round(    cicloService.getElemento("BP_P_PL",                              "Holding", periodo-1,listaGr.get((numGrupo)).getId()).getValor());
			matriz[12][coluna] = ""+Math.round(    cicloService.getElemento("IF_PE_DividPAcoes_NAc",                "Holding", periodo-1,listaGr.get((numGrupo)).getId()).getValor());
			matriz[13][coluna] = ""+Math.round(    cicloService.getElemento("BP_P_PL",                              "Holding", periodo-1,listaGr.get((numGrupo)).getId()).getValor()
					                                /cicloService.getElemento("IF_PE_DividPAcoes_NAc",                "Holding", periodo-1,listaGr.get((numGrupo)).getId()).getValor());
			matriz[14][coluna] = ""+Math.round(    cicloService.getElemento("IF_PE_ValorAcoes",                     "Holding", periodo-1,listaGr.get((numGrupo)).getId()).getValor());
		    numGrupo++;
		}
	};
	
	
	
	public void preencheVariaveisFluxo(String[][] matriz, String varFluxo, String organizacao) throws ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException {  
        VariaveisFluxoService pesoVar= new VariaveisFluxoService();
        
		//int col=1,contEmp=0;
		 int coluna;
		 double fap=0;
			//cria hassh de elementos do balan�o
		    listaGr =gr.getGruposParceiros(grupo);
		    //Hashtable<String, VariaveisFluxo> hashVariaveis = new Hashtable<String, VariaveisFluxo>();
		    Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			for (coluna=0;coluna<listaGr.size();coluna++){
				lista =es.getHerdeiros("%%","%%",periodo-1,listaGr.get(coluna).getId());
				for (Elemento e:lista){
					hash.put(e.getCampo()+e.getOrganizacao()+e.getGrupo(), e);
			     }   
			}			
		   coluna=0;
		   
		   //Pesos da Composi��o da vari�veis de fluxo	
		    matriz[ 1][1] =Math.round(pesoVar.getPeso(varFluxo,"CD_Conf"   ).getImpacto()*100) + "%";
		    matriz[ 2][1] =Math.round(pesoVar.getPeso(varFluxo,"CD_Conh"   ).getImpacto()*100) + "%";
		    matriz[ 3][1] =Math.round(pesoVar.getPeso(varFluxo,"CD_Sig"    ).getImpacto()*100) + "%";
		    matriz[ 4][1] =Math.round(pesoVar.getPeso(varFluxo,"CD_Comp"   ).getImpacto()*100) + "%";
		    matriz[ 5][1] =Math.round(pesoVar.getPeso(varFluxo,"JOTA_IF"   ).getImpacto()*100) + "%";
		    matriz[ 6][1] =Math.round(pesoVar.getPeso(varFluxo,"JOTA_Pacto").getImpacto()*100) + "%";
		    matriz[ 7][1] =Math.round(pesoVar.getPeso(varFluxo,"JOTA_MC"   ).getImpacto()*100) + "%";
		    matriz[ 8][1] =Math.round(pesoVar.getPeso(varFluxo,"JOTA_PG"   ).getImpacto()*100) + "%";
		    matriz[ 9][1] =Math.round(pesoVar.getPeso(varFluxo,"PSO_E"     ).getImpacto()*100) + "%";
		    matriz[10][1] =Math.round(pesoVar.getPeso(varFluxo,"PSO_F"     ).getImpacto()*100) + "%";
		    matriz[11][1] =Math.round(pesoVar.getPeso(varFluxo,"PSO_C"     ).getImpacto()*100) + "%";
		    matriz[12][1] =Math.round(pesoVar.getPeso(varFluxo,"PSO_Res"   ).getImpacto()*100) + "%";
		    matriz[13][1] =Math.round(pesoVar.getPeso(varFluxo,"PSO_Rel"   ).getImpacto()*100) + "%";
		    matriz[14][1] =Math.round(pesoVar.getPeso(varFluxo,"CVS_F_Soc" ).getImpacto()*100) + "%";
		    matriz[15][1] =Math.round(pesoVar.getPeso(varFluxo,"CVS_F_Cli" ).getImpacto()*100) + "%";
		    matriz[16][1] =Math.round(pesoVar.getPeso(varFluxo,"CVS_F_Col" ).getImpacto()*100) + "%";
		    matriz[17][1] =Math.round(pesoVar.getPeso(varFluxo,"VAD_ACV"   ).getImpacto()*100) + "%";
		    matriz[18][1] =Math.round(pesoVar.getPeso(varFluxo,"VAD_PC"    ).getImpacto()*100) + "%";
		    matriz[19][1] =Math.round(pesoVar.getPeso(varFluxo,"VAD_VE"    ).getImpacto()*100) + "%";
		   
		    for (int i=0;i<listaGr.size();i++){
			    //o proxy j� � criado para um grupo especifico 	
		    	cicloService= new CicloService(listaGr.get(i).getId(),periodo-1);
		 		cicloService.initializeProxy();
		    	 //matriz[0][coluna+3]=listaGr.get(i).getNome();
		 		    matriz[ 1][coluna+2] =   format(hash.get("CD_Conf"+organizacao+listaGr.get(i).getId()).getValor()); 
				    matriz[ 1][coluna+3] =   format(capService.cap(hash.get("CD_Conf"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CD")); 
				    matriz[ 1][coluna+4] =   format((pesoVar.getPeso(varFluxo,"CD_Conf").getImpacto() * capService.cap(hash.get("CD_Conf"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CD")));
				    matriz[ 2][coluna+2] =   format(hash.get("CD_Conh"+organizacao+listaGr.get(i).getId()).getValor()); 
				    matriz[ 2][coluna+3] =   format(capService.cap(hash.get("CD_Conh"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CD")); 
				    matriz[ 2][coluna+4] =   format((pesoVar.getPeso(varFluxo,"CD_Conh").getImpacto() * capService.cap(hash.get("CD_Conh"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CD")));
				    matriz[ 3][coluna+2] =   format(hash.get("CD_Sig"+organizacao+listaGr.get(i).getId()).getValor());
				    matriz[ 3][coluna+3] =   format(capService.cap(hash.get("CD_Sig"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CD")); 
				    matriz[ 3][coluna+4] =   format((pesoVar.getPeso(varFluxo,"CD_Sig").getImpacto() * capService.cap(hash.get("CD_Sig"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CD")));
				    matriz[ 4][coluna+2] =   format(hash.get("CD_Comp"+organizacao+listaGr.get(i).getId()).getValor());
				    matriz[ 4][coluna+3] =   format(capService.cap(hash.get("CD_Comp"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CD")); 
				    matriz[ 4][coluna+4] =   format((pesoVar.getPeso(varFluxo,"CD_Comp").getImpacto() * capService.cap(hash.get("CD_Comp"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CD")));
				    matriz[ 5][coluna+2] =   format(hash.get("JOTA_IF"+organizacao+listaGr.get(i).getId()).getValor());
				    matriz[ 5][coluna+3] =   format(capService.cap(hash.get("JOTA_IF"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "JOTA")); 
				    matriz[ 5][coluna+4] =   format((pesoVar.getPeso(varFluxo,"JOTA_IF").getImpacto() * capService.cap(hash.get("JOTA_IF"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "JOTA")));
					matriz[ 6][coluna+2] =   format(hash.get("JOTA_Pacto"+organizacao+listaGr.get(i).getId()).getValor());
					matriz[ 6][coluna+3] =   format(capService.cap(hash.get("JOTA_Pacto"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "JOTA")); 
					matriz[ 6][coluna+4] =   format(pesoVar.getPeso(varFluxo,"JOTA_Pacto").getImpacto() * capService.cap(hash.get("JOTA_Pacto"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "JOTA"));
					matriz[ 7][coluna+2] =   format(hash.get("JOTA_MC"+organizacao+listaGr.get(i).getId()).getValor()); 
					matriz[ 7][coluna+3] =   format(capService.cap(hash.get("JOTA_MC"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "JOTA")); 
				    matriz[ 7][coluna+4] =   format((pesoVar.getPeso(varFluxo,"JOTA_MC").getImpacto() * capService.cap(hash.get("JOTA_MC"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "JOTA")));
					matriz[ 8][coluna+2] =   format(hash.get("JOTA_PG"+organizacao+listaGr.get(i).getId()).getValor());
					matriz[ 8][coluna+3] =   format(capService.cap(hash.get("JOTA_PG"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "JOTA")); 
				    matriz[ 8][coluna+4] =   format((pesoVar.getPeso(varFluxo,"JOTA_PG").getImpacto() * capService.cap(hash.get("JOTA_PG"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "JOTA")));
					matriz[ 9][coluna+2] =   format(hash.get("PSO_E"+organizacao+listaGr.get(i).getId()).getValor());
					matriz[ 9][coluna+3] =   format(capService.cap(hash.get("PSO_E"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")); 
				    matriz[ 9][coluna+4] =   format((pesoVar.getPeso(varFluxo,"PSO_E").getImpacto() * capService.cap(hash.get("PSO_E"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")));
					matriz[10][coluna+2] =   format(hash.get("PSO_F"+organizacao+listaGr.get(i).getId()).getValor());
					matriz[10][coluna+3] =   format(capService.cap(hash.get("PSO_F"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")); 
				    matriz[10][coluna+4] =   format((pesoVar.getPeso(varFluxo,"PSO_F").getImpacto() * capService.cap(hash.get("PSO_F"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")));
					matriz[11][coluna+2] =   format(hash.get("PSO_C"+organizacao+listaGr.get(i).getId()).getValor());
					matriz[11][coluna+3] =   format(capService.cap(hash.get("PSO_C"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")); 
				    matriz[11][coluna+4] =   format((pesoVar.getPeso(varFluxo,"PSO_C").getImpacto() * capService.cap(hash.get("PSO_C"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")));
					matriz[12][coluna+2] =   format(hash.get("PSO_Res"+organizacao+listaGr.get(i).getId()).getValor());    
					matriz[12][coluna+3] =   format(capService.cap(hash.get("PSO_Res"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")); 
				    matriz[12][coluna+4] =   format((pesoVar.getPeso(varFluxo,"PSO_Res").getImpacto() * capService.cap(hash.get("PSO_Res"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")));
					matriz[13][coluna+2] =   format(hash.get("PSO_Rel"+organizacao+listaGr.get(i).getId()).getValor());  
					matriz[13][coluna+3] =   format(capService.cap(hash.get("PSO_Rel"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")); 
				    matriz[13][coluna+4] =   format((pesoVar.getPeso(varFluxo,"PSO_Rel").getImpacto() * capService.cap(hash.get("PSO_Rel"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "PSO")));
					matriz[14][coluna+2] =   format(hash.get("CVS_F_Soc"+organizacao+listaGr.get(i).getId()).getValor()); 
					matriz[14][coluna+3] =   format(capService.cap(hash.get("CVS_F_Soc"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CVS")); 
				    matriz[14][coluna+4] =   format((pesoVar.getPeso(varFluxo,"CVS_F_Soc").getImpacto() * capService.cap(hash.get("CVS_F_Soc"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CVS")));
					matriz[15][coluna+2] =   format(hash.get("CVS_F_Cli"+organizacao+listaGr.get(i).getId()).getValor()); 
					matriz[15][coluna+3] =   format(capService.cap(hash.get("CVS_F_Cli"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CVS")); 
				    matriz[15][coluna+4] =   format((pesoVar.getPeso(varFluxo,"CVS_F_Cli").getImpacto() * capService.cap(hash.get("CVS_F_Cli"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CVS")));
					matriz[16][coluna+2] =   format(hash.get("CVS_F_Col"+organizacao+listaGr.get(i).getId()).getValor());  
					matriz[16][coluna+3] =   format(capService.cap(hash.get("CVS_F_Col"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CVS")); 
				    matriz[16][coluna+4] =   format((pesoVar.getPeso(varFluxo,"CVS_F_Col").getImpacto() * capService.cap(hash.get("CVS_F_Col"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "CVS")));
					matriz[17][coluna+2] =   format(hash.get("VAD_ACV"+organizacao+listaGr.get(i).getId()).getValor());   
					matriz[17][coluna+3] =   format(capService.cap(hash.get("VAD_ACV"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "VAD")); 
				    matriz[17][coluna+4] =   format((pesoVar.getPeso(varFluxo,"VAD_ACV").getImpacto() * capService.cap(hash.get("VAD_ACV"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "VAD")));
					matriz[18][coluna+2] =   format(hash.get("VAD_PC"+organizacao+listaGr.get(i).getId()).getValor());   
					matriz[18][coluna+3] =   format(capService.cap(hash.get("VAD_PC"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "VAD")); 
				    matriz[18][coluna+4] =   format((pesoVar.getPeso(varFluxo,"VAD_PC").getImpacto() * capService.cap(hash.get("VAD_PC"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "VAD")));
					matriz[19][coluna+2] =   format(hash.get("VAD_VE"+organizacao+listaGr.get(i).getId()).getValor());   
					matriz[19][coluna+3] =   format(capService.cap(hash.get("VAD_VE"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "VAD")); 
				    matriz[19][coluna+4] =   format((pesoVar.getPeso(varFluxo,"VAD_VE").getImpacto() * capService.cap(hash.get("VAD_VE"+organizacao+listaGr.get(i).getId()).getValor(), periodo-1, "VAD")));
			    
				VariaveisFluxoService vfService = new VariaveisFluxoService();
			    Elemento elementoCampo = cicloService.getElemento(varFluxo,organizacao,periodo-1,listaGr.get(i).getId());
			    matriz[0][0] = "Composi��o - "+elementoCampo.getDescricao()+"-"+organizacao;

			    //SOMA PESOS
			    double somaPesos=0;
				for (String variavel:vfService.getListaVariaveis()){
					Elemento elementoVar = cicloService.getElemento(variavel,organizacao,periodo-1);
					double valorVar = elementoVar.getValor();
					double peso = vfService.getPeso(varFluxo, variavel).getImpacto();
					StringTokenizer st = new StringTokenizer(variavel,"_");
					String capNivel1 = st.nextToken();
					somaPesos+=capService.cap(valorVar,periodo-1,capNivel1)*peso;
				}
				matriz[20][coluna+4]=Long.toString(Math.round(somaPesos*100))+"%";
	    	 	
				////////////////////////////////////////
				//           MIN, BASE E MAX          //
				////////////////////////////////////////
				double max = elementoCampo.getMaximo();
				double base = elementoCampo.getBase();
				double min = (base-(max-base));
				double resultado = 0;
				
				if ((max==0)&&(base==0)&&(min==0)){
					max = elementoCampo.getValor();
					min = elementoCampo.getValor();
					base = elementoCampo.getValor();
				}
				
				
				//VARTEC
				double varTec=0;
				resultado = somaPesos*(max-base)+base;
				resultado=resultado>max?max:resultado;
				resultado=resultado<min?min:resultado;
				Elemento es = cicloService.getElemento("DO_Es", organizacao, (periodo-1));
				
				////////////////////////////////////////
				//              FAE e FPE             //
				////////////////////////////////////////
				//FAE Fator de Alinhamento Estrat�gico UN
				//penalidade para Es abaixo de 75% em Opex
				double r=resultado;
				double fae=0,fpe=0,fco=0, fcc=0;
				if ((es.getValor()<0.75) && (varFluxo.equals("DRE_Lair_Ebit_Ebitda_RL")==false) && (varFluxo.substring(0, 25).equals("DRE_Lair_Ebit_Ebitda_Opex")==true)) {
					double penalidadeEsOpex = 0.1;
					resultado=r*(1+penalidadeEsOpex);
				}
				fae = cicloService.getDiferencaAnoAnterior(es);
				
				//aplica limites de -10% a 10%
		    	if (fae>0.10)  fae =  0.10;
		    	if (fae<-0.10) fae = -0.10;
			  			
		    	//penalidade para Es abaixo de 75%cam
		    	if (es.getValor()<0.75) fpe = -0.10;
		    	resultado*=(1+fae+fpe);
	    	    
		    	
				////////////////////////////////////////
				//              FCO e FCC             //
				////////////////////////////////////////
		    	double fao=0;
				if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_RL")==true){	 
				    //COMPENSA��o DO CAPEX E OPEX
				    double vAdmCapex = cicloService.getElemento("DUN_AO_ADM_Capex",organizacao,periodo-2).getValor();//per�odo anterior
					double vPesCapex = cicloService.getElemento("DUN_AO_Pessoal_Capex",organizacao,periodo-2).getValor();//per�odo anterior
					double vPDCapex = cicloService.getElemento("DUN_AO_PD_Capex",organizacao,periodo-2).getValor();//per�odo anterior
					double vVMktCapex = cicloService.getElemento("DUN_AO_VMkt_Capex",organizacao,periodo-2).getValor();//per�odo anterior
					double vTecCapex = cicloService.getElemento("DUN_AO_TI_Capex",organizacao,periodo-2).getValor();//per�odo anterior
					double vOperCapex = cicloService.getElemento("DUN_AO_Oper_Capex",organizacao,periodo-2).getValor();//per�odo anterior
					
					double vAdmOpex = cicloService.getElemento("DUN_AO_ADM_Opex",organizacao,periodo-2).getValor();//per�odo anterior
					double vPesOpex = cicloService.getElemento("DUN_AO_Pessoal_Opex",organizacao,periodo-2).getValor();//per�odo anterior
					double vPDOpex = cicloService.getElemento("DUN_AO_PD_Opex",organizacao,periodo-2).getValor();//per�odo anterior
					double vVMktOpex = cicloService.getElemento("DUN_AO_VMkt_Opex",organizacao,periodo-2).getValor();//per�odo anterior
					double vTecOpex = cicloService.getElemento("DUN_AO_TI_Opex",organizacao,periodo-2).getValor();//per�odo anterior
					double vOperOpex = cicloService.getElemento("DUN_AO_Oper_Opex",organizacao,periodo-2).getValor();//per�odo anterior
					
					//10% da varia��o do capex em tecnologia no ano anterior reduzir� o opex de adm e Opera��es no ano corrente
					//varTec = vTecCapex*0.1;
					
					//Fator de Compensacao para Opex
					fco = ((vAdmOpex+vPesOpex+vPDOpex+vVMktOpex+vTecOpex+vOperOpex)/6)*0.05;
					
					//Fator de Compensacao para Capex
					fcc = ((vAdmCapex+vPesCapex+vPDCapex+vVMktCapex+vTecCapex+vOperCapex)/6)*0.1;
					
					//Atualiza a Receita com a compensA��o pelo investimento realizado no per�odo anterior
					resultado= resultado*(1+fco+fcc);
				}  
				
				
				
			    
				Elemento receita = cicloService.getElemento("DRE_Lair_Ebit_Ebitda_RL", organizacao, periodo-1, listaGr.get(i).getId());
				
				double variacaoReceita = cicloService.getVariacao(receita);
				if (variacaoReceita>0.75)  variacaoReceita = 0.75;

				//FAOEs penalidade para Es abaixo de 75% em Opex
				double faoes=0;
				if (es.getValor()<0.75) {
					faoes = 0.1;
				}
				
				
				//FAOVR Impacto da varia��o da receita em Opex e CMV
				double faovr    = 0;
				double fatorEn  = 0;
				double fatorLog = 0;
				double fatorMO  = 0;
				double fatorMP  = 0;
				
				if (variacaoReceita>0){
					//OPEX
					if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_RL")==false){	
						faovr = variacaoReceita*0.5;
					}
					//CMV
					if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Cst_En")==true){	
						faovr = variacaoReceita*0.2;
			  	    }
			
			  	    if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Cst_Log")==true){ 
			  	    	faovr = variacaoReceita*0.1;
			  	    }
			  	  
			  	    if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Cst_MO")==true){
			  	    	faovr  = variacaoReceita*0.15;
			  	    }
			
			  	    if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Cst_MP")==true){
			  	    	faovr  = variacaoReceita*0.17;
			  	    }
				}
		  	    
		  	    resultado=r*(1+faovr);
		  	    fao = faoes + faovr;
				
						
				//compensacao do Opex para or�amento.
				double x;
				x=resultado;
				varTec = cicloService.getElemento("DUN_AO_TI_Capex",organizacao,periodo-2).getValor()*0.1;
				
				ModeloService template = new ModeloService(0);
				
		  	  if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Opex_Adm")==true){
		  		  double vAdmOpex = template.getElemento("DUN_AO_ADM_Opex",organizacao,periodo-1).getValor();//per�odo atual 
		  		  resultado=x*(1+vAdmOpex-varTec);
		  	  }
		  
		  	  if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Opex_Pes")==true){
		  		  double vPesOpex = template.getElemento("DUN_AO_Pessoal_Opex",organizacao,periodo-1).getValor();//per�odo atual 
		  		  resultado=x*(1+vPesOpex);
		  	  }
		  	  
		  	  if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Opex_PD")==true){ 
		  		  double vPDOpex = template.getElemento("DUN_AO_PD_Opex",organizacao,periodo-1).getValor();//per�odo atual
		  		  resultado=x*(1+vPDOpex);
		  	  }
		  	  
		  	  if(varFluxo.equals("DRE_Lair_Ebit_Ebitda_Opex_VMkt")==true){
		  		  double vVMktOpex = template.getElemento("DUN_AO_VMkt_Opex",organizacao,periodo-1).getValor();//per�odo atual
		  		   resultado=x*(1+vVMktOpex);
		  	  }
		  	  
		  	  if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Opex_Tec")==true){
		  		  double vTecOpex = template.getElemento("DUN_AO_TI_Opex",organizacao,periodo-1).getValor();//per�odo atual
		  		  resultado=x*(1+vTecOpex);
		      }
		  	  
		  	  if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Opex_Op")==true){	
		  		  double vOperOpex = template.getElemento("DUN_AO_Oper_Opex",organizacao,periodo-1).getValor();//per�odo atual
		  		  resultado=x*(1+vOperOpex-varTec);
		  	  }
			    
	  	  	  //Impacto da escolha dos projetos especiais na BioMaster (afeta o pr�ximo per�odo)
	  	  	  fap=0;
			  if (periodo-1>1){
					double DUN_PE = cicloService.getElemento("DUN_PE","BioMaster",periodo-2).getValor();
					double DO_P_TP = cicloService.getElemento("DO_P_TP","BioMaster",0).getValor();
					if (DUN_PE==DO_P_TP){
						resultado*=1.1;
						//Garantia de que a receita n�o ultrapasse o CAP
						if (max<resultado) resultado = max;
					    fap=0.1;
					} else {
						resultado*=0.95;
						//Garantia de que a receita n�o ultrapasse o CAP
						double rec_max = receita.getMaximo();
						double rec_base = receita.getBase();
						double rec_min = (rec_base-(rec_max-rec_base));
						if (rec_min >resultado) resultado = rec_min;
						fap=-0.05;
					}
			   }
			         
		
			
			//Resultado antes da limita��o do teto
			matriz[22][coluna+4] = "";
			matriz[23][coluna+4] = "";
			matriz[24][coluna+4] = "";
			matriz[25][coluna+4] = "";
			matriz[26][coluna+4] = "";
			matriz[27][coluna+4] = "";
			matriz[28][coluna+4] = "";
			matriz[29][coluna+4] = "";
			matriz[30][coluna+4] = "";
			matriz[31][coluna+4] = "";
			matriz[32][coluna+4] = "";
			matriz[33][coluna+4] = "";
			matriz[34][coluna+4] = "";
			matriz[35][coluna+4] = "";
			matriz[36][coluna+4] = "";
			matriz[37][coluna+4] = "";
			matriz[38][coluna+4] = "";
			
			
			matriz[36][coluna+4] = ""+Math.round(resultado);
			
			
			resultado = resultado>max?max:resultado;	
			if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_RL")==true)
			{	
		   	   matriz[22][coluna+4] = Long.toString(Math.round(fae*100))+"%";//FAE
		       matriz[23][coluna+4] = Long.toString(Math.round(fpe*100))+"%";//FPE
			}
		
			if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_RL")==false)
			{
		      matriz[24][coluna+4] = ""+Math.round(fao*1000)/10.0+"%";      //FAO
			}
			if (varFluxo.equals("DRE_Lair_Ebit_Ebitda_RL")==true)
			{
			  matriz[25][coluna+4] = ""+Math.round(fco*1000)/10.0+"%";      //FCO
		      matriz[26][coluna+4] = ""+Math.round(fcc*1000)/10.0+"%";      //FCC
		      matriz[27][coluna+4] = ""+Math.round(fap*1000)/10.0+"%";      //FAP
			}
			if ((varFluxo.equals("DRE_Lair_Ebit_Ebitda_Opex_Op")==true) || (varFluxo.equals("DRE_Lair_Ebit_Ebitda_Opex_Adm")==true))
			{
			  matriz[28][coluna+4] = ""+Math.round(varTec*1000)/10.0+"%";    //FVTec
		   	}
			matriz[30][coluna+4] = Long.toString(Math.round(max));        //Max
		    matriz[31][coluna+4] = Long.toString(Math.round(base));       //base
		    matriz[32][coluna+4] = Long.toString(Math.round(min));    //Min
		    
		    
		    if ((max==base)&& (base==min )&&(min==max))
			{
		    	matriz[30][coluna+4] = "";        //Max
			    matriz[31][coluna+4] = "";       //base
			    matriz[32][coluna+4] = "";    //Min
			    
			
			}
			
		    
		    		    
		    matriz[34][coluna+4] = ""+Math.round(cicloService.getElemento("DRE_Lair_Ebit_Ebitda_RL",organizacao,periodo-2,listaGr.get(i).getId()).getValor()); //Resultado anterior
		    matriz[35][coluna+4] = ""+Math.round(cicloService.getVariacao(cicloService.getElemento("DRE_Lair_Ebit_Ebitda_RL",organizacao,periodo-1,listaGr.get(i).getId()))*100)+"%"; //Varia��o total no per�odo
		    matriz[38][coluna+4] = ""+Math.round(resultado);              //Resultado Calculado
					  	
			coluna=coluna+3;
	     }
     };
	
	public   void desenhaRelatorio(int numeroColunas, String[][] matriz, String[][] formato , int[] widths, boolean marcar,int relatorio) throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException
	{       
		double maior=0,menor=101;
		PdfPCell maiorCelula =new PdfPCell();
		PdfPCell menorCelula =new PdfPCell();
		switch(relatorio)
		{
		case 0: break;
		case 1: preencheExecucaoEstrategica(matriz);break;
		//case 2: preencheRetornoAcionista(matriz);break;
		case 3: preencheValorEconomicop1(matriz);break;
		case 4: preencheValorEconomicop2(matriz);break;
		case 5: preencheValorAcao(matriz);break;
		case 6: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_RL","Holding");break;
		case 7: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_RL",empresas[0]);break;
		case 8: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_RL",empresas[1]);break;
		case 9: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_RL",empresas[2]);break;
		case 10: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_RL",empresas[3]);break;
		case 11: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_RL",empresas[4]);break;
		case 12: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MP","Holding");break;
		case 13: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MP",empresas[0]);break;
		case 14: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MP",empresas[1]);break;
		case 15: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MP",empresas[2]);break;
		case 16: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MP",empresas[3]);break;
		case 17: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MP",empresas[4]);break;
		case 18: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MO","Holding");break;
		case 19: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MO",empresas[0]);break;
		case 20: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MO",empresas[1]);break;
		case 21: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MO",empresas[2]);break;
		case 22: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MO",empresas[3]);break;
		case 23: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_MO",empresas[4]);break;
		case 24: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_En","Holding");break;
		case 25: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_En",empresas[0]);break;
		case 26: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_En",empresas[1]);break;
		case 27: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_En",empresas[2]);break;
		case 28: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_En",empresas[3]);break;
		case 29: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_En",empresas[4]);break;
		case 30: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_Log","Holding");break;
		case 31: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_Log",empresas[0]);break;
		case 32: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_Log",empresas[1]);break;
		case 33: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_Log",empresas[2]);break;
		case 34: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_Log",empresas[3]);break;
		case 35: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Cst_Log",empresas[4]);break;
		case 36: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Adm","Holding");break;
		case 37: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Adm",empresas[0]);break;
		case 38: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Adm",empresas[1]);break;
		case 39: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Adm",empresas[2]);break;
		case 40: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Adm",empresas[3]);break;
		case 41: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Adm",empresas[4]);break;
		case 42: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Pes","Holding");break;
		case 43: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Pes",empresas[0]);break;
		case 44: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Pes",empresas[1]);break;
		case 45: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Pes",empresas[2]);break;
		case 46: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Pes",empresas[3]);break;
		case 47: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Pes",empresas[4]);break;
		case 48: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_PD","Holding");break;
		case 49: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_PD",empresas[0]);break;
		case 50: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_PD",empresas[1]);break;
		case 51: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_PD",empresas[2]);break;
		case 52: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_PD",empresas[3]);break;
		case 53: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_PD",empresas[4]);break;
		case 54: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_VMkt","Holding");break;
		case 55: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_VMkt",empresas[0]);break;
		case 56: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_VMkt",empresas[1]);break;
		case 57: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_VMkt",empresas[2]);break;
		case 58: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_VMkt",empresas[3]);break;
		case 59: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_VMkt",empresas[4]);break;
		case 60: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Tec","Holding");break;
		case 61: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Tec",empresas[0]);break;
		case 62: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Tec",empresas[1]);break;
		case 63: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Tec",empresas[2]);break;
		case 64: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Tec",empresas[3]);break;
		case 65: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Tec",empresas[4]);break;
		case 66: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Op","Holding");break;
		case 67: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Op",empresas[0]);break;
		case 68: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Op",empresas[1]);break;
		case 69: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Op",empresas[2]);break;
		case 70: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Op",empresas[3]);break;
		case 71: preencheVariaveisFluxo(matriz,"DRE_Lair_Ebit_Ebitda_Opex_Op",empresas[4]);break;
		}
		//Relat�rios Financeiros - BMI Simulador Octopus
		//determina o numero de colunas da tabela
		//cria a tabela com o numero de colunas passado
		PdfPTable table = new PdfPTable(numeroColunas);
		//seta o tamanho das colunas de acordo com o vetor de tamanho passado
		table.setWidths(widths);

		// document.add(linebreak);
//		document.add(new Paragraph(" "));
		for (int i = 0; i<matriz.length; i++){
			for (int j = 0; j<matriz[i].length; j++){
				StringTokenizer st = new StringTokenizer(formato[i][j]);
				char tipo=st.nextToken().charAt(0);
				int tamanho = Integer.parseInt(st.nextToken());
				char align=st.nextToken().charAt(0);
				int padding = Integer.parseInt(st.nextToken());
				float espessura =Float.parseFloat(st.nextToken());
				Font f = new Font();
				//seta o tipo de formatA��o da fonte
				f.setSize(tamanho);
				switch(tipo){
				case 'I': f.setStyle(Font.ITALIC);break;
				case 'B': f.setStyle(Font.BOLD);break;
				}
				Chunk c = new Chunk(matriz[i][j]);
				//System.out.println("primeira"+matriz[i][j]); 
				c.setFont(f);
				Phrase ph = new Phrase();
				ph.add(c);
				//configura a celula para bordas, espessura da borda e espa�amento antes e depois da c�lula.
				PdfPCell cell = new PdfPCell(ph);
				cell.setBorder(Rectangle.BOTTOM);
				cell.setBorderWidth(espessura);
				cell.setPaddingTop(padding);
				cell.setPaddingBottom(padding);
				//if ((i==2)&&(j==2)) cell.setBackgroundColor(BaseColor.ORANGE);
				//configura o alinhamento do texto
				switch(align){
				case 'R': cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);break;
				case 'L': cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);break;
				case 'C': cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);break;
				}
				//faz uma mescla nas c�lulas da primeira linha
				//  if ((j==0) && (i==0)){
				// cell.setColspan(numeroColunas-2);
				// }

				table.addCell(cell);
				//guarda a maior e a menor c�lula da linha
				if (marcar ==true)
				{
					//System.out.println("marcar" +matriz[i][j]); 
					try
					{
						if(Double.parseDouble(matriz[i][j].replace("%","")) > maior)  
						{ maior= Double.parseDouble(matriz[i][j].replace("%",""));
						//System.out.println("maior:" +maior);
						maiorCelula=cell ;
						//System.out.println(cell);
						}
						if(Double.parseDouble(matriz[i][j].replace("%",""))  < menor)
						{ menorCelula=cell ;
						menor= Double.parseDouble(matriz[i][j].replace("%",""));
						//System.out.println("menor:" +menor);
						//System.out.println(cell);
						}  
					}
					catch(Exception e)
					{//System.out.println("n�o �numero" +matriz[i][j]);}
					}
				}
			}
			if (marcar ==true)
			{ 
				maiorCelula.setBackgroundColor(BaseColor.ORANGE);
				menorCelula.setBackgroundColor(BaseColor.RED);   
			}
		}
		document.add(table);
		maior=0;
		menor=101;
	}

	public void inicio() throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException  
	{ 
		System.out.println("IN�CIO Gera��o RELAT�RIO DO FACILITADOR");
		//this.organizacao = organizacao;
		//CriA��o do Relat�rio

		// criA��o do DR

		try {

			//LineSeparator separator = new LineSeparator();
			//separator.setLineWidth((float) 1);
			//Chunk linebreak = new Chunk(separator);
			document.setPageSize(PageSize.A4.rotate());

			//Verifica se o diret�rio da turma existe
			int turma = gr.getGrupo(grupo).getTurma();
			File dir_turma = new File("turma_"+turma);
			if (!dir_turma.exists()){
				dir_turma.mkdir();
			}
			
//			File dir_grupo = new File("turma_"+turma+"/grupo_"+grupo);
//			if (!dir_grupo.exists()){
//				dir_grupo.mkdir();
//			}

			ConfigService configService = new ConfigService();
			Config config = configService.getConfig(grupo);
			periodo = config.getPeriodo();

		    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("turma_"+turma+"/"+"Facilitador"+"_periodo" +(periodo-1)+".pdf"));
			writer.setPageEvent(new CabecalhoRodape("turma_"+turma, periodo-1," Relat�rio do Facilitador"));
			document.open();
			document.addSubject("Simulador Eagle - BMI"); 
			document.addKeywords("www.bluemanagement.institute");
			document.addCreator("Eagle");
			document.addAuthor("BMI Tecnologia");
			Image img1 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/biomaster.png");
			Image img11 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/octopus.png");
			Image img2 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/cleansystem.png");
			Image img3 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/dentalbyte.png");
			Image img4 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/digimetrics.png");
			Image img5 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/hardtech.png");
			PdfContentByte cb = writer.getDirectContent();
			Phrase header = new Phrase("Relat�rio do facilitador da turma "+ turma + " - do periodo: "+""+(periodo-1)+  " - BioMaster " ,  new Font(Font.FontFamily.UNDEFINED, 14, Font.BOLD));
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header,(document.right() - document.left()) / 2 + document.leftMargin() ,document.top() - 30, 0);
			img1.scaleToFit(100,100);
			img11.scaleToFit(70,70);
			img2.scaleToFit(70,70);
			img3.scaleToFit(70,70);
			img4.scaleToFit(70,70);
			img5.scaleToFit(70,70);
			img1.setAbsolutePosition(350,350);
			img11.setAbsolutePosition(180,200);
			img2.setAbsolutePosition(280,200);
			img3.setAbsolutePosition(380,200);
			img4.setAbsolutePosition(480,200);
			img5.setAbsolutePosition(580,200);
			document.add(img1);
			document.add(img11);
			document.add(img2);
			document.add(img3);
			document.add(img4);
			document.add(img5);
		}
		//  document.add(linebreak);
		catch(DocumentException de) {
			System.err.println(de.getMessage());
		}
		catch(IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		//P�gina DO
		document.newPage();
		
		//P�gina EE
		document.newPage();
		this.desenhaRelatorio(6, execucaoEstrategica, execucaoEstrategicaFormato,new int[]{10,3,3,3,3,3},false,1);      
     	document.newPage();
//		this.desenhaRelatorio(6, retornoAcionista, retornoAcionistaFormato,new int[]{10,3,3,3,3,3},false,2);
//		//P�gina EVA (p1)
//		document.newPage();
		espaco();
		this.desenhaRelatorio(6, valorEconomicop1, valorEconomicoFormatop1,new int[]{10,3,3,3,3,3},false,3);   
		//P�gina EVA (p2)
		document.newPage();
		espaco();
		this.desenhaRelatorio(6, valorEconomicop2, valorEconomicoFormatop2,new int[]{10,3,3,3,3,3},false,4);   
		document.newPage();
		espaco();
		this.desenhaRelatorio(12, valorAcao, valorAcaoFormato,new int[]{6,3,3,3,3,3,3,3,3,3,3,3},false,5);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,6);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,7);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,8);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,9);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,10);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,11);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,12);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,13);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,14);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,15);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,16);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,17);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,18);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,19);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,20);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,21);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,22);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,23);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,24);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,25);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,26);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,27);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,28);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,29);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,30);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,31);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,32);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,33);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,34);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,35);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,36);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,37);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,38);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,39);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,40);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,41);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,42);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,43);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,44);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,45);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,46);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,47);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,48);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,49);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,50);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,51);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,52);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,53);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,54);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,55);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,56);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,57);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,58);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,59);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,60);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,61);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,62);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,63);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,64);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,65);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,66);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,67);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,68);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,69);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,70);
		document.newPage();
		espaco();
		subTituloGrupos();
		this.desenhaRelatorio(17, variavelFluxo, variavelFluxoFormato,new int[]{10,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},false,71);
		
		document.close();    
		
		System.out.println("FIM Gera��o DO RELAT�RIO DO FACILITADOR");
	} 
	
	public void tituloCapa(String descricao, String comentario) throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException{
		String[][] titulo = {{descricao,comentario}};
		String[][] formato = {{"B 14 C 0 0","N 6 C 0 0"}};
		int[] larguras = {3,1};
		espaco();
		this.desenhaRelatorio(2,titulo, formato, larguras, false, 0);
		espaco();

	}
	
	public void titulo(String descricao, String comentario) throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException{
		String[][] titulo = {{descricao,comentario}};
		String[][] formato = {{"B 8 L 0 0","N 6 R 0 0"}};
		int[] larguras = {3,1};
		espaco();
		this.desenhaRelatorio(2,titulo, formato, larguras, false, 0);
		espaco();
	}
	
	public void subTituloDO(int primeiraColuna) throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException{
		String[][] titulo = {{"","Excel�ncia em\nOperA��o", "Excel�ncia em\nInovA��o", "Excel�ncia em\nCustomizA��o", "Excel�ncia em\nIntegrA��o"}};
		String[][] formato = {{"N 8 L 1 0","B 8 C 1 0","B 8 C 1 0","B 8 C 1 0","B 8 C 1 0"}};
		int[] larguras = {primeiraColuna,7,7,7,7};
		this.desenhaRelatorio(5,titulo, formato, larguras, false, 0);
	}
	
	
	public void subTituloMultiNegocio() throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException{
		String[][] titulo = {{"Perfil MultiNeg�cio", "Situa��es Estrat�gicas", "N�vel de Centraliza��o Funcional", "Perfil Estrat�gico","HHI"}};
		String[][] formato = {{"B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0"}};
		int[] larguras = {4,2,8,2,3};
		this.desenhaRelatorio(5,titulo, formato, larguras, false, 0);
	}
	
	
	public void rodapeMultiNegocio() throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException{
		String[][] rodape = {{"1-Portfolio Management", "2-Strategic Design", "3-Operational Check & Balance", "4-Functional Alignment","5-Corporate Engagement"}};
		String[][] formato = {{"B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0"}};
		int[] larguras = {6,6,6,6,6};
		this.desenhaRelatorio(5,rodape, formato, larguras, false, 0);
	}
	public void subTituloATL() throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException{
		String[][] titulo = {{" ",empresas[0],empresas[1],empresas[2],empresas[3],empresas[4]}};
		String[][] formato = {{"N 8 L 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0"}};
		int[] larguras = {3,1,1,1,1,1};
		this.desenhaRelatorio(6,titulo, formato, larguras, false, 0);
	}
	
	
	public void subTituloGrupos() throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException, ProxyGrupoIncorretoException{
		//SEMPRE CRia os vetores com tamanho maximo 
		String[][] titulo = new String[1][6];	
		String[][] formato = new String[1][6];
		int[] larguras = new int [6] ;
		int i;
		for (i =0 ; i <=listaGr.size();i++)
		{
		if (i==0)
		 titulo[0][0]=" ";
		else			
		 titulo[0][i]=listaGr.get(i-1).getNome();
	    
		formato[0][i]="B 8 C 0 0";
	    if (i==0)
	    larguras[i]=6;
	    else
	     larguras[i]=3;	
		}
		
		//completa o formato do subtitulo
		for (int k=i; k<6;k++)
		{
			titulo[0][k]=" ";
			larguras[k]=3;
			formato[0][k]="B 8 C 0 0";
		    
		}
//		//completa subtitulo se 3 grupos
//		if (i<4){
//			titulo[0][3]=" ";
//			larguras[3]=3;
//			formato[0][3]="B 8 C 0 0";
//		    titulo[0][4]=" ";
//	    	larguras[4]=3;
//		    formato[0][4]="B 8 C 0 0";
//	     }
//		//completa subtitulo se 4 grupos
//		if (i<5){
//			titulo[0][5]=" ";
//		    larguras[5]=3;
//		    formato[0][5]="B 8 C 0 0";}
		
		this.desenhaRelatorio(6,titulo, formato, larguras, false, 0);
	}
	
	
	
	public void espaco() throws DocumentException{
		document.add(new Paragraph(" "));
//		String[][] rotulo = {{""}};
//		String[][] formato = {{"B 8 L 0 0"}};
//		int[] larguras = {1};
//		this.desenhaRelatorio(1,rotulo, formato, larguras, false, 0);
	}

}