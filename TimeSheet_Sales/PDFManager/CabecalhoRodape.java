package PDFManager;

import java.io.IOException;
import java.net.MalformedURLException;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
//XXX
public class CabecalhoRodape extends PdfPageEventHelper {
	        Font ffont = new Font(Font.FontFamily.UNDEFINED, 8, Font.ITALIC);
	        String nomeGrupo; 
	        int periodo;
	        String organizacao;
	        CabecalhoRodape(String nomeGrupo, int periodo, String organizacao)
	        {
	        	
	           this.nomeGrupo=nomeGrupo;
	           this.periodo=periodo;
	           this.organizacao=organizacao;
	        }
	        
	        
	        public void onEndPage(PdfWriter writer, Document document) {
	            Image img = null;
				try {
				 img = Image.getInstance("../applications/BMI_SIM_Eagle/img/logos/logo-eagle-pequeno-preto.png");
				} catch (BadElementException e) {
					e.printStackTrace();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} 
				{
			    img.scaleToFit(50,50);
			    img.setAbsolutePosition(20,500);
			    PdfContentByte cb = writer.getDirectContent();
			    PdfContentByte cb1 = writer.getDirectContent();
			    PdfContentByte cb2 = writer.getDirectContent();
//	            Chunk c= new Chunk(img,50,50);
//	            Phrase paraImg = new Phrase(c);
	            Phrase pagina =new Phrase(Integer.toString(document.getPageNumber()));
	        	Font f = new Font();
	        	f.setSize(10);
	        	pagina.setFont(f);
	            Phrase header = new Phrase(nomeGrupo + " - Periodo: "+""+(periodo)+ " - "+organizacao , ffont);
	            Phrase footer = new Phrase("BMI\u00A9. Todos os direitos reservados.", ffont);
	          
	            try {
					document.add(img);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
	         
	            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
	                    header,
	                    (document.right() - document.left()) / 2 + document.leftMargin() ,
	                    document.top() + 10, 0);
	            ColumnText.showTextAligned(cb1, Element.ALIGN_CENTER,
	                    footer,
	                    (document.right() - document.left()) / 2 + document.leftMargin(),
	                    document.bottom() - 10, 0);
	        
	            ColumnText.showTextAligned(cb2, Element.ALIGN_CENTER,
	                    pagina,
	                    (document.rightMargin()),
	                    document.bottom() - 10, 0);
//	            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
//	                    paraImg,
//	                    (document.right() - document.left()) / 2 + document.leftMargin() ,
//	                    document.top() + 40, 0);
//	            
	    }
	        
}
	
}

