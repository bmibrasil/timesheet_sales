package PDFManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import Actions.TMN_Action;
import Beans.Config;
import Beans.Elemento;
import Beans.Grupo;
import Beans.LE;
import Bundle.ReceitaEsperadaBundle;
import Exceptions.ElementoNaoEncontradoException;
import Exceptions.ProxyGrupoIncorretoException;
import Exceptions.ProxyNaoIniciadoException;
import Service.CicloService;
import Service.ConfigService;
import Service.ElementoService;
import Service.GrupoService;
import Service.LEService;
import Service.ModeloService;
import Util.AvaliacaoReceita;
import Util.Util;

public class PDFGenerator {
	private int grupo;
	private String nomeGrupo;
	private Document document;
	private ElementoService es;
	private LEService les;
	private GrupoService gr;
	private int periodo;
	private String organizacao;  
	private List<Elemento> lista; 
	private List<LE> listaLE;
	private List<Grupo> listaGr; 
	private String empresas[] = { "BioMaster", "CleanSystem", "DentalByte", "DigiMetrics", "HardTech" };
	private CicloService cicloService;

	public PDFGenerator(String organizacao, int grupo, String nomeGrupo){
		this.grupo=grupo;
		this.nomeGrupo=nomeGrupo;
		document = new Document();
		es= new ElementoService();
		les= new LEService(grupo);
		gr= new GrupoService();
		ConfigService configService = new ConfigService();
		Config config = configService.getConfig(grupo);
		periodo = config.getPeriodo();
		this.organizacao=organizacao;  
		lista=new ArrayList<Elemento>(); 
		listaLE=new ArrayList<LE>();
		listaGr=new ArrayList<Grupo>(); 
	}

	public PDFGenerator(String organizacao, Config config, int periodo){
		this.grupo = config.getGrupo();
		config.setPeriodo(periodo);
		this.nomeGrupo=config.getNome();
		this.periodo = periodo;
		document = new Document();
		es= new ElementoService();
		les= new LEService(grupo);
		gr= new GrupoService();
		this.organizacao=organizacao;  
		lista=new ArrayList<Elemento>(); 
		listaLE=new ArrayList<LE>();
		listaGr=new ArrayList<Grupo>(); 
	}

	private  String[][] DRE= {
			//Demonstrativo de Resultado
			//{"Demonstrativo de Resultados","","","","","","","","","","","","","", "Valores em $ Milh�es"},

			{"","P -2","RL%","\u2206 %","P -1","RL%","\u2206 %", "P 0","RL%","\u2206 %","P 1","RL%","\u2206 %","P 2","RL%","\u2206 %","P 3","RL%","\u2206 %","P 4","RL%","\u2206 %"},
			{"Receita l�quida","","","","","","","","","","","","","","","","","","","","",""},   
			{"Custos","","","","","","","","","","","","","", "","","","","","","",""},
			{"  M�o de Obra","","","","","","","","","","","","","", "","","","","","","",""},
			{"  Mat�ria Prima","","","","","","","","","","","","","", "","","","","","","",""},
			{"  Energia","","","","","","","","","","","","","", "","","","","","","",""},
			{"  Log�stica","","","","","","","","","","","","","", "","","","","","","",""},
			{"LUCRO BRUTO","","","","","","","","","","","","","", "","","","","","","",""},
			{"Opex","","","","","","","","","","","","","", "","","","","","","",""},
			{"  Pessoal","","","","","","","","","","","","","", "","","","","","","",""},
			{"  Administrativo","","","","","","","","","","","","","", "","","","","","","",""},
			{"  Tecnologia","","","","","","","","","","","","","", "","","","","","","",""},
			{"  Vendas e Marketing","","","","","","","","","","","","","", "","","","","","","",""},
			{"  P&D","","","","","","","","","","","","","","","","","","","","",""},
			{"  Opera��es","","","","","","","","","","","","","","","","","","","","",""},        
			{"EBITDA","","","","","","","","","","","","","", "","","","","","","",""},
			{"Deprecia��o","","","","","","","","","","","","","", "","","","","","","",""},
			{"EBIT","","","","","","","","","","","","","", "","","","","","","",""},
			{"Resultado n�o operacional","","","","","","","","","","","","","", "","","","","","","",""},
			{"  Rendimento de Aplica��es","","","","","","","","","","","","","", "","","","","","","",""},
			{"  Multas","","","","","","","","","","","","","", "","","","","","","",""},
			{"Juros","","","","","","","","","","","","","", "","","","","","","",""}, 
			{"Impostos","","","","","","","","","","","","","", "","","","","","","",""},
			{"LUCRO L�QUIDO","","","","","","","","","","","","","", "","","","","","","",""},  
	}; 
	private  String [] [] balanco={
			//Balan�o Patrimonial
			//{" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
			//{"Balan�o Patrimonial","","","","","","","","","","","","","", "Valores em $ Milh�es"},
			{"","P -2","%","P -1","%","P 0","%","P 1","%","P 2","%","P 3","%","P 4","%"},
			{"Caixa",                  "","","","","","","","","","","","","",""},
			{"Contas a Receber",       "","","","","","","","","","","","","",""},
			{"Estoque",                "","","","","","","","","","","","","",""},   
			{"Aplica��es Financeiras", "","","","","","","","","","","","","",""},     
			{"Ativo Fixo",             "","","","","","","","","","","","","",""},  
			{"(-)Deprecia��o Acumulada","","","","","","","","","","","","","",""},   
			{"Goodwill",               "","","","","","","","","","","","","",""}, 
			{"TOTAL ATIVO",            "","","","","","","","","","","","","",""},
			{"Contas a Pagar",           "","","","","","","","","","","","","",""},     
			{"D�vida de Curto Prazo",  "","","","","","","","","","","","","",""},  
			{"D�vida de Longo Prazo",  "","","","","","","","","","","","","",""},   
			{"Reserva de Lucro",       "","","","","","","","","","","","","",""}, 
			{"Capital Social",         "","","","","","","","","","","","","",""},
			{"TOTAL PASSIVO",          "","","","","","","","","","","","","",""},
	}; 
	private  String[][] roe={ 
			//ROE - Arvore de Desempenho
			//{""," "," "," "," "," "," "," "},
			//{"�rvore de Desempenho"," "," "," "," "," "," "," "},
			{"","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"ROE",                      " "," "," "," "," "," "," "},
			{"Margem de Lucro",          " "," "," "," "," "," "," "},
			{"Giro Ativo",               " "," "," "," "," "," "," "},
			{"Alavancagem Financeira",   " "," "," "," "," "," "," "},
	};
	// Matriz de BSC
	private  String[][] BSC= {
			{"Painel de Indicadores","","","","","",""},
			{"","Periodo Anterior","Realizado","Meta","Percentual Realizado", "Peso","Indicador Final"},
			{"","A","B","C","B/C","E","F"},
			{"Receita l�quida - Holding","","","","","",""},
			{"Pre�o da a��o - Holding","","","","","",""},
			{"Receita l�quida - BioMaster","","","","","",""},
			{"EBITDA - BioMaster","","","","","",""},
			{"Necessidade de Capital de Giro - BioMaster","","","","","",""},		
			{"Satisfa��o de Clientes - BioMaster","","","","","",""},
			{"Satisfa��o da Sociedade - BioMaster","","","","","",""},
			{"Satisfa��o da Equipe - BioMaster","","","","","",""},
			{"Execu��o Estrat�gica - BioMaster","","","","","",""},
			{"Pilares da Sustenta��o - BioMaster","","","","","",""},
			{"Total","","","","","",""},
			{"Regra Indicador Final:", "Perc. Realizado < 80%: 0","Perc. Realizado >= 80% e < 120%: Valor do Perc.","Perc. Realizado >= 120: 120","","",""}
	};
	// Matriz de BSC Comparativo
	private  String[][] BSCComp= {
			{" "," "," "," "," "," "," "," "," "," "," "},
			{"Painel de indicadores Comparativo","","","","","","","","","",""},
			{"","","","","", "","","", "", "", ""},
			{"Receita l�quida - Holding","","","","","","","","","","" },
			{"Pre�o da a��o - Holding","","","","","","","","","",""},
			{"Receita l�quida - BioMaster","","","","","","","","","","" },
			{"EBITDA - BioMaster","","","","","","","","","",""},
			{"Necessidade de Capital de Giro - BioMaster","","","","","","","","","",""},
			{"Satisfa��o de Clientes - BioMaster","","","","","","","","","",""},
			{"Satisfa��o da Sociedade - BioMaster","","","","","","","","","",""},
			{"Satisfa��o da Equipe - BioMaster","","","","","","","","","",""},
			{"Execu��o Estrat�gica - BioMaster","","","","","","","","","",""},
			{"Pilares da Sustenta��o - BioMaster","","","","","","","","","",""},
			{"Total","","","","","","","","","",""}
	};
	
	private  String[][] Do= {
			//Din�mica Organizacional
			//{"Din�mica Organizacional","","","","","","","","","","","",""},
			//{"","","Excel�ncia em OperA��o","","","Excel�ncia em InovA��o","","","Excel�ncia em CustomizA��o","","","Excel�ncia em IntegrA��o",""},
			{"", "Anterior","Atual","Alvo P4","","Anterior","Atual","Alvo P4","","Anterior","Atual","Alvo P4","","Anterior","Atual","Alvo P4"},
			{"DentalByte",  "","","", "", "","","", "", "","","", "", "","",""},
			{"Digimetrics", "","","", "", "","","", "", "","","", "", "","",""},
			{"CleanSystem", "","","", "", "","","", "", "","","", "", "","",""},
			{"HardTech",    "","","", "", "","","", "", "","","", "", "","",""},
			{"BioMaster",   "","","", "", "","","", "", "","","", "", "","",""},
	};
	
	
	private  String[][] Dov= {
			//Din�mica Organizacional Valores
			//{"Din�mica Organizacional","","","","","","","","","","","",""},
			//{"","","Excel�ncia em OperA��o","","","Excel�ncia em InovA��o","","","Excel�ncia em CustomizA��o","","","Excel�ncia em IntegrA��o",""},
			{"", "Estrat.","Cult.","Estrut.","Compet.","",  "Estrat.","Cult.","Estrut.","Compet.","", "Estrat.","Cult.","Estrut.","Compet.","", "Estrat.","Cult.","Estrut.","Compet."},
			{"DentalByte",  "","","","","","","","","","","","","","","","","","",""},
			{"Digimetrics", "","","","","","","","","","","","","","","","","","",""},
			{"CleanSystem", "","","","","","","","","","","","","","","","","","",""},
			{"HardTech",    "","","","","","","","","","","","","","","","","","",""},
			{"BioMaster",   "","","","","","","","","","","","","","","","","","",""}
	};
	
	
	private  String[][] TipoMult= {
			{"Alvo","Real","","Resultado","","FIN","RH", "TEC","PRJ","Resultado","","Resultado","","Receita","Ebitda"},
			{"","","","","","","", "","","","","","","",""},
	};
	

	
	private  String[][] Proposito= {
			{"Tipo de Proposito","Indicador 1","Indicador 2","Resultado"},
			{"","","",""}
	};
	
	
	private  String[][] lideranca=
		{   {"Lideran�a Essencial","","","","","","",""},
				{"","Holding","Holding","DentalByte","DigiMetrics","CleanSystem","HardTech","BioMaster"},
				{"","Presidente","CFO","CEO","CEO","CEO","CEO","CEO"},
				{"Lideran�a Essencial","","","","","","",""},
				{"Ess�ncia Individual","","","","","","",""},
				{"Autenticidade","","","","","","",""},
				{"Consci�ncia Plena","","","","","","",""},
				{"Intelig�ncia Emocional","","","","","","",""},
				{"Integridade","","","","","","",""},
				{"Excel�ncia Pessoal","","","","","","",""},
				{"Alinhamento","","","","","","",""},
				{"Coes�o","","","","","","",""},
				{"Execu��o","","","","","","",""},
				{"Mobiliza��o","","","","","","",""},
				{"Transforma��o Organizacional","","","","","","",""},
				{"Espiral da Excel�ncia","","","","","","",""},
				{"Gest�o da Sucess�o","","","","","","",""},
				{"Jornada da Transforma��o","","","","","","",""},
				{"Pilares Sustenta��o Organizacional","","","","","","",""}
		};
	private  String[][]catalisadores={
			{"Catalisadores de Desempenho","Holding","DentalByte","DigiMetrics","CleanSystem","HardTech","BioMaster"},
			{"Catalisadores de Desempenho","","","","","",""},
			{"Comprometimento","","","","","",""},
			{"Confiabilidade","","","","","",""},
			{"Conhecimento","","","","","",""},
			{"Significado","","","","","",""},

	};
	private  String[][] vetores={
			{"Vetores de Alto Desempenho","Holding","DentalByte","DigiMetrics","CleanSystem","HardTech","BioMaster"},
			{"Vetores de Alto Desempenho","","","","","",""},
			{"Ader�ncia a Cren�as e Valores","","","","","",""},
			{"Produtividade Consistente","","","","","",""},
			{"V�nculo Emocional","","","","","",""},


	};
	private  String stakeholders[][]={
			{"Cria��o de Valor para Stakeholders","Holding","DentalByte","DigiMetrics","CleanSystem","HardTech","BioMaster"},
			{"Cria��o de Valor para Stakeholders","","","","","","",},
			{"Acionistas","","","","","",""},
			{"Clientes","","","","","",""},
			{"Colaboradores","","","","","","",},
			{"Sociedade","","","","","","",},

	};


	private  String valorEconomicop1[][]={
			//{" "," "," "," ", " "," "," "," "},
			{"Gera��o de Lucro Econ�mico - Holding"," "," "," "," "," "," "," "},
			{"","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Lucro Econ�mico",   "","","", "","","",""},
			{"Spread",   "","","", "","","",""},
			{"RONA",   "","","", "","","",""},
			{"NOPAT",   "","","", "","","",""},
			{"WACC",   "","","", "","","",""},
			{"Custo Capital Pr�prio",   "","","", "","","",""},
			{"Custo Capital Terceiros",   "","","", "","","",""},
			{"Capital Investido",   "","","", "","","",""},
			{"Ativo Fixo",   "","","", "","","",""},
			{"Capital de Giro L�quido",   "","","", "","","",""},
			{"Disponibilidade (Caixa + Aplic. Fin)",   "","","", "","","",""},

			{" "," "," "," ", " "," "," "," "},
			{"Gera��o de Lucro Econ�mico - BioMaster"," "," "," "," "," "," "," "},
			{"","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Lucro Econ�mico",   "","","", "","","",""},
			{"Spread",   "","","", "","","",""},
			{"RONA",   "","","", "","","",""},
			{"NOPAT",   "","","", "","","",""},
			{"WACC",   "","","", "","","",""},
			{"Custo Capital Pr�prio",   "","","", "","","",""},
			{"Custo Capital Terceiros",   "","","", "","","",""},
			{"Capital Investido",   "","","", "","","",""},
			{"Ativo Fixo",   "","","", "","","",""},
			{"Capital de Giro L�quido",   "","","", "","","",""},
			{"Disponibilidade (Caixa + Aplic. Fin)",   "","","", "","","",""},
	};

	private  String execucaoEstrategica[][]={
			{"Execu��o Estrat�gica - Holding","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Total",   "","","", "","","",""},
			{"Gest�o de Opera��es",   "","","", "","","",""},
			{"Gest�o de Pessoas",   "","","", "","","",""},
			{"Gest�o de Clientes",   "","","", "","","",""},
			{"Gest�o de Finan�as",   "","","", "","","",""},


			{"Execu��o Estrat�gica - DentalByte","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Total",   "","","", "","","",""},
			{"Gest�o de Opera��es",   "","","", "","","",""},
			{"Gest�o de Pessoas",   "","","", "","","",""},
			{"Gest�o de Clientes",   "","","", "","","",""},
			{"Gest�o de Finan�as",   "","","", "","","",""},


			{"Execu��o Estrat�gica - Digimetrics","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Total",   "","","", "","","",""},
			{"Gest�o de Opera��es",   "","","", "","","",""},
			{"Gest�o de Pessoas",   "","","", "","","",""},
			{"Gest�o de Clientes",   "","","", "","","",""},
			{"Gest�o de Finan�as",   "","","", "","","",""},


			{"Execu��o Estrat�gica - CleanSystem","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Total",   "","","", "","","",""},
			{"Gest�o de Opera��es",   "","","", "","","",""},
			{"Gest�o de Pessoas",   "","","", "","","",""},
			{"Gest�o de Clientes",   "","","", "","","",""},
			{"Gest�o de Finan�as",   "","","", "","","",""},


			{"Execu��o Estrat�gica - HardTech","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Total",   "","","", "","","",""},
			{"Gest�o de Opera��es",   "","","", "","","",""},
			{"Gest�o de Pessoas",   "","","", "","","",""},
			{"Gest�o de Clientes",   "","","", "","","",""},
			{"Gest�o de Finan�as",   "","","", "","","",""},


			{"Execu��o Estrat�gica - BioMaster","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Total",   "","","", "","","",""},
			{"Gest�o de Opera��es",   "","","", "","","",""},
			{"Gest�o de Pessoas",   "","","", "","","",""},
			{"Gest�o de Clientes",   "","","", "","","",""},
			{"Gest�o de Finan�as",   "","","", "","","",""},

	}; 

	private  String psop1[][]={
			//{" "," "," "," ", " "," "," "," "},
			{"Pilares Sustenta��o Organizacional - Holding"," "," "," "," "," "," "," "},
			{"","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Total",   "","","", "","","",""},
			{"Relev�ncia",   "","","", "","","",""},
			{"Coes�o",   "","","", "","","",""},
			{"Resili�ncia",   "","","", "","","",""},
			{"Foco",   "","","", "","","",""},
			{"Excel�ncia",   "","","", "","","",""},

			
			{" "," "," "," ", " "," "," "," "},
			{"Pilares Sustenta��o Organizacional - BioMaster"," "," "," "," "," "," "," "},
			{"","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Total",   "","","", "","","",""},
			{"Relev�ncia",   "","","", "","","",""},
			{"Coes�o",   "","","", "","","",""},
			{"Resili�ncia",   "","","", "","","",""},
			{"Foco",   "","","", "","","",""},
			{"Excel�ncia",   "","","", "","","",""},
	};

	private  String retornoAcionista[][]={
			{" "," "," "," ", " "," "," "," "},
			{"Retorno Total para o Acionista - Holding"," "," "," "," "," "," "," "},
			{"","P -2","P -1","P 0","P 1","P 2","P 3","P 4"},
			{"Retorno Total ao Acionista",   "","","", "","","",""},
			{"Varia��o EBITDA",   "","","", "","","",""},
			{"Varia��o Receita",   "","","", "","","",""},
			{"Varia��o Margem EBITDA",   "","","", "","","",""},
			{"Varia��o Valor de Mercado/EBITDA",   "","","", "","","",""},
			{"Free Cash Flow Yield",   "","","", "","","",""},
	}; 

	private  String comunicacaoExecutiva[][]={
			{"Comunica��o Estrat�gica da Diretoria Executiva"},
			{" "}
	};



	private  String insightPessoas[][]={
			{" "," ",""},
			{"Insights Pessoas Chaves - Holding"," "," "},
			{"","",""},

	};

	private  String alocacaoTempoCEO[][]={
			{"Gest�o de Stakeholders", "Real","Alvo","Real","Alvo","Real","Alvo","Real","Alvo","Real","Alvo"},
			{"Relacionamento com Sociedade"," "," "," "," "," "," "," "," "," "," "},
			{"Relacionamento com Acionistas"," "," "," "," "," "," "," "," "," "," "},
			{"Relacionamento com Clientes"," "," "," "," "," "," "," "," "," "," "},
			{"Relacionamento com Colaboradores"," "," "," "," "," "," "," "," "," "," "},
			{"Tarefas Rotineiras"," "," "," "," "," "," "," "," "," "," "},
			{"Temas Importantes e Urgentes"," "," "," "," "," "," "," "," "," "," "},
			{"Auto Desenvolvimento"," "," "," "," "," "," "," "," "," "," "},
			{"Coaching para Equipe Gerencial"," "," "," "," "," "," "," "," "," "," "},
	};

	
	private  String[][] receitaEsperada=
	    {   {"","DentalByte","DigiMetrics","CleanSystem","HardTech","","BioMaster","","Holding"},
	    	{"Receita Esperada","","","","","","","",""},
			{" "," "," "," ", " "," "," "," ",""},
			{"Impactos","","","","","","","",""},
			{"Comunica��o","","","","","","","",""},
			{"Projetos Especiais ","","","","","","","",""},
			{"Situa��es de Lideran�a","","","","","","","",""},
			{"Lideran�a em A��o","","","","","","","",""},
			{"Aloca��o de Or�amento - Opex","","","","","","","",""},
			{"Aloca��o de Or�amento -Capex","","","","","","","",""},
			{" "," "," "," ", " "," "," "," ",""},
			{"Receita Final","","","","","","","",""},
	};
	
	private  String[][] receitaEsperadaComp=
	    {   {"","DentalByte","DigiMetrics","CleanSystem","HardTech","","BioMaster","","Holding"},
			{" "," "," "," ", " "," "," "," ",""},
			{" "," "," "," ", " "," "," "," ",""},
			{" "," "," "," ", " "," "," "," ",""},
			{" "," "," "," ", " "," "," "," ",""},
			{" "," "," "," ", " "," "," "," ",""}
	};
	
	private  String[][] DREFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			//Demonstrativo de Resultado

			//{"B 8 L 8 0.0","B 8 C 8 0.0","B 8 C 8 0.0","N 5 C 8 0.0","N 5 C 8 0.0","N 5 C 8 0.0","N 5 C 8 0.0","N 5 C 8 0.0","N 5 C 8 0.0","N 5 C 8 0.0","N 5 C 8 0.0","N 5 C 8 0.0","N 5 C 8 0.0","B 5 R 8 0.0","B 5 R 8 0.0"},
			{"B 8 L 3 1.5","B 8 C 3 1.5","B 5 C 3 1.5","B 5 C 3 1.5","B 8 C 3 1.5","B 5 C 3 1.5","B 5 C 3 1.5","B 8 C 3 1.5","B 5 C 3 1.5","B 5 C 3 1.5","B 8 C 3 1.5","B 5 C 3 1.5","B 5 C 3 1.5","B 8 C 3 1.5","B 5 C 3 1.5","B 5 C 3 1.5","B 8 C 3 1.5","B 5 C 3 1.5","B 5 C 3 1.5","B 8 C 3 1.5","B 5 C 3 1.5","B 5 C 3 1.5"},
			{"B 8 L 8 0.5","B 8 C 8 0.5","B 5 C 8 0.5","B 5 C 8 0.5","B 8 C 8 0.5","B 5 C 8 0.5","B 5 C 8 0.5","B 8 C 8 0.5","B 5 C 8 0.5","B 5 C 8 0.5","B 8 C 8 0.5","B 5 C 8 0.5","B 5 C 8 0.5","B 8 C 8 0.5","B 5 C 8 0.5","B 5 C 8 0.5","B 8 C 8 0.5","B 5 C 8 0.5","B 5 C 8 0.5","B 8 C 8 0.5","B 5 C 8 0.5","B 5 C 8 0.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5"},
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"}, 
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"}, 
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"}, 
			{"I 8 L 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5"}, 
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5"},
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"}, 
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"}, 
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"}, 
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"}, 
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"}, 
			{"I 8 L 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5","I 8 C 3 1.5","I 5 C 3 1.5","I 5 C 3 1.5"}, 
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5"},
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5"},
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5"},
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"}, 
			{"I 8 L 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5","I 8 C 3 0.5","I 5 C 3 0.5","I 5 C 3 0.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5","B 8 C 3 0.5","B 5 C 3 0.5","B 5 C 3 0.5"},
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5"},
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5","B 8 C 8 1.5","B 5 C 8 1.5","B 5 C 8 1.5"},
	};
	private  String[][] balancoFormato={
			//Balan�o Parimonial
			//{"N 6 L 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","N 6 C 20 0.0","B 6 R 20 0.0","B 6 R 20 0.0"},

			//{"B 8 L 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","N 6 C 8 0.5","B 6 R 8 0.5","B 6 R 8 0.5"},
			{"B 8 L 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5"},

			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5"},
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5","N 8 C 3 1.5"},
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
	}; 
	private  String[][] roeFormato={ 
			//Resumo

			//{"N 6 L 30 0.0", "N 6 C 30 0.0", "N 6 C 30 0.0", "N 6 C 30 0.0", "N 6 C 30 0.0", "N 6 C 30 0.0", "N 6 C 30 0.0", "N 6 C 30 0.0"},
			//{"B 8 L 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5", "B 8 C 3 0.5", "B 8 C 3 0.5", "B 8 C 3 0.5", "B 8 C 3 0.5", "B 8 C 3 0.5"},
			{"B 8 L 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
	};

	private  String[][] DOFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//{"B 8 L 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0"},
			//{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 8 L 8 1.5", "N 8 C 8 1.5","N 8 C 8 1.5","N 8 C 8 1.5", "N 8 C 8 1.5", "N 8 C 8 1.5","N 8 C 8 1.5","N 8 C 8 1.5", "N 8 C 8 1.5", "N 8 C 8 1.5","N 8 C 8 1.5","N 8 C 8 1.5", "N 8 C 8 1.5", "N 8 C 8 1.5","N 8 C 8 1.5","N 8 C 8 1.5"},
			{"N 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5"},
	};

	
//	private  String[][] TipoMultiNegocioFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
//			//Din�mica Organizacional
//
//			//{"B 8 L 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0"},
//			//{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
//			{"B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5", "B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5", "B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5", "B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5"},
//			{"N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5"},
//	};

	
	private  String[][] PropositoFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//{"B 8 L 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0"},
			//{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			
	};

	
	private  String[][] DOVFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//{"B 8 L 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0"},
			//{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 7 L 8 1.5", "N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5", "N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5", "N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5", "N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5","N 7 C 8 1.5", "N 7 C 8 1.5"},
			{"N 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5"},
			{"N 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5"},
			{"N 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5"},
			{"N 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5"}
	};

	

	private  String[][] BSCFormato=
		{
				{"B 8 L 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0"},
				{"B 8 L 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0"}, 
				{"B 8 L 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5"}, 
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"B 8 L 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5"},
				{"N 6 L 3 0.5","N 6 C 3 0.5","N 6 C 3 0.5","N 6 C 3 0.5","N 6 C 3 0.5","N 6 C 3 0.5","N 6 C 3 0.5"}
		};


	private  String[][] BSCCompFormato=
		{
				{"B 8 L 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0","N 6 C 5 0.0"},

				{"B 8 L 8 0.0","N 6 C 8 0.0","N 6 C 8 0.0","N 6 C 8 0.0","N 6 C 8 0.0","N 6 C 8 0.0","N 6 C 8 0.0","N 6 C 8 0.0","N 6 C 8 0.0","N 6 C 8 0.0","N 6 C 8 0.0"},
				{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"}, 

				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
				{"B 8 L 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","V 8 C 3 0.5"}

		};
	private  String[][] liderancaFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			{"B 8 L 3 0.0","B 8 C 3 0.0","B 8 C 3 0.0","B 8 C 3 0.0","B 8 C 3 0.0","B 8 C 3 0.0","B 8 C 3 0.0","B 8 C 3 0.0"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 8 1.5","N 8 C 8 1.5","N 8 C 8 1.5","N 8 C 8 1.5","N 8 C 8 1.5","N 8 C 8 1.5","N 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"}
	};

	private  String[][] catalisadoresFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
	};

	private  String[][] vetoresFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},

	};
	private  String[][] stakeholdersFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
	};
	// float[] tab = {100,50,50,50,50,50,50,50,50,50,50,50,50,50,50};


	private  String[][] valorEconomicoFormatop1 = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			//{"N 8 L 5  0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0"},
			{"B 8 L 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},

			
			{"N 8 L 1  0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0","B 8 C 1 0.0"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
			{"N 6 L 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5","N 6 C 2 0.5"},
	};

	private  String[][] execucaoEstrategicaFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},

			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},

			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},

			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},

			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},

			{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
	};


	private  String[][] psoFormatop1 = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			{"B 8 L 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0"},
			{"B 8 L 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0"},

			
			{"B 8 L 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0"},
			{"B 8 L 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5","B 8 C 3 1.5"},
			{"B 8 L 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"N 8 L 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0","N 8 C 3 0.0"},
	};

	private  String[][] retornoAcionistaFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			//Demonstrativo de Resultado
			{"N 8 L 5  0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0","B 8 C 5 0.0"},
			{"B 8 L 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0","B 8 C 2 0.0"},
			{"B 8 L 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5","B 8 C 2 1.5"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5"},
	};

	private  String[][] comunicacaoExecutivaFormato = {

			//Demonstrativo de Resultado
			{"B 8 C 30  0.0"},
			{"N 11  L 11  0.0"}
	};

	private  String[][] insigthPessoasFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			//Demonstrativo de Resultado
			{"N 8 L 5  0.0","B 8 C 5 0.0","B 8 C 5 0.0"},
			{"B 8 L 2 0.5","B 8 C 2 0.5","B 8 C 2 0.5"},
			{"N 8 L 2 0.5","N 8 C 2 0.5","N 8 C 2 0.5",}

	};

	private  String[][] alocacaoTempoCEOFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espaçamento_vertical(padding)espessura da linha
			//Demonstrativo de Resultado
			{"B 8 L 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5","B 8 C 5 1.5"},
			{"N 8 L 5 0.5","B 8 C 5 0.5","B 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5"},
			{"N 8 L 5 0.5","B 8 C 5 0.5","B 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5"},
			{"N 8 L 5 0.5","B 8 C 5 0.5","B 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5"},
			{"N 8 L 5 0.5","B 8 C 5 0.5","B 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5"},
			{"N 8 L 5 0.5","B 8 C 5 0.5","B 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5"},
			{"N 8 L 5 0.5","B 8 C 5 0.5","B 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5"},
			{"N 8 L 5 0.5","B 8 C 5 0.5","B 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5"},
			{"N 8 L 5 0.5","B 8 C 5 0.5","Bs 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5","N 8 C 5 0.5"},
	};

	private  String[][] receitaEsperadaFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Demonstrativo de Resultado

			
			
			{"B 8 L 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 0.0","B 8 C 1 1.5","B 8 C 1 0.0","B 8 C 1 1.5"},
			{"B 8 L 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.0","B 8 C 1 0.5","B 8 C 1 0.0","B 8 C 1 0.5"},
			{"B 8 L 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.0","B 8 C 1 0.5","B 8 C 1 0.0","B 8 C 1 0.5"},
			{"B 8 L 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.5","B 8 C 1 0.0","B 8 C 1 0.5","B 8 C 1 0.0","B 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"N 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"B 8 L 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 0.0","B 8 C 1 1.5","B 8 C 1 0.0","B 8 C 1 1.5"},
			{"B 8 L 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 0.0","B 8 C 1 1.5","B 8 C 1 0.0","B 8 C 1 1.5"}
	
	};
	
	
	private  String[][] receitaEsperadaCompFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Demonstrativo de Resultado

			
			
			{"B 8 L 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 1.5","B 8 C 1 0.0","B 8 C 1 1.5","B 8 C 1 0.0","B 8 C 1 1.5"},
			{"B 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"B 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"B 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"B 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			{"B 8 L 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5","N 8 C 1 0.0","N 8 C 1 0.5"},
			
			
	
	};
	
	
	public  void preencheDRE(String[][] matriz,String organizacao)
	{  int periodoIni=-2;
       int col=1;

	//cria hassh de elementos do dre
	Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
	//Insere os elementos da lista no hash pela chave "campo"
	//matriz[0][0] =  "Demonstrativo de Resultado - "+organizacao;
	while ((col<=20) && (periodoIni <periodo))
	{
		lista =es.getHerdeiros("DRE",organizacao,periodoIni,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo(), e);
		}
		ModeloService service = new ModeloService(grupo);
		Elemento variacao = new Elemento();

		matriz[1][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()));
		matriz[1][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_RL", organizacao, periodoIni);
		matriz[1][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
		matriz[2][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst").getValor()));
		matriz[2][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Cst", organizacao, periodoIni);
		matriz[2][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
		
		matriz[3][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst_MO").getValor()));
		matriz[3][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst_MO").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Cst_MO", organizacao, periodoIni);
		matriz[3][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
		matriz[4][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst_MP").getValor()));
		matriz[4][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst_MP").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Cst_MP", organizacao, periodoIni);
		matriz[4][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
		matriz[5][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst_En").getValor()));
		matriz[5][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst_En").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Cst_En", organizacao, periodoIni);
		matriz[5][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
		matriz[6][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst_Log").getValor()));
		matriz[6][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Cst_Log").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Cst_Log", organizacao, periodoIni);
		matriz[6][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
		matriz[7][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor() - hash.get("DRE_Lair_Ebit_Ebitda_Cst").getValor()));
		matriz[7][col+1] = Long.toString(Math.round((hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor() - hash.get("DRE_Lair_Ebit_Ebitda_Cst").getValor())/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_RL", organizacao, periodoIni);
		matriz[7][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	

		matriz[8][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex").getValor()));
		matriz[8][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Opex", organizacao, periodoIni);
		matriz[8][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[9][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_Pes").getValor()));
		matriz[9][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_Pes").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Opex_Pes", organizacao, periodoIni);
		matriz[9][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[10][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_Adm").getValor()));
		matriz[10][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_Adm").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Opex_Adm", organizacao, periodoIni);
		matriz[10][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[11][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_Tec").getValor()));
		matriz[11][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_Tec").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Opex_Tec", organizacao, periodoIni);
		matriz[11][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[12][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_VMkt").getValor()));
		matriz[12][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_VMkt").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Opex_VMkt", organizacao, periodoIni);
		matriz[12][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[13][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_PD").getValor()));
		matriz[13][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_PD").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Opex_PD", organizacao, periodoIni);
		matriz[13][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[14][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_Op").getValor()));
		matriz[14][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_Opex_Op").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda_Opex_Op", organizacao, periodoIni);
		matriz[14][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[15][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda").getValor()));
		matriz[15][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_Ebitda", organizacao, periodoIni);
		matriz[15][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[16][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_DA").getValor()));
		matriz[16][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit_DA").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit_DA", organizacao, periodoIni);
		matriz[16][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[17][col] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit").getValor()));
		matriz[17][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Ebit").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Ebit", organizacao, periodoIni);
		matriz[17][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[18][col] = Long.toString(Math.round(hash.get("DRE_Lair_RNO").getValor()));
		matriz[18][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_RNO").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_RNO", organizacao, periodoIni);
		matriz[18][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		
		
		matriz[19][col] = Long.toString(Math.round(hash.get("DRE_Lair_RNO_ApFin").getValor()*0.06));
		matriz[19][col+1] = Long.toString(Math.round((hash.get("DRE_Lair_RNO_ApFin").getValor() *0.06) /hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_RNO_ApFin", organizacao, periodoIni);
		matriz[19][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		
		matriz[20][col] = Long.toString(Math.round(hash.get("DRE_Lair_RNO_Multa").getValor()));
		matriz[20][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_RNO_Multa").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		//variacao = service.getElemento("DRE_Lair_RNO_Multa", organizacao, periodoIni);
		matriz[20][col+2] =  "-";
				
				
						
		matriz[21][col] = Long.toString(Math.round(hash.get("DRE_Lair_Juros").getValor()));
		matriz[21][col+1] = Long.toString(Math.round(hash.get("DRE_Lair_Juros").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_Lair_Juros", organizacao, periodoIni);
		matriz[21][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[22][col] = Long.toString(Math.round(hash.get("DRE_IR").getValor()));
		matriz[22][col+1] = Long.toString(Math.round(hash.get("DRE_IR").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE_IR", organizacao, periodoIni);
		matriz[22][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	
		matriz[23][col] = Long.toString(Math.round(hash.get("DRE").getValor()));
		matriz[23][col+1] = Long.toString(Math.round(hash.get("DRE").getValor()/hash.get("DRE_Lair_Ebit_Ebitda_RL").getValor()*100))+"%";
		variacao = service.getElemento("DRE", organizacao, periodoIni);
		matriz[23][col+2] =  Long.toString(Math.round(es.getVariacao(variacao)*100))+"%";
	

		col=col+3;
		periodoIni=periodoIni+1; 
	}

	}

	public  void preencheBalanco(String[][] matriz, String organizacao)
	{  int periodoIni=-2;
	int col=1;
	//matriz[1][0] ="Balan�o Patrimonial - " + organizacao;
	//cria hassh de elementos do Balan�o
	Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
	//Insere os elementos da lista no hash pela chave "campo"
	while ((col<=13) && (periodoIni <periodo))
	{
		int lin=1;
		lista =es.getHerdeiros("BP",organizacao,periodoIni,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo(), e);
		}
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_A_C_Cx").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_A_C_Cx").getValor()/hash.get("BP_A").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_A_C_CR").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_A_C_CR").getValor()/hash.get("BP_A").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_A_C_Est").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_A_C_Est").getValor()/hash.get("BP_A").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_A_C_ApFin").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_A_C_ApFin").getValor()/hash.get("BP_A").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_A_NC_AtFx").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_A_NC_AtFx").getValor()/hash.get("BP_A").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_A_NC_Dep").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_A_NC_Dep").getValor()/hash.get("BP_A").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_A_C_GW").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_A_C_GW").getValor()/hash.get("BP_A").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_A").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_A").getValor()/hash.get("BP_A").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_P_C_CP").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_P_C_CP").getValor()/hash.get("BP_P").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_P_NC_DivCP").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_P_NC_DivCP").getValor()/hash.get("BP_P").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_P_NC_DivLP").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_P_NC_DivLP").getValor()/hash.get("BP_P").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_P_PL_ResL").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_P_PL_ResL").getValor()/hash.get("BP_P").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_P_PL_CapSoc").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_P_PL_CapSoc").getValor()/hash.get("BP_P").getValor()*100))+"%";
		matriz[lin  ][col] = Long.toString(Math.round(hash.get("BP_P").getValor()));
		matriz[lin++][col+1] = Long.toString(Math.round(hash.get("BP_P").getValor()/hash.get("BP_P").getValor()*100))+"%";

		col=col+2;
		periodoIni=periodoIni+1; 
	}

	}
	public  void preencheRoe(String[][] matriz,String organizacao)
	{  int periodoIni=-2;
	int col=1;
	//matriz[1][0] ="�rvore de Desempenho - " + organizacao;
	//cria hassh de elementos do Balan�o
	Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
	//Insere os elementos da lista no hash pela chave "campo"
	while ((col<=13) && (periodoIni <periodo))
	{
		lista =es.getHerdeiros("ROE",organizacao,periodoIni,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao(), e);
		}

		matriz[1][col] = Long.toString(Math.round(hash.get("ROE"+organizacao).getValor()*100))+"%";
		matriz[2][col] = Long.toString(Math.round(hash.get("ROE_ROA_ML"+organizacao).getValor()*100))+"%";
		matriz[3][col] = ""+Util.round(hash.get("ROE_ROA_GA"+organizacao).getValor(),2);//n�o � um percentual
		matriz[4][col] = ""+Util.round(hash.get("ROE_AlFin"+organizacao).getValor(),2);//n�o � um percentual

		col=col+1;
		periodoIni=periodoIni+1; 
	}

	}

	public  void preencheDO(String[][] matriz)
	{ 
		//int col=1,
		int contEmp=0;
		String empresas[]={"DentalByte","DigiMetrics","CleanSystem","HardTech","BioMaster"};
		double mEi, mEo, mEc,mElt,total;

		//Busca o Planejamento Estrat�gico no P0
		//cria hash de elementos do Balan�o
		Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
		lista =es.getHerdeiros("DO","%%",0,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}

		for (int linha=1;linha<=5;linha++) 
		{
			matriz[linha][3]=Long.toString(Math.round(hash.get("DO_P_E_EO"+empresas[contEmp]+""+0).getValor()*100))+"%";
			matriz[linha][7]=Long.toString(Math.round(hash.get("DO_P_E_EI"+empresas[contEmp]+""+0).getValor()*100))+"%";
			matriz[linha][11]=Long.toString(Math.round(hash.get("DO_P_E_EC"+empresas[contEmp]+""+0).getValor()*100))+"%";
			matriz[linha][15]=Long.toString(Math.round(hash.get("DO_P_E_EIt"+empresas[contEmp]+""+0).getValor()*100))+"%";
			contEmp=contEmp+1;
		}

		//Resultado per�odo atual
		//busca a holding e as empresas do grupo periodo atual
		lista =es.getHerdeiros("DO","%%",periodo-1,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}
		contEmp=0;
		for (int linha=1;linha<=5;linha++) 
		{
			mEo=(hash.get("DO_Cm_EO"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Es_EO"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Cl_EO"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Et_EO"+empresas[contEmp]+""+(periodo-1)).getValor())/4;
			mEi=(hash.get("DO_Cm_EI"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Es_EI"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Cl_EI"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Et_EI"+empresas[contEmp]+""+(periodo-1)).getValor())/4;
			mEc=(hash.get("DO_Cm_EC"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Es_EC"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Cl_EC"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Et_EC"+empresas[contEmp]+""+(periodo-1)).getValor())/4;
			mElt=(hash.get("DO_Cm_EIt"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Es_EIt"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Cl_EIt"+empresas[contEmp]+""+(periodo-1)).getValor()+hash.get("DO_Et_EIt"+empresas[contEmp]+""+(periodo-1)).getValor())/4;
			total=mEi +mEo + mEc+mElt;
			matriz[linha][2] =Long.toString(Math.round((mEo/total) *100))+"%";
			matriz[linha][6] =Long.toString(Math.round((mEi/total) *100))+"%";
			matriz[linha][10] =Long.toString(Math.round((mEc/total) *100))+"%";
			matriz[linha][14] =Long.toString(Math.round((mElt/total) *100))+"%";
			contEmp=contEmp+1;
		}


		contEmp=0;
		lista =es.getHerdeiros("DO","%%",periodo-2,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}
		
		//Resultado per�odo anterior
		//Pega a primeira empresa
		//varre as linhas da tabela
		contEmp=0;
		for (int linha=1;linha<=5;linha++) 
		{
			mEo=(hash.get("DO_Cm_EO"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Es_EO"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Cl_EO"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Et_EO"+empresas[contEmp]+""+(periodo-2)).getValor())/4;
			mEi=(hash.get("DO_Cm_EI"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Es_EI"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Cl_EI"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Et_EI"+empresas[contEmp]+""+(periodo-2)).getValor())/4;
			mEc=(hash.get("DO_Cm_EC"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Es_EC"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Cl_EC"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Et_EC"+empresas[contEmp]+""+(periodo-2)).getValor())/4;
			mElt=(hash.get("DO_Cm_EIt"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Es_EIt"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Cl_EIt"+empresas[contEmp]+""+(periodo-2)).getValor()+hash.get("DO_Et_EIt"+empresas[contEmp]+""+(periodo-2)).getValor())/4;
			total=mEi +mEo + mEc+mElt;
			matriz[linha][1] =Long.toString(Math.round((mEo/total) *100))+"%";
			matriz[linha][5] =Long.toString(Math.round((mEi/total) *100))+"%";
			matriz[linha][9] =Long.toString(Math.round((mEc/total) *100))+"%";
			matriz[linha][13] =Long.toString(Math.round((mElt/total) *100))+"%";
			contEmp=contEmp+1;
		}
	}

	public  void preencheDOV(String[][] matriz)
	{ 
		String empresas[]={"DentalByte","DigiMetrics","CleanSystem","HardTech","BioMaster"};

		//cria hassh de elementos do Balan�o
		Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

		lista =es.getHerdeiros("DO","%%",periodo-1,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}
		int contEmp=0;
		for (int linha=1;linha<=5;linha++) 
		{
			matriz[linha][1] =Math.round(hash.get("DO_Et_EO"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][2] =Math.round(hash.get("DO_Cl_EO"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][3] =Math.round(hash.get("DO_Es_EO"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][4] =Math.round(hash.get("DO_Cm_EO"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			
			matriz[linha][6] =Math.round(hash.get("DO_Et_EI"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][7] =Math.round(hash.get("DO_Cl_EI"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][8] =Math.round(hash.get("DO_Es_EI"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][9] =Math.round(hash.get("DO_Cm_EI"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			
			matriz[linha][11] =Math.round(hash.get("DO_Et_EC"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][12] =Math.round(hash.get("DO_Cl_EC"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][13] =Math.round(hash.get("DO_Es_EC"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][14] =Math.round(hash.get("DO_Cm_EC"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			
			matriz[linha][16] =Math.round(hash.get("DO_Et_EIt"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][17] =Math.round(hash.get("DO_Cl_EIt"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][18] =Math.round(hash.get("DO_Es_EIt"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			matriz[linha][19] =Math.round(hash.get("DO_Cm_EIt"+empresas[contEmp]+""+(periodo-1)).getValor()*100)+"%";
			contEmp=contEmp+1;
		}

	}
	
	

	public void preencheProposito(String[][] matriz)
	{
		TMN_Action tmn = new TMN_Action();
	  //Tipo de Prop�sito Escolhido
		matriz[1][0] = tmn.getDescricaoTipoPropositoEscolhido(grupo, periodo-1);
		//Descri��oo do Indicador 1 
		matriz[1][1] = tmn.getDescricaoIndicador1TipoPropositoEscolhido(grupo, periodo-1);
		matriz[1][1] += " "+Math.round(tmn.getIndicador1TipoPropositoEscolhido(grupo, periodo-1)*100)+"%";
		//Descri��oo do Indicador 2 
		matriz[1][2] = tmn.getDescricaoIndicador2TipoPropositoEscolhido(grupo, periodo-1);
		matriz[1][2] += " "+Math.round(tmn.getIndicador2TipoPropositoEscolhido(grupo, periodo-1)*100)+"%";
		//Resultado
		int resultado = 0;
		if (Math.round(tmn.getIndicador1TipoPropositoEscolhido(grupo, periodo-1)*100)>=
			Math.round(tmn.getPercentual1TipoPropositoEscolhido(grupo, periodo-1)*100)){ 
			resultado++;
		}
		if (Math.round(tmn.getIndicador2TipoPropositoEscolhido(grupo, periodo-1)*100)>=
			Math.round(tmn.getPercentual2TipoPropositoEscolhido(grupo, periodo-1)*100)){ 
			resultado++;
		}
		String r = "";
		if (resultado==0) r = "N�o Atingido";
		if (resultado==1) r = "Parcialmente Atingido";
		if (resultado==2) r = "Atingido";
		matriz[1][3] = r;
	
	}
	
	
	public  void preencheStakeHolders(String[][] matriz)
	{ 
		Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
		lista =es.getHerdeiros("CVS","%%",periodo-1,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao(), e);
		}
		matriz[1][1] =   Long.toString(Math.round(hash.get("CVS"+"Holding").getValor()*100))+"%"; 
		matriz[1][2] =   Long.toString(Math.round(hash.get("CVS"+"DentalByte").getValor()*100))+"%"; 
		matriz[1][3] =   Long.toString(Math.round(hash.get("CVS"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[1][4] =   Long.toString(Math.round(hash.get("CVS"+"CleanSystem").getValor()*100))+"%"; 
		matriz[1][5] =   Long.toString(Math.round(hash.get("CVS"+"HardTech").getValor()*100))+"%"; 
		matriz[1][6] =   Long.toString(Math.round(hash.get("CVS"+"BioMaster").getValor()*100))+"%"; 

		matriz[2][1] =   Long.toString(Math.round(hash.get("CVS_Ac"+"Holding").getValor()*100))+"%"; 
		matriz[2][2] =   Long.toString(Math.round(hash.get("CVS_Ac"+"DentalByte").getValor()*100))+"%"; 
		matriz[2][3] =   Long.toString(Math.round(hash.get("CVS_Ac"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[2][4] =   Long.toString(Math.round(hash.get("CVS_Ac"+"CleanSystem").getValor()*100))+"%"; 
		matriz[2][5] =   Long.toString(Math.round(hash.get("CVS_Ac"+"HardTech").getValor()*100))+"%"; 
		matriz[2][6] =   Long.toString(Math.round(hash.get("CVS_Ac"+"BioMaster").getValor()*100))+"%";

		matriz[3][1] =   Long.toString(Math.round(hash.get("CVS_F_Cli"+"Holding").getValor()*100))+"%"; 
		matriz[3][2] =   Long.toString(Math.round(hash.get("CVS_F_Cli"+"DentalByte").getValor()*100))+"%"; 
		matriz[3][3] =   Long.toString(Math.round(hash.get("CVS_F_Cli"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[3][4] =   Long.toString(Math.round(hash.get("CVS_F_Cli"+"CleanSystem").getValor()*100))+"%"; 
		matriz[3][5] =   Long.toString(Math.round(hash.get("CVS_F_Cli"+"HardTech").getValor()*100))+"%"; 
		matriz[3][6] =   Long.toString(Math.round(hash.get("CVS_F_Cli"+"BioMaster").getValor()*100))+"%"; 


		matriz[4][1] =   Long.toString(Math.round(hash.get("CVS_F_Col"+"Holding").getValor()*100))+"%"; 
		matriz[4][2] =   Long.toString(Math.round(hash.get("CVS_F_Col"+"DentalByte").getValor()*100))+"%"; 
		matriz[4][3] =   Long.toString(Math.round(hash.get("CVS_F_Col"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[4][4] =   Long.toString(Math.round(hash.get("CVS_F_Col"+"CleanSystem").getValor()*100))+"%"; 
		matriz[4][5] =   Long.toString(Math.round(hash.get("CVS_F_Col"+"HardTech").getValor()*100))+"%"; 
		matriz[4][6] =   Long.toString(Math.round(hash.get("CVS_F_Col"+"BioMaster").getValor()*100))+"%"; 


		matriz[5][1] =   Long.toString(Math.round(hash.get("CVS_F_Soc"+"Holding").getValor()*100))+"%"; 
		matriz[5][2] =   Long.toString(Math.round(hash.get("CVS_F_Soc"+"DentalByte").getValor()*100))+"%"; 
		matriz[5][3] =   Long.toString(Math.round(hash.get("CVS_F_Soc"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[5][4] =   Long.toString(Math.round(hash.get("CVS_F_Soc"+"CleanSystem").getValor()*100))+"%"; 
		matriz[5][5] =   Long.toString(Math.round(hash.get("CVS_F_Soc"+"HardTech").getValor()*100))+"%"; 
		matriz[5][6] =   Long.toString(Math.round(hash.get("CVS_F_Soc"+"BioMaster").getValor()*100))+"%"; 
	}
	public  void preencheVetores(String[][] matriz)
	{ 
		Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
		lista =es.getHerdeiros("VAD","%%",periodo-1,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao(), e);
		}
		matriz[1][1] =   Long.toString(Math.round(hash.get("VAD"+"Holding").getValor()*100))+"%"; 
		matriz[1][2] =   Long.toString(Math.round(hash.get("VAD"+"DentalByte").getValor()*100))+"%"; 
		matriz[1][3] =   Long.toString(Math.round(hash.get("VAD"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[1][4] =   Long.toString(Math.round(hash.get("VAD"+"CleanSystem").getValor()*100))+"%"; 
		matriz[1][5] =   Long.toString(Math.round(hash.get("VAD"+"HardTech").getValor()*100))+"%"; 
		matriz[1][6] =   Long.toString(Math.round(hash.get("VAD"+"BioMaster").getValor()*100))+"%"; 

		matriz[2][1] =   Long.toString(Math.round(hash.get("VAD_ACV"+"Holding").getValor()*100))+"%"; 
		matriz[2][2] =   Long.toString(Math.round(hash.get("VAD_ACV"+"DentalByte").getValor()*100))+"%"; 
		matriz[2][3] =   Long.toString(Math.round(hash.get("VAD_ACV"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[2][4] =   Long.toString(Math.round(hash.get("VAD_ACV"+"CleanSystem").getValor()*100))+"%"; 
		matriz[2][5] =   Long.toString(Math.round(hash.get("VAD_ACV"+"HardTech").getValor()*100))+"%"; 
		matriz[2][6] =   Long.toString(Math.round(hash.get("VAD_ACV"+"BioMaster").getValor()*100))+"%";

		matriz[3][1] =   Long.toString(Math.round(hash.get("VAD_PC"+"Holding").getValor()*100))+"%"; 
		matriz[3][2] =   Long.toString(Math.round(hash.get("VAD_PC"+"DentalByte").getValor()*100))+"%"; 
		matriz[3][3] =   Long.toString(Math.round(hash.get("VAD_PC"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[3][4] =   Long.toString(Math.round(hash.get("VAD_PC"+"CleanSystem").getValor()*100))+"%"; 
		matriz[3][5] =   Long.toString(Math.round(hash.get("VAD_PC"+"HardTech").getValor()*100))+"%"; 
		matriz[3][6] =   Long.toString(Math.round(hash.get("VAD_PC"+"BioMaster").getValor()*100))+"%"; 

		matriz[4][1] =   Long.toString(Math.round(hash.get("VAD_VE"+"Holding").getValor()*100))+"%"; 
		matriz[4][2] =   Long.toString(Math.round(hash.get("VAD_VE"+"DentalByte").getValor()*100))+"%"; 
		matriz[4][3] =   Long.toString(Math.round(hash.get("VAD_VE"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[4][4] =   Long.toString(Math.round(hash.get("VAD_VE"+"CleanSystem").getValor()*100))+"%"; 
		matriz[4][5] =   Long.toString(Math.round(hash.get("VAD_VE"+"HardTech").getValor()*100))+"%"; 
		matriz[4][6] =   Long.toString(Math.round(hash.get("VAD_VE"+"BioMaster").getValor()*100))+"%"; 
	}
	public  void preencheCatalisadores(String[][] matriz)
	{ 
		Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
		//busca no periodo anterior ao atual
		lista =es.getHerdeiros("CD","%%",periodo-1,+grupo);
		for (Elemento e:lista)
		{
			hash.put(e.getCampo()+e.getOrganizacao(), e);
		}
		matriz[1][1] =   Long.toString(Math.round(hash.get("CD"+"Holding").getValor()*100))+"%"; 
		matriz[1][2] =   Long.toString(Math.round(hash.get("CD"+"DentalByte").getValor()*100))+"%"; 
		matriz[1][3] =   Long.toString(Math.round(hash.get("CD"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[1][4] =   Long.toString(Math.round(hash.get("CD"+"CleanSystem").getValor()*100))+"%"; 
		matriz[1][5] =   Long.toString(Math.round(hash.get("CD"+"HardTech").getValor()*100))+"%"; 
		matriz[1][6] =   Long.toString(Math.round(hash.get("CD"+"BioMaster").getValor()*100))+"%"; 

		matriz[2][1] =   Long.toString(Math.round(hash.get("CD_Comp"+"Holding").getValor()*100))+"%";
		matriz[2][2] =   Long.toString(Math.round(hash.get("CD_Comp"+"DentalByte").getValor()*100))+"%"; 
		matriz[2][3] =   Long.toString(Math.round(hash.get("CD_Comp"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[2][4] =   Long.toString(Math.round(hash.get("CD_Comp"+"CleanSystem").getValor()*100))+"%"; 
		matriz[2][5] =   Long.toString(Math.round(hash.get("CD_Comp"+"HardTech").getValor()*100))+"%"; 
		matriz[2][6] =   Long.toString(Math.round(hash.get("CD_Comp"+"BioMaster").getValor()*100))+"%";

		matriz[3][1] =   Long.toString(Math.round(hash.get("CD_Conf"+"Holding").getValor()*100))+"%"; 
		matriz[3][2] =   Long.toString(Math.round(hash.get("CD_Conf"+"DentalByte").getValor()*100))+"%"; 
		matriz[3][3] =   Long.toString(Math.round(hash.get("CD_Conf"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[3][4] =   Long.toString(Math.round(hash.get("CD_Conf"+"CleanSystem").getValor()*100))+"%"; 
		matriz[3][5] =   Long.toString(Math.round(hash.get("CD_Conf"+"HardTech").getValor()*100))+"%"; 
		matriz[3][6] =   Long.toString(Math.round(hash.get("CD_Conf"+"BioMaster").getValor()*100))+"%"; 

		matriz[4][1] =   Long.toString(Math.round(hash.get("CD_Conh"+"Holding").getValor()*100))+"%"; 
		matriz[4][2] =   Long.toString(Math.round(hash.get("CD_Conh"+"DentalByte").getValor()*100))+"%"; 
		matriz[4][3] =   Long.toString(Math.round(hash.get("CD_Conh"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[4][4] =   Long.toString(Math.round(hash.get("CD_Conh"+"CleanSystem").getValor()*100))+"%"; 
		matriz[4][5] =   Long.toString(Math.round(hash.get("CD_Conh"+"HardTech").getValor()*100))+"%"; 
		matriz[4][6] =   Long.toString(Math.round(hash.get("CD_Conh"+"BioMaster").getValor()*100))+"%"; 

		matriz[5][1] =   Long.toString(Math.round(hash.get("CD_Sig"+"Holding").getValor()*100))+"%"; 
		matriz[5][2] =   Long.toString(Math.round(hash.get("CD_Sig"+"DentalByte").getValor()*100))+"%"; 
		matriz[5][3] =   Long.toString(Math.round(hash.get("CD_Sig"+"DigiMetrics").getValor()*100))+"%"; 
		matriz[5][4] =   Long.toString(Math.round(hash.get("CD_Sig"+"CleanSystem").getValor()*100))+"%"; 
		matriz[5][5] =   Long.toString(Math.round(hash.get("CD_Sig"+"HardTech").getValor()*100))+"%"; 
		matriz[5][6] =   Long.toString(Math.round(hash.get("CD_Sig"+"BioMaster").getValor()*100))+"%"; 
	}
	public  void preencheLideranca(String[][] matriz)
	{  

		//cria hassh de elementos do Balan�o
		Hashtable<String, LE> hash = new Hashtable<String, LE>();
		//Insere os elementos da lista no hash pela chave "campo"
		listaLE =les.getValoresPeriodo(periodo-1,grupo);
		for (LE e:listaLE){
			hash.put(e.getGrupo()+e.getOrganizacao()+e.getCargo(), e);

		}  
		matriz[0][1] = hash.get(+grupo+"Holding"+"Presidente").getNomePersonagem(); 
		matriz[3][1] = Long.toString(Math.round(hash.get(+grupo+"Holding"+"Presidente").getLE()*100))+"%";
		matriz[4][1] = Long.toString(Math.round(hash.get(+grupo+"Holding"+"Presidente").getLE_EI()*100))+"%";
		matriz[5][1] = "-";
		matriz[6][1] = "-";
		matriz[7][1] = "-";
		matriz[8][1] = "-";
		matriz[9][1] = Long.toString(Math.round(hash.get(+grupo+"Holding"+"Presidente").getLE_EP()*100))+"%";
		matriz[10][1] = "-";
		matriz[11][1] = "-";
		matriz[12][1] = "-";
		matriz[13][1] = "-";
		matriz[14][1] = Long.toString(Math.round(hash.get(+grupo+"Holding"+"Presidente").getLE_TO()*100))+"%";
		matriz[15][1] = "-";
		matriz[16][1] = "-";
		matriz[17][1] = "-";
		matriz[18][1] = "-";

		matriz[0][2] = hash.get(+grupo+"Holding"+"CFO").getNomePersonagem(); 
		matriz[3][2] = Long.toString(Math.round(hash.get(+grupo+"Holding"+"CFO").getLE()*100))+"%";
		matriz[4][2] = Long.toString(Math.round(hash.get(+grupo+"Holding"+"CFO").getLE_EI()*100))+"%";
		matriz[5][2] = "-";
		matriz[6][2] = "-";
		matriz[7][2] = "-";
		matriz[8][2] = "-";
		matriz[9][2] = Long.toString(Math.round(hash.get(+grupo+"Holding"+"CFO").getLE_EP()*100))+"%";
		matriz[10][2] = "-";
		matriz[11][2] = "-";
		matriz[12][2] = "-";
		matriz[13][2] = "-";
		matriz[14][2] = Long.toString(Math.round(hash.get(+grupo+"Holding"+"CFO").getLE_TO()*100))+"%";
		matriz[15][2] = "-";
		matriz[16][2] = "-";
		matriz[17][2] = "-";
		matriz[18][2] = "-";

		matriz[0][3] = hash.get(+grupo+"DentalByte"+"CEO").getNomePersonagem(); 
		matriz[3][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE()*100))+"%";
		matriz[4][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EI()*100))+"%";
		matriz[5][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EI_Aut()*100))+"%";
		matriz[6][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EI_CP()*100))+"%";
		matriz[7][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EI_IE()*100))+"%";
		matriz[8][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EI_Int()*100))+"%";
		matriz[9][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EP()*100))+"%";
		matriz[10][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EP_A()*100))+"%";
		matriz[11][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EP_C()*100))+"%";
		matriz[12][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EP_E()*100))+"%";
		matriz[13][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_EP_M()*100))+"%";
		matriz[14][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_TO()*100))+"%";
		matriz[15][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_TO_EE()*100))+"%";
		matriz[16][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_TO_GS()*100))+"%";
		matriz[17][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_TO_JT()*100))+"%";
		matriz[18][3] = Long.toString(Math.round(hash.get(+grupo+"DentalByte"+"CEO").getLE_TO_PSO()*100))+"%";

		matriz[0][4] = hash.get(+grupo+"DigiMetrics"+"CEO").getNomePersonagem(); 
		matriz[3][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE()*100))+"%";
		matriz[4][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EI()*100))+"%";
		matriz[5][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EI_Aut()*100))+"%";
		matriz[6][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EI_CP()*100))+"%";
		matriz[7][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EI_IE()*100))+"%";
		matriz[8][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EI_Int()*100))+"%";
		matriz[9][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EP()*100))+"%";
		matriz[10][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EP_A()*100))+"%";
		matriz[11][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EP_C()*100))+"%";
		matriz[12][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EP_E()*100))+"%";
		matriz[13][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_EP_M()*100))+"%";
		matriz[14][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_TO()*100))+"%";
		matriz[15][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_TO_EE()*100))+"%";
		matriz[16][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_TO_GS()*100))+"%";
		matriz[17][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_TO_JT()*100))+"%";
		matriz[18][4] = Long.toString(Math.round(hash.get(+grupo+"DigiMetrics"+"CEO").getLE_TO_PSO()*100))+"%";        

		matriz[0][5] = hash.get(+grupo+"CleanSystem"+"CEO").getNomePersonagem(); 
		matriz[3][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE()*100))+"%";
		matriz[4][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EI()*100))+"%";
		matriz[5][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EI_Aut()*100))+"%";
		matriz[6][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EI_CP()*100))+"%";
		matriz[7][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EI_IE()*100))+"%";
		matriz[8][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EI_Int()*100))+"%";
		matriz[9][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EP()*100))+"%";
		matriz[10][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EP_A()*100))+"%";
		matriz[11][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EP_C()*100))+"%";
		matriz[12][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EP_E()*100))+"%";
		matriz[13][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_EP_M()*100))+"%";
		matriz[14][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_TO()*100))+"%";
		matriz[15][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_TO_EE()*100))+"%";
		matriz[16][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_TO_GS()*100))+"%";
		matriz[17][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_TO_JT()*100))+"%";
		matriz[18][5] = Long.toString(Math.round(hash.get(+grupo+"CleanSystem"+"CEO").getLE_TO_PSO()*100))+"%";


		matriz[0][6] = hash.get(+grupo+"HardTech"+"CEO").getNomePersonagem(); 
		matriz[3][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE()*100))+"%";
		matriz[4][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EI()*100))+"%";
		matriz[5][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EI_Aut()*100))+"%";
		matriz[6][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EI_CP()*100))+"%";
		matriz[7][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EI_IE()*100))+"%";
		matriz[8][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EI_Int()*100))+"%";
		matriz[9][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EP()*100))+"%";
		matriz[10][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EP_A()*100))+"%";
		matriz[11][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EP_C()*100))+"%";
		matriz[12][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EP_E()*100))+"%";
		matriz[13][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_EP_M()*100))+"%";
		matriz[14][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_TO()*100))+"%";
		matriz[15][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_TO_EE()*100))+"%";
		matriz[16][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_TO_GS()*100))+"%";
		matriz[17][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_TO_JT()*100))+"%";
		matriz[18][6] = Long.toString(Math.round(hash.get(+grupo+"HardTech"+"CEO").getLE_TO_PSO()*100))+"%";

		matriz[0][7] = hash.get(+grupo+"BioMaster"+"CEO").getNomePersonagem(); 
		matriz[3][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE()*100))+"%";
		matriz[4][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EI()*100))+"%";
		matriz[5][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EI_Aut()*100))+"%";
		matriz[6][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EI_CP()*100))+"%";
		matriz[7][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EI_IE()*100))+"%";
		matriz[8][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EI_Int()*100))+"%";
		matriz[9][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EP()*100))+"%";
		matriz[10][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EP_A()*100))+"%";
		matriz[11][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EP_C()*100))+"%";
		matriz[12][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EP_E()*100))+"%";
		matriz[13][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_EP_M()*100))+"%";
		matriz[14][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_TO()*100))+"%";
		matriz[15][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_TO_EE()*100))+"%";
		matriz[16][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_TO_GS()*100))+"%";
		matriz[17][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_TO_JT()*100))+"%";
		matriz[18][7] = Long.toString(Math.round(hash.get(+grupo+"BioMaster"+"CEO").getLE_TO_PSO()*100))+"%";
	}

	//Fun��oo que verifica a pontua��o do BSC
	public Long calculaPontuacao(Long valor)
	{
		Long result= (long) 0;
		if (valor <=80)
			result= (long)0;
		else
			if ((valor >80) && (valor <=120))
				return valor;
			else
				return (long) 120;
		return result;
	}
	public  void preencheBSC(String[][] matriz, String organizacao)
	{ 
		//organizacao ignorada para pegar a BioMaster e Holding
		Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
		lista =es.getHerdeiros("%%","Holding",periodo-1,grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}
		lista =es.getHerdeiros("%%","Holding",periodo-2,grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}
		
		
		lista =es.getHerdeiros("%%","BioMaster",periodo-1,grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}
		lista =es.getHerdeiros("%%","BioMaster",periodo-2,grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}
		
		
		
		
		ModeloService service = new ModeloService(grupo);
		Elemento variacao = new Elemento();
	
		matriz[0][0] =   "Painel de Indicadores - "+organizacao;
		matriz[3][1] =   Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"Holding"+(periodo-2)).getValor())); 
		matriz[3][2] =   Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"Holding"+(periodo-1)).getValor())); 
		
		
		matriz[3][3] =   Long.toString(Math.round(hash.get("DH_Metas_M1_M"+"BioMaster"+(periodo-1)).getValor())); 
		matriz[3][4] =   Long.toString(Math.round( (1-(((hash.get("DH_Metas_M1_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("DRE_Lair_Ebit_Ebitda_RL"+"Holding"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M1_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%";

		matriz[3][5] =   Long.toString(Math.round(hash.get("DH_Metas_M1_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[3][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[3][4].replace("%", ""))))
				* hash.get("DH_Metas_M1_P"+"BioMaster"+(periodo-1)).getValor())); 
		
		
		matriz[4][1] =   Long.toString(Math.round(hash.get("IF_PE_ValorAcoes"+"Holding"+(periodo-2)).getValor())); 
		matriz[4][2] =   Long.toString(Math.round(hash.get("IF_PE_ValorAcoes"+"Holding"+(periodo-1)).getValor())); 
		 
		matriz[4][3] =   Long.toString(Math.round(hash.get("DH_Metas_M2_M"+"BioMaster"+(periodo-1)).getValor())); 
		matriz[4][4] =   Long.toString(Math.round( (1-(((hash.get("DH_Metas_M2_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("IF_PE_ValorAcoes"+"Holding"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M2_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%"; 
		matriz[4][5] =   Long.toString(Math.round(hash.get("DH_Metas_M2_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[4][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[4][4].replace("%", ""))))
				* hash.get("DH_Metas_M2_P"+"BioMaster"+(periodo-1)).getValor()));  

		
		matriz[5][1] =   Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"BioMaster"+(periodo-2)).getValor())); 
		matriz[5][2] =   Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"BioMaster"+(periodo-1)).getValor())); 
		
		
		matriz[5][3] =   Long.toString(Math.round(hash.get("DH_Metas_M3_M"+"BioMaster"+(periodo-1)).getValor())); 
		matriz[5][4] =   Long.toString(Math.round( (1-(((hash.get("DH_Metas_M3_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("DRE_Lair_Ebit_Ebitda_RL"+"BioMaster"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M3_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%";

		matriz[5][5] =   Long.toString(Math.round(hash.get("DH_Metas_M3_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[5][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[5][4].replace("%", ""))))
				* hash.get("DH_Metas_M3_P"+"BioMaster"+(periodo-1)).getValor())); 
		
		

		matriz[6][1] =   Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda"+"BioMaster"+(periodo-2)).getValor())); 
		matriz[6][2] =   Long.toString(Math.round(hash.get("DRE_Lair_Ebit_Ebitda"+"BioMaster"+(periodo-1)).getValor())); 
		
		matriz[6][3] =   Long.toString(Math.round(hash.get("DH_Metas_M4_M"+"BioMaster"+(periodo-1)).getValor())); 
		matriz[6][4] =  Long.toString(Math.round( (1-(((hash.get("DH_Metas_M4_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("DRE_Lair_Ebit_Ebitda"+"BioMaster"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M4_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%";

		matriz[6][5] =   Long.toString(Math.round(hash.get("DH_Metas_M4_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[6][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[6][4].replace("%", ""))))
				* hash.get("DH_Metas_M4_P"+"BioMaster"+(periodo-1)).getValor()));
		
		
		
		matriz[7][1] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"BioMaster"+(periodo-2)).getValor())); 
		matriz[7][2] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"BioMaster"+(periodo-1)).getValor())); 
		 
		matriz[7][3] =   Long.toString(Math.round(hash.get("DH_Metas_M5_M"+"BioMaster"+(periodo-1)).getValor())); 
		matriz[7][4] =   Long.toString(Math.round( (1-(-((hash.get("DH_Metas_M5_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("EVA_CapInv_NetWorkCap"+"BioMaster"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M5_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%";  
		matriz[7][5] =   Long.toString(Math.round(hash.get("DH_Metas_M5_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[7][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[7][4].replace("%", ""))))
				* hash.get("DH_Metas_M5_P"+"BioMaster"+(periodo-1)).getValor()));   

		matriz[8][1] =   Long.toString(Math.round(hash.get("CVS_F_Cli"+"BioMaster"+(periodo-2)).getValor()*100))+"%"; 
		matriz[8][2] =   Long.toString(Math.round(hash.get("CVS_F_Cli"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		 
		matriz[8][3] =   Long.toString(Math.round(hash.get("DH_Metas_M6_M"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[8][4] =   Long.toString(Math.round( (1-(((hash.get("DH_Metas_M6_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("CVS_F_Cli"+"BioMaster"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M6_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%";  
		matriz[8][5] =   Long.toString(Math.round(hash.get("DH_Metas_M6_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[8][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[8][4].replace("%", ""))))
				* hash.get("DH_Metas_M6_P"+"BioMaster"+(periodo-1)).getValor()));   

		matriz[9][1] =   Long.toString(Math.round(hash.get("CVS_F_Soc"+"BioMaster"+(periodo-2)).getValor()*100))+"%"; 
		matriz[9][2] =   Long.toString(Math.round(hash.get("CVS_F_Soc"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		 
		matriz[9][3] =   Long.toString(Math.round(hash.get("DH_Metas_M7_M"+"BioMaster"+(periodo-1)).getValor()*100))+"%";
		matriz[9][4] =   Long.toString(Math.round( (1-(((hash.get("DH_Metas_M7_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("CVS_F_Soc"+"BioMaster"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M7_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%";  
		matriz[9][5] =   Long.toString(Math.round(hash.get("DH_Metas_M7_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[9][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[9][4].replace("%", ""))))
				* hash.get("DH_Metas_M7_P"+"BioMaster"+(periodo-1)).getValor()));   

		matriz[10][1] =   Long.toString(Math.round(hash.get("CVS_F_Col"+"BioMaster"+(periodo-2)).getValor()*100))+"%"; 
		matriz[10][2] =   Long.toString(Math.round(hash.get("CVS_F_Col"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		 
		matriz[10][3] =   Long.toString(Math.round(hash.get("DH_Metas_M8_M"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[10][4] =   Long.toString(Math.round( (1-(((hash.get("DH_Metas_M8_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("CVS_F_Col"+"BioMaster"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M8_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%";   
		matriz[10][5] =   Long.toString(Math.round(hash.get("DH_Metas_M8_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[10][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[10][4].replace("%", ""))))
				* hash.get("DH_Metas_M8_P"+"BioMaster"+(periodo-1)).getValor()));   

		matriz[11][1] =   Long.toString(Math.round(hash.get("EE"+"BioMaster"+(periodo-2)).getValor()*100))+"%"; 
		matriz[11][2] =   Long.toString(Math.round(hash.get("EE"+"BioMaster"+(periodo-1)).getValor()*100))+"%";
		 
		matriz[11][3] =   Long.toString(Math.round(hash.get("DH_Metas_M9_M"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[11][4] =   Long.toString(Math.round( (1-(((hash.get("DH_Metas_M9_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("EE"+"BioMaster"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M9_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%";    
		matriz[11][5] =   Long.toString(Math.round(hash.get("DH_Metas_M9_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[11][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[11][4].replace("%", ""))))
				* hash.get("DH_Metas_M9_P"+"BioMaster"+(periodo-1)).getValor()));   

		matriz[12][1] =   Long.toString(Math.round(hash.get("PSO"+"BioMaster"+(periodo-2)).getValor()*100))+"%";
		matriz[12][2] =   Long.toString(Math.round(hash.get("PSO"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		 
		matriz[12][3] =   Long.toString(Math.round(hash.get("DH_Metas_M10_M"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[12][4] =   Long.toString(Math.round( (1-(((hash.get("DH_Metas_M10_M"+"BioMaster"+(periodo-1)).getValor())- (hash.get("PSO"+"BioMaster"+(periodo-1)).getValor()))
				/Math.abs(hash.get("DH_Metas_M10_M"+"BioMaster"+(periodo-1)).getValor())))*100) )+"%";     
		matriz[12][5] =   Long.toString(Math.round(hash.get("DH_Metas_M10_P"+"BioMaster"+(periodo-1)).getValor()*100))+"%"; 
		matriz[12][6] =   Long.toString(Math.round(
				calculaPontuacao(Math.round(Double.parseDouble(matriz[12][4].replace("%", ""))))
				* hash.get("DH_Metas_M10_P"+"BioMaster"+(periodo-1)).getValor()));   
		matriz [13][6]=
				Long.toString( Math.round(
						( Double.parseDouble(matriz[3][6])+Double.parseDouble(matriz[4][6]) +Double.parseDouble(matriz[5][6])
						+Double.parseDouble(matriz[6][6])+Double.parseDouble(matriz[7][6]) +Double.parseDouble(matriz[8][6])
						+Double.parseDouble(matriz[9][6])+Double.parseDouble(matriz[10][6])+Double.parseDouble(matriz[11][6])  
						+Double.parseDouble(matriz[12][6]) ) )
						);

		
		

	}
	public  void preencheBSCComparativo(String[][] matriz, String organizacao)
	{ 
		Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
		//recuperar os grupos  
		listaGr =gr.getGruposParceiros(grupo);
		
		matriz[1][0] =   "Painel de Indicadores Comparativo - "+organizacao;
		for (int i=0; i<listaGr.size();i++)
		{
			lista =es.getHerdeiros("%%","Holding",periodo-1,listaGr.get(i).getId());  
			for (Elemento e:lista){
				hash.put(e.getCampo()+e.getOrganizacao()+(e.getPeriodo())+e.getGrupo(), e);
			}
			
			lista =es.getHerdeiros("%%","BioMaster",periodo-1,listaGr.get(i).getId());  
			for (Elemento e:lista){
				hash.put(e.getCampo()+e.getOrganizacao()+(e.getPeriodo())+e.getGrupo(), e);
			}
		}

		for (int j=0;j<listaGr.size();j++)
		{
			matriz[2][j+1] =listaGr.get(j).getNome();
		}
		for (int j=0;j<listaGr.size();j++)
		{

			matriz[2][j+1]=listaGr.get(j).getNome();
			
			matriz[3][j+1] =   
					Long.toString(Math.round( 
							calculaPontuacao(Math.round(
									(1-(((hash.get("DH_Metas_M1_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"Holding"+(periodo-1)+listaGr.get(j).getId()).getValor())
											)/Math.abs(hash.get("DH_Metas_M1_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100)) 
							* hash.get("DH_Metas_M1_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);

			matriz[4][j+1] =   
					Long.toString(Math.round(
							calculaPontuacao(Math.round(
									(1-(((hash.get("DH_Metas_M2_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("IF_PE_ValorAcoes"+"Holding"+(periodo-1)+listaGr.get(j).getId()).getValor())
											)/Math.abs(hash.get("DH_Metas_M2_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100))
							* hash.get("DH_Metas_M2_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);
			
			matriz[5][j+1] =   
					Long.toString(Math.round( 
							calculaPontuacao(Math.round(
									(1-(((hash.get("DH_Metas_M3_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
											)/Math.abs(hash.get("DH_Metas_M3_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100)) 
							* hash.get("DH_Metas_M3_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);
			
			
			matriz[6][j+1] =   
					Long.toString(Math.round(
							calculaPontuacao(Math.round(
									(1-(((hash.get("DH_Metas_M4_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("DRE_Lair_Ebit_Ebitda"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
											)/Math.abs(hash.get("DH_Metas_M4_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100))
							* hash.get("DH_Metas_M4_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);  
			
			
			matriz[7][j+1] =   
					Long.toString(Math.round(
							calculaPontuacao(Math.round(
									(1-(-((hash.get("DH_Metas_M5_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("EVA_CapInv_NetWorkCap"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
											)/Math.abs(hash.get("DH_Metas_M5_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100))
							* hash.get("DH_Metas_M5_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);  
			matriz[8][j+1] =
					Long.toString(Math.round(
							calculaPontuacao(Math.round(
									(1-(((hash.get("DH_Metas_M6_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("CVS_F_Cli"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
											)/Math.abs(hash.get("DH_Metas_M6_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100))
							* hash.get("DH_Metas_M6_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);  
			
			matriz[9][j+1] = 
					Long.toString(Math.round(
							calculaPontuacao(Math.round(
									(1-(((hash.get("DH_Metas_M7_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("CVS_F_Soc"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
											)/Math.abs(hash.get("DH_Metas_M7_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100))
							* hash.get("DH_Metas_M7_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);  
			matriz[10][j+1] =  
					Long.toString(Math.round(
							calculaPontuacao(Math.round(
									(1-(((hash.get("DH_Metas_M8_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("CVS_F_Col"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
											)/Math.abs(hash.get("DH_Metas_M8_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100))
							* hash.get("DH_Metas_M8_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);  
			matriz[11][j+1] =  
					Long.toString(Math.round(
							calculaPontuacao(Math.round(
									(1-(((hash.get("DH_Metas_M9_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("EE"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
											)/Math.abs(hash.get("DH_Metas_M9_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100))
							* hash.get("DH_Metas_M9_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);  
			matriz[12][j+1] =
					Long.toString(Math.round(
							calculaPontuacao(Math.round(
									(1-(((hash.get("DH_Metas_M10_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor()
											)-(hash.get("PSO"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
											) /Math.abs(hash.get("DH_Metas_M10_M"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())))*100))
							* hash.get("DH_Metas_M10_P"+"BioMaster"+(periodo-1)+listaGr.get(j).getId()).getValor())
							);  
			matriz [13][j+1]=Long.toString( Math.round(
					(Double.parseDouble(matriz[3][j+1])+Double.parseDouble(matriz[4][j+1]) +Double.parseDouble(matriz[5][j+1])
					+Double.parseDouble(matriz[6][j+1])+Double.parseDouble(matriz[7][j+1]) +Double.parseDouble(matriz[8][j+1])
					+Double.parseDouble(matriz[9][j+1])+Double.parseDouble(matriz[10][j+1])+Double.parseDouble(matriz[11][j+1])  
					+Double.parseDouble(matriz[12][j+1])))
					);
		};
	}
	public  void preencheValorEconomicop1(String[][] matriz)
	{ 
		//int col=1,contEmp=0;
		int coluna=1;
		for (int i=-2;i<=periodo-1;i++)
		{
			//cria hassh de elementos do Balan�o
			Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			lista =es.getHerdeiros("%%","%%",i,+grupo);
			for (Elemento e:lista){
				hash.put(e.getCampo()+e.getOrganizacao(), e);
			}

			matriz[2][coluna] =   Long.toString(Math.round(hash.get("EVA"+"Holding").getValor())); 
			matriz[3][coluna] =   Long.toString(Math.round(hash.get("EVA_Spread"+"Holding").getValor()*100))+"%";
			matriz[4][coluna] =   Long.toString(Math.round(hash.get("EVA_Spread_Roic"+"Holding").getValor()*100))+"%";
			matriz[5][coluna] =   Long.toString(Math.round(hash.get("TSR_FCFYld_FCF_Nopat"+"Holding").getValor()));
			matriz[6][coluna] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc"+"Holding").getValor()*100))+"%";
			matriz[7][coluna] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc_CstCap_PL_tx"+"Holding").getValor()*100))+"%";
			matriz[8][coluna] =   Long.toString(Math.round((
					(hash.get("DRE_Lair_Juros_DivCP"+"Holding").getValor()+hash.get("DRE_Lair_Juros_DivLP"+"Holding").getValor())/
					(hash.get("DRE_Lair_Juros_DivCP_vl"+"Holding").getValor() +hash.get("DRE_Lair_Juros_DivLP_vl"+"Holding").getValor()))*100))+"%";
			matriz[9][coluna] =   Long.toString(Math.round(hash.get("EVA_CapInv"+"Holding").getValor()));
			matriz[10][coluna] =   Long.toString(Math.round(hash.get("EVA_CapInv_AtFx"+"Holding").getValor()));
			matriz[11][coluna] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"Holding").getValor()));
			matriz[12][coluna] =   Long.toString(Math.round(hash.get("EVA_CapInv_Disp"+"Holding").getValor()));


			matriz[16][coluna] =   Long.toString(Math.round(hash.get("EVA"+"BioMaster").getValor())); 
			matriz[17][coluna] =   Long.toString(Math.round(hash.get("EVA_Spread"+"BioMaster").getValor()*100))+"%";
			matriz[18][coluna] =   Long.toString(Math.round(hash.get("EVA_Spread_Roic"+"BioMaster").getValor()*100))+"%";
			matriz[19][coluna] =   Long.toString(Math.round(hash.get("TSR_FCFYld_FCF_Nopat"+"BioMaster").getValor()));
			matriz[20][coluna] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc"+"BioMaster").getValor()*100))+"%";
			matriz[21][coluna] =   Long.toString(Math.round(hash.get("EVA_Spread_Wacc_CstCap_PL_tx"+"BioMaster").getValor()*100))+"%";
			matriz[22][coluna] =   Long.toString(Math.round((
					(hash.get("DRE_Lair_Juros_DivCP"+"BioMaster").getValor()+hash.get("DRE_Lair_Juros_DivLP"+"BioMaster").getValor())/
					(hash.get("DRE_Lair_Juros_DivCP_vl"+"BioMaster").getValor() +hash.get("DRE_Lair_Juros_DivLP_vl"+"BioMaster").getValor()))*100))+"%";
			matriz[23][coluna] =   Long.toString(Math.round(hash.get("EVA_CapInv"+"BioMaster").getValor()));
			matriz[24][coluna] =   Long.toString(Math.round(hash.get("EVA_CapInv_AtFx"+"BioMaster").getValor()));
			matriz[25][coluna] =   Long.toString(Math.round(hash.get("EVA_CapInv_NetWorkCap"+"BioMaster").getValor()));
			matriz[26][coluna] =   Long.toString(Math.round(hash.get("EVA_CapInv_Disp"+"BioMaster").getValor()));



			coluna=coluna+1;    
		}

	}

	public  void preencheExecucaoEstrategica(String[][] matriz)
	{ 
		//int col=1,contEmp=0;
		int coluna=1;//,linha=2;
		for (int i=-2;i<=periodo-1;i++)
		{
			//cria hassh de elementos do Balan�o
			Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			lista =es.getHerdeiros("%%","%%",i,+grupo);
			for (Elemento e:lista){
				hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
			}

			matriz[1][coluna] =   Long.toString(Math.round(hash.get("EE"+"Holding"+i).getValor()*100))+"%"; 
			matriz[2][coluna] =   Long.toString(Math.round(hash.get("EE_GO"+"Holding"+i).getValor()*100))+"%"; 
			matriz[3][coluna] =   Long.toString(Math.round(hash.get("EE_GP"+"Holding"+i).getValor()*100))+"%";
			matriz[4][coluna] =   Long.toString(Math.round(hash.get("EE_GC"+"Holding"+i).getValor()*100))+"%";
			matriz[5][coluna] =   Long.toString(Math.round(hash.get("EE_GF"+"Holding"+i).getValor()*100))+"%";

			matriz[7][coluna] =   Long.toString(Math.round(hash.get("EE"+"DentalByte"+i).getValor()*100))+"%";
			matriz[8][coluna] =   Long.toString(Math.round(hash.get("EE_GO"+"DentalByte"+i).getValor()*100))+"%"; 
			matriz[9][coluna] =   Long.toString(Math.round(hash.get("EE_GP"+"DentalByte"+i).getValor()*100))+"%";
			matriz[10][coluna] =   Long.toString(Math.round(hash.get("EE_GC"+"DentalByte"+i).getValor()*100))+"%";
			matriz[11][coluna] =   Long.toString(Math.round(hash.get("EE_GF"+"DentalByte"+i).getValor()*100))+"%";    

			matriz[13][coluna] =   Long.toString(Math.round(hash.get("EE"+"DigiMetrics"+i).getValor()*100))+"%";
			matriz[14][coluna] =   Long.toString(Math.round(hash.get("EE_GO"+"DigiMetrics"+i).getValor()*100))+"%"; 
			matriz[15][coluna] =   Long.toString(Math.round(hash.get("EE_GP"+"DigiMetrics"+i).getValor()*100))+"%";
			matriz[16][coluna] =   Long.toString(Math.round(hash.get("EE_GC"+"DigiMetrics"+i).getValor()*100))+"%";
			matriz[17][coluna] =   Long.toString(Math.round(hash.get("EE_GF"+"DigiMetrics"+i).getValor()*100))+"%";    


			matriz[19][coluna] =   Long.toString(Math.round(hash.get("EE"+"CleanSystem"+i).getValor()*100))+"%";
			matriz[20][coluna] =   Long.toString(Math.round(hash.get("EE_GO"+"CleanSystem"+i).getValor()*100))+"%"; 
			matriz[21][coluna] =   Long.toString(Math.round(hash.get("EE_GP"+"CleanSystem"+i).getValor()*100))+"%";
			matriz[22][coluna] =   Long.toString(Math.round(hash.get("EE_GC"+"CleanSystem"+i).getValor()*100))+"%";
			matriz[23][coluna] =   Long.toString(Math.round(hash.get("EE_GF"+"CleanSystem"+i).getValor()*100))+"%";    


			matriz[25][coluna] =   Long.toString(Math.round(hash.get("EE"+"HardTech"+i).getValor()*100))+"%";
			matriz[26][coluna] =   Long.toString(Math.round(hash.get("EE_GO"+"HardTech"+i).getValor()*100))+"%"; 
			matriz[27][coluna] =   Long.toString(Math.round(hash.get("EE_GP"+"HardTech"+i).getValor()*100))+"%";
			matriz[28][coluna] =   Long.toString(Math.round(hash.get("EE_GC"+"HardTech"+i).getValor()*100))+"%";
			matriz[29][coluna] =   Long.toString(Math.round(hash.get("EE_GF"+"HardTech"+i).getValor()*100))+"%"; 


			matriz[31][coluna] =   Long.toString(Math.round(hash.get("EE"+"BioMaster"+i).getValor()*100))+"%";
			matriz[32][coluna] =   Long.toString(Math.round(hash.get("EE_GO"+"BioMaster"+i).getValor()*100))+"%"; 
			matriz[33][coluna] =   Long.toString(Math.round(hash.get("EE_GP"+"BioMaster"+i).getValor()*100))+"%";
			matriz[34][coluna] =   Long.toString(Math.round(hash.get("EE_GC"+"BioMaster"+i).getValor()*100))+"%";
			matriz[35][coluna] =   Long.toString(Math.round(hash.get("EE_GF"+"BioMaster"+i).getValor()*100))+"%"; 


			coluna=coluna+1;
		}
	}  
	public String classificaEmpresa(long valor)
	{
		String cla=""; 
		// if (valor <=20) { cla="Cegueira da Paix�o";}
		// else
		// if ((valor >20) && (valor <=35)) {cla="Anarquia Destemida";}
		// else
		// if ((valor >35) && (valor <=50)) {cla="Equil�brio Cristalino";}
		// else
		// if ((valor >50) && (valor <=60)) {cla="Anos Dourados";}
		// else    
		// if ((valor >60) && (valor <=75)) {cla="Avestruz Empavonado";}
		// else
		// if ((valor >75) && (valor <=90)) {cla="Resgate Redentor";}
		// else
		// {cla="Sil�ncio Sepulcral";
		// }
		return cla;
	}
	public  void preenchePsop1(String[][] matriz)
	{  
		//int col=1,contEmp=0;
		int coluna=1;//,linha=2;
		for (int i=-2;i<=periodo-1;i++)
		{
			//cria hassh de elementos do Balan�o
			Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			lista =es.getHerdeiros("%%","%%",i,+grupo);
			for (Elemento e:lista){
				hash.put(e.getCampo()+e.getOrganizacao(), e);
			}
			int lin=1;
			matriz[++lin][coluna] =  Long.toString(Math.round(hash.get("PSO"+"Holding").getValor()*100))+"%"; 
			//matriz[4][coluna] =  classificaEmpresa(Math.round(hash.get("PSO"+"Holding").getValor()*100)); 
			matriz[++lin][coluna] =  Long.toString(Math.round(hash.get("PSO_Rel"+"Holding").getValor()*100))+"%"; 
			matriz[++lin][coluna] =  Long.toString(Math.round(hash.get("PSO_C"+"Holding").getValor()*100))+"%";
			matriz[++lin][coluna] =  Long.toString(Math.round(hash.get("PSO_Res"+"Holding").getValor()*100))+"%";
			matriz[++lin][coluna] =  Long.toString(Math.round(hash.get("PSO_F"+"Holding").getValor()*100))+"%";
			matriz[++lin][coluna] =  Long.toString(Math.round(hash.get("PSO_E"+"Holding").getValor()*100))+"%";
			
			matriz[lin+=4][coluna] =   Long.toString(Math.round(hash.get("PSO"+"BioMaster").getValor()*100))+"%"; 
			//matriz[49][coluna] =   classificaEmpresa(Math.round(hash.get("PSO"+"BioMaster").getValor()*100)); 
			matriz[++lin][coluna] =   Long.toString(Math.round(hash.get("PSO_Rel"+"BioMaster").getValor()*100))+"%"; 
			matriz[++lin][coluna] =   Long.toString(Math.round(hash.get("PSO_C"+"BioMaster").getValor()*100))+"%";
			matriz[++lin][coluna] =   Long.toString(Math.round(hash.get("PSO_Res"+"BioMaster").getValor()*100))+"%";
			matriz[++lin][coluna] =   Long.toString(Math.round(hash.get("PSO_F"+"BioMaster").getValor()*100))+"%";
			matriz[++lin][coluna] =   Long.toString(Math.round(hash.get("PSO_E"+"BioMaster").getValor()*100))+"%";
			coluna=coluna+1;
		}
	}


	public  void preencheRetornoAcionista(String[][] matriz)
	{  

		int coluna=1;//,linha=2;
		for (int i=-2;i<=periodo-1;i++)
		{
			//cria hassh de elementos do Balan�o
			Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			lista =es.getHerdeiros("%%","%%",i,+grupo);
			for (Elemento e:lista){
				hash.put(e.getCampo()+e.getOrganizacao(), e);
			}

			matriz[3][coluna] =  Long.toString(Math.round(hash.get("TSR"+"Holding").getValor()*100))+"%"; 
			matriz[4][coluna] =   Long.toString(Math.round((hash.get("TSR_VRec"+"Holding").getValor()+hash.get("TSR_VMEbitda"+"Holding").getValor())*100))+"%";
			matriz[5][coluna] =   Long.toString(Math.round(hash.get("TSR_VRec"+"Holding").getValor()*100))+"%"; 
			matriz[6][coluna] =   Long.toString(Math.round(hash.get("TSR_VMEbitda"+"Holding").getValor()*100))+"%";
			matriz[7][coluna] =   Long.toString(Math.round(hash.get("TSR_VEvEbitda"+"Holding").getValor()*100))+"%";
			matriz[8][coluna] =   Long.toString(Math.round(hash.get("TSR_FCFYld"+"Holding").getValor()*100))+"%";
			coluna=coluna+1;
		}
	}
	public  void preencheAlocacaoTempoCEO(String[][] matriz)
	{  

		int coluna=1;//,linha=2;
		//for (int i=-2;i<=periodo-1;i++)
		{
			//cria hassh de elementos do Balan�o
			Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			lista =es.getHerdeiros("%%","%%",periodo-1,+grupo);
			for (Elemento e:lista){
				hash.put(e.getCampo()+e.getOrganizacao(), e);
			}
			for (int col=0;col<5;col++){
				int lin=1;
				matriz[lin][2*col+2] = Long.toString(Math.round(hash.get("DUN_ATL_Soc"+empresas[col]).getValor()*40))+"h";
				matriz[lin++][2*col+1] = Long.toString(Math.round(hash.get("DUN_ATL_SocReal"+empresas[col]).getValor()*40))+"h";
				matriz[lin][2*col+2] = Long.toString(Math.round(hash.get("DUN_ATL_Ac"+empresas[col]).getValor()*40))+"h";
				matriz[lin++][2*col+1] = Long.toString(Math.round(hash.get("DUN_ATL_AcReal"+empresas[col]).getValor()*40))+"h";
				matriz[lin][2*col+2] = Long.toString(Math.round(hash.get("DUN_ATL_Cli"+empresas[col]).getValor()*40))+"h";
				matriz[lin++][2*col+1] = Long.toString(Math.round(hash.get("DUN_ATL_CliReal"+empresas[col]).getValor()*40))+"h";
				matriz[lin][2*col+2] = Long.toString(Math.round(hash.get("DUN_ATL_Col"+empresas[col]).getValor()*40))+"h";
				matriz[lin++][2*col+1] = Long.toString(Math.round(hash.get("DUN_ATL_ColReal"+empresas[col]).getValor()*40))+"h";
				matriz[lin][2*col+2] = Long.toString(Math.round(hash.get("DUN_ATL_AtRot"+empresas[col]).getValor()*40))+"h";
				matriz[lin++][2*col+1] = Long.toString(Math.round(hash.get("DUN_ATL_AtRotReal"+empresas[col]).getValor()*40))+"h";
				matriz[lin][2*col+2] = Long.toString(Math.round(hash.get("DUN_ATL_TemasUrgImp"+empresas[col]).getValor()*40))+"h";
				matriz[lin++][2*col+1] = Long.toString(Math.round(hash.get("DUN_ATL_TemasUrgImpReal"+empresas[col]).getValor()*40))+"h";
				matriz[lin][2*col+2] = Long.toString(Math.round(hash.get("DUN_ATL_EI"+empresas[col]).getValor()*40))+"h";
				matriz[lin++][2*col+1] = Long.toString(Math.round(hash.get("DUN_ATL_EIReal"+empresas[col]).getValor()*40))+"h";
				matriz[lin][2*col+2] = Long.toString(Math.round(hash.get("DUN_ATL_Coach"+empresas[col]).getValor()*40))+"h";
				matriz[lin++][2*col+1] = Long.toString(Math.round(hash.get("DUN_ATL_CoachReal"+empresas[col]).getValor()*40))+"h";
			}
		}
	}
	public void preencheComunicaoEstrategica(String[][] matriz)
	{
		Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

		//busca a holding e as empresas do grupo periodo atual
		lista =es.getHerdeiros("%%","%%",periodo-1,+grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao(), e);
		}

		matriz[1][0] =  hash.get("DH_Com_Txt"+"Holding").getConteudo();
	}
	public  void preencheInsightPessoas(String[][] matriz)
	{  
		// 
		//       String tipo='';
		//      //cria hassh de elementos do Balan�o
		//        Hashtable<String, PessoasChave> hash = new Hashtable<String, PessoasChave>();
		//   
		//    //busca a holding e as empresas do grupo periodo atual
		//        listaP =pc.getComunicacaoPessoas("%%",periodo+1);
		//    for (PessoasChave e:listaP){
		//   hash.put(e.getCargo()+e.getOrganizacao()+(periodo+1), e);
		//    }
		//    
		//    
		//    Hashtable<String, Elemento> hashE = new Hashtable<String, Elemento>();
		//   
		//    //busca a holding e as empresas do grupo periodo atual
		//        lista =es.getHerdeiros("%%","%%",periodo+1,grupo);
		//    for (Elemento e:lista){
		//   hashE.put(e.getCampo()+e.getOrganizacao(), e);
		//    }
		//    
		//    tipo=Hash.get(hash.get(""+(periodo+1)+"Holding")+"Holding").getValor()
		//    if (Math.round(hash.get("TSR_FCFYld"+"Holding").getValor() >
		//    matriz[2][1] =  hash.get(""+(periodo+1)+"Holding").getNome(); 
		//    matriz[2][2] =  hash.get(""+(periodo+1)+"Holding").getCargo();
		//    matriz[2][3] =  hash.get(""+(periodo+1)+"Holding").getComunicacao();
	}
	public   void desenhaRelatorio(int numeroColunas, String[][] matriz, String[][] formato , int[] widths, boolean marcar,int relatorio) throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException
	{       
		double maior=0,menor=101;
		PdfPCell maiorCelula =new PdfPCell();
		PdfPCell menorCelula =new PdfPCell();
		switch(relatorio)
		{
		case 0: break;
		case 1: preencheDRE(matriz,"Holding");break;
		case 2: preencheBalanco(matriz,"Holding");break;
		
		}
		//Relat�rios  TimeSheet
		//determina o numero de colunas da tabela
		//cria a tabela com o numero de colunas passado
		PdfPTable table = new PdfPTable(numeroColunas);
		//seta o tamanho das colunas de acordo com o vetor de tamanho passado
		table.setWidths(widths);

		// document.add(linebreak);
//		document.add(new Paragraph(" "));
		for (int i = 0; i<matriz.length; i++){
			for (int j = 0; j<matriz[i].length; j++){
				StringTokenizer st = new StringTokenizer(formato[i][j]);
				char tipo=st.nextToken().charAt(0);
				int tamanho = Integer.parseInt(st.nextToken());
				char align=st.nextToken().charAt(0);
				int padding = Integer.parseInt(st.nextToken());
				float espessura =Float.parseFloat(st.nextToken());
				Font f = new Font();
				//seta o tipo de formata��o da fonte
				f.setSize(tamanho);
				switch(tipo){
				case 'I': f.setStyle(Font.ITALIC);break;
				case 'B': f.setStyle(Font.BOLD);break;
				}
				Chunk c = new Chunk(matriz[i][j]);
				//System.out.println("primeira"+matriz[i][j]); 
				c.setFont(f);
				Phrase ph = new Phrase();
				ph.add(c);
				//configura a celula para bordas, espessura da borda e espaçamento antes e depois da c�lula.
				PdfPCell cell = new PdfPCell(ph);
				cell.setBorder(Rectangle.BOTTOM);
				cell.setBorderWidth(espessura);
				cell.setPaddingTop(padding);
				cell.setPaddingBottom(padding);
				//if ((i==2)&&(j==2)) cell.setBackgroundColor(BaseColor.ORANGE);
				//configura o alinhamento do texto
				switch(align){
				case 'R': cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);break;
				case 'L': cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);break;
				case 'C': cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);break;
				}
				//faz uma mescla nas c�lulas da primeira linha
				//  if ((j==0) && (i==0)){
				// cell.setColspan(numeroColunas-2);
				// }

				table.addCell(cell);
				//guarda a maior e a menor c�lula da linha
				if (marcar ==true)
				{
					//System.out.println("marcar" +matriz[i][j]); 
					try
					{

						if(Double.parseDouble(matriz[i][j].replace("%","")) > maior)  
						{ maior= Double.parseDouble(matriz[i][j].replace("%",""));
						//System.out.println("maior:" +maior);
						maiorCelula=cell ;
						//System.out.println(cell);
						}
						if(Double.parseDouble(matriz[i][j].replace("%",""))  < menor)  
						{ menorCelula=cell ;
						menor= Double.parseDouble(matriz[i][j].replace("%",""));
						//System.out.println("menor:" +menor);
						//System.out.println(cell);
						}  
					}
					catch(Exception e)
					{//System.out.println("n�o � numero" +matriz[i][j]);}

					}

				}

			}
			if (marcar ==true)
			{ 
				maiorCelula.setBackgroundColor(BaseColor.ORANGE);
				menorCelula.setBackgroundColor(BaseColor.RED);   
			}
		}
		document.add(table);
		maior=0;
		menor=101;
	}
	
	
	public void preencheReceitaEsperada(String matriz [][]) throws ProxyNaoIniciadoException, ElementoNaoEncontradoException
	{
		
		 
	    int numGrupo=0;	 
		Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
		lista =es.getHerdeiros("%%","%%",periodo-1,grupo);
		for (Elemento e:lista){
			hash.put(e.getCampo()+e.getOrganizacao()+e.getGrupo()+e.getPeriodo(), e);
		}
		
		cicloService= new CicloService(grupo,periodo-1);
		cicloService.initializeProxy();
		
		//int matrizValores[][] =new int[8][4];
		//int matrizValores[][] ={{1,1,1,1,1,1},{2,2,2,2,2,2},{3,3,3,3,3,3},{4,4,4,4,4,4},{5,5,5,5,5,5},{6,6,6,6,6,6},{7,7,7,7,7,7},{8,8,8,8,8,8},{9,9,9,9,9,9}};
		//matrizValores=matValores;

		AvaliacaoReceita avalReceita =new AvaliacaoReceita(cicloService);
		
		int j=1;
		int i=0;
		
       for(j=0;j<6;j++)
		{
			
			//if (j ==2) {j=j+2;};
			//if (j ==11){j=j+1;};
				
			
		
			if (j<=3) 
			{//CONFIRMAR
				if (periodo > 1) {
					matriz[1][j+1]=""+Math.round(ReceitaEsperadaBundle.rl[j][periodo-2]);
				} else {
					try {
						matriz[1][j+1]=""+Math.round(cicloService.getElemento("DRE_Lair_Ebit_Ebitda_RL", empresas[j+1], periodo-2, grupo).getValor());
					} catch (ProxyGrupoIncorretoException e1) {
						matriz[1][j+1]="0";
						e1.printStackTrace();
					}
				}
				matriz[4][j+1]=avalReceita.getCom(empresas[i+1], periodo-1);
		    	matriz[5][j+1]=avalReceita.getPE(empresas[i+1], periodo-1);
		    	matriz[6][j+1]=avalReceita.getSL(empresas[i+1], periodo-1);
			    matriz[7][j+1]=avalReceita.getLA(empresas[i+1], periodo-1);
			    matriz[8][j+1]=avalReceita.getAO_opex(empresas[i+1], periodo-1);
			    matriz[9][j+1]=avalReceita.getAO_capex(empresas[i+1], periodo-1);

			   matriz[11][j+1]=""+Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+empresas[j+1]+grupo+(periodo-1)).getValor());
	 		   i=i+1;
			}
	 		if (j==4)
	 		{
	 			//matriz[1][j+2]=""+0;//""+matrizValores[i][1];	
	 		   matriz[11][j+2]=""+Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+empresas[0]+grupo+(periodo-1)).getValor());
	 		}
	 		if (j==5)
	 		{
	 			//matriz[1][j+3]=""+0;//""+matrizValores[i][1];
	 		   matriz[11][j+3]=""+Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"Holding"+grupo+(periodo-1)).getValor());
	 		}
		    
		}
			
			
			//numGrupo++;
}
		




	
	public void preencheReceitaEsperadaComp(String matriz [][])
	{
			 int numGrupo=0;
			 int coluna;
			//cria hassh de elementos do balan�o
		    listaGr =gr.getGruposParceiros(grupo);
			Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();

			//busca a holding e as empresas do grupo periodo atual
			
			for (coluna=0;coluna<listaGr.size();coluna++)
			{
		    
				lista =es.getHerdeiros("%%","%%",periodo-1,listaGr.get(coluna).getId());
				for (Elemento e:lista){
					hash.put(e.getCampo()+e.getOrganizacao()+e.getGrupo()+e.getPeriodo(), e);
			}
			}
			
					
		
		int j=0;
		int i=0;
		

		//matriz[1][0] =   "Painel de Indicadores Comparativo - "+organizacao;
		for (j=0; j<listaGr.size();j++)
		{
			
				
			matriz[j+1][0]=""+listaGr.get(j).getNome();
		    matriz[j+1][1]=""+Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"CleanSystem"+listaGr.get(j).getId()+""+(periodo-1)).getValor());
			matriz[j+1][2]=""+Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"DentalByte"+listaGr.get(j).getId()+""+(periodo-1)).getValor());
			matriz[j+1][3]=""+Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"DigiMetrics"+listaGr.get(j).getId()+""+(periodo-1)).getValor());
			matriz[j+1][4]=""+Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"HardTech"+listaGr.get(j).getId()+""+(periodo-1)).getValor());
			matriz[j+1][6]=""+Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"BioMaster"+listaGr.get(j).getId()+""+(periodo-1)).getValor());
			matriz[j+1][8]=""+Math.round(hash.get("DRE_Lair_Ebit_Ebitda_RL"+"Holding"+listaGr.get(j).getId()+""+(periodo-1)).getValor());
			
			
		}
		
		
		//    	int linha=0;
//    	long matrizResultEsp[][] = new long[8][4];
//		
//	    listaGr = gr.getGruposParceiros(grupo);
//	    System.out.println("ReceitaEsperada-Grupos:" + listaGr.size());
//	    Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
//	   	for (int i = 0; i < listaGr.size(); i++)
//	   	{
//		   lista = es.getHerdeiros("%%", "%%", periodo - 1, listaGr.get(i).getId());
//		   for (Elemento e : lista) {
//			hash.put(e.getCampo() + (e.getPeriodo()) + e.getGrupo() + e.getOrganizacao(), e);
//		   }	
//		
//	     int numGrupo=0;
//	     while  ((linha <8) && (numGrupo< listaGr.size())) {
//	  	  
//		  }
//		linha=0;
//		numGrupo=numGrupo+1;
//	}	
}

	
	
	public void inicio() throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException  
	{ 
		System.out.println("IN�CIO GERA��O");
		//this.organizacao = organizacao;
		//Cria��o do Relat�rio

		// cria��o do DR

		try {

			//LineSeparator separator = new LineSeparator();
			//separator.setLineWidth((float) 1);
			//Chunk linebreak = new Chunk(separator);
			document.setPageSize(PageSize.A4.rotate());

			//Verifica se o diretório da turma existe
			//int turma = gr.getGrupo(grupo).getTurma();
			File dir_turma = new File("TimeSheet_"+nomeColaborador);
			if (!dir_turma.exists()){
				dir_turma.mkdir();
			}
			
			File dir_grupo = new File("TimeSheet_Colaborador_"+nomeColaborador;
			if (!dir_grupo.exists()){
				dir_grupo.mkdir();
			}

			ConfigService configService = new ConfigService();
			Config config = configService.getConfig(grupo);
			periodo = config.getPeriodo();

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("turma_"+turma+"/grupo_"+grupo+"/"+organizacao+"_grupo"+grupo+"_periodo"+(periodo-1)+".pdf"));
			writer.setPageEvent(new CabecalhoRodape("Grupo:"+nomeGrupo, periodo-1,organizacao));
			document.open();
			document.addSubject("Timesheet - WhiteFox"); 
			document.addKeywords("www.whitefox.capital");
			document.addCreator("TimeSheet");
			document.addAuthor("WhiteFox");
//			Image img1 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/biomaster.png");
//			Image img11 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/octopus.png");
//			Image img2 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/cleansystem.png");
//			Image img3 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/dentalbyte.png");
//			Image img4 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/digimetrics.png");
//			Image img5 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/hardtech.png");
			PdfContentByte cb = writer.getDirectContent();
			Phrase header = new Phrase("Relatório de TimeSheet do Colaborador "+ /*nomeColaborador + " - do periodo: "+""+(periodo-1)+  " -  BioMaster " ,  new Font(Font.FontFamily.UNDEFINED, 14, Font.BOLD)*/);
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header,(document.right() - document.left()) / 2 + document.leftMargin() ,document.top() - 30, 0);
//			img1.scaleToFit(100,100);
//			img11.scaleToFit(70,70);
//			img2.scaleToFit(70,70);
//			img3.scaleToFit(70,70);
//			img4.scaleToFit(70,70);
//			img5.scaleToFit(70,70);
//			img1.setAbsolutePosition(350,350);
//			img11.setAbsolutePosition(180,200);
//			img2.setAbsolutePosition(280,200);
//			img3.setAbsolutePosition(380,200);
//			img4.setAbsolutePosition(480,200);
//			img5.setAbsolutePosition(580,200);
//			document.add(img1);
//			document.add(img11);
//			document.add(img2);
//			document.add(img3);
//			document.add(img4);
//			document.add(img5);

		}
		//  document.add(linebreak);
		catch(DocumentException de) {
			System.err.println(de.getMessage());
		}
		catch(IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		//P�gina DO
		document.newPage();
		titulo("Tabela de Horas","");
		subTituloDO(6);
		this.desenhaRelatorio(3, resumoTimeSheet, resumoTimeSheetFormato,new int[]{6,2,2},false,1);
		espaco();
//		document.newPage();
		document.close();    
		System.out.println("FIM GERA��o");
	} 
	
	
	public void tituloCapa(String descricao, String comentario) throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException{
		String[][] titulo = {{descricao,comentario}};
		String[][] formato = {{"B 14 C 0 0","N 6 C 0 0"}};
		int[] larguras = {3,1};
		espaco();
		this.desenhaRelatorio(2,titulo, formato, larguras, false, 0);
		espaco();

	}
	
	public void titulo(String descricao, String comentario) throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException{
		String[][] titulo = {{descricao,comentario}};
		String[][] formato = {{"B 8 L 0 0","N 6 R 0 0"}};
		int[] larguras = {3,1};
		espaco();
		this.desenhaRelatorio(2,titulo, formato, larguras, false, 0);
		espaco();
	}
	
	public void subTituloDO(int primeiraColuna) throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException{
		String[][] titulo = {{"","Excel�ncia em\nOpera��o", "Excel�ncia em\nInova��o", "Excel�ncia em\nCustomiza��o", "Excel�ncia em\nIntegra��o"}};
		String[][] formato = {{"N 8 L 1 0","B 8 C 1 0","B 8 C 1 0","B 8 C 1 0","B 8 C 1 0"}};
		int[] larguras = {primeiraColuna,7,7,7,7};
		this.desenhaRelatorio(5,titulo, formato, larguras, false, 0);
	}
	
	
//	public void subTituloMultiNegocio() throws DocumentException{
//		String[][] titulo = {{"Perfil MultiNegócio", "Situa��es Estrat�gicas", "N�vel de Centraliza��o Funcional", "Perfil Estrat�gico","HHI"}};
//		String[][] formato = {{"B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0"}};
//		int[] larguras = {4,2,8,2,3};
//		this.desenhaRelatorio(5,titulo, formato, larguras, false, 0);
//	}
	
	
//	public void rodapeMultiNegocio() throws DocumentException{
//		String[][] rodape = {{"1-Portfolio Management", "2-Strategic Design", "3-Operational Check & Balance", "4-Functional Alignment","5-Corporate Engagement"}};
//		String[][] formato = {{"B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0"}};
//		int[] larguras = {6,6,6,6,6};
//		this.desenhaRelatorio(5,rodape, formato, larguras, false, 0);
//	}
	
	
	
	public void rodapeReceitaEsperada(String texto) throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException{
     String[][] rodape = {{texto}};
     String[][] formato = {{"B 8 C 0 0"}};
	 int[] larguras = {40};
	 espaco();
	 this.desenhaRelatorio(1,rodape, formato, larguras, false, 0);
	
	}
	
	public void subTituloATL() throws DocumentException, ProxyNaoIniciadoException, ElementoNaoEncontradoException{
		String[][] titulo = {{" ",empresas[0],empresas[1],empresas[2],empresas[3],empresas[4]}};
		String[][] formato = {{"N 8 L 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0"}};
		int[] larguras = {3,1,1,1,1,1};
		this.desenhaRelatorio(6,titulo, formato, larguras, false, 0);
	}
	public void espaco() throws DocumentException{
		document.add(new Paragraph(" "));
//		String[][] rotulo = {{""}};
//		String[][] formato = {{"B 8 L 0 0"}};
//		int[] larguras = {1};
//		this.desenhaRelatorio(1,rotulo, formato, larguras, false, 0);
	}

}