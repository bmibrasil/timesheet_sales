package webServiceController;

import action.AprovacaoAction;
import config.AprovacaoConfig;
import enumerations.AprovacaoRequests;

public class WSAprovacaoController extends Controller {

	@Override
	public String apply(String action) throws Exception, Throwable {
		
		AprovacaoConfig aprovacaoConfig = new AprovacaoConfig();
		
		AprovacaoRequests aprovacaoRequests = aprovacaoConfig.getAprovacaoRequests(action);
		
		AprovacaoAction aprovacaoAction = new AprovacaoAction( aprovacaoRequests, parserJsonRecebido );
		
		return aprovacaoAction.apply();
		
	}

}
