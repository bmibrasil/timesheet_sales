package webServiceController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Util.GsonPrettyJson;

public abstract class Controller {

	protected JsonObject parserJsonRecebido;
	
	public String process(String jsonRecebido) throws Exception, Throwable {
		
		String prettyJsonString = new GsonPrettyJson().toPrettyJson(jsonRecebido);
		
		String mensagem = String.format("[WEB-SERVICE-CONTROLLER::INTERFACE-CONTROLLER::NOTIFICATION] - %s :: \n%s", "Requisicao", prettyJsonString);
    	
		System.out.println(mensagem);
    	
    	JsonParser jsonParser = new JsonParser();
    	
    	parserJsonRecebido = (JsonObject) jsonParser.parse(jsonRecebido);
		
		String action = parserJsonRecebido.get("action").getAsString();
		String result = apply(action);
		
		String mensagemResposta = String.format("[WEB-SERVICE-CONTROLLER::INTERFACE-CONTROLLER::NOTIFICATION] - %s :: \n%s", "Resposta", result);
		
		System.out.println(mensagemResposta);
		
		return result;
		
	}
	
	protected abstract String apply( String action ) throws Exception, Throwable;
	
}
