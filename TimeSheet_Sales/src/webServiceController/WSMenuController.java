package webServiceController;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import Exceptions.ProxyNaoIniciadoException;
import action.PermissaoAction;
import config.MenuConfig;
import enumerations.MenuRequests;

public class WSMenuController extends Controller {
	
	@Override
	public String apply(String action) throws FileNotFoundException, IOException, ProxyNaoIniciadoException, SQLException{
		
		MenuConfig menuConfig = new MenuConfig();
		
		MenuRequests menuRequests = menuConfig.getMenuRequests(action);
		
		PermissaoAction permissaoAction = new PermissaoAction( menuRequests, parserJsonRecebido );
		
		return permissaoAction.apply();
	}
	
}
