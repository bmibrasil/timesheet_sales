package webServiceController;

import action.ParametrosAction;
import config.ParametrosConfig;
import enumerations.ParametrosRequests;

public class WSParametrosController extends Controller{

	@Override
	public String apply(String action) throws Exception, Throwable {
		
		ParametrosConfig parametrosConfig = new ParametrosConfig();
		
		ParametrosRequests parametrosRequest = parametrosConfig.getParametrosRequests(action);
		
		ParametrosAction parametrosAction = new ParametrosAction( parametrosRequest, parserJsonRecebido );
		
		return parametrosAction.apply();
		
	}

}
