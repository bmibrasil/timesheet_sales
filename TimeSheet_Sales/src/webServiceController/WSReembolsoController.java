package webServiceController;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

import javax.imageio.ImageIO;

import action.ReembolsoAction;
import config.ReembolsoConfig;
import enumerations.ReembolsoRequests;
import sun.misc.BASE64Decoder;

public class WSReembolsoController extends Controller{

	
	public static BufferedImage decodeToImage(String imageString) {
		 
        BufferedImage image = null;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }
	

	@Override
	public String apply(String action) throws Exception{
	
		ReembolsoConfig reembolsoConfig = new ReembolsoConfig();
		
		ReembolsoRequests reembolsoRequests = reembolsoConfig.getReembolsoRequests(action);
		
		ReembolsoAction reembolsoAction = new ReembolsoAction( reembolsoRequests, parserJsonRecebido );
		
		return reembolsoAction.apply();     
	
	}
}