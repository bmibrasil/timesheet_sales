package webServiceController;

import action.ConsultorAction;
import config.ConsultorConfig;
import enumerations.ConsultorRequests;

public class WSConsultorController extends Controller {

	@Override
	public String apply(String action) throws Exception, Throwable {
		
		ConsultorConfig consultorConfig = new ConsultorConfig();
		
		ConsultorRequests consultorRequests = consultorConfig.getConsultorRequests(action);
		
		ConsultorAction consultorAction = new ConsultorAction( consultorRequests, parserJsonRecebido );
		
		return consultorAction.apply();
		
	}

}
