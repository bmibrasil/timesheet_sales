package webServiceController;

import action.ItemTimeSheetAction;
import config.ItemTimesheetConfig;
import enumerations.ItemTimesheetRequests;

public class WSItemTimesheetController extends Controller {

	@Override
	public String apply(String action) throws Exception, Throwable {
		
		ItemTimesheetConfig itemTimesheetConfig = new ItemTimesheetConfig();
		
		ItemTimesheetRequests itemTimesheetRequests = itemTimesheetConfig.getItemTimesheetRequests(action);
		
		ItemTimeSheetAction itemTimeSheetAction = new ItemTimeSheetAction( itemTimesheetRequests, parserJsonRecebido );
		
		return itemTimeSheetAction.apply();
    	
	}

}
