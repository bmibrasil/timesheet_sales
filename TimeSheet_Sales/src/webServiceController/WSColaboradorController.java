package webServiceController;

import action.ColaboradorAction;
import config.ColaboradorConfig;
import enumerations.ColaboradorRequests;

public class WSColaboradorController extends Controller {

	@Override
	public String apply(String action) throws Exception, Throwable {
		
		ColaboradorConfig colaboradorConfig = new ColaboradorConfig();
		
		ColaboradorRequests colaboradorRequests = colaboradorConfig.getColaboradorRequests(action);
		
		ColaboradorAction colaboradorAction = new ColaboradorAction( colaboradorRequests, parserJsonRecebido );
		
		return colaboradorAction.apply();
    	
	}

}
