package webServiceController;

import action.ArquivoReembolsoAction;
import config.ArquivoReembolsoConfig;
import enumerations.ArquivoReembolsoRequests;

public class WSArquivoReembolsoController extends Controller {

	@Override
	public String apply(String action) throws Exception, Throwable {
		
		ArquivoReembolsoConfig arquivoReembolsoConfig = new ArquivoReembolsoConfig();
		
		ArquivoReembolsoRequests arquivoReembolsoRequests = arquivoReembolsoConfig.getArquivoReembolsoRequests(action);
		
		ArquivoReembolsoAction arquivoReembolsoAction = new ArquivoReembolsoAction( arquivoReembolsoRequests, parserJsonRecebido );
		
		return arquivoReembolsoAction.apply();
		
	}

}
