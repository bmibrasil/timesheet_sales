package webServiceController;

import action.TimeSheetAction;
import config.TimeSheetConfig;
import enumerations.TimeSheetRequests;

public class WSTimeSheetController extends Controller {

	@Override
	public String apply(String action) throws Exception{
		
		TimeSheetConfig timeSheetConfig = new TimeSheetConfig();
		
		TimeSheetRequests timeSheetRequests = timeSheetConfig.getTimeSheetRequests(action);
		
		TimeSheetAction timeSheetAction = new TimeSheetAction( timeSheetRequests, parserJsonRecebido );
		
		return timeSheetAction.apply();    
	
	}
	
	
}