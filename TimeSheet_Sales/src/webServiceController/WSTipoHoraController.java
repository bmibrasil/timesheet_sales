package webServiceController;

import action.TipoHoraAction;
import config.TipoHoraConfig;
import enumerations.TipoHoraRequests;

public class WSTipoHoraController extends Controller {

	@Override
	public String apply(String action) throws Exception, Throwable {
	
		TipoHoraConfig tipoHoraConfig = new TipoHoraConfig();
		
		TipoHoraRequests tipoHoraRequests = tipoHoraConfig.getTipoHoraRequests(action);
		
		TipoHoraAction tipoHoraAction = new TipoHoraAction( tipoHoraRequests, parserJsonRecebido );
		
		return tipoHoraAction.apply();
		
	}

}
