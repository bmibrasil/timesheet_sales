package webServiceController;

import action.TipoReembolsoAction;
import config.TipoReembolsoConfig;
import enumerations.TipoReembolsoRequests;

public class WSTipoReembolsoController extends Controller {

	@Override
	public String apply(String action) throws Exception, Throwable {
	
		TipoReembolsoConfig tipoReembolsoConfig = new TipoReembolsoConfig();
		
		TipoReembolsoRequests tipoReembolsoRequests = tipoReembolsoConfig.getTipoReembolsoRequests(action);
		
		TipoReembolsoAction tipoReembolsoAction = new TipoReembolsoAction( tipoReembolsoRequests, parserJsonRecebido );
		
		return tipoReembolsoAction.apply();
		
	}

}
