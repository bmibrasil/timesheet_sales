package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import webService.DMLSalesForce;

public class ItemTimeSheet {
	
	public static ItemTimeSheet ofInterval( final String comeco, final String fim ) {
		return new ItemTimeSheet(comeco, fim);
	}
	
	private ItemTimeSheet(String comeco, String fim) {
		inicio = comeco;
		termino = fim;
	}

	private String id;
	private String periodo;
	private String periodoBloqueado;
	private String data;
	private String duracaoMinutos;
	private String diaSemana;
	private String inicio;
	private String termino;
	private String horasTrabalhadas;
	private String cliente;
	private String projeto;
	private String etapa;
	private String consultor;
	private String tipo;
	private String descricao;

	private String nomeOrganizacao;
	private String nomeProjeto;
	private String nomeTipo;
	private String totalDuracaoMinutos;
	private String nomeConsultor;
	private String nomeEtapa;

	public ItemTimeSheet() {

		id = "";
		periodo = "";
		periodoBloqueado = "";
		data = "";
		duracaoMinutos = "";
		diaSemana = "";
		inicio = "";
		termino = "";
		horasTrabalhadas = "";
		cliente = "";
		projeto = "";
		etapa = "";
		consultor = "";
		tipo = "";
		descricao = "";
		nomeOrganizacao = "";
		nomeProjeto = "";
		nomeTipo = "";
		totalDuracaoMinutos = "";
		nomeConsultor="";
		nomeEtapa="";

	}

	public ItemTimeSheet( JsonElement element ) {

		JsonObject object = element.getAsJsonObject();

		try{id = object.get("Id").getAsString(); }
		catch(Exception e) { id = ""; }

		try{cliente = object.get("Organizacao__c").getAsString(); }
		catch(Exception e) { cliente = ""; }

		try{projeto = object.get("Projeto__c").getAsString(); }
		catch(Exception e) { projeto = ""; }

		try{consultor = object.get("Consultor__c").getAsString(); }
		catch(Exception e) { consultor = ""; }

		try{duracaoMinutos = object.get("Duracao_em_Minutos__c").getAsString(); }
		catch(Exception e) { duracaoMinutos = ""; }

		try {totalDuracaoMinutos = object.get("totalDuracaoMinutos").getAsString(); }
		catch(Exception e) {totalDuracaoMinutos = ""; }

		try{data = object.get("Data__c").getAsString(); }
		catch(Exception e) { data = ""; }

		try{periodo = object.get("Timesheet__c").getAsString(); }
		catch(Exception e) { periodo = ""; }

		try{descricao = object.get("Descricao__c").getAsString(); }
		catch(Exception e) { descricao = ""; }

		try{diaSemana = object.get("diaSemana__c").getAsString(); }
		catch(Exception e) { diaSemana = ""; }

		try{etapa = object.get("Etapa__c").getAsString(); }
		catch(Exception e) { etapa = ""; }

		try{horasTrabalhadas = object.get("Duracao__c").getAsString(); }
		catch(Exception e) { horasTrabalhadas = ""; }

		try{inicio = object.get("Hora_de_Inicio__c").getAsString(); }
		catch(Exception e) { inicio = ""; }

		try{termino = object.get("Hora_de_Termino__c").getAsString(); }
		catch(Exception e) { termino = ""; }

		try{tipo = object.get("Tipo_de_Hora__c").getAsString(); }
		catch(Exception e) { tipo = ""; }

		try{nomeTipo = getElementoArrayInterno(object, "Tipo_de_Hora__r", "Descricao__c"); }
		catch(Exception e) { nomeTipo = ""; }


		try{nomeProjeto = object.get("prj").getAsString(); }
		catch(Exception e) { nomeProjeto = ""; }

		if(nomeProjeto == "") {
			try{nomeProjeto = getElementoArrayInterno(object, "Projeto__r", "Nome_do_projeto__c"); }
			catch(Exception e) { nomeProjeto = ""; }
		}

		try{nomeOrganizacao = object.get("org").getAsString(); }
		catch(Exception e) { nomeOrganizacao = ""; }


		if(nomeOrganizacao == "") {
			try{nomeOrganizacao = getElementoArrayInterno(object, "Organizacao__r", "Name"); }
			catch(Exception e) { nomeOrganizacao = ""; }
		}


		try{nomeConsultor = getElementoArrayInterno(object, "Consultor__r", "Name"); }
		catch(Exception e) { nomeConsultor = ""; }



		try{nomeEtapa = getElementoArrayInterno(object, "Etapa__r", "Name"); }
		catch(Exception e) { nomeEtapa = ""; }


	}

	public ItemTimeSheet(final JsonObject jsonObjectRequests){

		id		= jsonObjectRequests.get("id").getAsString(); 
		data     = jsonObjectRequests.get("data").getAsString();
		diaSemana		= jsonObjectRequests.get("diaSemana").getAsString();
		inicio		= jsonObjectRequests.get("inicio").getAsString();
		termino		= jsonObjectRequests.get("termino").getAsString();
		horasTrabalhadas		= jsonObjectRequests.get("horasTrabalhadas").getAsString();
		cliente		= jsonObjectRequests.get("cliente").getAsString();
		projeto		= jsonObjectRequests.get("projeto").getAsString();
		tipo		= jsonObjectRequests.get("tipoHora").getAsString(); 
		etapa		= jsonObjectRequests.get("etapa").getAsString();
		periodo		= jsonObjectRequests.get("periodo").getAsString(); 
		descricao		= jsonObjectRequests.get("descricao").getAsString();
		consultor		= jsonObjectRequests.get("consultor").getAsString();

	}

	private String getElementoArrayInterno( JsonObject parametrosJson, String campo, String campoInterno ) {

		JsonElement tipoElement = parametrosJson.get( campo );
		JsonObject jsonObj = tipoElement.getAsJsonObject();

		return jsonObj.get( campoInterno ).getAsString();

	}
	
	private boolean intervaloIgualExistente( final String comeco, final String fim ) {
		
		return ((this.inicio.substring(0,5).compareTo(comeco)==0) || 
				(this.termino.substring(0,5).compareTo(fim)==0));
	}
	
	private boolean intervaloDentroExistente( final String comeco, final String fim ) {
		
		return((this.inicio.substring(0,5).compareTo(comeco)<0) && 
				(this.termino.substring(0,5).compareTo(fim)>0));
	}
	
	private boolean intervaloInicioDentroFimFora( final String comeco, final String fim ) {
		
		return((this.inicio.substring(0,5).compareTo(comeco)<0) && 
				(this.termino.substring(0,5).compareTo(fim)<0) && 
				(comeco.compareTo(this.termino.substring(0,5))<0));
	}
	
	private boolean intervaloFimDentroInicioFora( final String comeco, final String fim ) {
		
		return((this.inicio.substring(0,5).compareTo(comeco)>0) && 
				(this.termino.substring(0,5).compareTo(fim)>0) && 
				(fim.compareTo(this.inicio.substring(0,5))>0));
	}
	
	private boolean intervaloContendoExistente( final String comeco, final String fim ) {
		
		return((this.inicio.substring(0,5).compareTo(comeco)>0) && 
				(this.termino.substring(0,5).compareTo(fim)<0));
	}
	
	public boolean intervaloValido( final String comeco, final String fim ) {
		
		return (intervaloIgualExistente(comeco, fim) ||
				intervaloDentroExistente(comeco, fim) ||
				intervaloInicioDentroFimFora(comeco, fim) ||
				intervaloFimDentroInicioFora(comeco, fim) ||
				intervaloContendoExistente(comeco, fim));
	}

	public String getId() {
		return id;
	}
	public String getPeriodo() {
		return periodo;
	}
	public String getPeriodoBloqueado() {
		return periodoBloqueado;
	}
	public String getData() {
		return data;
	}
	public String getDuracaoMinutos() {
		return duracaoMinutos;
	}
	public String getDiaSemana() {
		return diaSemana;
	}
	public String getInicio() {
		return inicio;
	}
	public String getTermino() {
		return termino;
	}
	public String getHorasTrabalhadas() {
		return horasTrabalhadas;
	}
	public String getCliente() {
		return cliente;
	}
	public String getProjeto() {
		return projeto;
	}
	public String getEtapa() {
		return etapa;
	}
	public String getTipo() {
		return tipo;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getNomeOrganizacao() {
		return nomeOrganizacao;
	}
	public String getNomeProjeto() {
		return nomeProjeto;
	}
	public String getNomeTipo() {
		return nomeTipo;
	}
	public String getTotalDuracaoMinutos() {
		return totalDuracaoMinutos;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setHorasTrabalhadas(String horasTrabalhadas) {
		this.horasTrabalhadas = horasTrabalhadas;
	}

	public String getConsultor() {
		return consultor;
	}

	public String getNomeConsultor() {
		return nomeConsultor;
	}

	public String getNomeEtapa() {
		return nomeEtapa;
	}


}
