package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class TipoHora{

	private String id;
	private String nome;
	private String descricao;
	
	public TipoHora(){
		
		id = "";
		nome = "";
		descricao = "";
		
	}
	
	public TipoHora(JsonElement element) {
		
		JsonObject object = element.getAsJsonObject();
		
		try {id = object.get("Id").getAsString();}
		catch(Exception e) {id = "";}
		
		try {nome = object.get("Name").getAsString();}
		catch(Exception e) {nome = "";}
		
		try {descricao = object.get("Descricao__c").getAsString();}
		catch(Exception e) {descricao = "";}
		
	}

	public String getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getDescricao() {
		return descricao;
	}

}
