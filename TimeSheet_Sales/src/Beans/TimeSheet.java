package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class TimeSheet {
	
	private String id; 
	private String colaborador;
	private String periodo;
	private String periodoBloqueado;
	private String nomePeriodoLancado;
	private String nomeColaborador;
	private String gestor;
	private String bloqueado;
	
	public TimeSheet() {
		
		id = "";
		colaborador = "";
		periodo = "";
		periodoBloqueado = "";
		nomePeriodoLancado = "";
		nomeColaborador = "";
		gestor = "";
		bloqueado = "";
		
	}
	
	public TimeSheet( JsonElement element ) {
		
		JsonObject object = element.getAsJsonObject();
		
		try { id = object.get("Id").getAsString(); }
		catch(Exception e) {id = "";}
		
		try { colaborador = object.get("Usuario__c").getAsString(); }
		catch(Exception e) {colaborador = "";}
		
		try {periodo = object.get("Periodo_Lancado__c").getAsString(); }
		catch(Exception e) {periodo = "";}
		
		try {periodoBloqueado = object.get("Periodo_Bloqueado__c").getAsString(); }
		catch(Exception e) {periodoBloqueado = "";}
		
		try {nomePeriodoLancado = object.get("Nome_Periodo_Lancado__c").getAsString();}
		catch(Exception e) {nomePeriodoLancado = "";}
		
		try {nomeColaborador = getElementoArrayInterno(object, "Usuario__r", "Name");}
		catch(Exception e) {nomeColaborador = "";}
		
		try {gestor = getElementoArrayInterno(object, "Usuario__r", "Gestor_Direto__c");}
		catch(Exception e) {gestor = "";}
		
		try {bloqueado = getElementoArrayInterno(object, "Usuario__r", "Bloqueado__c");}
		catch(Exception e) {bloqueado = "";}
		
	}
	
	private String getElementoArrayInterno( JsonObject parametrosJson, String campo, String campoInterno ) {
		
		JsonElement tipoElement = parametrosJson.get( campo );
		JsonObject jsonObj = tipoElement.getAsJsonObject();
		
		return jsonObj.get( campoInterno ).getAsString();
		
	}
	
	public String getColaborador() {
		return colaborador;
	}
	public String getPeriodo() {
		return periodo;
	}
	public String getPeriodoBloqueado() {
		return periodoBloqueado;
	}
	public String getNomePeriodoLancado() {
		return nomePeriodoLancado;
	}
	public String getNomeColaborador() {
		return nomeColaborador;
	}
	public String getGestor() {
		return gestor;
	}
	public String getBloqueado() {
		return bloqueado;
	}

	public String getId() {
		return id;
	}
	
}

