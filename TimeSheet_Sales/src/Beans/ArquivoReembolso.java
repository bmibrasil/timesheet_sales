package Beans;

import java.sql.Blob;

public class ArquivoReembolso {
	private Integer id;
	private String idReembolso;
	private String numeroReembolso;
	private Blob arquivo;
	private String extensao;
	private Integer numeroArquivos;
	
	public Integer getNumeroArquivos() {
		return numeroArquivos;
	}


	public void setNumeroArquivos(Integer numeroArquivos) {
		this.numeroArquivos = numeroArquivos;
	}


	public ArquivoReembolso() {
		
	}


	public String getIdReembolso() {
		return idReembolso;
	}


	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setIdReembolso(String id) {
		this.idReembolso = id;
	}

	public String getNumeroReembolso() {
		return numeroReembolso;
	}


	public void setNumeroReembolso(String numeroReembolso) {
		this.numeroReembolso = numeroReembolso;
	}


	public Blob getArquivo() {
		return arquivo;
	}


	public void setArquivo(Blob arquivo) {
		this.arquivo = arquivo;
	}


	public String getExtensao() {
		return extensao;
	}


	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}

	/**
	 * @return the turma
	 */
	
}
