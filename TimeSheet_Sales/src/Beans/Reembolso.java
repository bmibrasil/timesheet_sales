package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Reembolso {
	
	private String id; 
	private String name;
	private String data; 
	private String colaborador;
	private String nomeColaborador;
	private String diasemana;
	private String cliente;
	private String projeto;
	private String codigoProjeto;
	private String etapa;
	private String tipo;
	private String local;
	private String origem;
	private String destino;
	private String observacao;
	private String statusreembolso;
	private String justificativa;
	
	private double quilometragem;
	private double valorsolicitado;
	private double valorreembolso;
	
	private int quantidadeDias;
	private int numeroArquivos;
	
	public Reembolso() {
		
		id = "";
		name = "";
		data = "";
		setNomeColaborador("");
		colaborador = "";
		diasemana = "";
		cliente = "";
		projeto = "";
		codigoProjeto = "";
		etapa = "";
		tipo = "";
		local = "";
		origem = "";
		destino = "";
		observacao = "";
		statusreembolso = "";
		justificativa = "";
		quilometragem = 0.0;
		valorsolicitado = 0.0;
		valorreembolso = 0.0;
		quantidadeDias = 0;
		numeroArquivos = 0;
		
	}
	
	public Reembolso( JsonElement element ) {
		
		JsonObject object = element.getAsJsonObject();
		
		try {id = object.get("Id").getAsString();}
		catch(Exception e) {id = "";}
		
		try {name = object.get("Name").getAsString();}
		catch(Exception e) {name = "";}
		
		try {data = object.get("Data__c").getAsString();}
		catch(Exception e) {data = "";}
		
		try {nomeColaborador = getElementoArrayInterno(object, "Usuario__r", "Name");}
		catch(Exception e) {nomeColaborador = "";}
		
		try {colaborador = object.get("Usuario__c").getAsString();}
		catch(Exception e) {colaborador = "";}
		
		try {cliente = getElementoArrayInterno(object, "Organizacao__c", "Name");}
		catch(Exception e) {cliente = "";}
		
		if(cliente == "") {
			try {cliente = object.get("Organizacao__c").getAsString();}
			catch(Exception e) {cliente = "";}	
		}
		
		try {projeto = getElementoArrayInterno(object, "Projeto__r", "Nome_do_projeto__c");}
		catch(Exception e) {projeto = "";}
		
		if(projeto == "") {
			try {projeto = object.get("Projeto__c").getAsString();}
			catch(Exception e) {projeto = "";}	
		}
		
		try {codigoProjeto = getElementoArrayInterno(object, "Projeto__r", "Name");}
		catch(Exception e) {codigoProjeto = "";}
		
		try {etapa = getElementoArrayInterno(object, "Etapa__r", "Descricao__c");}
		catch(Exception e) {etapa = "";}
		
		if(etapa == "") {
			try {etapa = object.get("Etapa__c").getAsString();}
			catch(Exception e) {etapa = "";}	
		}
		
		try {tipo = getElementoArrayInterno(object, "Tipo_de_Reembolso__r", "Descricao__c");}
		catch(Exception e) {tipo = "";}
		
		if(tipo == "") {
			try {tipo = object.get("Tipo_de_Reembolso__c").getAsString();}
			catch(Exception e) {tipo = "";}
		}
		
		try {observacao = object.get("Observacao__c").getAsString();}
		catch(Exception e) {observacao = "";}
		
		try {statusreembolso = object.get("Status_do_Reembolso__c").getAsString();}
		catch(Exception e) {statusreembolso = "";}
		
		try {valorsolicitado = object.get("Valor_Solicitado__c").getAsDouble();}
		catch(Exception e) {valorsolicitado = 0.0;}
		
		try {valorreembolso = object.get("Valor_Reembolsado__c").getAsDouble();}
		catch(Exception e) {valorreembolso = 0.0;}
		
		try {quilometragem = object.get("Quilometragem__c").getAsDouble();}
		catch(Exception e) {quilometragem = 0.0;}
		
		try {local = object.get("Local__c").getAsString();}
		catch(Exception e) {local = "";}
		
		try {diasemana = object.get("Dia_da_Semana__c").getAsString();}
		catch(Exception e) {diasemana = "";}
		
		try {quantidadeDias = object.get("Quantidade_de_Dias__c").getAsInt();}
		catch(Exception e) {quantidadeDias = 0;}
		
		try {origem = object.get("Origem__c").getAsString();}
		catch(Exception e) {origem = "";}
		
		try {destino = object.get("Destino__c").getAsString();}
		catch(Exception e) {destino = "";}
		
	}

	private String getElementoArrayInterno( JsonObject parametrosJson, String campo, String campoInterno ) {
		
		JsonElement tipoElement = parametrosJson.get( campo );
		JsonObject jsonObj = tipoElement.getAsJsonObject();
		
		return jsonObj.get( campoInterno ).getAsString();
		
	}
	
	public void setNumeroArquivos(int numeroArquivos) {
		this.numeroArquivos = numeroArquivos;
	}
	
	public void setData( String data ) {
		this.data = data;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getData() {
		return data;
	}

	public String getColaborador() {
		return colaborador;
	}

	public String getDiasemana() {
		return diasemana;
	}

	public String getCliente() {
		return cliente;
	}

	public String getProjeto() {
		return projeto;
	}

	public String getCodigoProjeto() {
		return codigoProjeto;
	}

	public String getEtapa() {
		return etapa;
	}

	public String getTipo() {
		return tipo;
	}

	public String getLocal() {
		return local;
	}

	public String getOrigem() {
		return origem;
	}

	public String getDestino() {
		return destino;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getStatusreembolso() {
		return statusreembolso;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public double getQuilometragem() {
		return quilometragem;
	}

	public double getValorsolicitado() {
		return valorsolicitado;
	}

	public double getValorreembolso() {
		return valorreembolso;
	}

	public int getQuantidadeDias() {
		return quantidadeDias;
	}

	public int getNumeroArquivos() {
		return numeroArquivos;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}

	public void setDiasemana(String diasemana) {
		this.diasemana = diasemana;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}

	public void setCodigoProjeto(String codigoProjeto) {
		this.codigoProjeto = codigoProjeto;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setStatusreembolso(String statusreembolso) {
		this.statusreembolso = statusreembolso;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public void setQuilometragem(double quilometragem) {
		this.quilometragem = quilometragem;
	}

	public void setValorsolicitado(double valorsolicitado) {
		this.valorsolicitado = valorsolicitado;
	}

	public void setValorreembolso(double valorreembolso) {
		this.valorreembolso = valorreembolso;
	}

	public void setQuantidadeDias(int quantidadeDias) {
		this.quantidadeDias = quantidadeDias;
	}

	public String getNomeColaborador() {
		return nomeColaborador;
	}

	public void setNomeColaborador(String nomeColaborador) {
		this.nomeColaborador = nomeColaborador;
	}
	
}

