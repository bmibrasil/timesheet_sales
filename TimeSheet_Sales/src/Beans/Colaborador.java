package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Colaborador {
	
	private String id;
	private String nome;
	private String realizado;
	private String email;
	private String senha;
	private String bloqueado;
	private String permissoes;
	private String gestor;
	private String dataInativacao;
	private String cargaHorariaPrevista;
	
	private Double valor;
	private Double horasEsperadas;

	private Integer distanciaCasaTrabalho;
	private Integer periodoDesabilitacao;
	
	public Colaborador() {
		
		id = "";
		nome = "";
		valor = 0.0;
		cargaHorariaPrevista = "";
		realizado = "";
		horasEsperadas = 0.0;
		email = "";
		senha = "";
		bloqueado = "";
		distanciaCasaTrabalho = 0;
		permissoes = "";
		gestor = "";
		dataInativacao = "";
	}
	
	public Colaborador( JsonElement element ){
		
		JsonObject object = element.getAsJsonObject();
		
		try{ id = object.get("Id").getAsString(); }
		catch(Exception e) { id = ""; }
		
		try{ nome = object.get("Name").getAsString(); }
		catch(Exception e) { nome = ""; }
		
		try{ realizado = object.get("Realizado__c").getAsString(); }
		catch(Exception e) { realizado = ""; }
		
		try{ email = object.get("Signature").getAsString(); }
		catch(Exception e) { email = ""; }
		
		try{ senha = object.get("Token__c").getAsString(); }
		catch(Exception e) { senha = ""; }
		
		try{ bloqueado = object.get("Bloqueado__c").getAsString();}
		catch(Exception e) { bloqueado = ""; }
		
		try{ distanciaCasaTrabalho = object.get("DintanciaCasaTrabalho__c").getAsInt(); }
		catch(Exception e){ distanciaCasaTrabalho = 0; }
		
		try{ permissoes = object.get("Permissoes__c").getAsString(); }
		catch(Exception e) { permissoes = ""; }
		
		try{ gestor = object.get("Gestor_Direto__c").getAsString(); }
		catch(Exception e){ gestor = ""; }
		
		try{ dataInativacao = object.get("Data_de_Inativacao__c").getAsString(); }
		catch(Exception e) { dataInativacao = ""; }
		
		try{ valor = object.get("Valor__c").getAsDouble(); }
		catch(Exception e) { valor = 0.0; }
		
		try{ cargaHorariaPrevista = object.get("Carga_Horaria_Prevista__c").getAsString(); }
		catch(Exception e) { cargaHorariaPrevista = ""; }
		
		try{ horasEsperadas = object.get("horasEsperadas__c").getAsDouble(); }
		catch(Exception e) { horasEsperadas = 0.0; }
		
	}

	public Integer getPeriodoDesabilitacao() {
		return periodoDesabilitacao;
	}

	public void setPeriodoDesabilitacao(Integer periodoDesabilitacao) {
		this.periodoDesabilitacao = periodoDesabilitacao;
	}

	public String getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getRealizado() {
		return realizado;
	}

	public String getEmail() {
		return email;
	}

	public String getSenha() {
		return senha;
	}
	
	public void setSenha( String senha ) {
		this.senha = senha;
	}

	public String getBloqueado() {
		return bloqueado;
	}

	public String getPermissoes() {
		return permissoes;
	}

	public String getGestor() {
		return gestor;
	}

	public String getDataInativacao() {
		return dataInativacao;
	}

	public Double getValor() {
		return valor;
	}

	public String getCargaHorariaPrevista() {
		return cargaHorariaPrevista;
	}

	public Double getHorasEsperadas() {
		return horasEsperadas;
	}

	public Integer getDistanciaCasaTrabalho() {
		return distanciaCasaTrabalho;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}

	public void setPermissoes(String permissoes) {
		this.permissoes = permissoes;
	}

	public void setGestor(String gestor) {
		this.gestor = gestor;
	}

	public void setDataInativacao(String dataInativacao) {
		this.dataInativacao = dataInativacao;
	}

	public void setCargaHorariaPrevista(String cargaHorariaPrevista) {
		this.cargaHorariaPrevista = cargaHorariaPrevista;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setHorasEsperadas(Double horasEsperadas) {
		this.horasEsperadas = horasEsperadas;
	}

	public void setDistanciaCasaTrabalho(Integer distanciaCasaTrabalho) {
		this.distanciaCasaTrabalho = distanciaCasaTrabalho;
	}

}