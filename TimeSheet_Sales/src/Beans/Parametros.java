package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Parametros {

	private String id;
	private String nomeParametro;
	private String valor;
	private String dataFinalizacaoVigencia;

	public Parametros() {
		
		id = "";
		nomeParametro = "";
		valor = "";
		dataFinalizacaoVigencia = "";
	}
	
	public Parametros( JsonElement element ) {
		
		JsonObject object = element.getAsJsonObject();
		
		try { id = object.get("Id").getAsString(); }
		catch(Exception e) { id = ""; }
		
		try { nomeParametro = object.get("Name").getAsString(); }
		catch(Exception e) { nomeParametro = ""; }
		
		try { valor = object.get("Valor__c").getAsString(); }
		catch(Exception e) { valor = ""; }
		
		try { dataFinalizacaoVigencia = object.get("Data_de_Finalizacao_da_Vigencia__c").getAsString(); }
		catch(Exception e) { dataFinalizacaoVigencia = ""; }
		
	}

	public String getId() {
		return id;
	}

	public String getNomeParametro() {
		return nomeParametro;
	}

	public String getValor() {
		return valor;
	}

	public String getDataFinalizacaoVigencia() {
		return dataFinalizacaoVigencia;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setNomeParametro(String nomeParametro) {
		this.nomeParametro = nomeParametro;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public void setDataFinalizacaoVigencia(String dataFinalizacaoVigencia) {
		this.dataFinalizacaoVigencia = dataFinalizacaoVigencia;
	}
	
}
