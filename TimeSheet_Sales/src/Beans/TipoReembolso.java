package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class TipoReembolso{

	private String id;
	private String nome;
	private String configuracao;	
	
	public TipoReembolso(){
		
		id = "";
		nome = "";
		configuracao = "";
	}
	
	public TipoReembolso( JsonElement element ) {
		
		JsonObject object = element.getAsJsonObject();
		
		try {id = object.get("Id").getAsString();}
		catch( Exception e) {id = "";}
		
		try {nome = object.get("Descricao__c").getAsString();}
		catch( Exception e) {nome = "";}
		
		try { configuracao = object.get("Configuracao__c").getAsString();}
		catch( Exception e) { configuracao = "";}
	}

	public String getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getConfiguracao() {
		return configuracao;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setConfiguracao(String configuracao) {
		this.configuracao = configuracao;
	}

}
