package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Permissao {
	
	private String id;
	private String colaborador_id;
	private String data;
	private String aceite;
	
	public Permissao() {
		
		id = "";
		colaborador_id = "";
		data = "";
		aceite = "";
		
	}
	
	public Permissao(JsonElement jsonElement) {
		
		JsonObject object = jsonElement.getAsJsonObject();
		
		try{ id = object.get("Id").getAsString(); }
		catch(Exception e) { id = ""; }
		
		try{ colaborador_id = object.get("Usuario__c").getAsString(); }
		catch(Exception e) { colaborador_id = ""; }
		
		try{ data = object.get("Data__c").getAsString(); }
		catch(Exception e) { data = ""; }
		
		try{ aceite = object.get("Aceite__c").getAsString(); }
		catch(Exception e) { aceite = ""; }
		
	}

	public String getId() {
		return id;
	}

	public String getColaborador_id() {
		return colaborador_id;
	}

	public String getData() {
		return data;
	}

	public String getAceite() {
		return aceite;
	}
	
}
