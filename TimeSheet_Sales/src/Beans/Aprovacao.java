package Beans;

import java.io.Serializable;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Aprovacao implements Serializable{
	
	private static final long serialVersionUID = 289756523432544921L;
	
	private String id;
	private String colaborador;
	private String periodo;
	private String aprovador;
	private String data;
	private String valorAprovacao;
	
	public Aprovacao() {
		
		id = "";
		colaborador = "";
		periodo = "";
		aprovador = "";
		data = "";
		valorAprovacao = "";
		
	}
	
	public Aprovacao( JsonElement element ) {
		
		JsonObject object = element.getAsJsonObject();
		
		try{ id = object.get("Id").getAsString(); }
		catch(Exception e) { id = ""; }
		
		try{ colaborador = object.get("Usuario__c").getAsString(); }
		catch(Exception e) { colaborador = ""; }
		
		try{periodo = object.get("Periodo__c").getAsString(); }
		catch(Exception e) { periodo = ""; }
		
		try{aprovador = object.get("Aprovador__c").getAsString(); }
		catch(Exception e) {aprovador = ""; }
		
		try{data = object.get("Data__c").getAsString(); }
		catch(Exception e) { data = ""; }
		
		try {valorAprovacao = object.get("Valor_Aprovacao__c").getAsString(); }
		catch(Exception e) {valorAprovacao = ""; }
		
	}

	public String getId() {
		return id;
	}

	public String getColaborador() {
		return colaborador;
	}

	public String getPeriodo() {
		return periodo;
	}

	public String getAprovador() {
		return aprovador;
	}

	public String getData() {
		return data;
	}

	public String getValorAprovacao() {
		return valorAprovacao;
	}

}

	