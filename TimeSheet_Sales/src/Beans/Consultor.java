package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Consultor {

	private String id;
	private String idEtapa;
	private String descricaoEtapa;
	private String colaboradorAlocado;
	private String nomeOrganizacao;
	private String nomeProjeto;
	private String nomeEtapa;

	private String idProj;
	private String idOrg;
	private String tipoDeProjeto;
	private String descricao;
	private String name;
	
	
	public Consultor() {
		
		id = "";
		idEtapa = "";
		colaboradorAlocado = "";
		nomeOrganizacao = "";
		nomeProjeto = "";
		nomeEtapa = "";
		descricaoEtapa = "";
		idProj = "";
		idOrg = "";
		tipoDeProjeto = "";
		descricao="";
		name="";
		
	}
	
	public Consultor( JsonElement element ) {
		
		JsonObject object = element.getAsJsonObject();
		
		try{ id = object.get("Id").getAsString(); }
		catch(Exception e) { id = ""; }
		
		try{ name = object.get("Name").getAsString(); }
		catch(Exception e) { name = ""; }
		
		try{ idEtapa = object.get("Etapa__c").getAsString(); }
		catch(Exception e) { idEtapa = ""; }
		
		try{ colaboradorAlocado = object.get("Colaborador_Alocado__c").getAsString(); }
		catch(Exception e) { colaboradorAlocado = ""; }
		
		try{ nomeOrganizacao = object.get("Nome_da_Organizacao__c").getAsString(); }
		catch(Exception e) { nomeOrganizacao = ""; }
		
		try{ nomeProjeto = object.get("Nome_do_Projeto__c").getAsString(); }
		catch(Exception e) { nomeProjeto = ""; }
		
		try{ nomeEtapa = object.get("Nome_da_Etapa__c").getAsString(); }
		catch(Exception e) { nomeEtapa = ""; }
		
		try{ nomeEtapa = object.get("Nome_da_Etapa__c").getAsString(); }
		catch(Exception e) { nomeEtapa = ""; }
		
		try{ idProj = getElementoArrayInterno(object, "Etapa__r", "Projeto__c"); }
		catch(Exception e) { idProj = ""; }
	
		try{ descricaoEtapa = getElementoArrayInterno(object, "Etapa__r", "Descricao__c"); }
		catch(Exception e) { descricaoEtapa = ""; }
		
		
		try{ descricao = object.get("Descricao__c").getAsString(); }
		catch(Exception e) { descricao = ""; }
		
		try{ 
			JsonElement etapa = object.get("Etapa__r");
			JsonObject objectEtapa = etapa.getAsJsonObject();

			JsonElement projeto = objectEtapa.get("Projeto__r");
			JsonObject objectProjeto = projeto.getAsJsonObject();

		tipoDeProjeto = objectProjeto.get("Tipo_de_Projeto__c").getAsString();
		}catch(Exception e) { tipoDeProjeto = ""; }
		
		try{ idOrg = object.get("OrganizacaoId__c").getAsString(); }
		catch(Exception e) { idOrg = ""; }
		
	}
	
	private String getElementoArrayInterno( JsonObject parametrosJson, String campo, String campoInterno ) {
		
		JsonElement tipoElement = parametrosJson.get( campo );
		JsonObject jsonObj = tipoElement.getAsJsonObject();
		
		return jsonObj.get( campoInterno ).getAsString();
		
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getIdEtapa() {
		return idEtapa;
	}
	public String getColaboradorAlocado() {
		return colaboradorAlocado;
	}
	public String getNomeOrganizacao() {
		return nomeOrganizacao;
	}
	public String getNomeProjeto() {
		return nomeProjeto;
	}
	public String getNomeEtapa() {
		return nomeEtapa;
	}
	public String getIdProj() {
		return idProj;
	}
	public String getIdOrg() {
		return idOrg;
	}
	public String getTipoDeProjeto() {
		return tipoDeProjeto;
	}

	public String getDescricaoEtapa() {
		return descricaoEtapa;
	}
	

	public String getDescricao() {
		return descricao;
	}
}