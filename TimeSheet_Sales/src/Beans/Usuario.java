package Beans;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Usuario {

	private String idSalesForce;
	private String name;
	private String primeiroNome;
	private String ultimoNome;
	private String bloqueado;
	private Integer distanciaCasaTrabalho;
	private String procedencia;

	public Usuario( JsonElement element ) {
		
		JsonObject object = element.getAsJsonObject();
		
		try { idSalesForce = object.get("Id").getAsString(); }
		catch(Exception e) { idSalesForce = ""; }
		
		try { name = object.get("Name").getAsString(); }
		catch (Exception e) { name = ""; }
		
		try { primeiroNome = object.get("FirstName").getAsString(); }
		catch(Exception e) { primeiroNome = ""; }
		
		try { ultimoNome = object.get("LastName").getAsString(); }
		catch(Exception e) { ultimoNome = ""; }
		
		try { bloqueado = object.get("Bloqueado__c").getAsString(); }
		catch(Exception e) { bloqueado = ""; }
		
		try { distanciaCasaTrabalho = object.get("Distancia_de_casa_para_trabalho__c").getAsInt(); }
		catch(Exception e) { distanciaCasaTrabalho = 0; }
		
		try {procedencia = object.get("Procedencia__c").getAsString();}
		catch(Exception e) {procedencia = "";}
		
	}

	public String getName() {
		return name;
	}
	
	public String getIdSalesForce() {
		return idSalesForce;
	}

	public String getPrimeiroNome() {
		return primeiroNome;
	}

	public String getUltimoNome() {
		return ultimoNome;
	}

	public String getBloqueado() {
		return bloqueado;
	}
	
	public Boolean temId() {
		return (idSalesForce != "");
	}

	public Integer getDistanciaCasaTrabalho() {
		return distanciaCasaTrabalho;
	}

	public String getProcedencia() {
		return procedencia;
	}
	
}

