package action;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.TipoHora;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.Protocolo;
import enumerations.TipoHoraRequests;
import webService.DMLSalesForce;

public class TipoHoraAction {
	
	private Gson gson;
	
	private final TipoHoraRequests tipoHoraRequests;
	
	private final JsonObject jsonObjectRequest;
	
	public TipoHoraAction( TipoHoraRequests tipoHoraRequests, JsonObject jsonObject ) {
		gson = new Gson();
		this.tipoHoraRequests = tipoHoraRequests;
		this.jsonObjectRequest = jsonObject;
	}

	public List<TipoHora> montaTipoHora(JsonArray jsonArray, String nomeReferenciaTabela){
		
		List<TipoHora> listaTipoHora = new ArrayList<TipoHora>();
		
		for(JsonElement jsonElement : jsonArray) {
			TipoHora tipoHora = new TipoHora(jsonElement);
			listaTipoHora.add(tipoHora);
		}
		
		return listaTipoHora;
	}

	public String buscarTipoHoras() {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<TipoHora> listaTipoHora = new ArrayList<TipoHora>();
		
		String condicaoWhere="";
		String[] campos = {"Id", "Name", "Descricao__c"};
		
		try {
			
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Tipo_de_Hora__c", campos, condicaoWhere, "")
					.getAsJsonArray("records");
			
			listaTipoHora = montaTipoHora(jsonArray, "Tipo_de_Hora__c");
			
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::TIPO-HORA::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String lista = gson.toJson(listaTipoHora);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarTipoHoras")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::TIPO-HORA::NOTIFICATION] :: \n%s", result));
		
		return result;
	}	
	
	public String apply() {
		return tipoHoraRequests.apply(this);
	}
}