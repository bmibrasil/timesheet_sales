package action;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Aprovacao;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.AprovacaoRequests;
import enumerations.Protocolo;
import webService.DMLSalesForce;

public class AprovacaoAction {
	
	private Gson gson;
	
	private final AprovacaoRequests aprovacaoRequests;
	
	private final JsonObject jsonObjectRequest;
	
	public AprovacaoAction( AprovacaoRequests aprovacaoRequests, JsonObject jsonObject ) {
		gson = new Gson();
		this.aprovacaoRequests = aprovacaoRequests;
		this.jsonObjectRequest = jsonObject;
	}

	public List<Aprovacao> montaAprovacao(JsonArray jsonArray, String nomeReferenciaTabela){
		
		List<Aprovacao> listaAprovacao = new ArrayList<Aprovacao>();
		
		for(JsonElement jsonElement : jsonArray) {
			Aprovacao aprovacao = new Aprovacao( jsonElement );
			listaAprovacao.add(aprovacao);
		}
		
		return listaAprovacao;
		
	}

	
	public String buscarAprovacao() throws Exception {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<Aprovacao> listaAprovacao = new ArrayList<Aprovacao>();

		String colaborador = jsonObjectRequest.get("colaborador").getAsString();
		String periodo = jsonObjectRequest.get("periodo").getAsString();

		String condicaoWhere = "Periodo__c='" + periodo + "'%20AND%20Usuario__c='" + colaborador + "'";
		String[] campos = {"Id","Usuario__c","Valor_Aprovacao__c","Aprovador__c","Periodo__c","Data_de_Aprovacao__c"};
		
		try {
			
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Aprovacao__c", campos,condicaoWhere,"")
					.getAsJsonArray("records");
			
			listaAprovacao = montaAprovacao(jsonArray, "Aprovacao__c");
			
		}catch(Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::APROVACAO::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}

		String lista = gson.toJson(listaAprovacao);  	
		
		JsonProperties jsonProperties = new JsonProperties.Builder()
				.addProperty("action", "buscarAprovacao")
				.addProperty("retorno", lista)
				.addProperty("protocol", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( jsonProperties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::APROVACAO::NOTIFICATION] :: \n%s", result));
		
		return result;
	}


	public String salvarAprovacao() throws Exception {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<Aprovacao> listaAprovacao= new ArrayList<Aprovacao>();
		
		int retorno = 0;
		
		String id = "0";
		String dataAprovacao = jsonObjectRequest.get("dataAprovacao").getAsString();
		String colaborador = jsonObjectRequest.get("colaborador").getAsString();
		String valorAprovacao = jsonObjectRequest.get("valorAprovacao").getAsString();
		String aprovador = jsonObjectRequest.get("aprovador").getAsString();
		String periodo = jsonObjectRequest.get("periodo").getAsString();
		
		String condicaoWhere = "Usuario__c='" + colaborador + "'%20AND%20Periodo__c='" + periodo + "'";
		String[] campoosPesquisa= {"Id","Data_de_Aprovacao__c","Usuario__c","Valor_Aprovacao__c","Aprovador__c","Periodo__c"};
		String[] campos= {"Data_de_Aprovacao__c","Usuario__c","Valor_Aprovacao__c","Aprovador__c","Periodo__c"};
		String[] vetorValores= {dataAprovacao+"T00:00:00.000-0000",colaborador,valorAprovacao,aprovador,periodo};
		
		try {
			
			JsonObject jsonObjSelecao = DMLSalesForce.selecionaSalesForce("Aprovacao__c", campoosPesquisa, condicaoWhere, "");
			
			JsonArray jsonArray = (jsonObjSelecao.getAsJsonArray("records"));
			
			listaAprovacao = montaAprovacao(jsonArray, "Aprovacao__c");
			
		}catch(Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::APROVACAO::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}

		if(listaAprovacao.size() > 0 ) {
			
			id = listaAprovacao.get(0).getId();
			
			DMLSalesForce.atualizaSalesForce("Aprovacao__c", id, campos, vetorValores);
			
		}else {
			
			DMLSalesForce.insereSalesForce("Aprovacao__c", campos,vetorValores);
			
		}	
		
		JsonProperties jsonProperties = new JsonProperties.Builder()
				.addProperty("action", "salvarAprovacao")
				.addProperty("retorno", retorno)
				.addProperty("protocol", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( jsonProperties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::APROVACAO::NOTIFICATION] :: \n%s", result));
		
		return result;
	}
	
	public String apply() throws Exception {
		return aprovacaoRequests.apply(this);
	}
	
}