package action;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import Beans.ArquivoReembolso;
import Service.ArquivoReembolsoService;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.ArquivoReembolsoRequests;

public class  ArquivoReembolsoAction {

	private final ArquivoReembolsoService reembolsoService;
	
	private final Gson gson;
	
	private final ArquivoReembolsoRequests arquivoReembolsoRequests;
	
	private final JsonObject jsonObjectRequest;
	
	public  ArquivoReembolsoAction(ArquivoReembolsoRequests arquivoReembolsoRequests, JsonObject jsonObject) {
		reembolsoService = new ArquivoReembolsoService();
		gson = new Gson();
		this.arquivoReembolsoRequests = arquivoReembolsoRequests;
		this.jsonObjectRequest = jsonObject;
	}

	public String buscarArquivosReembolso(){
		
		List<ArquivoReembolso> listaReembolso = new ArrayList<ArquivoReembolso>();
		
		String reembolso = jsonObjectRequest.get("reembolso").getAsString();
		
		listaReembolso = reembolsoService.buscarArquivos(reembolso);
		
		Integer numeroArquivos = listaReembolso.size();
		
		String lista = gson.toJson(listaReembolso);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarArquivosReembolso")
				.addProperty("listaArquivos", lista)
				.addProperty("numeroArquivos", numeroArquivos.toString())
				.addProperty("retorno", 0)
				.build();
		
		String response = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::ARQUIVO-REEMBOLSO::NOTIFICATION] :: \n%s", response));
		
		return response;
		
	}
	
	public String removerArquivoReembolso( ){
		
		int id = jsonObjectRequest.get("arquivo").getAsInt();
		
		reembolsoService.delete(id);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "removerArquivoReembolso")
				.addProperty("retorno", 0)
				.build();
		
		String response = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::ARQUIVO-REEMBOLSO::NOTIFICATION] :: \n%s", response));
		
		return response;
	}	 	
	
	public String apply() {
		return arquivoReembolsoRequests.apply(this);
	}
	
}