package action;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.TipoReembolso;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.Protocolo;
import enumerations.TipoReembolsoRequests;
import webService.DMLSalesForce;

public class TipoReembolsoAction {

	private Gson gson;
	
	private TipoReembolsoRequests tipoReembolsoRequests;
	
	private JsonObject jsonObjectRequest;
	
	public TipoReembolsoAction() {}
	
	public TipoReembolsoAction(TipoReembolsoRequests tipoReembolsoRequests, JsonObject jsonObject) {
		gson = new Gson();
		this.tipoReembolsoRequests = tipoReembolsoRequests;
		this.jsonObjectRequest = jsonObject;
	}
	
	public List<TipoReembolso> montaTipoReembolso(JsonArray jsonArray, String nomeReferenciaTabela){
		
		List<TipoReembolso> listaTipoReembolso = new ArrayList<TipoReembolso>();
		
		for(JsonElement jsonElement : jsonArray ) {
			TipoReembolso tipoReembolso = new TipoReembolso( jsonElement );
			listaTipoReembolso.add( tipoReembolso );
		}
		
		return listaTipoReembolso;

	}

	public String buscarTipoReembolsos() {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<TipoReembolso> listaTipoReembolso = new ArrayList<TipoReembolso>();

		String condicaoWhere="";
		String[] campos= {"Id","Descricao__c","Configuracao__c",};
		
		try {
			
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("NEXISIM_Tipo_Reembolso__c", campos,condicaoWhere,"Descricao__c")
					.getAsJsonArray("records");
			
			listaTipoReembolso = montaTipoReembolso(jsonArray,"NEXISIM_Tipo_Reembolso__c");
			
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::TIPO-REEMBOLSO::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String lista = gson.toJson(listaTipoReembolso);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarTipoReembolso")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::TIPO-REEMBOLSO::NOTIFICATION] :: \n%s", result));
		
		return result;
	}
	
	public TipoReembolso obterTipoReembolsoPorId( String id ) {
		
		List<TipoReembolso> listaTipoReembolso = new ArrayList<TipoReembolso>();
		
		String condicaoWhere="Id%20=%20'" + id + "'";
		String[] campos= {"Id","Descricao__c","Configuracao__c",};
		
		JsonArray jsonArray = DMLSalesForce
				.selecionaSalesForce("NEXISIM_Tipo_Reembolso__c", campos,condicaoWhere,"Descricao__c")
				.getAsJsonArray("records");
		
		listaTipoReembolso = montaTipoReembolso(jsonArray,"NEXISIM_Tipo_Reembolso__c");
		
		return listaTipoReembolso.get(0);
		
	}
	
	public String apply() {
		return tipoReembolsoRequests.apply(this);
	}
	
}