package action;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Parametros;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.ParametrosRequests;
import enumerations.Protocolo;
import webService.DMLSalesForce;

public class ParametrosAction {
	
	private Gson gson;
	
	private  ParametrosRequests parametrosRequests;
	
	private  JsonObject jsonObjectRequests;
	
	public ParametrosAction() {}
	
	public ParametrosAction(ParametrosRequests parametrosRequests, JsonObject jsonObject) {
		gson = new Gson();
		this.parametrosRequests = parametrosRequests;
		this.jsonObjectRequests = jsonObject;
	}

	public List<Parametros> montaParametros( JsonArray jsonArray, String nomeTabelaReferencia ){
		
		List<Parametros> listaParametros = new ArrayList<Parametros>();
		
		for(JsonElement jsonElement : jsonArray ) {
			Parametros parametro = new Parametros( jsonElement );
			listaParametros.add( parametro );
		}
		
		return listaParametros;
		
	}

	public String buscarParametros() {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<Parametros> listaParametros = new ArrayList<Parametros>();

		String[] campos = {"Id","Name","Valor__c","Data_de_Finalizacao_da_Vigencia__c"};
		
		try {
		
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Parametro__c", campos,"","Name")
					.getAsJsonArray("records");
			
			listaParametros = montaParametros(jsonArray,"Parametro__c");
			
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::PARAMETROS::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String lista = gson.toJson(listaParametros);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarParametros")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::PARAMETROS::NOTIFICATION] :: \n%s", result));
		
		return result;
	}
	
	
	
	public List<Parametros> obterTodosParametros(){

		String[] campos = {"Id","Name","Valor__c","Data_de_Finalizacao_da_Vigencia__c"};

		JsonArray jsonArray = DMLSalesForce
				.selecionaSalesForce("Parametro__c", campos,"","Name")
				.getAsJsonArray("records");
		
		return montaParametros(jsonArray,"Parametro__c");
		
	}
	
	public String apply() throws Exception {
		return parametrosRequests.apply(this);
	}
	
}