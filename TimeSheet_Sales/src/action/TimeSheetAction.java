package action;


import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.TimeSheet;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.Protocolo;
import enumerations.TimeSheetRequests;
import webService.DMLSalesForce;

public class TimeSheetAction {
	
	private Gson gson;
	
	private final TimeSheetRequests timesheetRequests;
	
	private final JsonObject jsonObjectRequests;
	
	public TimeSheetAction(TimeSheetRequests timesheetRequests, JsonObject jsonObject) {
		gson = new Gson();
		this.timesheetRequests = timesheetRequests;
		this.jsonObjectRequests = jsonObject;
	}
	
	public List<TimeSheet> montaTimesheet(JsonArray jsonArray, String nomeReferenciaTabela)	{
		
		List<TimeSheet> listaTimeSheet = new ArrayList<TimeSheet>();
		
		for(JsonElement jsonElement : jsonArray) {
			TimeSheet timeSheet = new TimeSheet(jsonElement);
			listaTimeSheet.add(timeSheet);
		}
		
		return listaTimeSheet;
		
	}

	public String buscarPeriodos() {
				Protocolo protocolo = Protocolo.OK;
		
		List<TimeSheet> listaPeriodo = new ArrayList<TimeSheet>();

		String colaborador = jsonObjectRequests.get("colaborador").getAsString();
		
		String condicaoWhere = "Usuario__c='"+colaborador+"'";
		String ordenacao = "%20ano__c%20desc,%20mes__c%20desc";
			
		String[] campos = {"Id","Periodo_Bloqueado__c","Periodo_Lancado__c", "Nome_Periodo_Lancado__c","Usuario__c", "Usuario__r.Name","Usuario__r.Bloqueado__c"};
		
		try {
			
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Timesheet__c", campos, condicaoWhere,ordenacao)
					.getAsJsonArray("records");
			
			listaPeriodo = montaTimesheet(jsonArray, "Timesheet__c");
			
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::TIMESHEET::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}

		String lista = gson.toJson(listaPeriodo);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarPeriodos")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::TIMESHEET::NOTIFICATION] :: \n%s", result));
		
		return result;
	}
	
	public String buscarPeriodosConsolidado() {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<TimeSheet> listaPeriodo = new ArrayList<TimeSheet>();
		
		String colaborador = jsonObjectRequests.get("colaborador").getAsString();
		String orderBy = "Ano__c%20Desc%20,Mes__c%20Desc%20";
		String condicaoWhere ="Usuario__c='"+colaborador+"'%20OR%20Usuario__r.Gestor_Direto__c='"+colaborador+"'";
		String[] campos = {"Id", "Periodo_Lancado__c", "Nome_Periodo_Lancado__c", "Usuario__r.Name"};
		
		try {
			
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Timesheet__c", campos, condicaoWhere, orderBy)
					.getAsJsonArray("records");
			
			listaPeriodo = montaTimesheet(jsonArray, "Timesheet__c");
			
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::TIMESHEET::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String lista = gson.toJson(listaPeriodo);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarPeriodosConsolidado")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::TIMESHEET::NOTIFICATION] :: \n%s", result));
		
		return result;
	}
	
	public String apply() {
		return timesheetRequests.apply(this);
	}
	
}