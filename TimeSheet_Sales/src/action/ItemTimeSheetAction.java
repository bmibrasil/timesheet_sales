package action;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.ItemTimeSheet;
import Teste.TesteIntervaloItemTimesheet;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.ItemTimesheetRequests;
import enumerations.Protocolo;
import webService.DMLSalesForce;

public class ItemTimeSheetAction {

	private Gson gson;

	private final ItemTimesheetRequests itemTimesheetRequests;

	private final JsonObject jsonObjectRequests;

	public ItemTimeSheetAction(ItemTimesheetRequests itemTimesheetRequests, JsonObject jsonObject) {
		gson = new Gson();
		this.itemTimesheetRequests = itemTimesheetRequests;
		this.jsonObjectRequests = jsonObject;
	}

	public List<ItemTimeSheet> montaItemTimesheet (JsonArray jsonArray, String nomeReferenciaTabela){

		List<ItemTimeSheet> listaItemTimesheet = new ArrayList<ItemTimeSheet>();

		for(JsonElement jsonElement : jsonArray) {
			ItemTimeSheet itemTimeSheet = new ItemTimeSheet(jsonElement);
			listaItemTimesheet.add(itemTimeSheet);
		}

		return listaItemTimesheet;

	}

	public String buscarTimeSheets() {

		Protocolo protocolo = Protocolo.OK;

		List<ItemTimeSheet> listaItemTimesheet = new ArrayList<ItemTimeSheet>();

		String periodo = jsonObjectRequests.get("periodo").getAsString();
		String colaborador = jsonObjectRequests.get("colaborador").getAsString();
		String dataFormato;

		String condicaoWhere = "Timesheet__c='"+periodo+"'%20AND%20Colaborador__c='"+ colaborador+"'";
		String[] campos = {"Id","Data__c", "diaSemana__c", "Organizacao__r.Name", "Organizacao__c", "Projeto__r.Nome_do_projeto__c", "Projeto__c", "Tipo_de_Hora__r.Descricao__c", "Tipo_de_Hora__c", "Duracao__c", "Hora_de_Termino__c", "Hora_de_Inicio__c", "Descricao__c", "Timesheet__c","Colaborador__c", "Consultor__c", "Consultor__r.name","Etapa__r.name"};
		String orderBy = "Data__c%20Desc%20,Hora_de_Inicio__c%20Desc%20";
		try {

			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Item_Timesheet__c", campos,condicaoWhere,orderBy)
					.getAsJsonArray("records");

			listaItemTimesheet = montaItemTimesheet(jsonArray, "Item_Timesheet__c");

		}catch (Exception e) {

			protocolo = Protocolo.NOT_FOUND;

			System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));

		}

		for(int i = 0; i < listaItemTimesheet.size(); i++){

			String ano = listaItemTimesheet.get(i).getData().substring(0, 4);
			String mes = listaItemTimesheet.get(i).getData().substring(5, 7);
			String dia = listaItemTimesheet.get(i).getData().substring(8, 10);

			dataFormato=dia + "/" + mes + "/" + ano;

			listaItemTimesheet.get(i).setData(dataFormato);

		}

		String lista = gson.toJson(listaItemTimesheet);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarLancamentos")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();

		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );

		System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", result));

		return result;
	}

	public String buscarLancamento() throws ParseException {

		Protocolo protocolo = Protocolo.OK;

		List<ItemTimeSheet> listaItemTimesheet = new ArrayList<ItemTimeSheet>();

		String lancamento = jsonObjectRequests.get("lancamento").getAsString();
		String periodo = jsonObjectRequests.get("periodo").getAsString();


		String condicaoWhere = "Timesheet__c='" +periodo+"'%20AND%20Id='"+ lancamento+"'";
		String[] campos = {"Id","Data__c", "diaSemana__c", "Organizacao__r.Name", "Organizacao__c", "Projeto__r.Nome_do_projeto__c", "Projeto__c", "Tipo_de_Hora__r.Name", "Tipo_de_Hora__c", "Duracao__c", "Hora_de_Termino__c", "Hora_de_Inicio__c", "Descricao__c", "Timesheet__c","Colaborador__c", "Consultor__c", "Consultor__r.name", "Etapa__c"};
		String orderBy = "Data__c%20Desc%20,Hora_de_Inicio__c%20Desc%20";


		try {
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Item_Timesheet__c", campos,condicaoWhere,orderBy)
					.getAsJsonArray("records");

			listaItemTimesheet = montaItemTimesheet(jsonArray, "Item_Timesheet__c");
		}catch (Exception e) {

			protocolo = Protocolo.NOT_FOUND;

			System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));

		}

		String ano = listaItemTimesheet.get(0).getData().substring(0, 4);
		String mes = listaItemTimesheet.get(0).getData().substring(5, 7);
		String dia = listaItemTimesheet.get(0).getData().substring(8, 10);

		String dataFormato = ano + "-" + mes + "-" + dia;

		listaItemTimesheet.get(0).setData(dataFormato);

		String lista = gson.toJson(listaItemTimesheet);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarLancamento")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();

		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );

		System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", result));

		return result;
	}

	public String excluirLancamento() {

		Protocolo protocolo = Protocolo.OK;

		String lancamento = jsonObjectRequests.get("lancamento").getAsString();

		DMLSalesForce.removeSalesForce("Item_Timesheet__c",lancamento);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "excluirLancamento")
				.addProperty("retorno", 0)
				.addProperty("protocolo", protocolo)
				.build();

		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );

		System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", result));

		return result;
	}

	public String salvarTimeSheet() throws Exception {
		
		List<ItemTimeSheet> listaItemTimesheet = new ArrayList<ItemTimeSheet>();
		JsonObject jsonObjSelecao = new JsonObject() ;
		String result;
		int retorno=0;

		final ItemTimeSheet timesheetRequest = new ItemTimeSheet(jsonObjectRequests);

		String colaborador = jsonObjectRequests.get("colaborador").getAsString(); 
		String edicao		= jsonObjectRequests.get("edicao").getAsString();

		String condicaoWhere = "%20Timesheet__c='"+
				timesheetRequest.getPeriodo()+
				"'%20AND%20Data__c="+
				timesheetRequest.getData()+
				"%20AND%20Colaborador__c='"
				+colaborador+"'";
		
		String[] campos= {"Id", "Timesheet__c", "Organizacao__c",  "Projeto__c",  "Etapa__c", "Data__c", "diaSemana__c", 
				"Hora_de_Inicio__c", "Hora_de_Termino__c", "Duracao__c", "Tipo_de_Hora__c", "Descricao__c", "Colaborador__c",
				"Consultor__c"};

		jsonObjSelecao = DMLSalesForce.selecionaSalesForce("Item_Timesheet__c", campos, condicaoWhere, "");

		if (jsonObjSelecao != null)
		{ 
			JsonArray jsonArray = (jsonObjSelecao.getAsJsonArray("records"));
			listaItemTimesheet = montaItemTimesheet(jsonArray, "Item_Timesheet__c");
		}
		
		if (listaItemTimesheet.size() > 0){
			
			for (int i=0 ; i <listaItemTimesheet.size();i++){
				
				if (!listaItemTimesheet.get(i).getId().equals(timesheetRequest.getId())){

						if(listaItemTimesheet.get(i).intervaloValido(timesheetRequest.getInicio(), timesheetRequest.getTermino()))
							retorno=1;
				}
			}
		}

		if(retorno == 0) {

			if(edicao.equals("0")) {
				String[] camposInsercao= {"Timesheet__c", "Organizacao__c", "Projeto__c", "Etapa__c", "Data__c", "diaSemana__c", "Hora_de_Inicio__c", 
						"Hora_de_Termino__c", "Duracao__c", "Tipo_de_Hora__c", "Descricao__c", "Colaborador__c","Consultor__c"};
				String[] vetorValores= {timesheetRequest.getPeriodo(),
						timesheetRequest.getCliente(),
						timesheetRequest.getProjeto(),
						timesheetRequest.getEtapa(),
						timesheetRequest.getData()+"T00:00:00.000-0000",
						timesheetRequest.getDiaSemana(),
						timesheetRequest.getInicio()+":00.000-0000",
						timesheetRequest.getTermino()+":00.000-0000",
						timesheetRequest.getHorasTrabalhadas()+":00.000-0000",
						timesheetRequest.getTipo(),
						timesheetRequest.getDescricao(),
						colaborador,
						timesheetRequest.getConsultor()};
				DMLSalesForce.insereSalesForce("Item_Timesheet__c", camposInsercao,vetorValores);

			}else {
				String[] campoAtualiza = {"Timesheet__c","Organizacao__c", "Projeto__c",  "Etapa__c", "Data__c", "diaSemana__c", "Hora_de_Inicio__c",
						"Hora_de_Termino__c", "Duracao__c", "Tipo_de_Hora__c", "Descricao__c", "Colaborador__c","Consultor__c"
						};
				String[] vetorValorAtualiza = {timesheetRequest.getPeriodo(),
						timesheetRequest.getCliente(),
						timesheetRequest.getProjeto(),
						timesheetRequest.getEtapa(),
						timesheetRequest.getData()+"T00:00:00.000-0000",
						timesheetRequest.getDiaSemana(),
						timesheetRequest.getInicio()+":00.000-0000",
						timesheetRequest.getTermino()+":00.000-0000",
						timesheetRequest.getHorasTrabalhadas()+":00.000-0000",
						timesheetRequest.getTipo(),
						timesheetRequest.getDescricao(),
						colaborador,
						timesheetRequest.getConsultor()};
				DMLSalesForce.atualizaSalesForce("Item_Timesheet__c",timesheetRequest.getId(), campoAtualiza,vetorValorAtualiza);
			}
		}

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "salvarTimeSheet")
				.addProperty("retorno", retorno)
				.addProperty("edicao", edicao)
				.build();

		result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );

		System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", result));

		return result;
	}

	public String consolidaTimeSheet() throws Exception {

		List<ItemTimeSheet> listaItemTimeSheet = new ArrayList<ItemTimeSheet>();
		JsonObject jsonObjSelecao = new JsonObject();
		String result,duracao;
		final String periodosId = jsonObjectRequests.get("periodosId").getAsString();

		String condicaoWhere = "Timesheet__c%20IN("+periodosId+")";
		String[] campos = {"Timesheet__c","SUM(Duracao_em_Minutos__c)totalDuracaoMinutos","Organizacao__r.Name%20org"};
		String groupBy = "%20GROUP%20BY%20Timesheet__c,Organizacao__r.Name";
		jsonObjSelecao = DMLSalesForce.selecionaSalesForce("Item_Timesheet__c", campos,condicaoWhere+groupBy,"");
		JsonArray jsonArray = (jsonObjSelecao.getAsJsonArray("records"));
		listaItemTimeSheet = montaItemTimesheet(jsonArray, "Item_Timesheet__c");

		for(int i=0;i<listaItemTimeSheet.size();i++) {

			duracao= calcularTotalHoras(Double.parseDouble(listaItemTimeSheet.get(i).getTotalDuracaoMinutos()));
			listaItemTimeSheet.get(i).setHorasTrabalhadas(duracao);
		}

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "consolidaTimeSheet")
				.addProperty("retorno", gson.toJson(listaItemTimeSheet))
				.build();

		result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );

		System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", result));

		return result;
	}

	public String relatorioConsolidado() throws Exception {

		String periodo	= jsonObjectRequests.get("periodo").getAsString();
		List<ItemTimeSheet> listaValores = new ArrayList<ItemTimeSheet>();
		String result="",duracao="";
		JsonObject jsonObjSelecao  = new JsonObject() ;

		String condicaoWhere = "Periodo__c="+periodo;
		
		String groupBy = "%20GROUP%20BY%20Organizacao__r.Name,Projeto__r.Nome_do_projeto__c,Tipo_de_Hora__r.Descricao__c";
		
		String[] campos = {"Organizacao__r.Name%20org", "Projeto__r.Nome_do_projeto__c%20prj", 
				"Tipo_de_Hora__r.Descricao__c%20tipoH", "SUM(Duracao_em_Minutos__c)%20totalDuracaoMinutos"
				};

		jsonObjSelecao = DMLSalesForce.selecionaSalesForce("Item_Timesheet__c", campos,condicaoWhere+groupBy,"");
		JsonArray jsonArray = (jsonObjSelecao.getAsJsonArray("records"));
		listaValores = montaItemTimesheet(jsonArray, "Item_Timesheet__c");

		for(int i=0;i<listaValores.size();i++) {

			duracao= calcularTotalHoras(Double.parseDouble(listaValores.get(i).getTotalDuracaoMinutos()));
			listaValores.get(i).setHorasTrabalhadas(duracao);
		}

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "relatorioConsolidado")
				.addProperty("retorno", gson.toJson(listaValores))
				.build();

		result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );

		System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", result));

		return result;
	}

	public String relatorioDetalhado() throws Exception {

		List<ItemTimeSheet> listaItemTimesheet = new ArrayList<ItemTimeSheet>();
		String periodo	= jsonObjectRequests.get("periodo").getAsString();
		String result="",duracao="";
		JsonObject jsonObjSelecao  = new JsonObject() ;

		String condicaoWhere = "Periodo__c="+periodo;
		
		String orderBy = "Data__c,Organizacao__r.Name,Projeto__r.Nome_do_projeto__c,Tipo_de_Hora__r.Descricao__c";
		
		String groupBy = "GROUP%20BY%20Data__c,Organizacao__r.Name,Projeto__r.Nome_do_projeto__c,Tipo_de_Hora__r.Descricao__c";
		
		String[] campos = {"Data__c", "Organizacao__r.Name%20org", "Projeto__r.Nome_do_projeto__c%20prj", 
				"Tipo_de_Hora__r.Descricao__c%20tipoH", "SUM(Duracao_em_Minutos__c)%20totalDuracaoMinutos"
				};

		jsonObjSelecao = DMLSalesForce.selecionaSalesForce("Item_Timesheet__c", campos, condicaoWhere+groupBy, orderBy);
		JsonArray jsonArray = (jsonObjSelecao.getAsJsonArray("records"));
		listaItemTimesheet = montaItemTimesheet(jsonArray, "Item_Timesheet__c");

		for(int i=0;i<listaItemTimesheet.size();i++) {

			duracao= calcularTotalHoras(Double.parseDouble(listaItemTimesheet.get(i).getTotalDuracaoMinutos()));
			listaItemTimesheet.get(i).setHorasTrabalhadas(duracao);
		}

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "relatorioDetalhado")
				.addProperty("retorno", gson.toJson(listaItemTimesheet))
				.build();

		result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );

		System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", result));

		return result;
	}

	public String buscaLancamentosColaboradorPeriodo() throws Exception {
		
		String result="";
		List<ItemTimeSheet> listaItemTimesheet = new ArrayList<ItemTimeSheet>();
		JsonObject jsonObjSelecao = new JsonObject() ;
		String colaborador	= jsonObjectRequests.get("colaborador").getAsString();
		String periodo	= jsonObjectRequests.get("periodo").getAsString();

		String condicaoWhere="Colaborador__c="+colaborador+" and Periodo__c=" + periodo;
		
		String[] campos= {"data__c", "Hora_de_Inicio__c", "Hora_de_Termino__c", "Tipo_de_Hora__c", "Projeto__r.Centro_de_Custo__c", 
				"Projeto__r.Codigo_do_Projeto_Financeiro__c"
				};
		
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Item_Timesheet__c", campos,condicaoWhere,"Data__c,Hora_de_Inicio__c desc,");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaItemTimesheet = montaItemTimesheet(jsonArray, "Item_Timesheet__c");

		gson.toJson(listaItemTimesheet);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscaLancamentosColaboradorPeriodo")
				.build();

		result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );

		System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", result));

		return result;
	}

	public String buscaResumoHorasProjetoPeriodo() throws Exception {

		List<ItemTimeSheet> listaItemTimeSheet = new ArrayList<ItemTimeSheet>();
		JsonObject jsonObjSelecao  = new JsonObject() ;
		String lista,result="";
		String horasDuracaoMinuto="";
		String periodo	= jsonObjectRequests.get("periodo").getAsString();

		String condicaoWhere = "Timesheet__c="+periodo;
		String groupBy = "%20GROUP%20BY%20Organizacao__r.Name";
		String[] campos= {"Organizacao__r.Name", "SUM(Duracao_em_Minutos__c)totalMinutos"};
		jsonObjSelecao = DMLSalesForce.selecionaSalesForce("Item_Timesheet__c", campos,condicaoWhere+groupBy,"");
		JsonArray jsonArray = (jsonObjSelecao.getAsJsonArray("records"));
		listaItemTimeSheet = montaItemTimesheet(jsonArray, "Item_Timesheet__c");

		for(int i=0;i<listaItemTimeSheet.size();i++) {
			horasDuracaoMinuto = calcularTotalHoras(Double.parseDouble(listaItemTimeSheet.get(i).getTotalDuracaoMinutos()));
			listaItemTimeSheet.get(i).setHorasTrabalhadas(horasDuracaoMinuto);
		}

		lista = gson.toJson(listaItemTimeSheet);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscaResumoHorasProjetoPeriodo")
				.addProperty("listaValores", lista)
				.build();

		result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );

		System.out.println(String.format("[ACTION::ITEM-TIMESHEET::NOTIFICATION] :: \n%s", result));

		return result;
	}

	private String calcularTotalHoras( Double minutos ) {

		Integer totalHoras = (int) (minutos / 60);
		Double totalMinutos = minutos % 60;

		return formataHora(totalHoras.toString(), totalMinutos.toString());
	}

	private String formataHora( String horas, String minutos ) {

		String valorMinutos = minutos.replaceAll("[.].*", "");

		if(Integer.parseInt(horas) < 10) 
			horas = "0".concat(horas);

		if(Integer.parseInt(valorMinutos) < 10) 
			valorMinutos = "0".concat(valorMinutos);

		return horas+":"+valorMinutos;
	}

	public String apply() throws Exception {

		return itemTimesheetRequests.apply(this);
	}

}
