package action;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Colaborador;
import Beans.Usuario;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.ColaboradorRequests;
import enumerations.Protocolo;
import webService.DMLSalesForce;

public class ColaboradorAction {
	
	private Gson gson;
	
	private ColaboradorRequests colaboradorRequests;
	
	private JsonObject jsonObjectRequest;
	
	public ColaboradorAction() {}
	
	public ColaboradorAction( ColaboradorRequests colaboradorRequests, JsonObject jsonObject ) {
		gson = new Gson();
		this.colaboradorRequests = colaboradorRequests;
		this.jsonObjectRequest = jsonObject;
	}

	public List<Colaborador> montaColaborador( JsonArray jsonArray, String nomeReferenciaTabela ){
		
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		
		for(JsonElement jsonElement : jsonArray ) {
			Colaborador colaborador = new Colaborador( jsonElement );
			listaColaborador.add( colaborador );
		}
		
		return listaColaborador;
	
	}
	
	public String bloqueiaColaborador() {

		String acesso = jsonObjectRequest.get("acesso").getAsString();
		String bloqueado = jsonObjectRequest.get("bloqueado").getAsString();

		String[] vetorCamposAtualizar= {"Bloqueado__c"};
		String[] vetorValores= {bloqueado};
		
		DMLSalesForce.atualizaSalesForce("User",acesso,vetorCamposAtualizar,vetorValores);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "bloqueiaColaborador")
				.addProperty("retorno", 0)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", result));
		
		return result;
	
	}
	
	public String buscarTodosColaboradores() {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		
		String[] campos = {"Id","Name"};
		String condicaoWhere = "";
		
		try {
		
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("User", campos, condicaoWhere, "")
					.getAsJsonArray("records");
			
			listaColaborador = montaColaborador(jsonArray, "User");
			
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String lista = gson.toJson(listaColaborador);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarTodosColaboradores")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", result));
		
		return result;
	}
	
	public String buscarColaboradores() {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		
		String colaborador = jsonObjectRequest.get("colaborador").getAsString();
		
		String condicaoWhere = "Id='" + colaborador + "'%20OR%20Gestor_Direto__c='" + colaborador + "'";
		String[] campos = {"Id","Name","Bloqueado__c","Signature","Token__c","Gestor_Direto__c", "Data_de_Inativacao__c","Carga_Horaria_Prevista__c"};
		
		try {
		
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("User", campos, condicaoWhere, "Name")
					.getAsJsonArray("records");
			
			listaColaborador = montaColaborador(jsonArray, "User");
			
		}catch(Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String lista = gson.toJson(listaColaborador);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarColaboradores")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", result));
		
		return result;
		
	}
	
	public String buscarColaborador(){
		
		String email = jsonObjectRequest.get("email").getAsString();
		String senha = jsonObjectRequest.get("senha").getAsString();
		
		String[] campos = {"id", "Name", "Token__c","FirstName","LastName","Bloqueado__c", "Distancia_de_casa_para_trabalho__c", "Procedencia__c"};
		String condicaoWhere = "email='"+email+"'%20AND%20Token__c='"+senha+"'"; 
		
		JsonProperties properties;
		
		try {
			
			JsonElement jsonElementArray = DMLSalesForce
					.selecionaSalesForce("User", campos,condicaoWhere,"id")
					.getAsJsonArray("records")
					.get( 0 );
			
			Usuario usuario = new Usuario(jsonElementArray);
			
			properties = new JsonProperties.Builder()
					.addProperty("action", "buscarColaborador")
					.addProperty("statusCode", Protocolo.OK.getCodigo())
					.addProperty("retorno", usuario.temId())
					.addProperty("idsalesForce", usuario.getIdSalesForce())
					.addProperty("primeiroNome", usuario.getPrimeiroNome())
					.addProperty("ultimoNome", usuario.getUltimoNome())
					.addProperty("bloqueado", usuario.getBloqueado())
					.addProperty("distanciaCasaTrabalho", usuario.getDistanciaCasaTrabalho())
					.addProperty("procedencia", usuario.getProcedencia())
					.build();
		}catch(Exception e) {
			properties = new JsonProperties.Builder()
					.addProperty("action", "buscarColaborador")
					.addProperty("retorno", 0)
					.addProperty("statusCode", Protocolo.NOT_FOUND.getCodigo())
					.build();
			
			System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", result));
		
		return result;

	}
	
	public String buscarColaboradorBloqueado(){
		
		Protocolo protocolo = Protocolo.OK;
		
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		  
		String colaborador = jsonObjectRequest.get("colaborador").getAsString();
		  
		String condicaoWhere="id='" + colaborador + "'";
		String[] campos= {"id","Name","Bloqueado__c"};
		
		try {
		
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("User", campos,condicaoWhere,"id")
					.getAsJsonArray("records");
					
			listaColaborador = montaColaborador(jsonArray, "User");
			
		}catch(Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String lista = gson.toJson(listaColaborador);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarColaboradorBloqueado")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", result));
		
		return result;
	}

	public String buscarColaboradorReembolso() {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		
		String idColaborador = jsonObjectRequest.get("proprietario").getAsString();
		
		String condicaoWhere="Id%20=%20'" + idColaborador + "'"; 
		String [] campos = {"Id", "FirstName", "LastName", "Name"};
		
		

		try {
			
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("User", campos,condicaoWhere,"FirstName")
					.getAsJsonArray("records");
		
			listaColaborador = montaColaborador(jsonArray, "User");
		
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String lista = gson.toJson(listaColaborador);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarColaboradorReembolso")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", result));
		
		return result;
		
	}

	public String removerColaborador() {
		
		String colaborador = jsonObjectRequest.get("colaborador").getAsString();
		
		DMLSalesForce.removeSalesForce("User", colaborador);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "removerColaborador")
				.addProperty("retorno", 0)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", result));
		
		return result;
		
	}

	public Colaborador obterColaboradorId( String id ){
		
		Colaborador colaborador = new Colaborador();
		
		String condicaoWhere="Id%20=%20'" + id + "'"; 
		String [] campos = {"Id", "FirstName", "LastName", "Distancia_de_casa_para_trabalho__c"};
		
		try {
		
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("User", campos,condicaoWhere,"")
					.getAsJsonArray("records");
			
			colaborador = montaColaborador(jsonArray, "User").get(0);
			
		}catch (Exception e) {
			
			System.out.println(String.format("[ACTION::COLABORADOR::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		return colaborador;
		
	}
	
	public String apply() {
		return colaboradorRequests.apply(this);
	}

}