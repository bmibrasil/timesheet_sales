package action;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Consultor;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.ConsultorRequests;
import enumerations.Protocolo;
import webService.DMLSalesForce;

public class ConsultorAction {
	
	private Gson gson;
	
	private final ConsultorRequests consultorRequests;
	
	private final JsonObject jsonObjectRequest;
	
	public ConsultorAction(ConsultorRequests consultorRequests, JsonObject jsonObject) {
		gson = new Gson();
		this.consultorRequests = consultorRequests;
		this.jsonObjectRequest = jsonObject;
	}

	public List<Consultor> montaConsultor(JsonArray jsonArray, String nomeReferenciaTabela){

		List<Consultor> listaConsultores = new ArrayList<Consultor>();
		
		for(JsonElement jsonElement : jsonArray) {
			Consultor consultor = new Consultor(jsonElement);
			listaConsultores.add(consultor);
		}
		
		return listaConsultores;
		
	}

	public String buscarConsultor() {
		
		Protocolo protocolo = Protocolo.OK;

		List<Consultor> listaConsultor = new ArrayList<Consultor>();
	
		String colaborador = jsonObjectRequest.get("colaborador").getAsString();

		String condicaoWhere="Colaborador_Alocado__c='" + colaborador + "'%20AND%20Etapa__r.Projeto__r.Status__c!='BD'" + "%20AND%20Diferimento_Gerado__c=false";
		String[] campos= {"Id","Etapa__r.Projeto__c", "Nome_do_Projeto__c", "OrganizacaoId__c", "Nome_da_Organizacao__c", "Etapa__c", "Nome_da_Etapa__c", "Colaborador_Alocado__c", "Etapa__r.Projeto__r.Tipo_de_Projeto__c", "Etapa__r.Descricao__c", "Descricao__c","Name"};
		
		try {
		
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("NEXISIM_Simulador_Consultores__c", campos,condicaoWhere,"Nome_do_Projeto__c")
					.getAsJsonArray("records");
			
			listaConsultor = montaConsultor(jsonArray,"NEXISIM_Simulador_Consultores__c");
		
		}catch (Exception e) {

			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::CONSULTOR::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarConsultor")
				.addProperty("retorno", gson.toJson(listaConsultor))
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::CONSULTOR::NOTIFICATION] :: \n%s", result));
		
		return result;
	}
	
	public String apply() {
		return consultorRequests.apply(this);
	}
	
}