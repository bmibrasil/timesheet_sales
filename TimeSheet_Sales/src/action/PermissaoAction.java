package action;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Permissao;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.MenuRequests;
import enumerations.Protocolo;
import webService.DMLSalesForce;

public class PermissaoAction {
	
	private final Gson gson;
	
	private final JsonObject jsonObjectRequest;
	
	private final MenuRequests menuRequests;
	
	public PermissaoAction( MenuRequests menuRequests, JsonObject jsonObject ){
		gson = new Gson();
		this.menuRequests = menuRequests;
		this.jsonObjectRequest = jsonObject;
	}
	
	public List<Permissao> montaPermissao(JsonArray jsonArray, String nomeReferenciaTabela)	{
		
		List<Permissao> listaPermissao = new ArrayList<Permissao>();
		
		for(JsonElement jsonElement : jsonArray) {
			Permissao permissao = new Permissao(jsonElement);
			listaPermissao.add(permissao);
		}
		
		return listaPermissao;
		
	}
	
	public String buscarPermissao() {
		
		Protocolo protocolo = Protocolo.OK;
		
		List<Permissao> listaPermissao = new ArrayList<Permissao>();
		
		String colaborador = jsonObjectRequest.get("colaborador").getAsString();
		
		String condicaoWhere = "Usuario__c='" + colaborador + "'";
		String[] campos= {"Id","Data__c","Aceite__c","Usuario__c"};
		
		try {
		
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Permissoes_de_Usuario__c", campos,condicaoWhere,"")
					.getAsJsonArray("records");
			
			listaPermissao = montaPermissao(jsonArray,"Permissoes_de_Usuario__c");
			
		}catch (Exception e) {

			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::PERMISSAO::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}
		
		String lista = gson.toJson(listaPermissao);
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarPermissao")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::PERMISSAO::NOTIFICATION] :: \n%s", result));
		
		return result;
	}
	
	public String inserirPermissao() {

		String retorno = "";
		String id = jsonObjectRequest.get("id").getAsString();
		String data = jsonObjectRequest.get("data").getAsString();
		String colaborador = jsonObjectRequest.get("colaborador").getAsString();
		String aceite = jsonObjectRequest.get("aceite").getAsString();
		
		String[] campos= {"Usuario__c","Data__c","Aceite__c"};
		String[] vetorValores= {colaborador,data+"T00:00:00.000-0000",aceite};
		
		if (id.equals("0") == true) {	
			DMLSalesForce.insereSalesForce("Permissoes_de_Usuario__c", campos,vetorValores);
			retorno = "Permissao Salva";
		}
		else {
			String[] vetorValoresAtualizados= {colaborador,data+"T00:00:00.000-0000","true"};
			DMLSalesForce.atualizaSalesForce("Permissoes_de_Usuario__c",id, campos,vetorValoresAtualizados);
			retorno = "Permissao Atualizada";
		}
		
		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "inserirPermissao")
				.addProperty("retorno", retorno)
				.build();
		
		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::PERMISSAO::NOTIFICATION] :: \n%s", result));
		
		return result;
	}
	
	public String apply() {
		return menuRequests.apply(this);
	}
	
}