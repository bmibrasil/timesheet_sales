package action;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.ArquivoReembolso;
import Beans.Colaborador;
import Beans.Parametros;
import Beans.Reembolso;
import Beans.TipoReembolso;
import Service.ArquivoReembolsoService;
import Util.GsonPrettyJson;
import Util.JsonProperties;
import enumerations.Protocolo;
import enumerations.ReembolsoRequests;
import webService.DMLSalesForce;


public class ReembolsoAction {

	private Gson gson;

	private final ReembolsoRequests reembolsoRequests;

	private final JsonObject jsonObjectRequests;

	public ReembolsoAction(ReembolsoRequests reembolsoRequests, JsonObject jsonObject) {
		gson = new Gson();
		this.reembolsoRequests = reembolsoRequests;
		this.jsonObjectRequests = jsonObject;
	}

	
	public List<Reembolso> montarReembolso( JsonArray jsonArray, String nomeTabelaReferencia ){

		List<Reembolso> listaReembolsos = new ArrayList<Reembolso>();

		for(JsonElement jsonElement : jsonArray ) {
			Reembolso reembolso = new Reembolso( jsonElement );
			listaReembolsos.add( reembolso );
		}

		return listaReembolsos;

	}

	
	public String buscarReembolsosColaborador() {
		
		Protocolo protocolo = Protocolo.OK;

		List<Reembolso> listaReembolso = new ArrayList<Reembolso>();

		String colaborador = jsonObjectRequests.get("colaborador").getAsString();
		String status = jsonObjectRequests.get("status").getAsString();

		String [] camposJoin = {"Id", "Data__c", "Usuario__r.Name", "Organizacao__r.Name", "Projeto__r.Nome_do_projeto__c", "Projeto__r.Name", "Etapa__r.Descricao__c", "Tipo_de_Reembolso__r.Descricao__c", "Status_do_Reembolso__c", "Observacao__c", "Quilometragem__c", "Valor_Solicitado__c", "Valor_Reembolsado__c", "Origem__c", "Destino__c"};
		String condicaoWhere = "Usuario__c%20=%20'" + colaborador + "'%20AND%20" + "Status_do_Reembolso__c%20=%20'" + status + "'";

		try {
			
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Reembolso__c", camposJoin,condicaoWhere,"")
					.getAsJsonArray("records");
	
			listaReembolso = montarReembolso( jsonArray, "Reembolso__c" );
			
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::REEMBOLSO::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}

		String lista = gson.toJson(listaReembolso);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarReembolsosColaborador")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();

		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::REEMBOLSO::NOTIFICATION] :: \n%s", result));

		return result;
	}

	
	public String buscarReembolsosStatus() {
		
		Protocolo protocolo = Protocolo.OK;

		List<Reembolso> listaReembolso = new ArrayList<Reembolso>();
		List<ArquivoReembolso> listaArquivos = new ArrayList<ArquivoReembolso>();

		ArquivoReembolsoService arquivoReembolsoService = new ArquivoReembolsoService();

		String status = jsonObjectRequests.get("status").getAsString();

		String [] camposJoin = {"Id", "Data__c", "Usuario__c", "Usuario__r.Name", "Organizacao__r.Name", "Projeto__r.Nome_do_projeto__c", "Projeto__r.Name", "Etapa__r.Descricao__c", "Tipo_de_Reembolso__r.Descricao__c", "Status_do_Reembolso__c", "Observacao__c", "Quilometragem__c", "Valor_Solicitado__c", "Valor_Reembolsado__c", "Origem__c", "Destino__c"};
		String condicaoWhere = "Status_do_Reembolso__c%20=%20'" + status + "'";

		try {
		
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Reembolso__c", camposJoin,condicaoWhere,"")
					.getAsJsonArray("records");
	
			listaReembolso = montarReembolso(jsonArray, "Reembolso__c");
			
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::REEMBOLSO::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
		}

		for(Reembolso reembolso :  listaReembolso) {
			listaArquivos = arquivoReembolsoService.buscarArquivos( "" + reembolso.getId());
			reembolso.setNumeroArquivos(listaArquivos.size());
		}

		String lista = gson.toJson(listaReembolso);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarReembolsosStatus")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();

		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::REEMBOLSO::NOTIFICATION] :: \n%s", result));

		return result;
	}

	
	public String buscarLancamentoReembolso() throws ParseException {
		
		Protocolo protocolo = Protocolo.OK;

		List<Reembolso> listaReembolso = new ArrayList<Reembolso>();

		String lancamento = jsonObjectRequests.get("lancamentoReembolso").getAsString();

		String [] camposJoin = {"Id", "Name", "Data__c", "Dia_da_Semana__c", "Usuario__c", "Usuario__r.Name", "Organizacao__c", "Projeto__c", "Etapa__c", "Tipo_de_Reembolso__c", "Local__c", "Observacao__c", "Quilometragem__c", "Valor_Solicitado__c", "Valor_Reembolsado__c", "Quantidade_de_Dias__c", "Status_do_Reembolso__c", "Origem__c", "Destino__c"};
		String condicaoWhere = "Id%20=%20'" + lancamento + "'";

		try {
			
			JsonArray jsonArray = DMLSalesForce
					.selecionaSalesForce("Reembolso__c", camposJoin,condicaoWhere,"")
					.getAsJsonArray("records");

			listaReembolso = montarReembolso( jsonArray, "Reembolso__c" );
			
		}catch (Exception e) {
			
			protocolo = Protocolo.NOT_FOUND;
			
			System.out.println(String.format("[ACTION::REEMBOLSO::NOTIFICATION] :: \n%s", "Este registro não existe na base de dados."));
			
			
		}

		String lista = "[]";
		if(listaReembolso.size() > 0) {
			listaReembolso.get(0).setData( construirData(listaReembolso.get(0)) );

			lista = gson.toJson(listaReembolso);
		}

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarLancamentoReembolso")
				.addProperty("retorno", lista)
				.addProperty("protocolo", protocolo)
				.build();

		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::REEMBOLSO::NOTIFICATION] :: \n%s", result));

		return result;
	}

	
	private String construirData( Reembolso reembolso ) {

		String ano = reembolso.getData().substring(0, 4);
		String mes = reembolso.getData().substring(5, 7);
		String dia = reembolso.getData().substring(8, 10);

		return ano.concat("-").concat(mes).concat("-").concat(dia);

	}

	
	public String avaliarReembolso( ) {

		List<Reembolso> listaReembolso = new ArrayList<Reembolso>();

		String status = jsonObjectRequests.get("status").getAsString();
		String id = jsonObjectRequests.get("reembolso").getAsString();
		String justificativa = jsonObjectRequests.get("justificativa").getAsString();

		String campos[] = {"Status_do_Reembolso__c", "Justificativa__c"};
		String valores[] = {status, justificativa};

		if( id != null && !id.isEmpty()) {
			DMLSalesForce.atualizaSalesForce("Reembolso__c", id, campos, valores);
		}

		String lista = gson.toJson(listaReembolso);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "avaliarReembolso")
				.addProperty("retorno", lista)
				.build();

		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::REEMBOLSO::NOTIFICATION] :: \n%s", result));

		return result;
	}


	public String excluirReembolso() {

		String lancamentoReembolso = jsonObjectRequests.get("lancamento").getAsString();

		DMLSalesForce.removeSalesForce("Reembolso__c", lancamentoReembolso);

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "excluirReembolso")
				.addProperty("retorno", 0)
				.build();

		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::REEMBOLSO::NOTIFICATION] :: \n%s", result));

		return result;
	}


	public String salvarReembolso() throws Exception {

		JsonObject jsonObjSelecao = new JsonObject();

		int retorno = 0;

		String id = jsonObjectRequests.get("id").getAsString();
		String data = jsonObjectRequests.get("data").getAsString();
		String colaborador = jsonObjectRequests.get("colaborador").getAsString();
		String diaSemana = jsonObjectRequests.get("diasemana").getAsString();
		String cliente = jsonObjectRequests.get("cliente").getAsString();
		String projeto = jsonObjectRequests.get("projeto").getAsString();
		String tipo = jsonObjectRequests.get("tiporeembolso").getAsString();
		String etapa = jsonObjectRequests.get("etapa").getAsString();
		String local = jsonObjectRequests.get("local").getAsString();
		Integer quilometragem = jsonObjectRequests.get("quilometragem").getAsInt();
		String origem = ( jsonObjectRequests.get("origem").getAsString().equals("undefined") ) ? "" : jsonObjectRequests.get("origem").getAsString();
		String destino = ( jsonObjectRequests.get("destino").getAsString().equals("undefined") ) ? "" : jsonObjectRequests.get("destino").getAsString();
		Integer quantidadeDias = jsonObjectRequests.get("quantidadeDias").getAsInt();
		Double valorsolicitado = jsonObjectRequests.get("valorsolicitado").getAsDouble();
		Double valorreembolso = jsonObjectRequests.get("valorreembolso").getAsDouble();
		String observacao = jsonObjectRequests.get("observacao").getAsString();
		String seraReembolsabel = jsonObjectRequests.get("serareembolsavel").getAsString();
		String mensagem = "";

		TipoReembolsoAction tipoReembolsoAction = new TipoReembolsoAction();
		TipoReembolso tipoReembolso = tipoReembolsoAction.obterTipoReembolsoPorId(tipo);

		if( !tipoReembolso.getNome().equals("Quilometragem fora da cidade") 
				&& !tipoReembolso.getNome().equals("Quilometragem local")
				&& !tipoReembolso.getNome().equals("Taxi/Uber/Aplicativos")) {
			quilometragem = 0;
			origem = "";
			destino = "";
		}

		String configuracaoReembolso = tipoReembolso.getConfiguracao();

		ParametrosAction parametrosAction = new ParametrosAction();
		List<Parametros> listaParametros = parametrosAction.obterTodosParametros();

		//verifica novamente se desconta a quilometragem padrão do colaborador

		if ((quilometragem != 0) && (configuracaoReembolso.substring(1,2).equals("1"))){

			ColaboradorAction colaboradorAction = new ColaboradorAction();

			Colaborador colaboradorObject = colaboradorAction.obterColaboradorId( colaborador );

			valorreembolso= valorsolicitado - colaboradorObject.getDistanciaCasaTrabalho() * Double.parseDouble(listaParametros.get(0).getValor());
		}

		if ((quantidadeDias != 0) && (configuracaoReembolso.substring(3,4).equals("0") && (configuracaoReembolso.substring(1,2).equals("1")))){

			valorreembolso=Double.parseDouble(listaParametros.get(1).getValor()) * quantidadeDias;
		}

		if ((quantidadeDias != 0) && (configuracaoReembolso.substring(3,4).equals("1") && (configuracaoReembolso.substring(1,2).equals("1")))){
			valorreembolso=Double.parseDouble(listaParametros.get(2).getValor()) * quantidadeDias;
		}

		if (((quantidadeDias != 0) && (configuracaoReembolso.substring(3,4).equals("0") && (configuracaoReembolso.substring(1,2).equals("0")))) 
				|| 
				(((quantidadeDias != 0) && (configuracaoReembolso.substring(3,4).equals("1") && (configuracaoReembolso.substring(1,2).equals("0")))))){
			valorreembolso=valorsolicitado;
		}

		String[] camposInsert = {"Data__c", "Dia_da_Semana__c", "Usuario__c", "Organizacao__c", "Projeto__c", "Etapa__c", "Tipo_de_Reembolso__c", "Local__c", "Quantidade_de_Dias__c", "Valor_Solicitado__c", "Valor_Reembolsado__c", "Origem__c", "Destino__c", "Status_do_Reembolso__c", "Quilometragem__c", "Observacao__c", "Sera_Reembolsavel__c"};
		String[] vetorValores= {data + "T00:00:00.000-0000", diaSemana, colaborador, cliente, projeto, etapa, tipo, local, quantidadeDias.toString(), valorsolicitado.toString(), valorreembolso.toString(), origem, destino, "Pendente", quilometragem.toString(), observacao, seraReembolsabel };

		String condicaoWhere = "Data__c%20=%20" + data + "%20AND%20" + 
				"Usuario__c%20=%20'" + colaborador + "'%20AND%20" +
				"Organizacao__c%20=%20'" + cliente + "'%20AND%20" +
				"Projeto__c%20=%20'" + projeto + "'%20AND%20" + 
				"Etapa__c%20=%20'" + etapa + "'%20AND%20" + 
				"Tipo_de_Reembolso__c%20=%20'" + tipo + "'%20AND%20" + 
				"Local__c%20=%20'" + local.replace(" ", "%20") + "'%20AND%20" + 
				"Quantidade_de_Dias__c%20=%20" + quantidadeDias + "%20AND%20" + 
				"Valor_Solicitado__c%20=%20" + valorsolicitado + "%20AND%20" + 
				"Origem__c%20=%20'" + origem + "'%20AND%20" + 
				"Destino__c%20=%20'" + destino + "'%20AND%20" +
				"Sera_Reembolsavel__c%20=%20'" + seraReembolsabel + "'";		

		if( id.equals("0") ) {

			String[] campos = {"Data__c", "Usuario__r.Name", "Projeto__r.Nome_do_Projeto__c", "Etapa__r.Descricao__c", "Tipo_de_Reembolso__r.Descricao__c", "Local__c", "Quantidade_de_Dias__c", "Valor_Solicitado__c", "Origem__c", "Destino__c", "Sera_Reembolsavel__c"};

			jsonObjSelecao = DMLSalesForce.selecionaSalesForce("Reembolso__c", campos, condicaoWhere,"");

			if( jsonObjSelecao == null ) {
				DMLSalesForce.insereSalesForce("Reembolso__c", camposInsert,vetorValores);
			}else if(jsonObjSelecao.get("records").toString().equals("[]")){
				DMLSalesForce.insereSalesForce("Reembolso__c", camposInsert,vetorValores);
			}else {
				mensagem = "Reembolso já Cadastrado";
				retorno = 1;
			}

		}else {

			DMLSalesForce.atualizaSalesForce("Reembolso__c",id, camposInsert,vetorValores);

		}

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "salvarReembolso")
				.addProperty("retorno", retorno)
				.addProperty("mensagem", mensagem)
				.build();

		String result = new GsonPrettyJson()
				.toPrettyJson( properties.getJsonObject().toString() );
		
		System.out.println(String.format("[ACTION::REEMBOLSO::NOTIFICATION] :: \n%s", result));

		return result;
	}
	
	
	public synchronized String buscarIdNumeroReembolso() throws Exception {

		String data = jsonObjectRequests.get("data").getAsString();
		String colaborador = jsonObjectRequests.get("colaborador").getAsString();
		String cliente = jsonObjectRequests.get("cliente").getAsString();
		String projeto = jsonObjectRequests.get("projeto").getAsString();
		String tipo = jsonObjectRequests.get("tiporeembolso").getAsString();
		String etapa = jsonObjectRequests.get("etapa").getAsString();
		String local = jsonObjectRequests.get("local").getAsString();
		String origem = ( jsonObjectRequests.get("origem").getAsString().equals("undefined") ) ? "" : jsonObjectRequests.get("origem").getAsString();
		String destino = ( jsonObjectRequests.get("destino").getAsString().equals("undefined") ) ? "" : jsonObjectRequests.get("destino").getAsString();
		Integer quantidadeDias = jsonObjectRequests.get("quantidadeDias").getAsInt();
		Double valorsolicitado = jsonObjectRequests.get("valorsolicitado").getAsDouble();
		String seraReembolsabel = jsonObjectRequests.get("serareembolsavel").getAsString();
		String codigo = "";
		String idReembolso = "";

		String[] campo = {"Id", "Name"};

		String condicaoWhere = "Data__c%20=%20" + data + "%20AND%20" + 
				"Usuario__c%20=%20'" + colaborador + "'%20AND%20" +
				"Organizacao__c%20=%20'" + cliente + "'%20AND%20" +
				"Projeto__c%20=%20'" + projeto + "'%20AND%20" + 
				"Etapa__c%20=%20'" + etapa + "'%20AND%20" + 
				"Tipo_de_Reembolso__c%20=%20'" + tipo + "'%20AND%20" + 
				"Local__c%20=%20'" + local.replace(" ", "%20") + "'%20AND%20" + 
				"Quantidade_de_Dias__c%20=%20" + quantidadeDias + "%20AND%20" + 
				"Valor_Solicitado__c%20=%20" + valorsolicitado + "%20AND%20" + 
				"Origem__c%20=%20'" + origem + "'%20AND%20" + 
				"Destino__c%20=%20'" + destino + "'%20AND%20" +
				"Sera_Reembolsavel__c%20=%20'" + seraReembolsabel + "'";		

		JsonObject jsonObjSelecao = DMLSalesForce.selecionaSalesForce("Reembolso__c", campo, condicaoWhere,"");

		if( jsonObjSelecao != null ) {
			JsonArray jsonArray = jsonObjSelecao.getAsJsonArray("records");

			if(jsonArray.size() != 0 ) {
				idReembolso = jsonArray.get(0).getAsJsonObject().get("Id").getAsString();
				codigo = jsonArray.get(0).getAsJsonObject().get("Name").getAsString();
			}
		}

		JsonProperties properties = new JsonProperties.Builder()
				.addProperty("action", "buscarIdNumeroReembolso")
				.addProperty("idReembolso", idReembolso)
				.addProperty("codigo", codigo)
				.build();

		String result = properties.getJsonObject().toString();

		System.out.println("Response -> " + result);

		return result;

	}
	
	
	public String apply() throws Exception {
		return reembolsoRequests.apply(this);
	}

}