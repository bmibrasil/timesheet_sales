package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.ArquivoReembolsoRequests;

public class ArquivoReembolsoConfig {

	private Map<String, ArquivoReembolsoRequests> configHash;
	
	public ArquivoReembolsoConfig() {
		configHash = new HashMap<>();
		
		configHash.put("buscarArquivosReembolso", ArquivoReembolsoRequests.BUSCAR_ARQUIVO_REEMBOLSO);
		configHash.put("removerArquivoReembolso", ArquivoReembolsoRequests.REMOVER_ARQUIVO_REEMBOLSO);
	}
	
	public ArquivoReembolsoRequests getArquivoReembolsoRequests( String key ) {
		
		return configHash.get(key);
		
	}
	
}
