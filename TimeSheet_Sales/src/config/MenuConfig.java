package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.MenuRequests;

public class MenuConfig {

	private Map<String, MenuRequests> configHash;
	
	public MenuConfig() {
		configHash = new HashMap<>();
		
		configHash.put("buscarPermissao", MenuRequests.BUSCAR_PERMISSAO);
		configHash.put("inserirPermissao", MenuRequests.INSERIR_PERMISSAO);
	}
	
	public MenuRequests getMenuRequests( String key ) {
		
		return configHash.get(key);
		
	}
	
}
