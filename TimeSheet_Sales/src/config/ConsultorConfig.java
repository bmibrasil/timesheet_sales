package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.ConsultorRequests;

public class ConsultorConfig {

	private Map<String, ConsultorRequests> configHash;
	
	public ConsultorConfig() {
		configHash = new HashMap<>();
		
		configHash.put("buscarConsultor", ConsultorRequests.BUSCAR_CONSULTOR);
	}
	
	public ConsultorRequests getConsultorRequests( String key ) {
		
		return configHash.get(key);
		
	}
	
}
