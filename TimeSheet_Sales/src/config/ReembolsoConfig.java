package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.ReembolsoRequests;


public class ReembolsoConfig {
	
	private Map<String, ReembolsoRequests> configHash;

	public ReembolsoConfig() {
		configHash = new HashMap<>();
		
		configHash.put("buscarReembolsosColaborador", ReembolsoRequests.BUSCAR_REEMBOLSO_COLABORADOR);
		configHash.put("buscarLancamentoReembolso", ReembolsoRequests.BUSCAR_LANCAMENTO_REEMBOLSO);
		configHash.put("buscarReembolsosStatus", ReembolsoRequests.BUSCAR_REEMBOLSO_STATUS);
		configHash.put("salvarReembolso", ReembolsoRequests.SALVAR_REEMBOLSO);
		configHash.put("avaliarReembolso", ReembolsoRequests.AVALIAR_REEMBOLSO);
		configHash.put("excluirReembolso", ReembolsoRequests.EXCLUIR_REEMBOLSO);
		configHash.put("buscarIdNumeroReembolso", ReembolsoRequests.BUSCAR_ID_NUMERO_REEMBOLSO);
	}

	public ReembolsoRequests getReembolsoRequests( String key ) {

		return configHash.get(key);

	}

}
