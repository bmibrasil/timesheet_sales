package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.ColaboradorRequests;

public class ColaboradorConfig {

	private Map<String, ColaboradorRequests> configHash;
	
	public ColaboradorConfig() {
		configHash = new HashMap<>();
		
		configHash.put("buscarTodosColaboradores", ColaboradorRequests.BUSCAR_TODOS_COLABORADORES);
		configHash.put("buscarColaboradorReembolso", ColaboradorRequests.BUSCAR_COLABORADOR_REEMBOLSO);
		configHash.put("buscarColaborador", ColaboradorRequests.BUSCAR_COLABORADOR);
		configHash.put("buscarColaboradores", ColaboradorRequests.BUSCAR_COLABORADORES);
		configHash.put("bloqueiaColaborador", ColaboradorRequests.BLOQUEIA_COLABORADOR);
	}
	
	public ColaboradorRequests getColaboradorRequests( String key ) {
		
		return configHash.get(key);
		
	}
	
}
