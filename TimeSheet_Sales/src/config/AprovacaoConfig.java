package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.AprovacaoRequests;

public class AprovacaoConfig {

	private Map<String, AprovacaoRequests> configHash;
	
	public AprovacaoConfig() {
		configHash = new HashMap<>();
		
		configHash.put("aprovaTimeSheet", AprovacaoRequests.APROVA_TIMESHEET);
		configHash.put("buscarAprovacao", AprovacaoRequests.BUSCAR_APROVACAO);
	}
	
	public AprovacaoRequests getAprovacaoRequests( String key ) {
		
		return configHash.get(key);
		
	}
	
}
