package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.ItemTimesheetRequests;

public class ItemTimesheetConfig {
	
private Map<String, ItemTimesheetRequests> configHash;
	
	public ItemTimesheetConfig() {
		configHash = new HashMap<>();
		
		configHash.put("consolidaTimeSheet", ItemTimesheetRequests.CONSOLIDA_TIMESHEET);
		configHash.put("buscaResumoHorasProjetoPeriodo", ItemTimesheetRequests.BUSCA_RESUMO_HORAS_PROJETO_PERIODO);
		configHash.put("relatorioConsolidado", ItemTimesheetRequests.RELATORIO_CONSOLIDADO);
		configHash.put("relatorioDetalhado", ItemTimesheetRequests.RELATORIO_DETALHADO);
		configHash.put("buscaLancamentosColaboradorPeriodo", ItemTimesheetRequests.BUSCA_LANCAMENTOS_COLABORADOR_PERIODO);
		configHash.put("buscarLancamentos", ItemTimesheetRequests.BUSCAR_TIMESHEETS);
		configHash.put("buscarLancamento", ItemTimesheetRequests.BUSCAR_LANCAMENTO);
		configHash.put("excluirLancamento", ItemTimesheetRequests.EXCLUIR_LANCAMENTO);
		configHash.put("salvarTimeSheet", ItemTimesheetRequests.SALVAR_TIMESHEET);
	}
	
	public ItemTimesheetRequests getItemTimesheetRequests( String key ) {
		
		return configHash.get(key);
		
	}

}
