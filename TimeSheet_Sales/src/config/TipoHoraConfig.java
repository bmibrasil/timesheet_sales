package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.TipoHoraRequests;

public class TipoHoraConfig {

	private Map<String, TipoHoraRequests> configHash;
	
	public TipoHoraConfig() {
		configHash = new HashMap<>();
		
		configHash.put("buscarTipoHoras", TipoHoraRequests.BUSCAR_TIPO_HORA);
	}
	
	public TipoHoraRequests getTipoHoraRequests( String key ) {
		
		return configHash.get(key);
		
	}
	
}
