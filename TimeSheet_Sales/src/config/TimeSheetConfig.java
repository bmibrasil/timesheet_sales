package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.TimeSheetRequests;

public class TimeSheetConfig {
	
	private Map<String, TimeSheetRequests> configHash;

	public TimeSheetConfig() {
		configHash = new HashMap<>();
		
		configHash.put("buscarPeriodos", TimeSheetRequests.BUSCA_PERIODOS);
		configHash.put("buscarPeriodosConsolidado", TimeSheetRequests.BUSCA_PERIODOS_CONSOLIDADO);
	}

	public TimeSheetRequests getTimeSheetRequests( String key ) {

		return configHash.get(key);

	}

}
