package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.Controllers;
import webServiceController.Controller;

public class ResourcesConfig{
	
	private Map<String, Controllers> configHashRequest;
	
	public ResourcesConfig() {
		configHashRequest = new HashMap<>();
		
		configHashRequest.put("colaborador", Controllers.COLABORADOR);
		configHashRequest.put("menu", Controllers.MENU);
		configHashRequest.put("tipoHora", Controllers.TIPO_HORA);
		configHashRequest.put("consultor", Controllers.CONSULTOR);
		configHashRequest.put("timesheet", Controllers.TIMESHEET);
		configHashRequest.put("reembolso", Controllers.REEMBOLSO);
		configHashRequest.put("itemTimesheet", Controllers.ITEM_TIMESHEET);
		configHashRequest.put("aprovacao", Controllers.APROVACAO);
		configHashRequest.put("arquivoReembolso", Controllers.ARQUIVO_REEMBOLSO);
		configHashRequest.put("tipoReembolso", Controllers.TIPO_REEMBOLSO);
		configHashRequest.put("parametros", Controllers.PARAMETROS);
	}
	
	public Controller getController( String key ) {
		
		return configHashRequest.get(key).getController();
		
	}
	
}