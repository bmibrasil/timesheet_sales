package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.ParametrosRequests;

public class ParametrosConfig {

	private Map<String, ParametrosRequests> configHash;

	public ParametrosConfig() {
		configHash = new HashMap<>();
		
		configHash.put("buscarParametros", ParametrosRequests.BUSCAR_PARAMETROS);
	}

	public ParametrosRequests getParametrosRequests( String key ) {

		return configHash.get(key);

	}


}
