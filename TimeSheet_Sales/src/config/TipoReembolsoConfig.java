package config;

import java.util.HashMap;
import java.util.Map;

import enumerations.TipoReembolsoRequests;

public class TipoReembolsoConfig {
	
	private Map<String, TipoReembolsoRequests> configHash;
	
	public TipoReembolsoConfig() {
		configHash = new HashMap<>();
		
		configHash.put("buscarTipoReembolso", TipoReembolsoRequests.BUSCAR_TIPO_REEMBOLSO);
	}
	
	public TipoReembolsoRequests getTipoReembolsoRequests( String key ) {
		
		return configHash.get(key);
		
	}

}
