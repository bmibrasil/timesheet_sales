package Teste;

import Beans.ItemTimeSheet;

public class TesteIntervaloItemTimesheet {
	
	public void testar() {
		
		inserirContendoIntervalo();
		inserirDentroIntervalo();
		inserirFimDentroInicioFora();
		inserirInicioDentroFimFora();
		inserirMesmoIntervalo();
		intervaloAnteriorCadastradoBorda();
		intervaloAnteriorUmCadastrado();
		intervaloAposCadastradoBorda();
		intervaloAposUmCadastrado();
	}

	//caso 1: listaInicio {10:30} listaFim {11:30}
	//  inicio:      10:30  fim:      11:30
	//comentario: inserir no mesmo intervalo

	public void inserirMesmoIntervalo(){

		ItemTimeSheet itemTimeSheet = ItemTimeSheet.ofInterval("10:30", "11:30");

		boolean retorno = itemTimeSheet.intervaloValido("10:30", "11:30");

		if(retorno)
			System.out.println("inserirMesmoIntervalo - Passed");
		else
			System.out.println("inserirMesmoIntervalo - Failed ");
	}

	//caso 2: listaInicio {10:00} listaFim {11:00}
	//  inicio:      10:01  fim:      10:59
	//comentario: inserir dentro de um intervalo

	public void inserirDentroIntervalo(){

		ItemTimeSheet itemTimeSheet = ItemTimeSheet.ofInterval("10:00", "11:00");

		boolean retorno = itemTimeSheet.intervaloValido("10:01", "10:59");

		if(retorno)
			System.out.println("inserirDentroIntervalo - Passed");
		else
			System.out.println("inserirDentroIntervalo - Failed ");
	}

	//caso 3: listaInicio {10:00} listaFim {11:00}
	//  inicio:      10:01  fim:      11:01
	//  inicio:      10:01  listaFim {11:00}
	//comentario: inserir com inicio dentro e fim fora

	public void inserirInicioDentroFimFora(){

		ItemTimeSheet itemTimeSheet = ItemTimeSheet.ofInterval("10:00", "11:00");

		boolean retorno = itemTimeSheet.intervaloValido("10:01", "11:01");

		if(retorno)
			System.out.println("inserirInicioDentroFimFora - Passed");
		else
			System.out.println("inserirInicioDentroFimFora - Failed ");
	}

	//caso 4: listaInicio {10:00} listaFim {11:00}
	//  inicio:      09:00  fim:      10:01
	//  fim:         10:01  listaInicio {10:00}
	//comentario: inserir com fim dentro e inicio fora

	public void inserirFimDentroInicioFora(){

		ItemTimeSheet itemTimeSheet = ItemTimeSheet.ofInterval("10:00", "11:00");

		boolean retorno = itemTimeSheet.intervaloValido("09:00", "10:01");

		if(retorno)
			System.out.println("inserirFimDentroInicioFora - Passed");
		else
			System.out.println("inserirFimDentroInicioFora - Failed ");
	}

	//caso 5: listaInicio {10:01} listaFim {10:59}
	//  inicio:      10:00  fim:      11:00
	//comentario: inserir contendo um intervalo 

	public void inserirContendoIntervalo(){

		ItemTimeSheet itemTimeSheet = ItemTimeSheet.ofInterval("10:01", "10:59");

		boolean retorno = itemTimeSheet.intervaloValido("10:00", "11:00");

		if(retorno)
			System.out.println("inserirContendoIntervalo - Passed");
		else
			System.out.println("inserirContendoIntervalo - Failed ");
	}
	
	public void intervaloAposUmCadastrado(){

		ItemTimeSheet itemTimeSheet = ItemTimeSheet.ofInterval("10:00", "11:00");

		boolean retorno = itemTimeSheet.intervaloValido("12:00", "13:00");

		if(!retorno)
			System.out.println("intervaloAposUmCadastrado - Passed");
		else
			System.out.println("intervaloAposUmCadastrado - Failed ");
	}
	
	public void intervaloAnteriorUmCadastrado(){

		ItemTimeSheet itemTimeSheet = ItemTimeSheet.ofInterval("10:00", "11:00");

		boolean retorno = itemTimeSheet.intervaloValido("08:00", "09:00");

		if(!retorno)
			System.out.println("intervaloAnteriorUmCadastrado - Passed");
		else
			System.out.println("intervaloAnteriorUmCadastrado - Failed ");
	}
	
	public void intervaloAposCadastradoBorda(){

		ItemTimeSheet itemTimeSheet = ItemTimeSheet.ofInterval("10:00", "11:00");

		boolean retorno = itemTimeSheet.intervaloValido("11:01", "13:00");

		if(!retorno)
			System.out.println("intervaloAposCadastradoBorda - Passed");
		else
			System.out.println("intervaloAposCadastradoBorda - Failed ");
	}
	
	public void intervaloAnteriorCadastradoBorda(){

		ItemTimeSheet itemTimeSheet = ItemTimeSheet.ofInterval("10:00", "11:00");

		boolean retorno = itemTimeSheet.intervaloValido("08:00", "09:59");

		if(!retorno)
			System.out.println("intervaloAnteriorCadastradoBorda - Passed");
		else
			System.out.println("intervaloAnteriorCadastradoBorda - Failed ");
	}

}
