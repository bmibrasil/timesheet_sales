package Exceptions;

public class ElementoNaoEncontradoException extends Exception{

	private static final long serialVersionUID = 3172995477132070769L;

    public String message;

    public ElementoNaoEncontradoException(String campo){
        message = "Elemento n�o encontrado(Campo:"+campo+")";
    }

    @Override
    public String getMessage(){
    	String message = super.getMessage();
        return message+"\n"+this.message;
    }

}