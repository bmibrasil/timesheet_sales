package Dao;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import Service.DataSourceUtil;

public abstract class AbstractDAO<T> implements GenericDAO<T> {

	public List<String> getFieldNamesList() throws InstantiationException, IllegalAccessException{
		List<String> result = new ArrayList<String>();
		try {
			Class<?> c = Class.forName(getClassName());
			for (Field nome:c.getDeclaredFields()){
				result.add(nome.getName());
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String getClassName(){
		return "Beans."+getClass().getName().replace("Dao", "").replace(".","");
	}

	@SuppressWarnings({ "unchecked" })
	public List<T> findAll() {
		Connection conn=null;

		List<T> result = new ArrayList<T>();
		try {
			conn = DataSourceUtil.getConnection();
			Statement st = conn.createStatement();

			String query = "SELECT ";
			try {
				for (String nome:getFieldNamesList()){
					query+=nome+",";
				}
				query=query.substring(0, query.length()-1); //remove v�rgula final
				query += " FROM "+Class.forName(getClassName()).getSimpleName().toLowerCase();
			} catch (Exception e1) {e1.printStackTrace();}

			//System.out.println(query);

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sfm = new SimpleDateFormat("HH:MM");
			ResultSet rs = null;
			try {
				rs = st.executeQuery(query);
				while(rs.next()) {
					T obj = null;
					Class<?> clazz = null;
					try {
						clazz = Class.forName(getClassName());
						Constructor<?> ctor;
						ctor = clazz.getDeclaredConstructor();
						obj = (T) ctor.newInstance();
					} catch (Exception e) {
						e.printStackTrace();
					}
					int i=0;
					for (Field field : clazz.getDeclaredFields()) {
						field.setAccessible(true);
						String tipo = field.getType().getName();
						try {
							if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
								field.set(obj, rs.getInt(i+1));
							} else if (tipo.equals("java.lang.String")){
								field.set(obj, rs.getString(i+1));
							} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
								field.set(obj, rs.getDouble(i+1));
							} else if (tipo.equals("java.util.Date")) {
								field.set(obj, sm.parse(sm.format(rs.getDate(i+1))));
							} else if (tipo.equals("java.sql.Time")) {
								field.set(obj, rs.getTime(i+1));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						i++;
					}
					result.add(obj);
				}
			} finally {
				rs.close();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}
	
	
	public List<T> findAllOrdered(String fieldNameOrder) {
		Connection conn=null;

		List<T> result = new ArrayList<T>();
		try {
			conn = DataSourceUtil.getConnection();
			Statement st = conn.createStatement();

			String query = "SELECT ";
			try {
				for (String nome:getFieldNamesList()){
					query+=nome+",";
				}
				query=query.substring(0, query.length()-1); //remove v�rgula final
				query += " FROM "+Class.forName(getClassName()).getSimpleName().toLowerCase();
			
			} catch (Exception e1) {e1.printStackTrace();}
			query +=" ORDER BY " +fieldNameOrder;
			//System.out.println(query);

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sfm = new SimpleDateFormat("HH:MM");
			ResultSet rs = null;
			try {
				rs = st.executeQuery(query);
				while(rs.next()) {
					T obj = null;
					Class<?> clazz = null;
					try {
						clazz = Class.forName(getClassName());
						Constructor<?> ctor;
						ctor = clazz.getDeclaredConstructor();
						obj = (T) ctor.newInstance();
					} catch (Exception e) {
						e.printStackTrace();
					}
					int i=0;
					for (Field field : clazz.getDeclaredFields()) {
						field.setAccessible(true);
						String tipo = field.getType().getName();
						try {
							if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
								field.set(obj, rs.getInt(i+1));
							} else if (tipo.equals("java.lang.String")){
								field.set(obj, rs.getString(i+1));
							} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
								field.set(obj, rs.getDouble(i+1));
							} else if (tipo.equals("java.util.Date")) {
								field.set(obj, sm.parse(sm.format(rs.getDate(i+1))));
							} else if (tipo.equals("java.sql.Time")) {
								field.set(obj, rs.getTime(i+1));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						i++;
					}
					result.add(obj);
				}
			} finally {
				rs.close();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}


	@SuppressWarnings("unchecked")
	public List<T> findByField(String fieldName, Object content) {
		Connection conn=null;
		List<T> result = new ArrayList<T>();
		try {
			T obj = null;
			Class<?> clazz = null;
			try {
				clazz = Class.forName(getClassName());
				Constructor<?> ctor;
				ctor = clazz.getDeclaredConstructor();
				obj = (T) ctor.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}

			//Busca o tipo do dado a ser buscado
			String tipo=null;
			for (Field field : clazz.getDeclaredFields()) {
				if(field.getName().equals(fieldName)){
					tipo=field.getType().getName();
				}
			}

			String query = "SELECT ";
			try {
				for (String nome:getFieldNamesList()){
					query+=nome+",";
				}
				query=query.substring(0, query.length()-1); //remove v�rgula final
				query += " FROM "+Class.forName(getClassName()).getSimpleName().toLowerCase();
				if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
					query += " WHERE "+fieldName+"="+content;
				} else if (tipo.equals("java.lang.String")) {
					query += " WHERE "+fieldName+"='"+content+"'";
				} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
					query += " WHERE "+fieldName+"="+content.toString().replace(",", ".");
				} else if ((tipo.equals("java.sql.Time")||(tipo.equals("java.util.Date")))) {
					query += " WHERE "+fieldName+"='"+content+"'";
				
				}

			} catch (Exception e1) {e1.printStackTrace();}

			//System.out.println(query);

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat stf = new SimpleDateFormat("HH:MM");
			ResultSet rs = null;
			try {
				conn = DataSourceUtil.getConnection();
				Statement st = conn.createStatement();
				rs = st.executeQuery(query);
				while(rs.next()) {
					obj = null;
					clazz = null;
					try {
						clazz = Class.forName(getClassName());
						Constructor<?> ctor;
						ctor = clazz.getDeclaredConstructor();
						obj = (T) ctor.newInstance();
					} catch (Exception e) {
						e.printStackTrace();
					}

					int i=1;
					for (Field field : clazz.getDeclaredFields()) {
						field.setAccessible(true);
						tipo = field.getType().getName();
						try {
							if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
								field.set(obj, rs.getInt(i));
							} else if (tipo.equals("java.lang.String")){
								field.set(obj, rs.getString(i));
							} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
								field.set(obj, rs.getDouble(i));
							} else if (tipo.equals("java.util.Date")) {
								field.set(obj, sm.parse(sm.format(rs.getDate(i))));
							} else if (tipo.equals("java.sql.Time")) {
								field.set(obj, rs.getTime(i));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						i++;
					}
					result.add(obj);
				}
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				rs.close();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<T> findByFieldOrdered(String fieldName, Object content, String fieldNameOrder) {
		Connection conn=null;
		List<T> result = new ArrayList<T>();
		try {
			T obj = null;
			Class<?> clazz = null;
			try {
				clazz = Class.forName(getClassName());
				Constructor<?> ctor;
				ctor = clazz.getDeclaredConstructor();
				obj = (T) ctor.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}

			//Busca o tipo do dado a ser buscado
			String tipo=null;
			for (Field field : clazz.getDeclaredFields()) {
				if(field.getName().equals(fieldName)){
					tipo=field.getType().getName();
				}
			}

			String query = "SELECT ";
			try {
				for (String nome:getFieldNamesList()){
					query+=nome+",";
				}
				query=query.substring(0, query.length()-1); //remove v�rgula final
				query += " FROM "+Class.forName(getClassName()).getSimpleName().toLowerCase();
				if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
					query += " WHERE "+fieldName+"="+content;
				} else if (tipo.equals("java.lang.String")) {
					query += " WHERE "+fieldName+"='"+content+"'";
				} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
					query += " WHERE "+fieldName+"="+content.toString().replace(",", ".");
				} else if ((tipo.equals("java.sql.Time")||(tipo.equals("java.util.Date")))) {
					query += " WHERE "+fieldName+"='"+content+"'";
				
				}
			
			} catch (Exception e1) {e1.printStackTrace();}
			query +=  " ORDER BY "+fieldNameOrder;

			//System.out.println(query);

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat stf = new SimpleDateFormat("HH:MM");
			ResultSet rs = null;
			try {
				conn = DataSourceUtil.getConnection();
				Statement st = conn.createStatement();
				rs = st.executeQuery(query);
				while(rs.next()) {
					obj = null;
					clazz = null;
					try {
						clazz = Class.forName(getClassName());
						Constructor<?> ctor;
						ctor = clazz.getDeclaredConstructor();
						obj = (T) ctor.newInstance();
					} catch (Exception e) {
						e.printStackTrace();
					}

					int i=1;
					for (Field field : clazz.getDeclaredFields()) {
						field.setAccessible(true);
						tipo = field.getType().getName();
						try {
							if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
								field.set(obj, rs.getInt(i));
							} else if (tipo.equals("java.lang.String")){
								field.set(obj, rs.getString(i));
							} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
								field.set(obj, rs.getDouble(i));
							} else if (tipo.equals("java.util.Date")) {
								field.set(obj, sm.parse(sm.format(rs.getDate(i))));
							} else if (tipo.equals("java.sql.Time")) {
								field.set(obj, rs.getTime(i));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						i++;
					}
					result.add(obj);
				}
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				rs.close();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<T> findByFields(Hashtable<String,Object> hash) {
		Connection conn=null;
		List<T> result = new ArrayList<T>();
		try {
			T obj = null;
			Class<?> clazz = null;
			try {
				clazz = Class.forName(getClassName());
				Constructor<?> ctor;
				ctor = clazz.getDeclaredConstructor();
				obj = (T) ctor.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}

			//Busca o tipo do dado a ser buscado
			String tipo=null;

			String query = "SELECT ";
			try {
				for (String nome:getFieldNamesList()){
					query+=nome+",";
				}
				query=query.substring(0, query.length()-1); //remove v�rgula final
				query += " FROM "+Class.forName(getClassName()).getSimpleName().toLowerCase();
				query += " WHERE ";
				for (Object key:hash.keySet()){

					//Busca o tipo do elemento
					for (Field field : clazz.getDeclaredFields()) {
						if(field.getName().equals((String)(key))){
							tipo=field.getType().getName();
						}
					}
					if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
						query += (String)(key)+"="+hash.get(key)+" AND ";
					} else if (tipo.equals("java.lang.String")) {
						query += (String)(key)+"='"+hash.get(key)+"' AND ";
					} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
						query += (String)(key)+"="+hash.get(key).toString().replace(",", ".")+" AND ";
					} else if (tipo.equals("java.util.Date")) {
						query += (String)(key)+"='"+hash.get(key).toString().replace(",", ".")+"' AND ";
					} else if (tipo.equals("java.sql.Time")) {
						query += (String)(key)+"='"+hash.get(key).toString().replace(",", ".")+"' AND ";
					}
				}
				query = query.substring(0,query.length()-5); //remove �ltimo AND

			} catch (Exception e1) {e1.printStackTrace();}

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd HH:MM");
			SimpleDateFormat sfm = new SimpleDateFormat("HH:MM");
			ResultSet rs = null;
			try {
				conn = DataSourceUtil.getConnection();
				Statement st = conn.createStatement();
				rs = st.executeQuery(query);
				while(rs.next()) {
					obj = null;
					clazz = null;
					try {
						clazz = Class.forName(getClassName());
						Constructor<?> ctor;
						ctor = clazz.getDeclaredConstructor();
						obj = (T) ctor.newInstance();
					} catch (Exception e) {
						e.printStackTrace();
					}

					int i=1;
					for (Field field : clazz.getDeclaredFields()) {
						field.setAccessible(true);
						tipo = field.getType().getName();
						try {
							if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
								field.set(obj, rs.getInt(i));
							} else if (tipo.equals("java.lang.String")){
								field.set(obj, rs.getString(i));
							} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
								field.set(obj, rs.getDouble(i));
							} else if (tipo.equals("java.util.Date")) {
								field.set(obj, sm.parse(sm.format(rs.getDate(i))));
							} else if (tipo.equals("java.util.Time")) {
								field.set(obj, rs.getTime(i));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						i++;
					}
					result.add(obj);
				}
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				rs.close();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}

	
	@SuppressWarnings("unchecked")
	public List<T> findByFieldsOrdered(Hashtable<String,Object> hash, String fieldNameOrder) {
		Connection conn=null;
		List<T> result = new ArrayList<T>();
		try {
			T obj = null;
			Class<?> clazz = null;
			try {
				clazz = Class.forName(getClassName());
				Constructor<?> ctor;
				ctor = clazz.getDeclaredConstructor();
				obj = (T) ctor.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}

			//Busca o tipo do dado a ser buscado
			String tipo=null;

			String query = "SELECT ";
			try {
				for (String nome:getFieldNamesList()){
					query+=nome+",";
				}
				query=query.substring(0, query.length()-1); //remove v�rgula final
				query += " FROM "+Class.forName(getClassName()).getSimpleName().toLowerCase();
				query += " WHERE ";
				for (Object key:hash.keySet()){

					//Busca o tipo do elemento
					for (Field field : clazz.getDeclaredFields()) {
						if(field.getName().equals((String)(key))){
							tipo=field.getType().getName();
						}
					}
					if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
						query += (String)(key)+"="+hash.get(key)+" AND ";
					} else if (tipo.equals("java.lang.String")) {
						query += (String)(key)+"='"+hash.get(key)+"' AND ";
					} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
						query += (String)(key)+"="+hash.get(key).toString().replace(",", ".")+" AND ";
					} else if (tipo.equals("java.util.Date")) {
						query += (String)(key)+"='"+hash.get(key).toString().replace(",", ".")+"' AND ";
					} else if (tipo.equals("java.sql.Time")) {
						query += (String)(key)+"='"+hash.get(key).toString().replace(",", ".")+"' AND ";
					}
				}
				query = query.substring(0,query.length()-5); //remove �ltimo AND

			} catch (Exception e1) {e1.printStackTrace();}
			query +=  " ORDER BY "+fieldNameOrder;


			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd HH:MM");
			SimpleDateFormat sfm = new SimpleDateFormat("HH:MM");
			ResultSet rs = null;
			try {
				conn = DataSourceUtil.getConnection();
				Statement st = conn.createStatement();
				rs = st.executeQuery(query);
				while(rs.next()) {
					obj = null;
					clazz = null;
					try {
						clazz = Class.forName(getClassName());
						Constructor<?> ctor;
						ctor = clazz.getDeclaredConstructor();
						obj = (T) ctor.newInstance();
					} catch (Exception e) {
						e.printStackTrace();
					}

					int i=1;
					for (Field field : clazz.getDeclaredFields()) {
						field.setAccessible(true);
						tipo = field.getType().getName();
						try {
							if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
								field.set(obj, rs.getInt(i));
							} else if (tipo.equals("java.lang.String")){
								field.set(obj, rs.getString(i));
							} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
								field.set(obj, rs.getDouble(i));
							} else if (tipo.equals("java.util.Date")) {
								field.set(obj, sm.parse(sm.format(rs.getDate(i))));
							} else if (tipo.equals("java.util.Time")) {
								field.set(obj, rs.getTime(i));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						i++;
					}
					result.add(obj);
				}
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				rs.close();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<T> executeSQL(String sql) {
		Connection conn=null;
		String tipo="";
		List<T> result = new ArrayList<T>();
		try {
			T obj = null;
			Class<?> clazz = null;
			try {
				clazz = Class.forName(getClassName());
				Constructor<?> ctor;
				ctor = clazz.getDeclaredConstructor();
				obj = (T) ctor.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}

			//System.out.println(sql);

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sfm = new SimpleDateFormat("HH:MM");
			ResultSet rs = null;
			try {
				conn = DataSourceUtil.getConnection();
				Statement st = conn.createStatement();
				rs = st.executeQuery(sql);
				while(rs.next()) {
					obj = null;
					clazz = null;
					try {
						clazz = Class.forName(getClassName());
						Constructor<?> ctor;
						ctor = clazz.getDeclaredConstructor();
						obj = (T) ctor.newInstance();
					} catch (Exception e) {
						e.printStackTrace();
					}

					int i=1;
					for (Field field : clazz.getDeclaredFields()) {
						field.setAccessible(true);
						tipo = field.getType().getName();
						try {
							if ((tipo.equals("int"))||(tipo.equals("java.lang.Integer"))) {
								field.set(obj, rs.getInt(i));
							} else if (tipo.equals("java.lang.String")){
								field.set(obj, rs.getString(i));
							} else if ((tipo.equals("double")||(tipo.equals("java.lang.Double")))) {
								field.set(obj, rs.getDouble(i));
							} else if (tipo.equals("java.util.Date"))   						   {
								field.set(obj, sm.parse(sm.format(rs.getDate(i))));
							} else if (tipo.equals("java.sql.Time"))   						   {
								field.set(obj, rs.getTime(i));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						i++;
					}
					result.add(obj);
				}
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				rs.close();
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}

	public int executeCountSQL(String sql) {
		Connection conn=null;

		int result = 0;
		try {

			ResultSet rs = null;
			try {
				conn = DataSourceUtil.getConnection();
				Statement st = conn.createStatement();
				rs = st.executeQuery(sql);
				result = rs.getInt(1);
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				rs.close();
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}

	public ResultSet executeRawSQL(String sql) {
		Connection conn=null;
		ResultSet result = null;
		ResultSet rs = null;
		try {
			conn = DataSourceUtil.getConnection();
			Statement st = conn.createStatement();
			rs = st.executeQuery(sql);
			result = rs;
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}

	public void execute(String sql) {//no result
		Connection conn=null;
		try {
			conn = DataSourceUtil.getConnection();
			Statement st = conn.createStatement();
			st.execute(sql);
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}

	}

	public void insert(T content) {
		Connection conn=null;
		String tipo=null;
		String tabela="";
		try {tabela = Class.forName(getClassName()).getSimpleName().toLowerCase();
		} catch (ClassNotFoundException e2) {e2.printStackTrace();	}
		String query="";
		query = "INSERT INTO "+tabela+" (";
		try {
			for (Field field:content.getClass().getDeclaredFields()){
				query+=field.getName()+",";
			}
			query=query.substring(0, query.length()-1); //remove v�rgula final
			query += ") VALUES (";

			for (Field field:content.getClass().getDeclaredFields()){
				tipo = field.getType().getName();
				String conteudo="null";
				try {
					if (content!=null) {
						conteudo = runGetter(content,field.getName()).toString();
						if        ((tipo.equals("int"))||(tipo.equals("java.lang.Integer")))   {query += conteudo+",";
						} else if (tipo.equals("java.lang.String"))                            {query += "'"+conteudo+"',";
						} else if ((tipo.equals("double"))||(tipo.equals("java.lang.Double"))) {query += conteudo.replace(",", ".")+",";
						} else if (tipo.equals("java.util.Date"))   						   {query += "'"+conteudo+"',";
						} else if (tipo.equals("java.sql.Time"))   						   	   {query += "'"+conteudo+"',";
						}
					}
				} catch (Exception e){
					conteudo = "null";
					if        ((tipo.equals("int"))||(tipo.equals("java.lang.Integer")))   { query += conteudo+",";
					} else if (tipo.equals("java.lang.String"))                            { query += "'"+conteudo+"',";
					} else if ((tipo.equals("double"))||(tipo.equals("java.lang.Double"))) { query += conteudo+",";
					} else if (tipo.equals("java.util.Date"))   						   { query += "'"+conteudo+"',";
					} else if (tipo.equals("java.sql.Time"))	   						   { query += "'"+conteudo+"',";
					}
				}

			}
			query=query.substring(0, query.length()-1); //remove v�rgula final
			query+=")";

			query = query.replaceAll("'null'", "null");

		} catch (Exception er) {er.printStackTrace();}

		//System.out.println(query);

		try {
			conn = DataSourceUtil.getConnection();
			Statement st = conn.createStatement();
			st.execute(query);
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}

	}

	public void save(T content) {
		Connection conn=null;

		String tipo=null;
		String id="";
		String tabela="";
		try {tabela = Class.forName(getClassName()).getSimpleName().toLowerCase();
		} catch (ClassNotFoundException e2) {e2.printStackTrace();	}
		String query="";
		query = "UPDATE "+tabela+" SET ";
		try {
			for (Field field:content.getClass().getDeclaredFields()){
				tipo = field.getType().getName();
				String conteudo="null";
				try {
					if (content!=null) {
						conteudo = runGetter(content,field.getName()).toString();
						if (field.getName().equals("id")){
							id = conteudo;
						}
						if        ((tipo.equals("int"))||(tipo.equals("java.lang.Integer")))   {query += field.getName()+"="+conteudo+",";
						} else if (tipo.equals("java.lang.String"))                            {query += field.getName()+"="+"'"+conteudo+"',";
						} else if ((tipo.equals("double"))||(tipo.equals("java.lang.Double"))) {query += field.getName()+"="+conteudo.replace(",", ".")+",";
						} else if (tipo.equals("java.util.Date"))                              {query += field.getName()+"="+"'"+conteudo+"',";
						} else if (tipo.equals("java.sql.Time"))                               {query += field.getName()+"="+"'"+conteudo+"',";
						}
					}
				} catch (Exception e){
					conteudo = "null";
					if        ((tipo.equals("int"))||(tipo.equals("java.lang.Integer")))   { query += field.getName()+"="+conteudo+",";
					} else if (tipo.equals("java.lang.String"))                            { query += field.getName()+"='"+conteudo+"',";
					} else if ((tipo.equals("double"))||(tipo.equals("java.lang.Double"))) { query += field.getName()+"="+conteudo+",";
					} else if (tipo.equals("java.util.Date"))                              { query += field.getName()+"='"+conteudo+"',";
					} else if (tipo.equals("java.sql.Time"))                               { query += field.getName()+"='"+conteudo+"',";
					}
				}

			}
			query=query.substring(0, query.length()-1); //remove v�rgula final
			query+=" WHERE id="+id;

		} catch (Exception er) {er.printStackTrace();}

		query = query.replaceAll("'null'", "null");

		//System.out.println(query);

		try {
			conn = DataSourceUtil.getConnection();
			Statement st = conn.createStatement();
			if (!id.equals("")) {
				try{
					st.execute(query);
				} catch (Exception e){
					System.out.println(query);
					e.printStackTrace();
				}
			} else {
				System.out.println("[AbstractDao.java] registro n�o pode ser atualizado. O id n�o foi encontrado na base");
			}
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
	}

	public Object runGetter(T obj, String field){
		for (Method method : obj.getClass().getDeclaredMethods()){
			if ((method.getName().startsWith("get")) && (method.getName().length() == (field.length() + 3))){
				if (method.getName().toLowerCase().endsWith(field.toLowerCase())) {
					try {
						return method.invoke(obj);
					}
					catch (Exception e){}
				}
			}
		}
		return null;
	}

	public void delete(Integer id){
		String tabela="";
		try {tabela = Class.forName(getClassName()).getSimpleName().toLowerCase();
		} catch (ClassNotFoundException e2) {e2.printStackTrace();	}
		String sql="DELETE FROM "+tabela+" WHERE id="+id;
		execute(sql);
	}
	
	public int getIntValueSQL(String sql) {
		Connection conn=null;

		int result = 0;
		try {

			ResultSet rs = null;
			try {
				conn = DataSourceUtil.getConnection();
				Statement st = conn.createStatement();
				rs = st.executeQuery(sql);
				rs.next();  // o result set tem que ser acessado  para pegar a 1 linha
				result = rs.getInt(1);
			} catch (Exception e){
				//resultado vazio
				result=0;
			} finally {
				rs.close();
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}

	
	public String getStringValueSQL(String sql) {
		Connection conn=null;

		String result = "";
		try {

			ResultSet rs = null;
			try {
				conn = DataSourceUtil.getConnection();
				Statement st = conn.createStatement();
				rs = st.executeQuery(sql);
				rs.next();  // o result set tem que ser acessado  para pegar a 1 linha
				result = rs.getString(1);
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				rs.close();
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			DataSourceUtil.discard(conn);
		}
		return result;
	}
	
}
