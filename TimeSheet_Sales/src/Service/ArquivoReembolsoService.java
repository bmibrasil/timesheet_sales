package Service;

import java.io.FileOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Beans.ArquivoReembolso;
import Dao.ArquivoReembolsoDao;

public class ArquivoReembolsoService {
	
	public void delete(int id){
		ArquivoReembolsoDao dao = new ArquivoReembolsoDao();
		dao.delete(id);
	}

	public List<ArquivoReembolso> buscarArquivos(String reembolso)
	{
		List<ArquivoReembolso> listaArquivo= new ArrayList<ArquivoReembolso>();
		int tamanho=0;
		try{
				Class.forName("com.mysql.jdbc.Driver");
				//Connection con=DriverManager.getConnection("jdbc:mysql://localhost/timesheet?useUnicode=true&characterEncoding=utf-8","root","octo3417");
				Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/reembolso","root","");
				byte b[];
				Blob blob;

				PreparedStatement ps=con.prepareStatement("select * from arquivoreembolso where idReembolso=\""+reembolso+"\""); 
				ResultSet rs=ps.executeQuery();
				FileOutputStream fos=null;
				while(rs.next()){
					ArquivoReembolso arquivoReembolso = new ArquivoReembolso();
					blob=rs.getBlob("arquivo");
					b=blob.getBytes(1,(int)blob.length());
					//Salva o arquivo no disco
				    String extensao=rs.getString("extensao");	
					//File file=new File("/opt/glassfish4/glassfish/domains/domain1/applications/TimeSheet/arquivosReembolso/reembolso_"+reembolso+"_"+tamanho+"."+extensao);
					//File file=new File("D:\\Program Files\\glassfish5\\glassfish\\domains\\domain1\\applications\\TimeSheet\\arquivosReembolso\\reembolso_"+reembolso+"_"+tamanho+"."+extensao);
					//File file=new File("D:\\temp\\reembolso_"+reembolso+"_"+tamanho+"."+extensao);
					String file = "C:\\Users\\Tecnologia 2\\Desktop\\Timesheet_real\\WebContent\\reembolso\\arquivosReembolso\\reembolso_" +reembolso+"_"+tamanho+"."+extensao;

				    //File file = new File("C:\\Users\\Tecnologia-1\\git\\timesheet_sales\\TimeSheet_Sales\\WebContent\\arquivosReembolso\\reembolso_" + reembolso + "_" + tamanho + "." + extensao);

					fos=new FileOutputStream(file);
				
					fos.write(b);
					arquivoReembolso.setId(rs.getInt("id"));
					arquivoReembolso.setIdReembolso((rs.getString("idReembolso")));
					arquivoReembolso.setNumeroReembolso((rs.getString("numeroreembolso")));
					arquivoReembolso.setExtensao((rs.getString("extensao")));
					arquivoReembolso.setNumeroArquivos((rs.getInt("numeroArquivos")));
					listaArquivo.add(arquivoReembolso);
					tamanho++;
				}

				ps.close();
				if (fos!=null)
				 {fos.close();}
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return listaArquivo;
	}


}
