package Service;

import java.sql.Connection;
import java.sql.SQLException;

public class AbstractService {
	
	private static Connection conn;
	
//	public static Connection getConnectionY(){
//		return DataSourceUtil.getConnection();
//	}
	
//	public static Connection getConnectionX(){
//		if (!isConnected()){
//			try {
//				Class.forName("com.mysql.jdbc.Driver");
//				String dbURL = "jdbc:mysql://localhost/octopus";
//				conn = DriverManager.getConnection(dbURL,"root","octo3417");
//			} catch (ClassNotFoundException ex) {
//				ex.printStackTrace();
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//			} catch (Exception ex2){
//				ex2.printStackTrace();
//			}
//		}
//		return conn;
//	}
	
	public static void disconnect(){
		if (isConnected()){
			try {
				conn.close();
				conn=null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean isConnected(){
		return conn!=null;
	}
	
}
