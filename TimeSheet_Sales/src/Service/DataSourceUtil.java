package Service;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DataSourceUtil {

	private static DataSource ds = null;
	//private static Connection conn = null;

	public static DataSource getDataSource(){
		if (ds==null){
			try{
				InitialContext ic = new InitialContext();
				//Context ctx  = (Context)ic.lookup("");
				ds = (DataSource)ic.lookup("jdbc/timesheet");
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		return ds;
	}

	public static Connection getConnection() throws Exception{
		DataSource ds = getDataSource();
/*
		if (conn!=null){
			try {
				conn.close();
				nConn--;
				conn=null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (conn==null){
			try {
				conn = ds.getConnection();
				nConn++;
			} catch (Exception e){
				e.printStackTrace();
			}	
		} else {

		}
		*/
		Connection conn = ds.getConnection();
		if (conn==null){
			throw new Exception("Não obteve uma nova conexão do DataSource");
		}
		return conn;
	}

	public static void discard(Connection conn){
		try {
			if (conn!=null){
				conn.close();
			} else {
				System.out.println("Tentativa de fechar conexão null!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
