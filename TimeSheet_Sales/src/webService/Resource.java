package webService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Util.GsonPrettyJson;
import config.ResourcesConfig;
import webServiceController.Controller;

/**
 *
 * @author BMI Tecnologia
 */

@Path("/srv_timesheet_sales")
public class Resource {
	
    @GET
   	@Path("ws_timesheet_sales/{valor}")
	public String webservice(@PathParam("valor") String jsonRecebido) throws Throwable{
    	
    	String prettyJsonString = new GsonPrettyJson().toPrettyJson(jsonRecebido);
    	
    	String mensagem = String.format("[WEB-SERVICE::RESOURCES::NOTIFICATION] :: \n%s", prettyJsonString);
    	System.out.println(mensagem);
    	
    	String retorno = "";
    	
    	JsonParser jsonParser = new JsonParser();

        JsonObject jsonObj = (JsonObject) jsonParser.parse(jsonRecebido);
        
        ResourcesConfig resourcesConfig = new ResourcesConfig();
        
        Controller controller = resourcesConfig.getController(jsonObj.get("page").getAsString());
        retorno = controller.process(jsonRecebido);
        
        //define metodo de tratamento da resposta no client
        String result = "callback( "+retorno+" )";
        
        if (jsonObj.get("jsonp")!=null) {
        	result = "" + jsonObj.get("jsonp");
        	result = result.trim().replaceAll("\"","") + "( " + retorno + " )";
        }
        
        return result;
        
    }
    
}