package webService;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class DMLSalesForce {

	
	//homolog
		private static final String CLIENT_ID = "client_id=3MVG9PG9sFc71i9niSy3spF8Y.l4DHEZBmksypIvnl9YGsjG1lSh9oqHjCW8Y62OVwv5LUAyiF4B4JqOZKNl_";
		private static final String CLIENT_SECRET = "client_secret=82817ADF80DF917C621668A21684227177A2E4F965FBC1361461DCFEA57C9F43";
		private static final String END_PASSWORD = "F8j0pwjz5YJvwg6pj59M5vfJj";
		private static final String NOME_AMBIENTE = "whitefox--homolog.cs29";
		private static final String LOGIN = "wf.tecnologia@whitefox.capital.homolog";
		private static final String SENHA = "Sa13swHt3.645";
		
	  private static final String URL_SALESFORCE_CONNECTION = "https://"+NOME_AMBIENTE+".my.salesforce.com/services/oauth2/token?grant_type=password&"+CLIENT_ID+"&"+CLIENT_SECRET+"&username=wf.tecnologia@whitefox.capital.homolog&password=Sa13swHt3.645"+END_PASSWORD;
	
		
	
	
	public static void insereSalesForce (String tabela, String[] vetorCamposInsert,String[] vetorValores)
	{
		try {

			URL url = new URL(URL_SALESFORCE_CONNECTION);
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");
			System.out.println(conn.getContent().toString());
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : "
						+ conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				String jsonString = output;
				JsonParser jsonParser = new JsonParser();
				JsonObject objectFromString = jsonParser.parse(jsonString).getAsJsonObject();


				String access=objectFromString.get("access_token").getAsString();
				String token=objectFromString.get("token_type").getAsString();
				//String urlMontada="https://whitefox--NexiSim.cs21.my.salesforce.com/services/data/v42.0/sobjects/"+tabela;
				//String urlMontada="https://whitefox.my.salesforce.com/services/data/v42.0/sobjects/"+tabela;
				String urlMontada="https://" + NOME_AMBIENTE + ".my.salesforce.com/services/data/v42.0/sobjects/"+tabela;
				
				URL url2 = new URL(urlMontada);//your url i.e fetch data from .

				HttpURLConnection conn2=null;
				conn2= (HttpURLConnection) url2.openConnection();
				conn2.addRequestProperty("Authorization",token+" "+access);
				conn2.setRequestMethod("POST");	        
				conn2.setRequestProperty("Accept", "application/json");
				conn2.setRequestProperty("Content-Type", "application/json");

				//Cria o Json
				JsonObject camposValoresJson = new JsonObject() ;
				for (int i=0; i <vetorCamposInsert.length;i++)
				{
					String propriedade=vetorCamposInsert[i] ;
 					String valor=vetorValores[i];
					camposValoresJson.addProperty(propriedade,valor);
			
				}
				
				// Writes the JSON parsed as string to the connection
				conn2.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(conn2.getOutputStream());
				wr.write(camposValoresJson.toString().getBytes("UTF-8"));
				Integer responseCode = conn2.getResponseCode();

				BufferedReader bufferedReader;

				// Creates a reader buffer
				if (responseCode > 199 && responseCode < 300) {
					bufferedReader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
				} else {
					bufferedReader = new BufferedReader(new InputStreamReader(conn2.getErrorStream()));
				}

				// To receive the response
				StringBuilder content = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					content.append(line).append("\n");
				}
				bufferedReader.close();

				// Prints the response
				System.out.println(content.toString());

				conn2.disconnect();

			}
			conn.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		}
	}


	public static synchronized JsonObject selecionaSalesForce(String tabela, String [] campos, String condicoes, String ordenacao)
	{
		String retornoSelect="";
		JsonObject jsonRetorno=null;
		
		
		try {
			URL url = new URL(URL_SALESFORCE_CONNECTION);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");
			System.out.println(conn.getContent().toString());
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : "
						+ conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				String jsonString = output;
				JsonParser jsonParser = new JsonParser();
				JsonObject objectFromString = jsonParser.parse(jsonString).getAsJsonObject();

				String access=objectFromString.get("access_token").getAsString();
				String token=objectFromString.get("token_type").getAsString();

				String listaCampos="";
				for (int i=0; i<campos.length;i++)
				{
					listaCampos+="%20"+campos[i]+",";
				}
				listaCampos=listaCampos.substring(0,listaCampos.length()-1);
                if (condicoes.equals("")==false)
                {
                	condicoes= "%20WHERE%20" + condicoes;
                }
                
                if (ordenacao.equals("")==false)
                {
                	ordenacao= "%20ORDER%20BY%20" + ordenacao;
                }

				//String urlMontada="https://https://whitefox--homolog.cs92.my.salesforce.com/services/data/v42.0/query?q=SELECT"+listaCampos+"%20from%20"+tabela+condicoes+"&authToken="+token+"%2"+access;
                //String urlMontada="https://whitefox.my.salesforce.com/services/data/v42.0/query?q=SELECT"+listaCampos+"%20from%20"+tabela+condicoes+ordenacao+"&authToken="+token+"%2"+access;
                
                String urlMontada="https://" + NOME_AMBIENTE + ".my.salesforce.com/services/data/v42.0/query?q=SELECT"+listaCampos+"%20from%20"+tabela+condicoes+ordenacao+"&authToken="+token+"%2"+access;            
                
                URL url2 = new URL(urlMontada);//your url i.e fetch data from .
				HttpURLConnection conn2=null;
				conn2= (HttpURLConnection) url2.openConnection();
				conn2.setRequestMethod("GET");	        

				conn2.addRequestProperty("Authorization",token+" "+access);

				System.out.println("-- Response  --");
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream(),"UTF-8"))) {

					retornoSelect=reader.readLine();

					//Imprime o retorno do Select
					System.out.println(retornoSelect);
				}

				jsonRetorno = jsonParser.parse(retornoSelect).getAsJsonObject();
				

				if (conn2.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP Error code : "
							+ conn2.getResponseCode());
				}
	
				conn2.disconnect();

			}
			conn.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		}

		return jsonRetorno;
	}


	public static void atualizaSalesForce (String tabela,String id, String[] vetorCamposAtualizar,String[] vetorValores)
	{

		try {
			URL url = new URL(URL_SALESFORCE_CONNECTION);
			
			
					
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");
			System.out.println(conn.getContent().toString());
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : "
						+ conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				String jsonString = output;
				JsonParser jsonParser = new JsonParser();
				JsonObject objectFromString = jsonParser.parse(jsonString).getAsJsonObject();


				String access=objectFromString.get("access_token").getAsString();
				String token=objectFromString.get("token_type").getAsString();
				String urlMontada="https://" + NOME_AMBIENTE + ".my.salesforce.com/services/data/v42.0/sobjects/"+tabela+"/"+id+"?_HttpMethod=PATCH";
				
				
				URL url2 = new URL(urlMontada);//your url i.e fetch data from .

				HttpURLConnection conn2=null;
				conn2= (HttpURLConnection) url2.openConnection();
				conn2.addRequestProperty("Authorization",token+" "+access);
				conn2.setRequestMethod("POST");	        
				//conn2.setRequestProperty("X-HTTP-Method-Override", "PATCH");
				conn2.setRequestProperty("Accept", "application/json");
				conn2.setRequestProperty("Content-Type", "application/json");

				//Cria o Json
				JsonObject camposValoresJson = new JsonObject() ;
				for (int i=0; i <vetorCamposAtualizar.length;i++)
				{
					String propriedade=vetorCamposAtualizar[i] ;
 					String valor=vetorValores[i];
					camposValoresJson.addProperty(propriedade,valor);
			
				}
				
				// Writes the JSON parsed as string to the connection
				conn2.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(conn2.getOutputStream());
				wr.write(camposValoresJson.toString().getBytes("UTF-8"));
				Integer responseCode = conn2.getResponseCode();

				BufferedReader bufferedReader;

				// Creates a reader buffer
				if (responseCode > 199 && responseCode < 300) {
					bufferedReader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
				} else {
					bufferedReader = new BufferedReader(new InputStreamReader(conn2.getErrorStream()));
				}

				// To receive the response
				StringBuilder content = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					content.append(line).append("\n");
				}
				bufferedReader.close();

				// Prints the response
				System.out.println(content.toString());

				conn2.disconnect();

			}
			conn.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		}
		
	}
	
	public static void removeSalesForce(String tabela, String id) {
		
		try {

			URL url = new URL(URL_SALESFORCE_CONNECTION);			
					
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");
			System.out.println(conn.getContent().toString());
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : "
						+ conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				String jsonString = output;
				JsonParser jsonParser = new JsonParser();
				JsonObject objectFromString = jsonParser.parse(jsonString).getAsJsonObject();


				String access=objectFromString.get("access_token").getAsString();
				String token=objectFromString.get("token_type").getAsString();
				String urlMontada="https://" + NOME_AMBIENTE + ".my.salesforce.com/services/data/v42.0/sobjects/"+tabela+"/"+id+"?_HttpMethod=DELETE";
				
				
				URL url2 = new URL(urlMontada);//your url i.e fetch data from .

				HttpURLConnection conn2=null;
				conn2= (HttpURLConnection) url2.openConnection();
				conn2.addRequestProperty("Authorization",token+" "+access);
				conn2.setRequestMethod("POST");	        
				//conn2.setRequestProperty("X-HTTP-Method-Override", "PATCH");
				conn2.setRequestProperty("Accept", "application/json");
				conn2.setRequestProperty("Content-Type", "application/json");

				
				// Writes the JSON parsed as string to the connection
				conn2.setDoOutput(true);
				//DataOutputStream wr = new DataOutputStream(conn2.getOutputStream());
				
				Integer responseCode = conn2.getResponseCode();

				BufferedReader bufferedReader;

				// Creates a reader buffer
				if (responseCode > 199 && responseCode < 300) {
					bufferedReader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
				} else {
					bufferedReader = new BufferedReader(new InputStreamReader(conn2.getErrorStream()));
				}

				// To receive the response
				StringBuilder content = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					content.append(line).append("\n");
				}
				bufferedReader.close();

				// Prints the response
				System.out.println(content.toString());

				conn2.disconnect();

			}
			conn.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		}
		
	}

}



