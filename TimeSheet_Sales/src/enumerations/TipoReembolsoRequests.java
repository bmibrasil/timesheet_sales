package enumerations;

import action.TipoReembolsoAction;

public enum TipoReembolsoRequests {

	BUSCAR_TIPO_REEMBOLSO{

		@Override
		public String apply(TipoReembolsoAction tipoReembolsoAction) {
			return tipoReembolsoAction.buscarTipoReembolsos();
		}
		
	};
	
	public abstract String apply( final TipoReembolsoAction tipoReembolsoAction );
	
}
