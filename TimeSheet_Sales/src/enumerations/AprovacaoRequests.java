package enumerations;

import action.AprovacaoAction;

public enum AprovacaoRequests{

	APROVA_TIMESHEET{

		@Override
		public String apply(AprovacaoAction aprovacaoAction) throws Exception {
			return aprovacaoAction.salvarAprovacao();
		}
		
	},
	BUSCAR_APROVACAO{

		@Override
		public String apply(AprovacaoAction aprovacaoAction) throws Exception {
			return aprovacaoAction.buscarAprovacao();
		}
		
	};
	
	public abstract String apply( final AprovacaoAction aprovacaoAction ) throws Exception;
	
}
