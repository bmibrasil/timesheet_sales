package enumerations;

import action.ColaboradorAction;

public enum ColaboradorRequests {

	BUSCAR_COLABORADORES{

		@Override
		public String apply(ColaboradorAction colaboradorAction) {
			return colaboradorAction.buscarColaboradores();
		}
		
	},
	BUSCAR_COLABORADOR{

		@Override
		public String apply(ColaboradorAction colaboradorAction) {
			return colaboradorAction.buscarColaborador();
		}
		
	},
	BUSCAR_COLABORADOR_REEMBOLSO{

		@Override
		public String apply(ColaboradorAction colaboradorAction) {
			return colaboradorAction.buscarColaboradorReembolso();
		}
		
	},
	BUSCAR_TODOS_COLABORADORES{

		@Override
		public String apply(ColaboradorAction colaboradorAction) {
			return colaboradorAction.buscarTodosColaboradores();
		}
		
	},
	BLOQUEIA_COLABORADOR{

		@Override
		public String apply(ColaboradorAction colaboradorAction) {
			return colaboradorAction.bloqueiaColaborador();
		}
		
	};
	
	public abstract String apply( final ColaboradorAction colaboradorAction );
	
}
