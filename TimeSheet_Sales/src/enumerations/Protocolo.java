package enumerations;

import java.io.Serializable;

public enum Protocolo implements Serializable{
	
	OK( 200, "Ok"),
	NOT_FOUND( 404, "Nao Encontrado");
	
	private Integer codigo;
	private String mensagem;
	
	Protocolo( Integer codigo, String mensagem ) {
		this.codigo = codigo;
		this.mensagem = mensagem;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

}
