package enumerations;

import java.text.ParseException;

import action.ItemTimeSheetAction;

public enum ItemTimesheetRequests {
	
	CONSOLIDA_TIMESHEET{

		@Override
		public String apply(ItemTimeSheetAction itemTimeSheetAction) throws Exception {
			return itemTimeSheetAction.consolidaTimeSheet();
		}
		
	},
	BUSCA_RESUMO_HORAS_PROJETO_PERIODO{

		@Override
		public String apply(ItemTimeSheetAction itemTimeSheetAction) throws Exception {
			return itemTimeSheetAction.buscaResumoHorasProjetoPeriodo();
		}
		
	},
	RELATORIO_CONSOLIDADO{

		@Override
		public String apply(ItemTimeSheetAction itemTimeSheetAction) throws Exception {
			return itemTimeSheetAction.relatorioConsolidado();
		}
		
	},
	RELATORIO_DETALHADO{

		@Override
		public String apply(ItemTimeSheetAction itemTimeSheetAction) throws Exception {
			return itemTimeSheetAction.relatorioDetalhado();
		}
		
	},
	BUSCA_LANCAMENTOS_COLABORADOR_PERIODO{

		@Override
		public String apply(ItemTimeSheetAction itemTimeSheetAction) throws Exception {
			return itemTimeSheetAction.buscaLancamentosColaboradorPeriodo();
		}
		
	},
	BUSCAR_TIMESHEETS{

		@Override
		public String apply(ItemTimeSheetAction itemTimeSheetAction) {
			return itemTimeSheetAction.buscarTimeSheets();
		}
		
	},
	BUSCAR_LANCAMENTO{

		@Override
		public String apply(ItemTimeSheetAction itemTimeSheetAction) throws ParseException {
			return itemTimeSheetAction.buscarLancamento();
		}
		
	},
	SALVAR_TIMESHEET{

		@Override
		public String apply(ItemTimeSheetAction itemTimeSheetAction) throws Exception {
			return itemTimeSheetAction.salvarTimeSheet();
		}
		
	},
	EXCLUIR_LANCAMENTO{

		@Override
		public String apply(ItemTimeSheetAction itemTimeSheetAction) {
			return itemTimeSheetAction.excluirLancamento();
		}
		
	};
	
	public abstract String apply(final ItemTimeSheetAction itemTimeSheetAction) throws Exception;

}
