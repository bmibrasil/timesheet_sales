package enumerations;

import action.ArquivoReembolsoAction;

public enum ArquivoReembolsoRequests {

	BUSCAR_ARQUIVO_REEMBOLSO{

		@Override
		public String apply(ArquivoReembolsoAction arquivoReembolsoAction) {
			return arquivoReembolsoAction.buscarArquivosReembolso();
		}
		
	},
	REMOVER_ARQUIVO_REEMBOLSO{

		@Override
		public String apply(ArquivoReembolsoAction arquivoReembolsoAction) {
			return arquivoReembolsoAction.removerArquivoReembolso();
		}
		
	};
	
	public abstract String apply( final ArquivoReembolsoAction arquivoReembolsoAction );
	
}
