package enumerations;

import action.TipoHoraAction;

public enum TipoHoraRequests {

	BUSCAR_TIPO_HORA{

		@Override
		public String apply(TipoHoraAction tipoHoraAction) {
			return tipoHoraAction.buscarTipoHoras();
		}
		
	};
	
	public abstract String apply( final TipoHoraAction tipoHoraAction );
	
}
