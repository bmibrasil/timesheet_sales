package enumerations;

import action.ParametrosAction;

public enum ParametrosRequest {
	
	BUSCAR_PARAMETROS{

		@Override
		public String apply(ParametrosAction parametrosAction) throws Exception {
			return parametrosAction.buscarParametros();
		}
		
	};
	
	public abstract String apply(final ParametrosAction parametrosAction) throws Exception;
	
}
