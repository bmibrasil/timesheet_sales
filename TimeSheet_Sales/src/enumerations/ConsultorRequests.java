package enumerations;

import action.ConsultorAction;

public enum ConsultorRequests {

	BUSCAR_CONSULTOR{

		@Override
		public String apply(ConsultorAction consultorAction) {
			return consultorAction.buscarConsultor();
		}
		
	};
	
	public abstract String apply( final ConsultorAction consultorAction );
	
}
