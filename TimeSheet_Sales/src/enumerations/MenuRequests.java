package enumerations;

import action.PermissaoAction;

public enum MenuRequests {

	BUSCAR_PERMISSAO{

		@Override
		public String apply(PermissaoAction permissaoAction) {
			return permissaoAction.buscarPermissao();
		}
		
	},
	INSERIR_PERMISSAO{

		@Override
		public String apply(PermissaoAction permissaoAction) {
			return permissaoAction.inserirPermissao();			
		}
		
	};
	
	public abstract String apply( final PermissaoAction permissaoAction );
	
	
}
