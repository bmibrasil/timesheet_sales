package enumerations;

import action.TimeSheetAction;

public enum TimeSheetRequests {
	
	BUSCA_PERIODOS{

		@Override
		public String apply(TimeSheetAction timeSheetAction) {
			return timeSheetAction.buscarPeriodos();
		}
		
	},
	BUSCA_PERIODOS_CONSOLIDADO{

		@Override
		public String apply(TimeSheetAction timeSheetAction) {
			return timeSheetAction.buscarPeriodosConsolidado();
		}
		
	};
	
	public abstract String apply(final TimeSheetAction timeSheetAction);

}
