package enumerations;

import action.ReembolsoAction;

public enum ReembolsoRequests {
	
	BUSCAR_REEMBOLSO_COLABORADOR{

		@Override
		public String apply(ReembolsoAction reembolsoAction) throws Exception {
			return reembolsoAction.buscarReembolsosColaborador();
		}
		
	},
	BUSCAR_LANCAMENTO_REEMBOLSO{

		@Override
		public String apply(ReembolsoAction reembolsoAction) throws Exception {
			return reembolsoAction.buscarLancamentoReembolso();
		}
		
	},
	BUSCAR_REEMBOLSO_STATUS{

		@Override
		public String apply(ReembolsoAction reembolsoAction) throws Exception {
			return reembolsoAction.buscarReembolsosStatus();
		}
		
	},
	SALVAR_REEMBOLSO{

		@Override
		public String apply(ReembolsoAction reembolsoAction) throws Exception {
			return reembolsoAction.salvarReembolso();
		}
		
	},
	AVALIAR_REEMBOLSO{

		@Override
		public String apply(ReembolsoAction reembolsoAction) throws Exception {
			return reembolsoAction.avaliarReembolso();
		}
		
	},
	EXCLUIR_REEMBOLSO{

		@Override
		public String apply(ReembolsoAction reembolsoAction) throws Exception {
			return reembolsoAction.excluirReembolso();
		}
		
	},
	BUSCAR_ID_NUMERO_REEMBOLSO{

		@Override
		public String apply(ReembolsoAction reembolsoAction) throws Exception {
			return reembolsoAction.buscarIdNumeroReembolso();
		}
		
	};
	
	public abstract String apply(final ReembolsoAction reembolsoAction)throws Exception;

}
