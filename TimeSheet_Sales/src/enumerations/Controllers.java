package enumerations;

import webServiceController.Controller;
import webServiceController.WSAprovacaoController;
import webServiceController.WSArquivoReembolsoController;
import webServiceController.WSColaboradorController;
import webServiceController.WSConsultorController;
import webServiceController.WSItemTimesheetController;
import webServiceController.WSMenuController;
import webServiceController.WSParametrosController;
import webServiceController.WSReembolsoController;
import webServiceController.WSTimeSheetController;
import webServiceController.WSTipoHoraController;
import webServiceController.WSTipoReembolsoController;

public enum Controllers {

	COLABORADOR{

		@Override
		public Controller getController() {
			return new WSColaboradorController();
		}
		
	},
	MENU{

		@Override
		public Controller getController() {
			return new WSMenuController();
		}
		
	},
	TIPO_HORA{

		@Override
		public Controller getController() {
			return new WSTipoHoraController();
		}
		
	},
	CONSULTOR{

		@Override
		public Controller getController() {
			return new WSConsultorController();
		}
		
	},
	TIMESHEET{

		@Override
		public Controller getController() {
			return new WSTimeSheetController();
		}
		
	},
	REEMBOLSO{

		@Override
		public Controller getController() {
			return new WSReembolsoController();
		}
		
	},
	ITEM_TIMESHEET{

		@Override
		public Controller getController() {
			return new WSItemTimesheetController();
		}
		
	},
	APROVACAO{

		@Override
		public Controller getController() {
			return new WSAprovacaoController();
		}
		
	},
	ARQUIVO_REEMBOLSO{

		@Override
		public Controller getController() {
			return new WSArquivoReembolsoController();
		}
		
	},
	TIPO_REEMBOLSO{

		@Override
		public Controller getController() {
			return new WSTipoReembolsoController();
		}
		
	},
	PARAMETROS{

		@Override
		public Controller getController() {
			return new WSParametrosController();
		}
		
	};
	
	public abstract Controller getController();
}
