package Util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class GsonPrettyJson {

	public String toPrettyJson(String json) {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
    	JsonParser jsonParser = new JsonParser();
    	
    	JsonElement jsonElement = jsonParser.parse(json);
    	
    	String prettyJsonString = gson.toJson(jsonElement);
    	
    	return prettyJsonString;
    	
	}
	
}
