package Util;

import java.io.IOException;
import java.util.StringTokenizer;

public class Util {

	private static String simulationCode="";

	public static double round(double x, int casas){
		x = x * Math.pow(10, casas);
		x = Math.round(x);
		x = x / Math.pow(10, casas);
		return x;
	}

	public static long round(double x){
		return Math.round(x);
	}

	public String getPath(){
		String current="";
		try {
			current = new java.io.File( "." ).getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return current;
	}

	public static String getDataFilename(){
		return "OctopusData"+simulationCode+".xlsx";
	}

	public static boolean isIn(String text, String content){
		boolean result = false;
		StringTokenizer st = new StringTokenizer(content,";");
		while (st.hasMoreTokens()){
			String t = st.nextToken();
			if (t.equals(text)) result = true;
		}
		return result;
	}

	public static void anotar(int  grupo, int periodo, boolean ativo, String msg){
//		if (ativo){
//			try {
//				File file = new File("C:/log_grupo"+grupo+"_periodo"+periodo+".txt");
//				if (!file.exists()) {
//					file.createNewFile();
//				}
//				FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
//				BufferedWriter bw = new BufferedWriter(fw);
//				bw.write(msg);
//				bw.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
	}

}
