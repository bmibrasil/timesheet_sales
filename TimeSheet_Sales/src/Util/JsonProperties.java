package Util;

import com.google.gson.JsonObject;

public class JsonProperties {

	private JsonObject response;
	
	public JsonProperties( Builder builder ) {
		response = builder.response;
	}

	public static class Builder {

		private final JsonObject response;

		public Builder() {
			response = new JsonObject();
		}
		
		public <T> Builder addProperty( String property, T value ) {
			response.addProperty(property, value.toString());
			return this;
		}

		public JsonProperties build() {
			return new JsonProperties(this);
		}

	}
	
	public JsonObject getJsonObject() {
		return response;
	}
	
}
